package model.materials.database;

import org.junit.Test;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Created by sayadi on 11/07/2018.
 */

public class IronBHCurveParserTest {

    private String name = "BHiron1";
    private int nbLine = 3;
    private double scaling = 1.0;
    private static final String bHCurveDatabasePath = "functional/BHCurveParser/bh-curve-database.txt";

    List<String> listOfLines = Arrays.asList("BHiron1", "3 1.0", "0 0", "5.0 3968951.199", "10.0 7937902.399");
    Map<String, IronBHCurve> ironBHCurveMap = IronBHCurveParser.parseBHCurve(listOfLines);

    @Test
    public void getBValueTest() throws FileNotFoundException {
        String path = IronBHCurveParserTest.class.getClassLoader().getResource(bHCurveDatabasePath).getFile();
        String[][] bhCurveActual = DatabaseMockUp.getBhCurveFromIronDatabase(path, "BH_SIGMA");
        String[][] bhCurveExpected = IronDatabaseROXIE.HBCurveIron1;
        for (int i = 0; i <bhCurveExpected.length ; i++) {
            double expectedBValue = Double.parseDouble(bhCurveExpected[i][0]);
            double actualBValue = Double.parseDouble(bhCurveActual[i][0]);
            assertEquals(expectedBValue, actualBValue, 1e-6);
        }
    }

    @Test
    public void getHValueTest() throws FileNotFoundException {
        String path = IronBHCurveParserTest.class.getClassLoader().getResource(bHCurveDatabasePath).getFile();
        String[][] bhCurveActual = DatabaseMockUp.getBhCurveFromIronDatabase(path, "BH_SIGMA");
        String[][] bhCurveExpected = IronDatabaseROXIE.HBCurveIron1;
        for (int i = 0; i <bhCurveExpected.length ; i++) {
            double expectedHValue = Double.parseDouble(bhCurveExpected[i][1]);
            double actualHValue = Double.parseDouble(bhCurveActual[i][1]);
            assertEquals(expectedHValue, actualHValue, 1e-6);
        }
    }

    @Test
    public void parseBHCurveTestName() {
        String expectedName = name;
        String actualName = ironBHCurveMap.get(name).getName();

        assertEquals(actualName, expectedName);
    }

    @Test
    public void parseBHCurveTestNBLines() {
        int expectedNBLines = nbLine;
        int actualNBLines = ironBHCurveMap.get(name).getNbLines();

        assertEquals(expectedNBLines, actualNBLines, 1e-6);
    }

    @Test
    public void parseBHCurveTestScaling() {
        double expectedScaling = scaling;
        double actualScaling = ironBHCurveMap.get(name).getScaling();

        assertEquals(expectedScaling, actualScaling, 1e-6);
    }

    @Test
    public void readNbLine() {
        String line = "40 0.85";
        int expectedNBLine = 40;
        int nbLine = (int) IronBHCurveParser.readNbLineScaling(line)[0];

        assertEquals(expectedNBLine, nbLine, 1e-6);
    }

    @Test
    public void readScaling() {
        String line = "40 0.85";
        double expectedScaling = 0.85;
        double scaling = IronBHCurveParser.readNbLineScaling(line)[1];

        assertEquals(expectedScaling, scaling, 1e-6);
    }
}
