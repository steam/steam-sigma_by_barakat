package server;

import comsol.constants.MPHC;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by mmacieje on 09/08/2018.
 */
public class TxtSigmaServerTest {
    @Test
    public void genExprModelCreate() throws Exception {
        String expected = "Model mph = ModelUtil.create(\"Model\");";
        String actual = TxtSigmaServer.genExprModelCreate("Model");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamFeatureSol5() throws Exception {
        String expected = "mph.sol(\"solutionName\").feature(\"t1\").set(\"probes\", new String[]{\"a\", \"b\"});";
        String actual = TxtSigmaServer.genExprSetParamFeatureSol("solutionName", "t1", "probes", new String[]{"a", "b"});
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamFuncPropertyGroupMaterial1() throws Exception {
        String expected = "mph.material(\"label\").propertyGroup(\"HBCurve\").func(\"HB\").set(\"table\", new String[][]{{\"a\", \"b\"}, {\"c\", \"d\"}});";
        String actual = TxtSigmaServer.genExprSetParamFuncPropertyGroupMaterial("label", "HBCurve", "HB", "table", new String[][]{{"a", "b"}, {"c", "d"}});
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamSelectionFeatureGeom() throws Exception {
        String expected = "mph.geom(\"geom1\").feature(\"label\").selection(\"selection\").set(\"input\", new int[]{1});";
        String actual = TxtSigmaServer.genExprSetParamSelectionFeatureGeom(MPHC.LABEL_GEOM, "label", "selection", "input", new int[]{1});
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamFeatureGeometry4() throws Exception {
        String expected = "mph.geom(\"geom1\").feature(\"label\").set(\"degree\", new int[]{1});";
        String actual = TxtSigmaServer.genExprSetParamFeatureGeometry(MPHC.LABEL_GEOM, "label", "degree", new int[]{1});
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetFeaturePhysics2() throws Exception {
        String expected = "mph.physics(\"mf\").feature(\"nodeLabel\").set(\"Je\", new String[]{\"a\", \"b\"});";
        String actual = TxtSigmaServer.genExprSetFeaturePhysics(MPHC.LABEL_PHYSICSMF, "nodeLabel", "Je", new String[]{"a", "b"});
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamNumericalResult1() throws Exception {
        String expected = "mph.result().numerical(\"expression\").set(\"expr\", new String[]{});";
        String actual = TxtSigmaServer.genExprSetParamNumericalResult("expression", "expr", new String[]{});
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamPropertyGroupMaterial1() throws Exception {
        String expected = "mph.material(\"label\").propertyGroup(\"def\").set(\"electricconductivity\", new String[]{\"a\", \"b\"});";
        String actual = TxtSigmaServer.genExprSetParamPropertyGroupMaterial("label", "def", "electricconductivity", new String[]{"a", "b"});
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetSelectionFeatureGeom() throws Exception {
        String expected = "mph.geom(\"geom1\").feature(\"label\").selection(\"localFeatureLabel\").set(new String[]{\"a\", \"b\"});";
        String actual = TxtSigmaServer.genExprSetSelectionFeatureGeom(MPHC.LABEL_GEOM, "label", "localFeatureLabel", new String[]{"a", "b"});
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamFeatureGeometry3() throws Exception {
        String expected = "mph.geom(\"geom1\").feature(\"label\").set(\"input\", new String[]{\"a\", \"b\"});";
        String actual = TxtSigmaServer.genExprSetParamFeatureGeometry(MPHC.LABEL_GEOM, "label", "input", new String[]{"a", "b"});
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetFunctionParam1() throws Exception {
        String expected = "mph.func(\"nodeName\").set(\"funcs\", new String[][]{{\"a\", \"b\"}, {\"c\", \"d\"}});";
        String actual = TxtSigmaServer.genExprSetFunctionParam("nodeName", "funcs", new String[][]{{"a", "b"}, {"c", "d"}});
        assertEquals(expected, actual);
    }

    @Test
    public void convert2DStringArrayToString() throws Exception {
        String actual = TxtSigmaServerUtils.convert2DStringArrayToString(new String[][]{{"a", "b"}, {"c", "d"}});
        String expected = "new String[][]{{\"a\", \"b\"}, {\"c\", \"d\"}}";
        assertEquals(expected, actual);
    }

    @Test
    public void convertArrayToStringExpression() throws Exception {
        String actualString = TxtSigmaServerUtils.convertArrayToStringExpression(new String[]{"a", "b"});
        String expectedString = "new String[]{\"a\", \"b\"}";

        assertEquals(expectedString, actualString);
    }

    @Test
    public void genExprSetMaterial() throws Exception {
        String expected = "mph.material(\"label\").set(\"family\", \"elem_air\");";
        String actual = TxtSigmaServer.genExprSetMaterial("label", "family", "elem_air");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetFeatureFeaturePhysics() throws Exception {
        String expected = "mph.physics(\"ht\").feature(\"parentNode\").feature(\"nodeLabel\").set(\"Qs1\", \"Qs[0]\");";
        String actual = TxtSigmaServer.genExprSetFeatureFeaturePhysics(MPHC.LABEL_PHYSICSTH, "parentNode", "nodeLabel", "Qs1", "Qs[0]");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamFeatureGeometry2() throws Exception {
        String expected = "mph.physics(\"ht\").feature(\"parentNode\").create(\"nodeLabel\", \"LayerHeatSource\", 1);";
        String actual = TxtSigmaServer.genExprSetParamFeatureGeometry(MPHC.LABEL_PHYSICSTH, "parentNode", "nodeLabel", "LayerHeatSource", 1);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamFeatureGeometry1() throws Exception {
        String expected = "mph.geom(\"geom1\").feature(\"label\").set(\"r\", 1.000000);";
        String actual = TxtSigmaServer.genExprSetParamFeatureGeometry(MPHC.LABEL_GEOM, "label", "r", 1.000000);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetComponentFieldPhysics() throws Exception {
        String expected = "mph.physics(\"dode\").field(\"dimensionless\").component(1, \"t_star\");";
        String actual = TxtSigmaServer.genExprSetComponentFieldPhysics(MPHC.LABEL_PHYSICSDODE_DODE, "dimensionless", 1, "t_star");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetFieldFieldPhysics() throws Exception {
        String expected = "mph.physics(\"dode\").field(\"dimensionless\").field(\"t_star\");";
        String actual = TxtSigmaServer.genExprSetFieldFieldPhysics(MPHC.LABEL_PHYSICSDODE_DODE, "dimensionless", "t_star");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetIndexPropPhysics() throws Exception {
        String expected = "mph.physics(\"dode\").prop(\"Units\").setIndex(\"CustomDependentVariableUnit\", \"s\", 0, 0);";
        String actual = TxtSigmaServer.genExprSetIndexPropPhysics(MPHC.LABEL_PHYSICSDODE_DODE, "Units", "CustomDependentVariableUnit", "s", 0, 0);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetNamedSelectionFeaturePhysics1() throws Exception {
        String expected = "mph.physics(\"dode\").selection().named(\"selectionLabel\");";
        String actual = TxtSigmaServer.genExprSetNamedSelectionFeaturePhysics(MPHC.LABEL_PHYSICSDODE_DODE, "selectionLabel");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetFeaturePhysics1() throws Exception {
        String expected = "mph.physics(\"ht\").feature(\"nodeLabel\").set(\"MultipleLayersStructure\", true);";
        String actual = TxtSigmaServer.genExprSetFeaturePhysics(MPHC.LABEL_PHYSICSTH, "nodeLabel", "MultipleLayersStructure", true);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetNamedSelectionCoordSystem() throws Exception {
        String expected = "mph.coordSystem(\"nodeLabel\").selection().named(\"selectionLabel\");";
        String actual = TxtSigmaServer.genExprSetNamedSelectionCoordSystem("nodeLabel","selectionLabel");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamCoordSystem() throws Exception {
        String expected = "mph.coordSystem(\"nodeLabel\").set(\"ScalingType\", \"Cylindrical\");";
        String actual = TxtSigmaServer.genExprSetParamCoordSystem("nodeLabel", "ScalingType", "Cylindrical");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetLabelCoordSystem() throws Exception {
        String expected = "mph.coordSystem(\"nodeLabel\").label(\"nodeLabel\");";
        String actual = TxtSigmaServer.genExprSetLabelCoordSystem("nodeLabel", "nodeLabel");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprCreateCoordSystem() throws Exception {
        String expected = "mph.coordSystem().create(\"nodeLabel\", \"geom1\", \"InfiniteElement\");";
        String actual = TxtSigmaServer.genExprCreateCoordSystem("nodeLabel", MPHC.LABEL_GEOM, "InfiniteElement");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetNamedSelectionFeaturePhysics() throws Exception {
        String expected = "mph.physics(\"ht\").feature(\"nodeLabel\").selection().named(\"selectionLabel\");";
        String actual = TxtSigmaServer.genExprSetNamedSelectionFeaturePhysics(MPHC.LABEL_PHYSICSTH, "nodeLabel", "selectionLabel");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprRunExportResult() throws Exception {
        String expected = "mph.result().export(\"plot\").run();";
        String actual = TxtSigmaServer.genExprRunExportResult("plot");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamExportResult2() throws Exception {
        String expected = "mph.result().export(\"plot\").set(\"filename\", \"resultsPath\");";
        String actual = TxtSigmaServer.genExprSetParamExportResult("plot","filename", "resultsPath");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamExportResult() throws Exception {
        String expected = "mph.result().export(\"plot\").set(\"header\", false);";
        String actual = TxtSigmaServer.genExprSetParamExportResult("plot","header", false);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprCreateExportResult() throws Exception {
        String expected = "mph.result().export().create(\"plot\", \"pg\", \"plot1\", \"Plot\");";
        String actual = TxtSigmaServer.genExprCreateExportResult("plot", "pg", "plot1", "Plot");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprRunResult() throws Exception {
        String expected = "mph.result(\"pg\").run();";
        String actual = TxtSigmaServer.genExprRunResult("pg");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetIndexFunction1() throws Exception {
        String expected = "mph.func(\"funName\").setIndex(\"plotargs\", -1.000000, 1, 0);";
        String actual = TxtSigmaServer.genExprSetIndexFunction("funName", "plotargs", -1.0, 1, 0);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprCreatePlotFunc() throws Exception {
        String expected = "mph.func(\"funName\").createPlot(\"pg\");";
        String actual = TxtSigmaServer.genExprCreatePlotFunc("funName", "pg");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamDatasetResult() throws Exception {
        String expected = "mph.result().dataset(\"datasetName\").set(\"solution\", \"solutionName\");";
        String actual = TxtSigmaServer.genExprSetParamDatasetResult("datasetName", "solution", "solutionName");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetLabelDatasetResult() throws Exception {
        String expected = "mph.result().dataset().create(\"datasetName\", \"Solution\");";
        String actual = TxtSigmaServer.genExprSetLabelDatasetResult("datasetName", "Solution");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprCreateDatasetResult() throws Exception {
        String expected = "mph.result().dataset().create(\"datasetName\", \"Solution\");";
        String actual = TxtSigmaServer.genExprCreateDatasetResult("datasetName", "Solution");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprRunAllSol() throws Exception {
        String expected = "mph.sol(\"solutionName\").runAll();";
        String actual = TxtSigmaServer.genExprRunAllSol("solutionName");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprRemoveDatasetResult() throws Exception {
        String expected = "mph.result().dataset().remove(\"datasetName\");";
        String actual = TxtSigmaServer.genExprRemoveDatasetResult("datasetName");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprRemoveFeatureFeatureSol() throws Exception {
        String expected = "mph.sol(\"solutionName\").feature(\"s1\").feature().remove(\"fcDef\");";
        String actual = TxtSigmaServer.genExprRemoveFeatureFeatureSol("solutionName", "s1", "fcDef");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamFeatureSol4() throws Exception {
        String expected = "mph.sol(\"solutionName\").feature(\"t1\").feature(\"fc1\").set(\"damp\", 1.000000);";
        String actual = TxtSigmaServer.genExprSetParamFeatureSol("solutionName", "t1", "fc1", "damp", 1.0);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamFeatureSol3() throws Exception {
        String expected = "mph.sol(\"solutionName\").feature(\"t1\").feature(\"d1\").set(\"linsolver\", \"mumps\");";
        String actual = TxtSigmaServer.genExprSetParamFeatureSol("solutionName", "t1", "d1", "linsolver", "mumps");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamFeatureStudy2() throws Exception {
        String expected = "mph.study(\"studyName\").feature(\"stat\").set(\"notlistsolnum\", 1);";
        String actual = TxtSigmaServer.genExprSetParamFeatureStudy("studyName", "stat", "notlistsolnum", 1);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamFeatureStudy1() throws Exception {
        String expected = "mph.study(\"studyName\").feature(\"time\").set(\"useinitsol\", true);";
        String actual = TxtSigmaServer.genExprSetParamFeatureStudy("studyName", "time", "useinitsol", true);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprRemoveVariable() throws Exception {
        String expected = "mph.variable().remove(\"label\");";
        String actual = TxtSigmaServer.genExprRemoveVariable("label");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprCreateLabelPhysics() throws Exception {
        String expected = "mph.physics(\"ht\").create(\"nodeLabel\", \"HeatSource\", 2);";
        String actual = TxtSigmaServer.genExprCreateLabelPhysics(MPHC.LABEL_PHYSICSTH, "nodeLabel", "HeatSource", 2);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprRemoveFeaturePhysics() throws Exception {
        String expected = "mph.physics(\"ge\").feature().remove(\"ODE_kvl\");";
        String actual = TxtSigmaServer.genExprRemoveFeaturePhysics("ge", "ODE_kvl");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetIndexNumericalResult() throws Exception {
        String expected = "mph.result().numerical(\"numerical\").setIndex(\"expr\", \"expression\", 0);";
        String actual = TxtSigmaServer.genExprSetIndexNumericalResult("numerical", "expr", "expression", 0);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamNumericalResult() throws Exception {
        String expected = "mph.result().numerical(\"numerical\").set(\"data\", \"dataSetLabel\");";
        String actual = TxtSigmaServer.genExprSetParamNumericalResult("numerical", "data", "dataSetLabel");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetLabelNumericalResult() throws Exception {
        String expected = "mph.result().numerical(\"numerical\").label(\"expression\");";
        String actual = TxtSigmaServer.genExprSetLabelNumericalResult("numerical", "expression");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprCreateNumericalResult() throws Exception {
        String expected = "mph.result().numerical().create(\"expression\", \"EvalGlobal\");";
        String actual = TxtSigmaServer.genExprCreateNumericalResult("expression", "EvalGlobal");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprRemoveNumericalResult() throws Exception {
        String expected = "mph.result().numerical().remove(\"expression\");";
        String actual = TxtSigmaServer.genExprRemoveNumericalResult("expression");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprActivateFeature() throws Exception {
        String expected = "mph.study(\"studyName\").feature(\"stat\").activate(\"mf\", true);";
        String actual = TxtSigmaServer.genExprActivateFeature("studyName", "stat", "mf", true);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprAttachSol() throws Exception {
        String expected = "mph.sol(\"solutionName\").attach(\"studyName\");";
        String actual = TxtSigmaServer.genExprAttachSol("solutionName", "studyName");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprCreateFeatureSol() throws Exception {
        String expected = "mph.sol(\"solutionName\").feature(\"t1\").create(\"d1\", \"Direct\");";
        String actual = TxtSigmaServer.genExprCreateFeatureSol("solutionName", "t1", "d1", "Direct");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamFeatureSol2() throws Exception {
        String expected = "mph.sol(\"solutionName\").feature(\"t1\").set(\"maxorder\", 5);";
        String actual = TxtSigmaServer.genExprSetParamFeatureSol("solutionName", "t1", "maxorder", 5);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamFeatureSol1() throws Exception {
        String expected = "mph.sol(\"solutionName\").feature(\"t1\").set(\"atolglobal\", 0.001000);";
        String actual = TxtSigmaServer.genExprSetParamFeatureSol("solutionName", "t1", "atolglobal", 0.001);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamFeatureSol() throws Exception {
        String expected = "mph.sol(\"solutionName\").feature(\"st1\").set(\"study\", \"studyName\");";
        String actual = TxtSigmaServer.genExprSetParamFeatureSol("solutionName", "st1", "study", "studyName");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprCreateSol1() throws Exception {
        String expected = "mph.sol(\"solutionName\").create(\"st1\", \"StudyStep\");";
        String actual = TxtSigmaServer.genExprCreateSol("solutionName", "st1", "StudyStep");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetStudySol() throws Exception {
        String expected = "mph.sol(\"solutionName\").study(\"studyName\");";
        String actual = TxtSigmaServer.genExprSetStudySol("solutionName", "studyName");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprCreateSol() throws Exception {
        String expected = "mph.sol().create(\"solutionName\");";
        String actual = TxtSigmaServer.genExprCreateSol("solutionName");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamFeatureStudy() throws Exception {
        String expected = "mph.study(\"studyName\").feature(\"time\").set(\"tlist\", \"timeRangeString\");";
        String actual = TxtSigmaServer.genExprSetParamFeatureStudy("studyName", "time", "tlist", "timeRangeString");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprCreateStudyType() throws Exception {
        String expected = "mph.study(\"studyName\").create(\"time\", \"Transient\");";
        String actual = TxtSigmaServer.genExprCreateStudyType("studyName", "time", "Transient");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetLabelStudy() throws Exception {
        String expected = "mph.study(\"studyName\").label(\"studyName\");";
        String actual = TxtSigmaServer.genExprSetLabelStudy("studyName", "studyName");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprCreateStudy() throws Exception {
        String expected = "mph.study().create(\"studyName\");";
        String actual = TxtSigmaServer.genExprCreateStudy("studyName");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprRemoveStudy() throws Exception {
        String expected = "mph.study().remove(\"studyName\");";
        String actual = TxtSigmaServer.genExprRemoveStudy("studyName");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprRunMesh() throws Exception {
        String expected = "mph.mesh(\"mesh1\").run();";
        String actual = TxtSigmaServer.genExprRunMesh(MPHC.LABEL_MESH);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprCreateMesh() throws Exception {
        String expected = "mph.mesh().create(\"mesh1\", \"geom1\");";
        String actual = TxtSigmaServer.genExprCreateMesh(MPHC.LABEL_MESH, MPHC.LABEL_GEOM);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetActiveVariable() throws Exception {
        String expected = "mph.variable(\"labelGroup\").active(false);";
        String actual = TxtSigmaServer.genExprSetActiveVariable("labelGroup", false);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetIndexFunction() throws Exception {
        String expected = "mph.func(\"nodeName\").setIndex(\"table\", \"1.0\", 1, 0);";
        String actual = TxtSigmaServer.genExprSetIndexFunction("nodeName", "table", "1.0", 1, 0);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamFunction() throws Exception {
        String expected = "mph.func(\"nodeName\").set(\"funcname\", \"nodeName\");";
        String actual = TxtSigmaServer.genExprSetParamFunction("nodeName", "funcname", "nodeName");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetLabelFunction() throws Exception {
        String expected = "mph.func(\"nodeName\").label(\"nodeName\");";
        String actual = TxtSigmaServer.genExprSetLabelFunction("nodeName", "nodeName");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetModelFunction() throws Exception {
        String expected = "mph.func(\"nodeName\").model(\"compName\");";
        String actual = TxtSigmaServer.genExprSetModelFunction("nodeName", "compName");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprRemoveFunc() throws Exception {
        String expected = "mph.func().remove(\"nodeName\");";
        String actual = TxtSigmaServer.genExprRemoveFunc("nodeName");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamFeaturePhysics() throws Exception {
        String expected = "mph.physics(\"ge\").feature(\"nodeLabel\").set(\"CustomDependentVariableUnit\", \"1\");";
        String actual = TxtSigmaServer.genExprSetParamFeaturePhysics(MPHC.LABEL_PHYSICSGE_GE, "nodeLabel", "CustomDependentVariableUnit", "1");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetIndexFeaturePhysics1() throws Exception {
        String expected = "mph.physics(\"ge\").feature(\"nodeLabel\").setIndex(\"operation\", \"value\", 1, 0);";
        String actual = TxtSigmaServer.genExprSetIndexFeaturePhysics(MPHC.LABEL_PHYSICSGE_GE, "nodeLabel", "operation", "value", 1, 0);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamFuncPropertyGroupMaterial() throws Exception {
        String expected = "mph.material(\"label\").propertyGroup(\"HBCurve\").func(\"HB\").set(\"sourcetype\", \"user\");";
        String actual = TxtSigmaServer.genExprSetParamFuncPropertyGroupMaterial("label", "HBCurve", "HB", "sourcetype", "user");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprCreateFunctionPropertyGroupMaterial() throws Exception {
        String expected = "mph.material(\"label\").propertyGroup(\"HBCurve\").func().create(\"HB\", \"Interpolation\");";
        String actual = TxtSigmaServer.genExprCreateFunctionPropertyGroupMaterial("label", "HBCurve", "HB", "Interpolation");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprCreatePropertyGroupMaterial() throws Exception {
        String expected = "mph.material(\"label\").propertyGroup().create(\"HBCurve\", \"HB curve\");";
        String actual = TxtSigmaServer.genExprCreatePropertyGroupMaterial("label", "HBCurve", "HB curve");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamPropertyGroupMaterial() throws Exception {
        String expected = "mph.material(\"label\").propertyGroup(\"def\").set(\"relpermittivity\", \"1e-9\");";
        String actual = TxtSigmaServer.genExprSetParamPropertyGroupMaterial("label", "def", "relpermittivity", "1e-9");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetNamedSelectionMaterial() throws Exception {
        String expected = "mph.material(\"label\").selection().named(\"selLabel\");";
        String actual = TxtSigmaServer.genExprSetNamedSelectionMaterial("label", "selLabel");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetLabelMaterial() throws Exception {
        String expected = "mph.material(\"label\").label(\"label\");";
        String actual = TxtSigmaServer.genExprSetLabelMaterial("label", "label");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprCreateLabelMaterial() throws Exception {
        String expected = "mph.material().create(\"label\",\"Common\", \"comp1\");";
        String actual = TxtSigmaServer.genExprCreateLabelMaterial("label", "Common", MPHC.LABEL_COMP);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetNamedSelectionCpl() throws Exception {
        String expected = "mph.cpl(\"label\").selection().named(\"selLabel\");";
        String actual = TxtSigmaServer.genExprSetNamedSelectionCpl("label", "selLabel");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamCpl() throws Exception {
        String expected = "mph.cpl(\"label\").set(\"opname\", \"label\");";
        String actual = TxtSigmaServer.genExprSetParamCpl("label", "opname", "label");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetLabelCpl() throws Exception {
        String expected = "mph.cpl(\"label\").label(\"label\");";
        String actual = TxtSigmaServer.genExprSetLabelCpl("label", "label");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprCreateCpl() throws Exception {
        String expected = "mph.cpl().create(\"label\", \"Minimum\", \"geom1\");";
        String actual = TxtSigmaServer.genExprCreateCpl("label", "Minimum", MPHC.LABEL_GEOM);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetNamedSelectionVariable() throws Exception {
        String expected = "mph.variable(\"label\").selection().named(\"selLabel\");";
        String actual = TxtSigmaServer.genExprSetNamedSelectionVariable("label", "selLabel");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamGeomSelectionVariable() throws Exception {
        String expected = "mph.variable(\"label\").selection().geom(\"geom1\", 2);";
        String actual = TxtSigmaServer.genExprSetParamGeomSelectionVariable("label", MPHC.LABEL_GEOM, 2);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetGlobalSelectionVariable() throws Exception {
        String expected = "mph.variable(\"label\").selection().global();";
        String actual = TxtSigmaServer.genExprSetGlobalSelectionVariable("label");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamVariable() throws Exception {
        String expected = "mph.variable(\"groupLabel\").set(\"label\", \"value\");";
        String actual = TxtSigmaServer.genExprSetParamVariable("groupLabel", "label", "value");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetModelVariable() throws Exception {
        String expected = "mph.variable(\"label\").model(\"comp1\");";
        String actual = TxtSigmaServer.genExprSetModelVariable("label", MPHC.LABEL_COMP);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetLabelVariable() throws Exception {
        String expected = "mph.variable(\"label\").label(\"label\");";
        String actual = TxtSigmaServer.genExprSetLabelVariable("label", "label");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprCreateLabelVariable() throws Exception {
        String expected = "mph.variable().create(\"label\");";
        String actual = TxtSigmaServer.genExprCreateLabelVariable("label");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetNamedHideEntitiesView() throws Exception {
        String expected = "mph.view(\"view1\").hideEntities(\"nodeName\").named(\"compName\");";
        String actual = TxtSigmaServer.genExprSetNamedHideEntitiesView(MPHC.LABEL_VIEW, "nodeName", "compName");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetGeomHideEntitiesView() throws Exception {
        String expected = "mph.view(\"view1\").hideEntities(\"nodeName\").geom(\"geom1\", 1);";
        String actual = TxtSigmaServer.genExprSetGeomHideEntitiesView(MPHC.LABEL_VIEW, "nodeName", MPHC.LABEL_GEOM, 1);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprCreateHideEntitiesView() throws Exception {
        String expected = "mph.view(\"view1\").hideEntities().create(\"nodeName\");";
        String actual = TxtSigmaServer.genExprCreateHideEntitiesView(MPHC.LABEL_VIEW, "nodeName");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprInitSelectionFeatureGeometry() throws Exception {
        String expected = "mph.geom(\"geom1\").feature(\"label\").selection(\"selection\").init(1);";
        String actual = TxtSigmaServer.genExprInitSelectionFeatureGeometry(MPHC.LABEL_GEOM, "label", "selection", 1);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamFeatureGeometryBoolean() throws Exception {
        String expected = "mph.geom(\"geom1\").feature(\"label\").set(\"interior\", true);";
        String actual = TxtSigmaServer.genExprSetParamFeatureGeometry(MPHC.LABEL_GEOM, "label", "interior", true);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprRunPreLabelGeometry() throws Exception {
        String expected = "mph.geom(\"geom1\").runPre(\"label\");";
        String actual = TxtSigmaServer.genExprRunPreLabelGeometry(MPHC.LABEL_GEOM, "label");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprRunLabelGeometry() throws Exception {
        String expected = "mph.geom(\"geom1\").run(\"label\");";
        String actual = TxtSigmaServer.genExprRunLabelGeometry(MPHC.LABEL_GEOM, "label");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetIndexFeatureGeometry() throws Exception {
        String expected = "mph.geom(\"geom1\").feature(\"label\").setIndex(\"table\", \"1.0\", 1, 0);";
        String actual = TxtSigmaServer.genExprSetIndexFeatureGeometry(MPHC.LABEL_GEOM, "label", "table", "1.0", 1, 0);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprCreateGeom1() throws Exception {
        String expected = "mph.geom(\"geom1\").create(\"label\", \"UnionSelection\");";
        String actual = TxtSigmaServer.genExprCreateGeom(MPHC.LABEL_GEOM, "label", "UnionSelection");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParamFeatureGeometry() throws Exception {
        String expected = "mph.geom(\"geom1\").feature(\"label\").set(\"entitydim\", \"-1\");";
        String actual = TxtSigmaServer.genExprSetParamFeatureGeometry(MPHC.LABEL_GEOM, "label", "entitydim", "-1");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetLabelFeatureGeometry() throws Exception {
        String expected = "mph.geom(\"geom1\").feature(\"label\").label(\"label\");";
        String actual = TxtSigmaServer.genExprSetLabelFeatureGeometry(MPHC.LABEL_GEOM, "label", "label");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprCreateLabelGeometry() throws Exception {
        String expected = "mph.geom(\"geom1\").create(\"label\", \"UnionSelection\");";
        String actual = TxtSigmaServer.genExprCreateLabelGeometry(MPHC.LABEL_GEOM, "label", "UnionSelection");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetLabelSelectionGeometry() throws Exception {
        String expected = "mph.geom(\"geom1\").selection(\"label\").label(\"label\");";
        String actual = TxtSigmaServer.genExprSetLabelSelectionGeometry(MPHC.LABEL_GEOM, "label", "label");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprCreateSelectionGeometry() throws Exception {
        String localFeatureLabel = "CumulativeSelection";
        String expected = "mph.geom(\"geom1\").selection().create(\"label\", \"CumulativeSelection\");";
        String actual = TxtSigmaServer.genExprCreateSelectionGeometry(MPHC.LABEL_GEOM, "label", localFeatureLabel);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetFunctionParam() throws Exception {
        String expected = "mph.func(\"CFUN_quenchState\").set(\"path\", \"C:/functions/CFUN_quenchState.dll\");";
        String actual = TxtSigmaServer.genExprSetFunctionParam("CFUN_quenchState", "path", "C:/functions/CFUN_quenchState.dll");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetFunctionNodeLabel() throws Exception {
        String expected = "mph.func(\"CFUN_quenchState\").label(\"CFUN_quenchState\");";
        String actual = TxtSigmaServer.genExprSetFunctionNodeLabel("CFUN_quenchState", "CFUN_quenchState");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprCreateFunctionNode() throws Exception {
        String expected = "mph.func().create(\"C:/functions/CFUN_quenchState\", \"External\");";
        String actual = TxtSigmaServer.genExprCreateFunctionNode("C:/functions/CFUN_quenchState", "External");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetParam() throws Exception {
        int defaultValue = 0;
        String expected = "mph.param().set(\"FLAG_iscc_crossover\", \"0\");";
        String actual = TxtSigmaServer.genExprSetParam(MPHC.LABEL_FLAG_ISCC_CROSSOVER, String.valueOf(defaultValue));
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetLabelFeaturePhysics() throws Exception {
        String expected = "mph.physics(\"ht\").feature(\"coil\").label(\"coil\");";
        String actual = TxtSigmaServer.genExprSetLabelFeaturePhysics(MPHC.LABEL_PHYSICSTH, "coil", "coil");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprCreateTagFeaturePhysics() throws Exception {
        String expected = "mph.physics(\"ge\").feature(\"ge1\").tag(\"ODE_cliq\");";
        String actual = TxtSigmaServer.genExprCreateTagFeaturePhysics(MPHC.LABEL_PHYSICSGE_GE, "ge1", MPHC.LABEL_PHYSICSGE_ODE_CLIQ);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetFeaturePhysics() throws Exception {
        String expected = "mph.physics(\"ht\").feature(\"init1\").set(\"Tinit\", \"1.9[K]\");";
        String actual = TxtSigmaServer.genExprSetFeaturePhysics(MPHC.LABEL_PHYSICSTH, "init1", "Tinit", MPHC.VALUE_T_OPERATIONAL);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetIndexFeaturePhysics() throws Exception {
        String expected = "mph.physics(\"dode\").feature(\"dode1\").setIndex(\"f\", \"quenchState_PE\", 0);";
        String actual = TxtSigmaServer.genExprSetIndexFeaturePhysics(MPHC.LABEL_PHYSICSDODE_DODE, "dode1", "f", MPHC.LABEL_QUENCHSTATE, 0);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetPropertyPhysicsString() throws Exception {
        String expected = "mph.physics(\"dode\").prop(\"Units\").set(\"CustomDependentVariableUnit\", \"1\");";
        String actual = TxtSigmaServer.genExprSetPropertyPhysics(MPHC.LABEL_PHYSICSDODE_DODE, "Units", "CustomDependentVariableUnit", "1");
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetPropertyPhysicsBoolean() throws Exception {
        String expected = "mph.physics(\"ht\").prop(\"PhysicalModelProperty\").set(\"IsothermalDomain\", true);";
        String actual = TxtSigmaServer.genExprSetPropertyPhysics(MPHC.LABEL_PHYSICSTH, "PhysicalModelProperty", "IsothermalDomain", true);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprCreatePhysicsNode() throws Exception {
        String expected = "mph.physics().create(\"dode\", \"DomainODE\", \"geom1\");";
        String actual = TxtSigmaServer.genExprCreatePhysicsNode(MPHC.LABEL_PHYSICSDODE_DODE, "DomainODE", MPHC.LABEL_GEOM);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetGeomRepairTol() throws Exception {
        String expected = "mph.geom(\"geom1\").repairTol(0.000050);";
        String actual = TxtSigmaServer.genExprSetGeomRepairTol(MPHC.LABEL_GEOM, Double.parseDouble(MPHC.PARAM_REPAIRTOL_REL));
        assertEquals(expected, actual);
    }

    @Test
    public void genExprSetGeomLabel() throws Exception {
        String expected = "mph.geom(\"geom1\").label(\"geom1\");";
        String actual = TxtSigmaServer.genExprSetGeomLabel(MPHC.LABEL_GEOM, MPHC.LABEL_GEOM);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprCreateGeom() throws Exception {
        String expected = "mph.geom().create(\"geom1\", 2);";
        String actual = TxtSigmaServer.genExprCreateGeom(MPHC.LABEL_GEOM, 2);
        assertEquals(expected, actual);
    }

    @Test
    public void genExprClearFile() throws Exception {
        String expected = "mph.file().clear();";
        String actual = TxtSigmaServer.genExprClearFile();
        assertEquals(expected, actual);
    }

    @Test
    public void genExprCreateModelNode() throws Exception {
        String expected = "mph.modelNode().create(\"comp1\");";
        String actual = TxtSigmaServer.genExprCreateModelNode(MPHC.LABEL_COMP);
        assertEquals(expected, actual);
    }
}