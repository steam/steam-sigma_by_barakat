package geometry;

import model.geometry.basic.Arc;
import model.geometry.basic.Point;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by STEAM on 03/05/2016.
 */

public class ArcTest {
    @Test
    public void constructorArc1() throws Exception {
        Arc a = Arc.ofEndPointsCenter(Point.ofCartesian(0, 1), Point.ofCartesian(1, 0), Point.ofCartesian(0, 0));
        Point kp1 = a.getKp1();
        Point kpc = a.getKpc();

        assertEquals(0, kp1.getX(), 1e-6);
        assertEquals(1, kp1.getY(), 1e-6);

        assertEquals(0, kpc.getX(), 1e-6);
        assertEquals(0, kpc.getY(), 1e-6);
    }

    @Test
    public void constructorArc2() throws Exception {
        double[] xy1 = new double[]{10.1, -0.5};
        Point kp1 = Point.ofCartesian(xy1[0], xy1[1]);
        double[] xy2 = new double[]{-6.4, 4.5};
        Point kp2 = Point.ofCartesian(xy2[0], xy2[1]);
        double[] xcyc = new double[]{-1, -2.1};
        Point kpc = Point.ofCartesian(xcyc[0], xcyc[1]);
        Arc a = Arc.ofEndPointsCenter(kp1, kp2, kpc);

        Point kp = a.getKp1();
        Point center = a.getKpc();
        double dTheta = a.getDTheta();

        assertEquals(xy1[0], kp.getX(), Math.abs(xy1[0]) / 1e6);
        assertEquals(xy1[1], kp.getY(), Math.abs(xy1[1]) / 1e6);
        assertEquals(2.113367749717534, dTheta, Math.abs(xy2[1]) / 1e6);
        assertEquals(xcyc[0], center.getX(), Math.abs(xcyc[0]) / 1e6);
        assertEquals(xcyc[1], center.getY(), Math.abs(xcyc[1]) / 1e6);
    }
}