package geometry;

import model.geometry.basic.Point;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by STEAM on 02/05/2016.
 */
public class PointTest {
    @Test
    public void constructorCartesian1() throws Exception {
        Point p = Point.ofCartesian(0.0, 0.0);
        double x = p.getX();
        double y = p.getY();

        assertEquals(0, x, 1e-6);
        assertEquals(0, y, 1e-6);
    }

    @Test
    public void constructorCartesian2() throws Exception {
        double x_actual = 10.1;
        double y_actual = 0.5;
        Point p = Point.ofCartesian(x_actual, y_actual);
        double x = p.getX();
        double y = p.getY();

        assertEquals(x_actual, x, Math.abs(x)/1e6);
        assertEquals(y_actual, y, Math.abs(y)/1e6);
    }

    @Test
    public void constructorPolar1() throws Exception {
        double r_actual = 1;
        double phi_actual = Math.PI/2;
        Point p = Point.ofPolar(r_actual, phi_actual);
        double x = p.getX();
        double y = p.getY();

        assertEquals(0, x, 1e6);
        assertEquals(1, y, 1e6);
    }

    @Test
    public void constructorPolar2() throws Exception {
        double r_actual = 1;
        double phi_actual = 0;
        Point p = Point.ofPolar(r_actual, phi_actual);
        double x = p.getX();
        double y = p.getY();

        assertEquals(1, x, 1e6);
        assertEquals(0, y, 1e6);
    }

    @Test
    public void getPolar1() throws Exception {
        double r_actual = 4.7;
        double phi_actual = 0.33;

        Point p = Point.ofPolar(r_actual, phi_actual);

        double r = p.getR();
        double phi = p.getPhi();

        assertEquals(r_actual, r, Math.abs(r)/1e6);
        assertEquals(phi_actual, phi, Math.abs(phi)/1e6);
    }

    @Test
    public void getPolar2() throws Exception {
        double x = 1;
        double y = 1;
        Point p = Point.ofCartesian(x, y);

        double r_actual = Math.sqrt(2.0);
        double phi_actual = Math.PI/4;

        double r = p.getR();
        double phi = p.getPhi();

        assertEquals(r_actual, r, Math.abs(r)/1e6);
        assertEquals(phi_actual, phi, Math.abs(phi)/1e6);
    }

    @Test
    public void translate() throws Exception {
        double x_init = 12.4;
        double y_init = 1.6;
        Point p = Point.ofCartesian(x_init, y_init);

        double dx = 0.6;
        double dy = -6.78;

        p = p.translate(dx, dy);
        double x = p.getX();
        double y = p.getY();

        assertEquals(x_init + dx, x, Math.abs(x)/1e6);
        assertEquals(y_init + dy, y, Math.abs(y)/1e6);
    }

    @Test
    public void mirrorX() throws Exception {
        double x_init = 0.01;
        double y_init = -0.75;
        Point p = Point.ofCartesian(x_init, y_init);

        p = p.mirrorX();
        double x = p.getX();
        double y = p.getY();

        assertEquals(x_init, x, Math.abs(x)/1e6);
        assertEquals(-y_init, y, Math.abs(y)/1e6);
    }

    @Test
    public void mirrorY() throws Exception {
        double x_init = 0.01;
        double y_init = -0.75;
        Point p = Point.ofCartesian(x_init, y_init);

        p = p.mirrorY();
        double x = p.getX();
        double y = p.getY();

        assertEquals(-x_init, x, Math.abs(x)/1e6);
        assertEquals(y_init, y, Math.abs(y)/1e6);
    }

    @Test
    public void rotate1() throws Exception {
        double x_init = 1;
        double y_init = 1;
        Point p = Point.ofCartesian(x_init, y_init);

        double angle = Math.PI/2;
        p = p.rotate(angle);
        double x = p.getX();
        double y = p.getY();

        assertEquals(-x_init, x, Math.abs(x)/1e6);
        assertEquals(y_init, y, Math.abs(y)/1e6);
    }

    @Test
    public void rotate2() throws Exception {
        double x_init = 2;
        double y_init = 5;
        Point p = Point.ofCartesian(x_init, y_init);


        Point kpCenter = Point.ofCartesian(0, 5);
        double angle = Math.PI;
        p = p.rotate(angle, kpCenter);
        double x = p.getX();
        double y = p.getY();

        assertEquals(-x_init, x, Math.abs(x)/1e6);
        assertEquals(y_init, y, Math.abs(y)/1e6);
    }

    @Test
    public void isEqualToPoint() throws Exception {
        double x1 = 2;
        double y1 = 5;
        Point p1 = Point.ofCartesian(x1, y1);

        double x2 = 2;
        double y2 = 5;
        Point p2 = Point.ofCartesian(x2, y2);

        double x3 = 2;
        double y3 = 4;
        Point p3 = Point.ofCartesian(x3, y3);

        assertTrue(p1.isEqualToPoint(p2));
        assertFalse(p1.isEqualToPoint(p3));
    }
}