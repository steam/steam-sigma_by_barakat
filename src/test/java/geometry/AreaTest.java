package geometry;

import model.geometry.basic.*;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by STEAM on 03/05/2016.
 */
public class AreaTest {
    @Test
    public void constructorHyperArea() throws Exception {
        double[] xy1 = new double[]{0, 0};
        Point kp1 = Point.ofCartesian(xy1[0], xy1[1]);
        double[] xy2 = new double[]{1, 0};
        Point kp2 = Point.ofCartesian(xy2[0], xy2[1]);
        double[] xy3 = new double[]{0, 1};
        Point kp3 = Point.ofCartesian(xy3[0], xy3[1]);
        Line l1 = Line.ofEndPoints(kp1, kp2);
        Arc a2 = Arc.ofEndPointsCenter(kp2, kp3, kp1);
        Line l3 = Line.ofEndPoints(kp3, kp1);
        HyperLine[] hl = {l1, a2, l3};
        Area ha = Area.ofHyperLines(hl);
        HyperLine[] lines = ha.getHyperLines();

        assertEquals(xy1[0], lines[0].extractEndPoints()[0].getX(), Math.abs(xy1[0]) / 1e6);
        assertEquals(xy1[1], lines[0].extractEndPoints()[0].getY(), Math.abs(xy1[1]) / 1e6);
        assertEquals(xy2[0], lines[1].extractEndPoints()[0].getX(), Math.abs(xy2[0]) / 1e6);
        assertEquals(xy2[1], lines[1].extractEndPoints()[0].getY(), Math.abs(xy2[1]) / 1e6);
        assertEquals(xy3[0], lines[2].extractEndPoints()[0].getX(), Math.abs(xy3[0]) / 1e6);
        assertEquals(xy3[1], lines[2].extractEndPoints()[0].getY(), Math.abs(xy3[1]) / 1e6);
    }

    @Test
    public void translate() throws Exception {
        double[] xy1 = new double[]{1.5, -0.6};
        Point kp1 = Point.ofCartesian(xy1[0], xy1[1]);
        double[] xy2 = new double[]{0.77, 1.3};
        Point kp2 = Point.ofCartesian(xy2[0], xy2[1]);
        double[] xy3 = new double[]{1.1, 6.4};
        Point kp3 = Point.ofCartesian(xy3[0], xy3[1]);
        Line l1 = Line.ofEndPoints(kp1, kp2);
        Arc a2 = Arc.ofEndPointsCenter(kp2, kp3, kp1);
        Line l3 = Line.ofEndPoints(kp3, kp1);
        HyperLine[] hl = {l1, a2, l3};
        Area ha = Area.ofHyperLines(hl);

        double dx = 2.0;
        double dy = -3.5;
        ha = ha.translate(2, -3.5);
        HyperLine[] lines = ha.getHyperLines();

        assertEquals(xy1[0] + dx, lines[0].extractEndPoints()[0].getX(), Math.abs(xy1[0] + dx) / 1e6);
        assertEquals(xy1[1] + dy, lines[0].extractEndPoints()[0].getY(), Math.abs(xy1[1] + dy) / 1e6);
        assertEquals(xy2[0] + dx, lines[1].extractEndPoints()[0].getX(), Math.abs(xy2[0] + dx) / 1e6);
        assertEquals(xy2[1] + dy, lines[1].extractEndPoints()[0].getY(), Math.abs(xy2[1] + dy) / 1e6);
        assertEquals(xy3[0] + dx, lines[2].extractEndPoints()[0].getX(), Math.abs(xy3[0] + dx) / 1e6);
        assertEquals(xy3[1] + dy, lines[2].extractEndPoints()[0].getY(), Math.abs(xy3[1] + dy) / 1e6);
    }

    @Test
    public void mirror() throws Exception {
        double[] xy1 = new double[]{1.5, -0.6};
        Point kp1 = Point.ofCartesian(xy1[0], xy1[1]);
        double[] xy2 = new double[]{0.77, 1.3};
        Point kp2 = Point.ofCartesian(xy2[0], xy2[1]);
        double[] xy3 = new double[]{1.1, 6.4};
        Point kp3 = Point.ofCartesian(xy3[0], xy3[1]);
        Line l1 = Line.ofEndPoints(kp1, kp2);
        Arc a2 = Arc.ofEndPointsCenter(kp2, kp3, kp1);
        Line l3 = Line.ofEndPoints(kp3, kp1);
        HyperLine[] hl = {l1, a2, l3};
        Area ha = Area.ofHyperLines(hl);

        ha = ha.mirrorX();
        HyperLine[] lines = ha.getHyperLines();

        assertEquals(xy1[0], lines[0].extractEndPoints()[0].getX(), Math.abs(xy1[0]) / 1e6);
        assertEquals(-xy1[1], lines[0].extractEndPoints()[0].getY(), Math.abs(xy1[1]) / 1e6);
        assertEquals(xy2[0], lines[1].extractEndPoints()[0].getX(), Math.abs(xy2[0]) / 1e6);
        assertEquals(-xy2[1], lines[1].extractEndPoints()[0].getY(), Math.abs(xy2[1]) / 1e6);
        assertEquals(xy3[0], lines[2].extractEndPoints()[0].getX(), Math.abs(xy3[0]) / 1e6);
        assertEquals(-xy3[1], lines[2].extractEndPoints()[0].getY(), Math.abs(xy3[1]) / 1e6);
    }

    @Test
    public void rotate1() throws Exception {
        double[] xy1 = new double[]{0, 0};
        Point kp1 = Point.ofCartesian(xy1[0], xy1[1]);
        double[] xy2 = new double[]{1, 0};
        Point kp2 = Point.ofCartesian(xy2[0], xy2[1]);
        double[] xy3 = new double[]{0, 1};
        Point kp3 = Point.ofCartesian(xy3[0], xy3[1]);
        Line l1 = Line.ofEndPoints(kp1, kp2);
        Arc a2 = Arc.ofEndPointsCenter(kp2, kp3, kp1);
        Line l3 = Line.ofEndPoints(kp3, kp1);
        HyperLine[] hl = {l1, a2, l3};
        Area ha = Area.ofHyperLines(hl);

        ha = ha.rotate(Math.PI / 2);
        HyperLine[] lines = ha.getHyperLines();

        assertEquals(0, lines[0].extractEndPoints()[0].getX(), 1e-6);
        assertEquals(0, lines[0].extractEndPoints()[0].getY(), 1e-6);
        assertEquals(0, lines[1].extractEndPoints()[0].getX(), 1e-6);
        assertEquals(1, lines[1].extractEndPoints()[0].getY(), 1e-6);
        assertEquals(-1, lines[2].extractEndPoints()[0].getX(), 1e-6);
        assertEquals(0, lines[2].extractEndPoints()[0].getY(), 1e-6);
    }

    @Test
    public void rotate2() throws Exception {
        double[] xy1 = new double[]{0, 0};
        Point kp1 = Point.ofCartesian(xy1[0], xy1[1]);
        double[] xy2 = new double[]{1, 0};
        Point kp2 = Point.ofCartesian(xy2[0], xy2[1]);
        double[] xy3 = new double[]{0, 1};
        Point kp3 = Point.ofCartesian(xy3[0], xy3[1]);
        Line l1 = Line.ofEndPoints(kp1, kp2);
        Arc a2 = Arc.ofEndPointsCenter(kp2, kp3, kp1);
        Line l3 = Line.ofEndPoints(kp3, kp1);
        HyperLine[] hl = {l1, a2, l3};
        Area ha = Area.ofHyperLines(hl);

        ha = ha.rotate(Math.PI / 2, kp2);
        HyperLine[] lines = ha.getHyperLines();

        assertEquals(1, lines[0].extractEndPoints()[0].getX(), 1e-6);
        assertEquals(-1, lines[0].extractEndPoints()[0].getY(), 1e-6);
        assertEquals(1, lines[1].extractEndPoints()[0].getX(), 1e-6);
        assertEquals(0, lines[1].extractEndPoints()[0].getY(), 1e-6);
        assertEquals(0, lines[2].extractEndPoints()[0].getX(), 1e-6);
        assertEquals(-1, lines[2].extractEndPoints()[0].getY(), 1e-6);
    }
}