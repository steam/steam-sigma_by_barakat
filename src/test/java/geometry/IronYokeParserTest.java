package geometry;

import comsol.utils.models.Magnet_FCC_cosTheta_v22b_38_v1_DA_mech;
import model.geometry.Element;
import model.geometry.IronYokeParser;
import model.geometry.Unit;
import model.geometry.basic.*;
import model.materials.database.IronBHCurveParserTest;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by sayadi on 04/06/2018.
 */
public class IronYokeParserTest {

    @Test
    public void skipCommentsTest() throws Exception {
        ArrayList<String> ironGeometryText = new ArrayList<>();
        ironGeometryText.add("Comment1");
        ironGeometryText.add("Comment2");
        ironGeometryText.add("Comment3");
        ironGeometryText.add("");
        ironGeometryText.add("--VARIABLES");
        int actualIndex = IronYokeParser.skipComments(ironGeometryText);
        int expectedIndex = ironGeometryText.indexOf("--VARIABLES");
        assertEquals(actualIndex, expectedIndex);
    }

    @Test
    public void parseVariablesTest() throws Exception {
        String line = "pi       = 3.14159265;";
        String[] dividedLine = line.split("=");
        Map<String, Double> actualMap = new HashMap<>();
        actualMap = IronYokeParser.parseVariable(actualMap, dividedLine);
        Map<String, Double> expectedMap = new HashMap<>();
        expectedMap.put("pi", 3.14159265);
        assertEquals(expectedMap, actualMap);
    }

    @Test
    public void parsePointsTest() throws Exception {
        Map<String, Double> variableMap = new HashMap<>();
        variableMap.put("kp1x", 1.82);
        variableMap.put("kp1y", 300.2);
        String pointLine = "kp1  = [kp1x,kp1y];";
        String[] dividedLine = pointLine.split("=");
        Map<String, Point> actualPointMap = new HashMap<>();
        actualPointMap = IronYokeParser.parsePoint(variableMap, actualPointMap, dividedLine, Unit.MILLIMETER);

        Map<String, Point> expectedPointMap = new HashMap<>();
        double kp1x = 0.00182;
        double kp1y = 0.3002;
        Point expectedPoint = Point.ofCartesian(kp1x, kp1y);
        expectedPointMap.put("kp1", expectedPoint);
        //Test
        assertEquals(actualPointMap.get("kp1").getX(), expectedPointMap.get("kp1").getX(), 1e-6);
        assertEquals(actualPointMap.get("kp1").getY(), expectedPointMap.get("kp1").getY(), 1e-6);
    }


    @Test
    public void parseHyperlinesTest() throws Exception {
        //Create Map of variables
        Map<String, Double> variableMap = new HashMap<>();
        variableMap.put("kp1x", 1.82);
        variableMap.put("kp2x", 0.82);
        variableMap.put("kp1y", 30.42);
        variableMap.put("kp2y", 301.78);
        //Create Map of points
        Map<String, Point> pointMap = new HashMap<>();
        Point kp1 = Point.ofCartesian(1.82, 0.82);
        Point kp2 = Point.ofCartesian(30.42, 301.78);
        pointMap.put("kp1", kp1);
        pointMap.put("kp2", kp2);
        String hyperLine = "ln1  = HyperLine(kp1,kp2,Line);";
        String[] dividedLine = hyperLine.split("=");
        Map<String, HyperLine> actualLineMap = new HashMap<>();
        actualLineMap = IronYokeParser.parseHyperLine(pointMap, variableMap, actualLineMap, dividedLine, Unit.MILLIMETER);

        Map<String, HyperLine> expectedLineMap = new HashMap<>();
        Line line = Line.ofEndPoints(kp1, kp2);

        expectedLineMap.put("ln1", line);
        Line actualLine = (Line) actualLineMap.get("ln1");
        //Test
        assertEquals(kp1.getX(), actualLine.getKp1().getX(), 1e-6);
        assertEquals(kp1.getY(), actualLine.getKp1().getY(), 1e-6);
        assertEquals(kp2.getX(), actualLine.getKp2().getX(), 1e-6);
        assertEquals(kp2.getY(), actualLine.getKp2().getY(), 1e-6);
    }


    @Test
    public void lineParser() throws Exception {
        Element[] expectedElement = Magnet_FCC_cosTheta_v22b_38_v1_DA_mech.iron_yoke();
        String bHCurveIronYokeRelPath = "geometry/IronYokeParser/ironYokeGeometryTest.txt";
        String bHCurveIronYokeAbsPath = IronYokeParserTest.class.getClassLoader().getResource(bHCurveIronYokeRelPath).getFile();
        Map<String, List<Element>> actualElement = IronYokeParser.parseIronYokeGeometry(bHCurveIronYokeAbsPath,
                Unit.MILLIMETER);
        List<Element> elementList = new ArrayList<>();
        Set<String> setString = actualElement.keySet();
        for (String key : setString) {
            elementList.addAll(actualElement.get(key));
        }
        for (int i = 0; i < expectedElement.length; i++) {
            for (int j = 0; j < expectedElement[i].getGeometry().getHyperLines().length; j++) {
                if (expectedElement[i].getGeometry().getHyperLines()[j] instanceof Line) {
                    Line expectedLine = (Line) expectedElement[i].getGeometry().getHyperLines()[j];
                    System.out.println(expectedLine);
                    Line actualLine = (Line) elementList.get(i).getGeometry().getHyperLines()[j];
                    System.out.println(expectedLine.getKp1().getX() + "   " + expectedLine.getKp1().getY());
                    System.out.println(actualLine.getKp1().getX() + "   " + actualLine.getKp1().getY());
                    System.out.println(expectedLine.getKp2().getX() + "   " + expectedLine.getKp2().getY());
                    System.out.println(actualLine.getKp2().getX() + "   " + actualLine.getKp2().getY());
                    assertEquals(expectedLine.getKp1().getX(), actualLine.getKp1().getX(), 1e-6);
                    assertEquals(expectedLine.getKp1().getY(), actualLine.getKp1().getY(), 1e-6);
                    assertEquals(expectedLine.getKp2().getX(), actualLine.getKp2().getX(), 1e-6);
                    assertEquals(expectedLine.getKp2().getY(), actualLine.getKp2().getY(), 1e-6);
                }
            }
        }
    }


    @Test
    public void arcParser() throws Exception {
        Element[] expectedElement = Magnet_FCC_cosTheta_v22b_38_v1_DA_mech.iron_yoke();
        String bHCurveIronYokeRelPath = "geometry/IronYokeParser/ironYokeGeometryTest.txt";
        String bHCurveIronYokeAbsPath = IronBHCurveParserTest.class.getClassLoader().getResource(bHCurveIronYokeRelPath).getFile();
        Map<String, List<Element>> actualElement = IronYokeParser.parseIronYokeGeometry(bHCurveIronYokeAbsPath,
                Unit.MILLIMETER);
        List<Element> elementList = new ArrayList<>();
        Set<String> setString = actualElement.keySet();
        for (String key : setString) {
            elementList.addAll(actualElement.get(key));
        }
        for (int i = 0; i < expectedElement.length; i++) {
            for (int j = 0; j < expectedElement[i].getGeometry().getHyperLines().length; j++) {
                if (expectedElement[i].getGeometry().getHyperLines()[j] instanceof Arc) {
                    Arc expectedLine = (Arc) expectedElement[i].getGeometry().getHyperLines()[j];
                    System.out.println(expectedLine);
                    Arc actualLine = (Arc) elementList.get(i).getGeometry().getHyperLines()[j];
                    System.out.println(expectedLine.getKp1().getX() + "   " + expectedLine.getKp1().getY());
                    System.out.println(actualLine.getKp1().getX() + "   " + actualLine.getKp1().getY());
                    System.out.println(expectedLine.getKpc().getX() + "   " + expectedLine.getKpc().getY());
                    System.out.println(actualLine.getKpc().getX() + "   " + actualLine.getKpc().getY());
                    assertEquals(expectedLine.getKp1().getX(), actualLine.getKp1().getX(), 1e-6);
                    assertEquals(expectedLine.getKp1().getY(), actualLine.getKp1().getY(), 1e-6);
                    assertEquals(expectedLine.getKpc().getX(), actualLine.getKpc().getX(), 1e-6);
                    assertEquals(expectedLine.getKpc().getY(), actualLine.getKpc().getY(), 1e-6);
                }
            }
        }
    }


    @Test
    public void circumferenceParser() throws Exception {
        Element[] expectedElement = Magnet_FCC_cosTheta_v22b_38_v1_DA_mech.iron_yoke();
        String bHCurveIronYokeRelPath = "geometry/IronYokeParser/ironYokeGeometryTest.txt";
        String bHCurveIronYokeAbsPath = IronBHCurveParserTest.class.getClassLoader().getResource(bHCurveIronYokeRelPath).getFile();
        Map<String, List<Element>> actualElement = IronYokeParser.parseIronYokeGeometry(bHCurveIronYokeAbsPath,
                Unit.MILLIMETER);
        List<Element> elementList = new ArrayList<>();
        Set<String> setString = actualElement.keySet();
        for (String key : setString) {
            System.out.println(key);
            elementList.addAll(actualElement.get(key));
        }
        for (int i = 0; i < expectedElement.length; i++) {
            for (int j = 0; j < expectedElement[i].getGeometry().getHyperLines().length; j++) {
                if (expectedElement[i].getGeometry().getHyperLines()[j] instanceof Circumference) {
                    Circumference expectedLine = (Circumference) expectedElement[i].getGeometry().getHyperLines()[j];
                    Circumference actualLine = (Circumference) elementList.get(i).getGeometry().getHyperLines()[j];
                    assertEquals(expectedLine.getCenter().getX(), actualLine.getCenter().getX(), 1e-6);
                    assertEquals(expectedLine.getCenter().getY(), actualLine.getCenter().getY(), 1e-6);
                    assertEquals(expectedLine.getRadius(), actualLine.getRadius(), 1e-6);
                }
            }
        }
    }
}