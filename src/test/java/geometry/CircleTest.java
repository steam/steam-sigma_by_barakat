package geometry;

import model.geometry.basic.Circumference;
import model.geometry.basic.Point;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by STEAM on 03/05/2016.
 */
public class CircleTest {
    @Test
    public void constructorCircle1() throws Exception {
        Point kpc = Point.ofCartesian(0, 0);
        double r = 1.0;
        Circumference c = Circumference.ofCenterRadius(kpc, r);


        assertEquals(0, kpc.getX(), 1e-6);
        assertEquals(0, kpc.getY(), 1e-6);
        assertEquals(1, r, 1e-6);
    }

    @Test
    public void constructorCircle2() throws Exception {
        double[] xcyc = new double[]{10.1, -0.5};
        Point pcenter = Point.ofCartesian(xcyc[0], xcyc[1]);
        double radius = 5.4;
        Circumference c = Circumference.ofCenterRadius(pcenter, radius);
        Point kpc = c.getCenter();
        double r = c.getRadius();

        assertEquals(pcenter.getX(), kpc.getX(), Math.abs(pcenter.getX()) / 1e6);
        assertEquals(pcenter.getY(), kpc.getY(), Math.abs(pcenter.getY()) / 1e6);
        assertEquals(radius, r, r / 1e6);
    }

    @Test
    public void constructorCircle3() throws Exception {
        double[] xy1 = new double[]{-1, 1};
        Point kp1 = Point.ofCartesian(xy1[0], xy1[1]);
        double[] xy2 = new double[]{1, -1};
        Point kp2 = Point.ofCartesian(xy2[0], xy2[1]);
        Circumference c = Circumference.ofDiameterEndPoints(kp1, kp2);
        Point kpc = c.getCenter();
        double r = c.getRadius();

        assertEquals(0, kpc.getX(), 1e-6);
        assertEquals(0, kpc.getY(), 1e-6);
        assertEquals(Math.sqrt(2), r, Math.sqrt(2) / 1e6);
    }

    @Test
    public void evaluate() throws Exception {
        double[] xcyc = new double[]{0, 0};
        Point pcenter = Point.ofCartesian(xcyc[0], xcyc[1]);
        double radius = 1;
        Circumference c = Circumference.ofCenterRadius(pcenter, radius);

        Point[] parray = c.extractPoints(4);

        assertEquals(-1, parray[0].getX(), 1e-6);
        assertEquals(0, parray[0].getY(), 1e-6);
        assertEquals(0, parray[1].getX(), 1e-6);
        assertEquals(-1, parray[1].getY(), 1e-6);
        assertEquals(1, parray[2].getX(), 1e-6);
        assertEquals(0, parray[2].getY(), 1e-6);
        assertEquals(0, parray[3].getX(), 1e-6);
        assertEquals(1, parray[3].getY(), 1e-6);
    }

    @Test
    public void translate() throws Exception {
        double[] xcyc = new double[]{0.7, -1.4};
        Point pcenter = Point.ofCartesian(xcyc[0], xcyc[1]);
        double radius = 0.94;
        Circumference c = Circumference.ofCenterRadius(pcenter, radius);

        double dx = 5.6;
        double dy = -3.4;
        c = c.translate(dx, dy);
        Point kpc = c.getCenter();

        assertEquals(xcyc[0] + dx, kpc.getX(), Math.abs(xcyc[0] + dx) / 1e6);
        assertEquals(xcyc[1] + dy, kpc.getY(), Math.abs(xcyc[1] + dy) / 1e6);
    }

    @Test
    public void mirror() throws Exception {
        double[] xcyc = new double[]{-6.5, -8.4};
        Point pcenter = Point.ofCartesian(xcyc[0], xcyc[1]);
        double radius = 21.6;
        Circumference c = Circumference.ofCenterRadius(pcenter, radius);

        c = c.mirrorX();
        Point kpc = c.getCenter();

        assertEquals(xcyc[0], kpc.getX(), Math.abs(xcyc[0]) / 1e6);
        assertEquals(-xcyc[1], kpc.getY(), Math.abs(-xcyc[1]) / 1e6);
    }

    @Test
    public void rotate1() throws Exception {
        double[] xcyc = new double[]{-1, 1};
        Point pcenter = Point.ofCartesian(xcyc[0], xcyc[1]);
        double radius = 6.4;
        Circumference c = Circumference.ofCenterRadius(pcenter, radius);

        double angle = Math.PI / 2;
        c = c.rotate(angle);
        Point kpc = c.getCenter();

        assertEquals(-1, kpc.getX(), 1e-6);
        assertEquals(-1, kpc.getY(), 1e-6);
    }

    @Test
    public void rotate2() throws Exception {
        double[] xcyc = new double[]{-1, 1};
        Point pcenter = Point.ofCartesian(xcyc[0], xcyc[1]);
        double radius = 6.4;
        Circumference c = Circumference.ofCenterRadius(pcenter, radius);

        double angle = Math.PI;
        double[] xryr = {0, 1};
        Point kpr = Point.ofCartesian(xryr[0], xryr[1]);
        c = c.rotate(angle, kpr);
        Point kpc = c.getCenter();

        assertEquals(0, kpc.getX(), 1e-6);
        assertEquals(1, kpc.getY(), 1e-6);
    }
}