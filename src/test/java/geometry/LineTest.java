package geometry;

import model.geometry.basic.Line;
import model.geometry.basic.Point;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by STEAM on 02/05/2016.
 */
public class LineTest {
    @Test
    public void constructorLine1() throws Exception {
        Point kp1 = Point.ofCartesian(0.0, 0.0);
        Point kp2 = Point.ofCartesian(0.0, 0.0);
        Line l = Line.ofEndPoints(kp1, kp2);

        assertEquals(0, l.getKp1().getX(), 1e-6);
        assertEquals(0, l.getKp1().getY(), 1e-6);
        assertEquals(0, l.getKp2().getX(), 1e-6);
        assertEquals(0, l.getKp2().getY(), 1e-6);
    }

    @Test
    public void constructorLine2() throws Exception {
        Point kp1 = Point.ofCartesian(10.1, -0.5);
        Point kp2 = Point.ofCartesian(-6.4, 4.5);
        Line l = Line.ofEndPoints(kp1, kp2);

        assertEquals(10.1, l.getKp1().getX(), 1e-6);
        assertEquals(-0.5, l.getKp1().getY(), 1e-6);
        assertEquals(-6.4, l.getKp2().getX(), 1e-6);
        assertEquals(4.5, l.getKp2().getY(), 1e-6);
    }

    @Test
    public void translate() throws Exception {
        Point kp1 = Point.ofCartesian(-6.1, -1.5);
        Point kp2 = Point.ofCartesian(4.4, -8.9);
        Line l = Line.ofEndPoints(kp1, kp2);

        double dx = 5.6;
        double dy = -3.4;
        l = l.translate(dx, dy);

        assertEquals(kp1.getX() + dx, l.getKp1().getX(), Math.abs(kp1.getX() + dx)/1e6);
        assertEquals(kp1.getY() + dy, l.getKp1().getY(), Math.abs(kp1.getY() + dy)/1e6);
        assertEquals(kp2.getX() + dx, l.getKp2().getX(), Math.abs(kp2.getX() + dx)/1e6);
        assertEquals(kp2.getY() + dy, l.getKp2().getY(), Math.abs(kp2.getY() + dy)/1e6);
    }

    @Test
    public void mirrorX() throws Exception {
        Point kp1 = Point.ofCartesian(10.1, -0.5);
        Point kp2 = Point.ofCartesian(-6.4, 4.5);
        Line l = Line.ofEndPoints(kp1, kp2);

        l = l.mirrorX();

        assertEquals(10.1, l.getKp1().getX(), 1e-6);
        assertEquals(0.5, l.getKp1().getY(), 1e-6);
        assertEquals(-6.4, l.getKp2().getX(), 1e-6);
        assertEquals(-4.5, l.getKp2().getY(), 1e-6);
    }

    @Test
    public void mirrorY() throws Exception {
        Point kp1 = Point.ofCartesian(10.1, -0.5);
        Point kp2 = Point.ofCartesian(-6.4, 4.5);
        Line l = Line.ofEndPoints(kp1, kp2);

        l = l.mirrorY();

        assertEquals(-10.1, l.getKp1().getX(), 1e-6);
        assertEquals(-0.5, l.getKp1().getY(), 1e-6);
        assertEquals(6.4, l.getKp2().getX(), 1e-6);
        assertEquals(4.5, l.getKp2().getY(), 1e-6);
    }

    @Test
    public void rotate1() throws Exception {
        double x1 = 1;
        double y1 = 1;
        Point kp1 = Point.ofCartesian(x1,y1);
        double x2 = -1;
        double y2 = 1;
        Point kp2 = Point.ofCartesian(x2, y2);
        Line l = Line.ofEndPoints(kp1, kp2);

        double angle = Math.PI/2;

        l = l.rotate(angle);

        assertEquals(-x1, l.getKp1().getX(), Math.abs(x1)/1e6);
        assertEquals(y1,  l.getKp1().getY(), Math.abs(y1)/1e6);
        assertEquals(x2,  l.getKp2().getX(), Math.abs(x2)/1e6);
        assertEquals(-y2, l.getKp2().getY(), Math.abs(y2)/1e6);
    }

    @Test
    public void rotate2() throws Exception {
        double x1 = 1;
        double y1 = 1;
        Point kp1 = Point.ofCartesian(x1, y1);
        double x2 = -1;
        double y2 = 1;
        Point kp2 = Point.ofCartesian(x2, y2);
        Line l = Line.ofEndPoints(kp1, kp2);

        double angle = Math.PI;

        l = l.rotate(angle, kp1);

        assertEquals(x1, l.getKp1().getX(), Math.abs(x1)/1e6);
        assertEquals(y1,  l.getKp1().getY(), Math.abs(y1)/1e6);
        assertEquals(3.0,  l.getKp2().getX(), Math.abs(x2)/1e6);
        assertEquals(y2, l.getKp2().getY(), Math.abs(y2)/1e6);
    }

    @Test
    public void reverse() {
        double x1 = 1.3;
        double y1 = 0.1;
        Point kp1 = Point.ofCartesian(x1, y1);
        double x2 = -1.0;
        double y2 = 15.3;
        Point kp2 = Point.ofCartesian(x2, y2);
        Line l = Line.ofEndPoints(kp1, kp2);

        l = l.reverse();

        assertEquals(x2, l.getKp1().getX(), Math.abs(x2)/1e6);
        assertEquals(y2,  l.getKp1().getY(), Math.abs(y2)/1e6);
        assertEquals(x1,  l.getKp2().getX(), Math.abs(x1)/1e6);
        assertEquals(y1, l.getKp2().getY(), Math.abs(y1)/1e6);
    }

    @Test
    public void getCoordinatesExpr(){
        double x1 = 1.3;
        double y1 = 0.1;
        Point kp1 = Point.ofCartesian(x1, y1);
        double x2 = -1.0;
        double y2 = 15.3;
        Point kp2 = Point.ofCartesian(x2, y2);
        Line l = Line.ofEndPoints(kp1, kp2);

        String[] coordExpr = l.getCoordinatesExpr();

        assertEquals("1.3+(-1.0-1.3)*s", coordExpr[0]);
        assertEquals("0.1+(15.3-0.1)*s", coordExpr[1]);
    }

    @Test
    public void calculateVersor(){
        double x1 = 0;
        double y1 = 0;
        Point kp1 = Point.ofCartesian(x1, y1);
        double x2 = 3;
        double y2 = 4;
        Point kp2 = Point.ofCartesian(x2, y2);
        Line l = Line.ofEndPoints(kp1, kp2);

        Point versor = l.calculateVersor();

        assertEquals(3.0/5.0, versor.getX(), 1e-6);
        assertEquals(4.0/5.0, versor.getY(), 1e-6);
    }

    @Test
    public void calculateLength(){
        double x1 = 0;
        double y1 = 0;
        Point kp1 = Point.ofCartesian(x1, y1);
        double x2 = 3;
        double y2 = 4;
        Point kp2 = Point.ofCartesian(x2, y2);
        Line l = Line.ofEndPoints(kp1, kp2);

        double length = l.calculateLength();

        assertEquals(5.0, length, Math.abs(5.0)/1e6);
    }

    @Test
    public void extractCommonPointWith1(){
        double x1 = 1;
        double y1 = 1;
        Point kp1 = Point.ofCartesian(x1, y1);
        double x2 = 3;
        double y2 = 4;
        Point kp2 = Point.ofCartesian(x2, y2);
        Line l1 = Line.ofEndPoints(kp1, kp2);

        Point kp3 = Point.ofCartesian(x1, y1);
        double x4 = 2;
        double y4 = 2;
        Point kp4 = Point.ofCartesian(x4, y4);
        Line l2 = Line.ofEndPoints(kp3, kp4);

        Point common = l1.extractCommonPointWith(l2);

        assertEquals(x1, common.getX(), Math.abs(x1)/1e6);
        assertEquals(y1, common.getY(), Math.abs(y1)/1e6);
    }

    @Test
    public void extractCommonPointWith2(){
        double x1 = 2;
        double y1 = 2;
        Point kp1 = Point.ofCartesian(x1, y1);
        double x2 = 3;
        double y2 = 4;
        Point kp2 = Point.ofCartesian(x2, y2);
        Line l1 = Line.ofEndPoints(kp1, kp2);

        double x3 = 0;
        double y3 = 1;
        Point kp3 = Point.ofCartesian(x3, y3);

        Point kp4 = Point.ofCartesian(x1, y1);
        Line l2 = Line.ofEndPoints(kp3, kp4);

        Point common = l1.extractCommonPointWith(l2);

        assertEquals(x1, common.getX(), Math.abs(x1)/1e6);
        assertEquals(y1, common.getY(), Math.abs(y1)/1e6);
    }

    @Test
    public void extractCommonPointWith3(){
        double x1 = 2;
        double y1 = 2;
        Point kp1 = Point.ofCartesian(x1, y1);
        double x2 = 3;
        double y2 = 4;
        Point kp2 = Point.ofCartesian(x2, y2);
        Line l1 = Line.ofEndPoints(kp1, kp2);

        Point kp3 = Point.ofCartesian(x2, y2);
        double x4 = 4;
        double y4 = 5;
        Point kp4 = Point.ofCartesian(x4, y4);
        Line l2 = Line.ofEndPoints(kp3, kp4);

        Point common = l1.extractCommonPointWith(l2);

        assertEquals(x2, common.getX(), Math.abs(x1)/1e6);
        assertEquals(y2, common.getY(), Math.abs(y1)/1e6);
    }

    @Test
    public void extractCommonPointWith4(){
        double x1 = 2;
        double y1 = 1;
        Point kp1 = Point.ofCartesian(x1, y1);
        double x2 = 3;
        double y2 = 4;
        Point kp2 = Point.ofCartesian(x2, y2);
        Line l1 = Line.ofEndPoints(kp1, kp2);

        double x3 = 0;
        double y3 = 1;
        Point kp3 = Point.ofCartesian(x3, y3);

        Point kp4 = Point.ofCartesian(x2, y2);
        Line l2 = Line.ofEndPoints(kp3, kp4);

        Point common = l1.extractCommonPointWith(l2);

        assertEquals(x2, common.getX(), Math.abs(x1)/1e6);
        assertEquals(y2, common.getY(), Math.abs(y1)/1e6);
    }
}