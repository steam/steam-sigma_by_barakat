package geometry;

import model.geometry.basic.Line;
import model.geometry.basic.Point;
import model.geometry.basic.Polygon;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by STEAM on 10/05/2016.
 */
public class PolygonTest {
    @Test
    public void constructorPolygon() throws Exception {
        double[] xy1 = new double[]{0, 0};
        Point kp1 = Point.ofCartesian(xy1[0], xy1[1]);
        double[] xy2 = new double[]{1, 0};
        Point kp2 = Point.ofCartesian(xy2[0], xy2[1]);
        double[] xy3 = new double[]{1, 1};
        Point kp3 = Point.ofCartesian(xy3[0], xy3[1]);
        double[] xy4 = new double[]{0, 1};
        Point kp4 = Point.ofCartesian(xy4[0], xy4[1]);

        Line l1 = Line.ofEndPoints(kp2, kp1);
        Line l2 = Line.ofEndPoints(kp2, kp3);
        Line l3 = Line.ofEndPoints(kp3, kp4);
        Line l4 = Line.ofEndPoints(kp1, kp4);
        Polygon po = Polygon.ofLines(new Line[]{l1, l2, l3, l4});
        Point[] parray = po.getVertices();

        assertEquals(xy2[0], parray[0].getX(), 1e-6);
        assertEquals(xy2[1], parray[0].getY(), 1e-6);
        assertEquals(xy3[0], parray[1].getX(), 1e-6);
        assertEquals(xy3[1], parray[1].getY(), 1e-6);
        assertEquals(xy4[0], parray[2].getX(), 1e-6);
        assertEquals(xy4[1], parray[2].getY(), 1e-6);
        assertEquals(xy1[0], parray[3].getX(), 1e-6);
        assertEquals(xy1[1], parray[3].getY(), 1e-6);
    }

    @Test
    public void calculateSurface() throws Exception {
        double[] xy1 = new double[]{0,0};
        Point kp1 = Point.ofCartesian(xy1[0], xy1[1]);
        double[] xy2 = new double[]{2,0};
        Point kp2 = Point.ofCartesian(xy2[0], xy2[1]);
        double[] xy3 = new double[]{2,1};
        Point kp3 = Point.ofCartesian(xy3[0], xy3[1]);
        double[] xy4 = new double[]{0,1};
        Point kp4 = Point.ofCartesian(xy4[0], xy4[1]);

        Line l1 = Line.ofEndPoints(kp2, kp1);
        Line l2 = Line.ofEndPoints(kp2, kp3);
        Line l3 = Line.ofEndPoints(kp3, kp4);
        Line l4 = Line.ofEndPoints(kp1, kp4);
        Polygon po = Polygon.ofLines(new Line[] {l1,l2,l3,l4});
        double area = po.calculateSurface();

        assertEquals(2, area, 2/1e6);
    }

}