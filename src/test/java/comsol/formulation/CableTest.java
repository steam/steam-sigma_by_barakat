package comsol.formulation;

import comsol.definitions.VariablesCable;
import model.materials.database.MatDatabase;
import model.geometry.coil.Cable;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by STEAM on 15/12/2016.
 */
public class CableTest {

    // Geometry
    // *****************************************************************************************************************

    @Test
    public void defineWidthInsulationEdgeNarrow(){
        double val = 1.0;
        String actual = String.format("%s", VariablesCable.defineWidthInsulationEdgeNarrow(val));
        String expected = "1.0[m]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineWidthInsulationEdgeWide(){
        double val = 1.0;
        String actual = String.format("%s", VariablesCable.defineWidthInsulationEdgeWide(val));
        String expected = "1.0[m]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineDiameterFilament(){
        double val = 1.0;
        String actual = String.format("%s", VariablesCable.defineDiameterFilament(val));
        String expected = "1.0[m]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineTwistPitchFilament(){
        double val = 1.0;
        String actual = String.format("%s", VariablesCable.defineTwistPitchFilament(val));
        String expected = "1.0[m]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineDiameterStrand(){
        double val = 1.0;
        String actual = String.format("%s", VariablesCable.defineDiameterStrand(val));
        String expected = "1.0[m]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineTwistPitchStrand(){
        double val = 1.0;
        String actual = String.format("%s", VariablesCable.defineTwistPitchStrand(val));
        String expected = "1.0[m]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineThetaTwistPitchStrand(){
        double val = 45.0;
        String actual = String.format("%s", VariablesCable.defineThetaTwistPitchStrand(val));
        String expected = "45.0[1]";
        assertEquals(expected, actual);
    }

//    @Test
//    public void defineThetaTwistPitchElectric(){
//        String actual = String.format("%s", VariablesCable.defineThetaTwistPitchElectric());
//        String expected = "atan((w_bare_C-d_strand_C)/(l_tpStrand_C/2))[1]";
//        assertEquals(expected, actual);
//    }

    @Test
    public void defineWidthBareCable(){
        double val = 1.0;
        String actual = String.format("%s", VariablesCable.defineWidthBareCable(val));
        String expected = "1.0[m]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineInnerHeightBareCable(){
        double val = 1.0;
        String actual = String.format("%s", VariablesCable.defineInnerHeightBareCable(val));
        String expected = "1.0[m]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineOuterHeightBareCable(){
        double val = 1.0;
        String actual = String.format("%s", VariablesCable.defineOuterHeightBareCable(val));
        String expected = "1.0[m]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineAverageHeightBareCable(){
        double val1 = 1.0;
        double val2 = 2.0;
        String actual = String.format("%s", VariablesCable.defineAverageHeightBareCable(val1, val2));
        String expected = "1.5[m]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineWidthCore(){
        double val1 = 1.0;
        String actual = String.format("%s", VariablesCable.defineWidthCore(val1));
        String expected = "1.0[m]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineHeightCore(){
        double val1 = 1.0;
        String actual = String.format("%s", VariablesCable.defineHeightCore(val1));
        String expected = "1.0[m]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineNoOfStrands(){
        double val = 1.0;
        String actual = String.format("%s", VariablesCable.defineNoOfStrands(val));
        String expected = "1.0[1]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineNoOfStrandsPerLayer(){
        double val = 1.0;
        String actual = String.format("%s", VariablesCable.defineNoOfStrandsPerLayer(val));
        String expected = "1.0[1]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineNoOfLayersOfStrands(){
        double val = 1.0;
        String actual = String.format("%s", VariablesCable.defineNoOfLayersOfStrands(val));
        String expected = "1.0[1]";
        assertEquals(expected, actual);
    }

    // Areas
    // *****************************************************************************************************************

    @Test
    public void defineSurfaceBare(){
        String actual = String.format("%s", VariablesCable.defineSurfaceBare());
        String expected = "w_bare_C*h_avgBare_C";
        assertEquals(expected, actual);
    }

    @Test
    public void defineSurfaceCore(){
        String actual = String.format("%s", VariablesCable.defineSurfaceCore());
        String expected = "w_core_C*h_core_C";
        assertEquals(expected, actual);
    }

    @Test
    public void defineSurfaceInsulation(){
        String actual = String.format("%s", VariablesCable.defineSurfaceInsulation());
        String expected = "(w_bare_C+2*w_insulNarrow_C)*(h_avgBare_C+2*w_insulWide_C)-(w_bare_C*h_avgBare_C)";
        assertEquals(expected, actual);
    }

    @Test
    public void defineSurfaceConductor(){
        String actual = String.format("%s", VariablesCable.defineSurfaceConductor());
        String expected = "(1/cos(theta_lTpStrand_C))*num_strands_C*pi*(d_strand_C/2)^2";
        assertEquals(expected, actual);
    }

    @Test
    public void defineSurfaceInnerVoids(){
        String actual = String.format("%s", VariablesCable.defineSurfaceInnerVoids());
        String expected = "(num_strandsPerLayer_C-1)*(num_layersOfStrands_C-1)/(num_strandsPerLayer_C*num_layersOfStrands_C)*surf_voids_C";
        assertEquals(expected, actual);
    }

    @Test
    public void defineSurfaceOuterVoids(){
        String actual = String.format("%s", VariablesCable.defineSurfaceOuterVoids());
        String expected = "(num_strandsPerLayer_C+num_layersOfStrands_C-1)/(num_strandsPerLayer_C*num_layersOfStrands_C)*surf_voids_C";
        assertEquals(expected, actual);
    }

    @Test
    public void defineSurfaceVoids(){
        String actual = String.format("%s", VariablesCable.defineSurfaceVoids());
        String expected = "surf_bare_C-(surf_cond_C+surf_core_C)";
        assertEquals(expected, actual);
    }

    // Fractions
    // *****************************************************************************************************************

    @Test
    public void defineFractionCopper(){
        double val = 1.0;
        String actual = String.format("%s", VariablesCable.defineFractionCopper(val));
        String expected = "1.0[1]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineFractionSuperconductor(){
        double val = 1.0;
        String actual = String.format("%s", VariablesCable.defineFractionSuperconductor(val));
        String expected = "1.0[1]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineFractionHelium(){
        double val = 1.0;
        String actual = String.format("%s", VariablesCable.defineFractionHelium(val));
        String expected = "1.0[1]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineFractionFillingInnerVoids(){
        double val = 1.0;
        String actual = String.format("%s", VariablesCable.defineFractionFillingInnerVoids(val));
        String expected = "1.0[1]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineFractionFillingOuterVoids(){
        double val = 1.0;
        String actual = String.format("%s", VariablesCable.defineFractionFillingOuterVoids(val));
        String expected = "1.0[1]";
        assertEquals(expected, actual);
    }

    // Resistance
    // *****************************************************************************************************************

    @Test
    public void defineRRR(){
        double val = 1.0;
        String actual = String.format("%s", VariablesCable.defineRRR(val));
        String expected = "1.0[1]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineTupReferenceForRRR(){
        double val = 1.0;
        String actual = String.format("%s", VariablesCable.defineTupReferenceForRRR(val));
        String expected = "1.0[K]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineRc(){
        double val = 1.0;
        String actual = String.format("%s", VariablesCable.defineRc(val));
        String expected = "1.0[ohm]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineRa(){
        double val = 1.0;
        String actual = String.format("%s", VariablesCable.defineRa(val));
        String expected = "1.0[ohm]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineFactorRhoEffective(){
        double val = 1.0;
        String actual = String.format("%s", VariablesCable.defineFactorRhoEffective(val));
        String expected = "1.0[1]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineC1NbTi(){
        double val = 1.0;
        String actual = String.format("%s", VariablesCable.defineC1NbTi(val));
        String expected = "1.0[A]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineC2NbTi(){
        double val = 1.0;
        String actual = String.format("%s", VariablesCable.defineC2NbTi(val));
        String expected = "1.0[A/T]";
        assertEquals(expected, actual);
    }

    // Time constants
    // *****************************************************************************************************************

    @Test
    public void defineTauISCCxOver(){
        // TauISCCxOver
        String actual = String.format("%s", VariablesCable.defineTauISCCxOver());
        String expected = "mu0_const/120*l_tpStrand_C*num_strands_C*(num_strands_C-1)*w_bare_C/h_avgBare_C*1/Rc_C";
        assertEquals(expected, actual);

    }

    @Test
    public void defineTauISCCadjN(){
        // TauISCCadjN
        String actual = String.format("%s", VariablesCable.defineTauISCCadjN());
        String expected = "mu0_const/8*l_tpStrand_C*h_avgBare_C/w_bare_C*1/Ra_C";
        assertEquals(expected, actual);
    }

    @Test
    public void defineTauISCCadjW(){
        // TauISCCadjW
        String actual = String.format("%s", VariablesCable.defineTauISCCadjW());
        String expected = "2*mu0_const/6*l_tpStrand_C*w_bare_C/h_avgBare_C*1/Ra_C";
        assertEquals(expected, actual);
    }

    @Test
    public void defineTauIFCC(){
        String actual = String.format("%s", VariablesCable.defineTauIFCC());
        String expected = "mu0_const*(l_tpFil_C/(2*pi))^2*1/(rho_C*fRhoEff_C)";
        assertEquals(expected, actual);
    }


    // Fits
    // *****************************************************************************************************************

    @Test
    public void defineRhoCu(){
        String actual;
        String expected;

        // case rho_Cu_CUDI
        actual = String.format("%s", VariablesCable.defineRhoCu(Cable.ResitivityCopperFitEnum.rho_Cu_CUDI));
        expected = "CFUN_rhoCuCUDI(T[1/K],mf.normB[1/T],RRR_C,Tup_RRR_C[1/K])[ohm*m]";
        assertEquals(expected, actual);

        // case rho_Cu_NIST
        actual = String.format("%s", VariablesCable.defineRhoCu(Cable.ResitivityCopperFitEnum.rho_Cu_NIST));
        expected = "CFUN_rhoCuNIST(T[1/K],mf.normB[1/T],RRR_C,Tup_RRR_C[1/K])[ohm*m]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineRhoCuZeroB(){
        String actual;
        String expected;

        // case rho_Cu_CUDI
        actual = String.format("%s", VariablesCable.defineRhoCuZeroB(Cable.ResitivityCopperFitEnum.rho_Cu_CUDI));
        expected = "CFUN_rhoCuCUDI(T[1/K],0,RRR_C,Tup_RRR_C[1/K])[ohm*m]";
        assertEquals(expected, actual);

        // case rho_Cu_NIST
        actual = String.format("%s", VariablesCable.defineRhoCuZeroB(Cable.ResitivityCopperFitEnum.rho_Cu_NIST));
        expected = "CFUN_rhoCuNIST(T[1/K],0,RRR_C,Tup_RRR_C[1/K])[ohm*m]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineIc(){
        String actual;
        String expected;
        Cable c = new Cable();
//        VariablesCable vl = new VariablesCable("coil_OLD", c);

        // case Ic_NbTi_GSI
        actual = String.format("%s", VariablesCable.defineIc(Cable.CriticalSurfaceFitEnum.Ic_NbTi_GSI));
        expected = "CFUN_IcNbTiGSI(T[1/K],mf.normB[1/T],frac_sc_C*surf_cond_C[1/m^2])[A]";
        assertEquals(expected, actual);

        // case Ic_Nb3Sn_NIST
        actual = String.format("%s", VariablesCable.defineIc(Cable.CriticalSurfaceFitEnum.Ic_Nb3Sn_NIST));
        expected = "CFUN_IcNb3SnHiLumi(T[1/K],mf.normB[1/T],frac_sc_C*surf_cond_C[1/m^2])[A]";
        assertEquals(expected, actual);

        // case Ic_Nb3Sn_FCC
        actual = String.format("%s", VariablesCable.defineIc(Cable.CriticalSurfaceFitEnum.Ic_Nb3Sn_FCC));
        expected = "CFUN_IcNb3SnFCC(T[1/K],mf.normB[1/T],frac_sc_C*surf_cond_C[1/m^2])[A]";
        assertEquals(expected, actual);
    }

    // Cv Materials
    // ***************************************************************0*************************************************

    @Test
    public void defineCvCopperMat(){
        String actual = String.format("%s", VariablesCable.defineCvCopperMat(Cable.ResitivityCopperFitEnum.rho_Cu_CUDI));
        String expected = "CFUN_CvCu(T[1/K])[J/(m^3*K)]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineCvScMat(){
        String actual;
        String expected;

        // case Ic_NbTi_GSI
        actual = String.format("%s", VariablesCable.defineCvSuperconductorMat(Cable.CriticalSurfaceFitEnum.Ic_NbTi_GSI));
        expected = "CFUN_CvNbTi(T[1/K],mf.normB[1/T],Is_HT[1/A],C1_C[1/A],C2_C[T/A])[J/m^3/K]";
        assertEquals(expected, actual);

        // case Ic_Nb3Sn_NIST
        actual = String.format("%s", VariablesCable.defineCvSuperconductorMat(Cable.CriticalSurfaceFitEnum.Ic_Nb3Sn_NIST));
        expected = "CFUN_CvNb3SnNIST(T[1/K],mf.normB[1/T])[J/m^3/K]";
        assertEquals(expected, actual);

        // case Ic_Nb3Sn_FCC
        actual = String.format("%s", VariablesCable.defineCvSuperconductorMat(Cable.CriticalSurfaceFitEnum.Ic_Nb3Sn_FCC));
        expected = "CFUN_CvNb3SnNIST(T[1/K],mf.normB[1/T])[J/m^3/K]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineCpCoreMat(){
        String actual;
        String expected;

        // case Steel
        actual = String.format("%s", VariablesCable.defineCvCoreMat(MatDatabase.MAT_STEEL));
        expected = "CFUN_CvSteel(T[1/K])[J/m^3/K]";
        assertEquals(expected, actual);

        // case Kapton
        actual = String.format("%s", VariablesCable.defineCvVoidsMat(MatDatabase.MAT_KAPTON));
        expected = "CFUN_CvKapton(T[1/K])[J/m^3/K]";
        assertEquals(expected, actual);

        // case G10
        actual = String.format("%s", VariablesCable.defineCvVoidsMat(MatDatabase.MAT_GLASSFIBER));
        expected = "CFUN_CvG10(T[1/K])[J/m^3/K]";
        assertEquals(expected, actual);

        // case VOID
        actual = String.format("%s", VariablesCable.defineCvVoidsMat(MatDatabase.MAT_VOID));
        expected = "0[J/m^3/K]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineCvInsulationMat(){
        String actual;
        String expected;

        // case Kapton
        actual = String.format("%s", VariablesCable.defineCvInsulationMat(MatDatabase.MAT_KAPTON));
        expected = "CFUN_CvKapton(T[1/K])[J/m^3/K]";
        assertEquals(expected, actual);

        // case G10
        actual = String.format("%s", VariablesCable.defineCvInsulationMat(MatDatabase.MAT_GLASSFIBER));
        expected = "CFUN_CvG10(T[1/K])[J/m^3/K]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineCvHeliumMat(){
        String actual = String.format("%s", VariablesCable.defineCvHeliumMat());
        String expected = "CFUN_CvHe(T[1/K])[J/(m^3*K)]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineCpVoidsInsulationMat(){
        String actual;
        String expected;

        // case Kapton
        actual = String.format("%s", VariablesCable.defineCvVoidsMat(MatDatabase.MAT_KAPTON));
        expected = "CFUN_CvKapton(T[1/K])[J/m^3/K]";
        assertEquals(expected, actual);

        // case G10
        actual = String.format("%s", VariablesCable.defineCvVoidsMat(MatDatabase.MAT_GLASSFIBER));
        expected = "CFUN_CvG10(T[1/K])[J/m^3/K]";
        assertEquals(expected, actual);

        // case VOID
        actual = String.format("%s", VariablesCable.defineCvVoidsMat(MatDatabase.MAT_VOID));
        expected = "0[J/m^3/K]";
        assertEquals(expected, actual);
    }


    // Cv Cable
    // ***************************************************************0*************************************************

    @Test
    public void defineCvConductor(){
        String actual = String.format("%s", VariablesCable.defineCvConductor());
        String expected = "((frac_cu_C*cv_cuMat_C)+(frac_sc_C*cv_scMat_C)+(frac_he_C*cv_heMat_C))*surf_cond_C";
        assertEquals(expected, actual);
    }

    @Test
    public void defineCvCore(){
        String actual = String.format("%s", VariablesCable.defineCvCore());
        String expected = "cv_coreMat_C*surf_core_C";
        assertEquals(expected, actual);
    }

    @Test
    public void defineCvInsulation(){
        String actual = String.format("%s", VariablesCable.defineCvInsulation());
        String expected = "(cv_insMat_C*surf_ins_C)";
        assertEquals(expected, actual);
    }

    @Test
    public void defineCvVoids(){
        String actual = String.format("%s", VariablesCable.defineCvVoids());
        String expected = "((frac_fill_inVoids_C*cv_inVoidsMat_C*surf_inVoids_C)+(frac_fill_outVoids_C*cv_outVoidsMat_C*surf_outVoids_C))";
        assertEquals(expected, actual);
    }

}
