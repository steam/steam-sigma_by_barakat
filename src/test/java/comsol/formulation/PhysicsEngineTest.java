package comsol.formulation;

import comsol.definitions.VariablesPhysicsEngine;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by STEAM on 06/07/2016.
 */
public class PhysicsEngineTest {

    @Test
    public void defineJs(){
        String expected = "tau_PE*Is_HT";
        String actual = VariablesPhysicsEngine.defineJs();
        assertEquals(expected, actual);
    }

    @Test
    public void defineTauDensity(){
        String expected = "Jz_versor_HT*ratio_HT/surf_HT";
        String actual = VariablesPhysicsEngine.defineTauDensity();
        assertEquals(expected, actual);
    }

    @Test
    public void defineKsurfScalingFactor(){
        String expected = "ratio_HT/surf_HT*surf_cond_C";
        String actual = VariablesPhysicsEngine.defineKsurfScalingFactor();
        assertEquals(expected, actual);
    }

    @Test
    public void defineMIFCCx(){
//        String expected = "if(FLAG_ifcc>0,(-2)/mu0_const*tau_ifcc_C*(d(mf.Bx,t))*k_surf_PE*dampFactorCC_PE,0)";
        String expected = "if(FLAG_ifcc>0,(-1)/mu0_const*tau_ifcc_C*(d(mf.Bx,t))*k_surf_PE*dampFactorCC_PE,0)";
        String actual = VariablesPhysicsEngine.defineMIFCCx();
        assertEquals(expected, actual);
    }

    @Test
    public void defineMIFCCy(){
//        String expected = "if(FLAG_ifcc>0,(-2)/mu0_const*tau_ifcc_C*(d(mf.By,t))*k_surf_PE*dampFactorCC_PE,0)";
        String expected = "if(FLAG_ifcc>0,(-1)/mu0_const*tau_ifcc_C*(d(mf.By,t))*k_surf_PE*dampFactorCC_PE,0)";
        String actual = VariablesPhysicsEngine.defineMIFCCy();
        assertEquals(expected, actual);
    }

    @Test
    public void defineMISCCadjN(){
        String expected = "if(FLAG_iscc_adjn>0,(-1)/mu0_const*tau_isccadjn_C*d(B_avgw_HT,t)*k_surf_PE*dampFactorCC_PE,0)";
        String actual = VariablesPhysicsEngine.defineMISCCadjN();
        assertEquals(expected, actual);
    }

    @Test
    public void defineMISCCxOver(){
        String expected = "if(FLAG_iscc_crossover>0,(-1)/mu0_const*tau_isccxover_C*d(B_avgn_HT,t)*k_surf_PE*dampFactorCC_PE,0)";
        String actual = VariablesPhysicsEngine.defineMISCCxOver();
        assertEquals(expected, actual);
    }

    @Test
    public void defineMISCCadjW(){
        String expected = "if(FLAG_iscc_adjw>0,(-1)/mu0_const*tau_isccadjw_C*d(B_avgn_HT,t)*k_surf_PE*dampFactorCC_PE,0)";
        String actual = VariablesPhysicsEngine.defineMISCCadjW();
        assertEquals(expected, actual);
    }

    @Test
    public void defineMISCCx(){
        String expected = "M_isccxover_PE*(-sin(alpha_HT))+M_isccadjw_PE*(-sin(alpha_HT))+M_isccadjn_PE*(+cos(alpha_HT))";
        String actual = VariablesPhysicsEngine.defineMISCCx();
        assertEquals(expected, actual);
    }

    @Test
    public void defineMISCCy(){
        String expected = "M_isccxover_PE*(+cos(alpha_HT))+M_isccadjw_PE*(+cos(alpha_HT))+M_isccadjn_PE*(+sin(alpha_HT))";
        String actual = VariablesPhysicsEngine.defineMISCCy();
        assertEquals(expected, actual);
    }

    @Test
    public void defineMPersx(){
        String expected = "if(FLAG_M_pers>0,(d_fil_C/2)*(4/3*pi)*Ic_C/surf_HT*(1-(Is_HT/Ic_C)^2)*(-mf.Bx/(mf.normB+eps)),0)";
        String actual = VariablesPhysicsEngine.defineMPersx();
        assertEquals(expected, actual);
    }

    @Test
    public void defineMPersy(){
        String expected = "if(FLAG_M_pers>0,(d_fil_C/2)*(4/3*pi)*Ic_C/surf_HT*(1-(Is_HT/Ic_C)^2)*(-mf.By/(mf.normB+eps)),0)";
        String actual = VariablesPhysicsEngine.defineMPersy();
        assertEquals(expected, actual);
    }

    @Test
    public void defineMEqx(){
        String expected = "M_isccx_PE+M_ifccx_PE+M_persx_PE";
        String actual = VariablesPhysicsEngine.defineMEqx();
        assertEquals(expected, actual);
    }

    @Test
    public void defineMEqy(){
        String expected = "M_isccy_PE+M_ifccy_PE+M_persy_PE";
        String actual = VariablesPhysicsEngine.defineMEqy();
        assertEquals(expected, actual);
    }

    @Test
    public void definequenchState(){
        String expected = "if(FLAG_quench_off>0,0,if(FLAG_quench_all>0,if(t>=PARAM_time_quench,1,0),CFUN_quenchState(Is_HT[1/A],Ic_HT[1/A])))";
        String actual = VariablesPhysicsEngine.definequenchState();
        assertEquals(expected, actual);
    }

    @Test
    public void defineQIFCC(){
        String expected = "-(M_ifccx_PE*d(mf.Bx,t)+M_ifccy_PE*d(mf.By,t))";
        String actual = VariablesPhysicsEngine.defineQIFCC();
        assertEquals(expected, actual);
    }

    @Test
    public void defineQISCC(){
        String expected = "-(M_isccxover_PE*d(B_avgn_HT,t)+M_isccadjw_PE*d(B_avgn_HT,t)+M_isccadjn_PE*d(B_avgw_HT,t))";
        String actual = VariablesPhysicsEngine.defineQISCC();
        assertEquals(expected, actual);
    }

    @Test
    public void defineQOhm(){
        String expected = "rho_HT*Js_PE^2";
        String actual = VariablesPhysicsEngine.defineQOhm();
        assertEquals(expected, actual);
    }

    @Test
    public void defineQtot(){
        String expected = "Q_ohm_PE+Q_ifcc_PE+Q_iscc_PE";
        String actual = VariablesPhysicsEngine.defineQtot();
        assertEquals(expected, actual);
    }

    @Test
    public void defineR(){
        String expected = "rho_HT/surf_HT*ratio_HT^2";
        String actual = VariablesPhysicsEngine.defineR();
        assertEquals(expected, actual);
    }

    @Test
    public void definePhi(){
        String expected = "(mf.Az)*tau_PE*surf_HT";
        String actual = VariablesPhysicsEngine.definePhi();
        assertEquals(expected, actual);
    }

    @Test
    public void defineVdelta(){
        String expected = "R_PE*Is_HT+d(phi_PE,t)";
        String actual = VariablesPhysicsEngine.defineVdelta();
        assertEquals(expected, actual);
    }

}