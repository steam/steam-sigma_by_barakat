package comsol.formulation;

import comsol.definitions.VariablesHalfturn;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by STEAM on 06/07/2016.
 */
public class HalfturnTest {

    @Test
    public void defineIndex(){
        double val = 1.0;
        String actual = VariablesHalfturn.defineIndex(Double.toString(val));
        String expected = "1.0[1]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineRatio(){
        double val = 1.0;
        String actual = VariablesHalfturn.defineRatio(val);
        String expected = "1.0[1]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineAlpha(){
        double val = 1.0;
        String actual = VariablesHalfturn.defineAlpha(val);
        String expected = "1.0[rad]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineTauDensityVersor(){
        double val = 1.0;
        String actual = VariablesHalfturn.defineTauDensityVersor(val);
        String expected = "1.0[1]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineSurface(){
        double val = 1.0;
        String actual = VariablesHalfturn.defineSurface(val);
        String expected = "1.0[m^2]";
        assertEquals(expected, actual);
    }

    @Test
    public void defineCurrent(){
        String label = "labelTest";
        String actual = VariablesHalfturn.defineCurrent(label);
        String expected = "Iw_labelTest";
        assertEquals(expected, actual);
    }

    @Test
    public void defineBAvgWAxis(){
        String label = "labelTest";
        String index = "1";
        String actual = VariablesHalfturn.defineBAvgWAxis(label);
        String expected = "labelTest_avgLineN(mf.Bx*cos(alpha_HT)+mf.By*sin(alpha_HT))";
        assertEquals(expected, actual);
    }

    @Test
    public void defineBAvgNAxis(){
        String label = "labelTest";
        String index = "1";
        String actual = VariablesHalfturn.defineBAvgNAxis(label);
        String expected = "labelTest_avgLineW(mf.Bx*sin(-alpha_HT)+mf.By*cos(alpha_HT))";
        assertEquals(expected, actual);
    }

    @Test
    public void defineRhoAvg(){
        String label = "labelTest";
        String index = "1";
        String actual = VariablesHalfturn.defineRhoAvg(label);
        String expected = "quenchState_PE*(1/(k_surf_PE*frac_cu_C))*labelTest_avgSurf(rho_C)*1/(cos(theta_lTpStrand_C)^2)";
        assertEquals(expected, actual);
    }

    @Test
    public void defineCpAvg(){
        String label = "labelTest";
        String index = "1";
        String actual = VariablesHalfturn.defineCpAvg(label);
        String expected = "labelTest_avgSurf((Cv_conductor_C+Cv_voids_C+Cv_core_C+Cv_ins_C)/surf_HT)";
        assertEquals(expected, actual);
    }

    @Test
    public void defineIcMin(){
        String label = "labelTest";
        String index = "1";
        String actual = VariablesHalfturn.defineIcMin(label);
        String expected = "labelTest_minSurf(Ic_C)";
        assertEquals(expected, actual);
    }

    @Test
    public void defineVgnd(){
        String index = "1";
        String actual = VariablesHalfturn.defineVgnd(index);
        String expected = "V_gnd_halfturn1";
        assertEquals(expected, actual);
    }

}