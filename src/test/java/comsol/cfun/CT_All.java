package comsol.cfun;

import comsol.MagnetMPHBuilder;
import comsol.utils.models.Input_Air;
import config.ConfigSigma;
import org.junit.*;
import server.MphSigmaServer;
import server.MphSigmaServerAPI;
import server.SigmaServer;

import java.io.IOException;

/**
 * Created by STEAM on 22/01/2018.
 */

//todo: CSV referece values sometimes are truncated to the second decimal (thanks EXCEL), enforcing the relative precision to 1%.
//todo: update all the reference values with full precision digit

public class CT_All {
    private static final String JSON_PATH = "cfun/STEAMconfigTEST.json";
    private static final String TEST_ROOT_DIR = "cfun";
    private static ConfigSigma cfg;
    private static SigmaServer srv;

    // *****************************************************************************************************************
    // Before-After
    // *****************************************************************************************************************

    @BeforeClass
    public static void executeBeforeClass() throws IOException {
        cfg = ConfigSigma.parseFileJSON(CT_All.class.getClassLoader().getResource(JSON_PATH).getFile());

        // Construction of the test model
        Input_Air input = new Input_Air();
        srv = new MphSigmaServer(cfg.getOutputModelPath());
        MagnetMPHBuilder builder = new MagnetMPHBuilder(cfg, srv);
        builder.connectToServer();
        builder.prepareModelTemplate();
        builder.buildMPH(input.getDomains());
    }

    @Before
    public void executeBeforeTest() {
    }

    @After
    public void executeAfterTest() {
    }

    @AfterClass
    public static void executeAfterClass() throws IOException {
        MphSigmaServerAPI.disconnect();
    }

    // *****************************************************************************************************************
    // Tests
    // *****************************************************************************************************************

    @Test
    public void runTest_CFUN_CvCu() throws IOException {
        String testLabel = "CFUN_CvCu";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel + "   ************************************************");

        CT cft = new CT(TEST_ROOT_DIR, testLabel, toleranceREL);
        cft.runStudy(srv);
    }

    @Test
    public void runTest_CFUN_CvG10() throws IOException {
        String testLabel = "CFUN_CvG10";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel + "   ************************************************");

        CT cft = new CT(TEST_ROOT_DIR, testLabel, toleranceREL);
        cft.runStudy(srv);
    }

    @Test
    public void runTest_CFUN_CvHe() throws IOException {
        String testLabel = "CFUN_CvHe";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel + "   ************************************************");

        CT cft = new CT(TEST_ROOT_DIR, testLabel, toleranceREL);
        cft.runStudy(srv);
    }

    @Test
    public void runTest_CFUN_CvKapton() throws IOException {
        String testLabel = "CFUN_CvKapton";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel + "   ************************************************");

        CT cft = new CT(TEST_ROOT_DIR, testLabel, toleranceREL);
        cft.runStudy(srv);
    }

    @Test
    public void runTest_CFUN_CvNb3SnNIST() throws IOException {
        String testLabel = "CFUN_CvNb3SnNIST";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel + "   ************************************************");

        CT cft = new CT(TEST_ROOT_DIR, testLabel, toleranceREL);
        cft.runStudy(srv);
    }

    @Test
    public void runTest_CFUN_CvNbTi() throws IOException {
        String testLabel = "CFUN_CvNbTi";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel + "   ************************************************");

        CT cft = new CT(TEST_ROOT_DIR, testLabel, toleranceREL);
        cft.runStudy(srv);
    }

    @Test
    public void runTest_CFUN_CvSteel() throws IOException {
        String testLabel = "CFUN_CvSteel";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel + "   ************************************************");

        CT cft = new CT(TEST_ROOT_DIR, testLabel, toleranceREL);
        cft.runStudy(srv);
    }

    @Test
    public void runTest_CFUN_IcNb3SnHiLumi() throws IOException {
        String testLabel = "CFUN_IcNb3SnHiLumi";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel + "   ************************************************");

        CT cft = new CT(TEST_ROOT_DIR, testLabel, toleranceREL);
        cft.runStudy(srv);
    }

    @Test
    public void runTest_CFUN_IcNbTiGSI() throws IOException {
        String testLabel = "CFUN_IcNbTiGSI";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel + "   ************************************************");

        CT cft = new CT(TEST_ROOT_DIR, testLabel, toleranceREL);
        cft.runStudy(srv);
    }

    @Test
    public void runTest_CFUN_IcNb3SnFCC() throws IOException {
        String testLabel = "CFUN_IcNb3SnFCC";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel + "   ************************************************");

        CT cft = new CT(TEST_ROOT_DIR, testLabel, toleranceREL);
        cft.runStudy(srv);
    }

    @Test
    public void runTest_CFUN_kCuNIST() throws IOException {
        String testLabel = "CFUN_kCuNIST";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel + "   ************************************************");

        CT cft = new CT(TEST_ROOT_DIR, testLabel, toleranceREL);
        cft.runStudy(srv);
    }

    @Test
    public void runTest_CFUN_kG10() throws IOException {
        String testLabel = "CFUN_kG10";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel + "   ************************************************");

        CT cft = new CT(TEST_ROOT_DIR, testLabel, toleranceREL);
        cft.runStudy(srv);
    }

    @Test
    public void runTest_CFUN_kHe() throws IOException {
        String testLabel = "CFUN_kHe";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel + "   ************************************************");

        CT cft = new CT(TEST_ROOT_DIR, testLabel, toleranceREL);
        cft.runStudy(srv);
    }

    @Test
    public void runTest_CFUN_kKapton() throws IOException {
        String testLabel = "CFUN_kKapton";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel + "   ************************************************");

        CT cft = new CT(TEST_ROOT_DIR, testLabel, toleranceREL);
        cft.runStudy(srv);
    }

    @Test
    public void runTest_CFUN_kSteel() throws IOException {
        String testLabel = "CFUN_kSteel";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel + "   ************************************************");

        CT cft = new CT(TEST_ROOT_DIR, testLabel, toleranceREL);
        cft.runStudy(srv);
    }

    @Test
    public void runTest_CFUN_quenchState() throws IOException {
        String testLabel = "CFUN_quenchState";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel + "   ************************************************");

        CT cft = new CT(TEST_ROOT_DIR, testLabel, toleranceREL);
        cft.runStudy(srv);
    }

    @Test
    public void runTest_CFUN_rhoCuCUDI() throws IOException {
        String testLabel = "CFUN_rhoCuCUDI";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel + "   ************************************************");

        CT cft = new CT(TEST_ROOT_DIR, testLabel, toleranceREL);
        cft.runStudy(srv);
    }

    @Test
    public void runTest_CFUN_rhoCuNIST() throws IOException {
        String testLabel = "CFUN_rhoCuNIST";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel + "   ************************************************");

        CT cft = new CT(TEST_ROOT_DIR, testLabel, toleranceREL);
        cft.runStudy(srv);
    }

    @Test
    public void runTest_CFUN_TcsNbTi() throws IOException {
        String testLabel = "CFUN_TcsNbTi";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel + "   ************************************************");

        CT cft = new CT(TEST_ROOT_DIR, testLabel, toleranceREL);
        cft.runStudy(srv);
    }

    // *****************************************************************************************************************
    // Specific Methods
    // *****************************************************************************************************************

}
