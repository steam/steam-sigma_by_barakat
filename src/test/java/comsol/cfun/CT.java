package comsol.cfun;

import comsol.utils.Util;
import server.SigmaServer;
import utils.TextFile;

import java.io.File;
import java.io.IOException;
import java.util.List;


/**
 * Created by STEAM on 09/02/2018.
 */
public class CT {

    private static String LABEL_RESULTS_FOLDER = "results";
    private static String LABEL_REFERENCE_FOLDER = "reference";
    private static String EXTENSION_JSON = ".json";
    private static String EXTENSION_CSV = ".csv";

    private static String configPath;
    private static String inputDir;
    private static String outputDir;
    private static configCT cfgCt;
    private static double TOL_REL;
    private static int STUDY_COUNTER = 0;

    public CT(String rootDir, String testLabel, double tolerance){
        configPath = String.format("%s/%s/%s%s", rootDir, testLabel, testLabel, EXTENSION_JSON);
        inputDir = String.format("%s/%s/%s", rootDir, testLabel, LABEL_REFERENCE_FOLDER);
        outputDir = String.format("%s/%s/%s", rootDir, testLabel, LABEL_RESULTS_FOLDER);
        TOL_REL = tolerance;

        try {
            cfgCt = configCT.parseFileJSON(configPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void runStudy(SigmaServer srv) throws IOException {

        System.out.println("TEST: preparation");
        // Set args
        String funName = cfgCt.getTestLabel();
        String resultsPath = String.format("%s/%s%s", outputDir, funName, EXTENSION_CSV);
        String referencePath = String.format("%s/%s.csv", inputDir, funName);

        for(int arg_i = 0; arg_i < cfgCt.getFunctionArgsName().length; arg_i++){
            double min = Double.parseDouble(cfgCt.getFunctionArgsMin()[arg_i]);
            double max = Double.parseDouble(cfgCt.getFunctionArgsMax()[arg_i]);

            srv.setIndexFunction(funName, "plotargs", min, arg_i, 0);

            srv.setIndexFunction(funName,"plotargs", max, arg_i, 1);
        }

        STUDY_COUNTER++;
        String pg = String.format("pg%d", STUDY_COUNTER);
        String plot = String.format("plot%d", STUDY_COUNTER);

        // create plot
        srv.createPlotFunc(funName, pg);

        srv.runResult(pg);

        // export data
        System.out.println("TEST: results export");
        srv.createExportResult(plot, pg, "plot1", "Plot");

        srv.setParamExportResult(plot, "header", false);

        File resultsFile = new File("out\\test\\resources\\" + resultsPath);
        srv.setParamExportResult(plot, "filename", resultsFile.getAbsolutePath());

        srv.runExportResult(plot);

        // parse results

        List<double[]> varResList = TextFile.readAsListOfDoubleArray(CT.class.getClassLoader().getResource(resultsPath).getFile(), ",");
        // parse referecedata
        List<double[]> varRefList = TextFile.readAsListOfDoubleArray(CT.class.getClassLoader().getResource(referencePath).getFile(), ",");

        System.out.println("TEST: analysis");
        // compare
        Util.compareSignal(varRefList, varResList, TOL_REL);
    }
}
