package comsol.cfun;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by STEAM on 09/02/2018.
 */
public class configCT {

    private String testLabel;

    // parameters
    private String[] functionArgsName;
    private String[] functionArgsMin;
    private String[] functionArgsMax;


    // *****************************************************************************************************************
    // Constructor
    // *****************************************************************************************************************

    public static configCT parseFileJSON(String arg) throws IOException {
        configCT cfg = new configCT();
        BufferedReader br = null;
        try {
            Gson gson = new Gson();
            br =  new BufferedReader(new FileReader(configCT.class.getClassLoader().getResource(arg).getFile()));
            cfg = gson.fromJson(br, configCT.class);
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                br.close();
            }
        }
        return cfg;
    }

    // *****************************************************************************************************************
    // Getters
    // *****************************************************************************************************************

    public String getTestLabel() {
        return testLabel;
    }

    public String[] getFunctionArgsName() {
        return functionArgsName;
    }

    public String[] getFunctionArgsMin() {
        return functionArgsMin;
    }

    public String[] getFunctionArgsMax() {
        return functionArgsMax;
    }
}
