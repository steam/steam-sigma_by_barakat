package comsol.utils.models.cable.T0.withThetaTp;

import model.geometry.coil.Cable;
import model.materials.database.MatDatabase;

/**
 * Created by STEAM on 05/09/2016. TALES #492 and roxie_vs10.cadata as references
 */
public class Cable_T0_Insulated_thetaTp extends Cable {

    public Cable_T0_Insulated_thetaTp() {
        this.setCableParameters();
    }

    public void setCableParameters() {

        this.label = "cable_T0_thetaTp";

        //Insulation
        this.wInsulNarrow = 100e-6; // [m];
        this.wInsulWide = 100e-6; // [m];
        //Filament
        this.dFilament = 50.0e-6; // [m];
        //Strand
        this.dstrand = 0.75e-3; // [m];
        this.fracCu = 0.6;
        this.fracSc = 0.4;
        this.RRR = 100;
        this.TupRRR = 295; // [K];
        this.Top = 1.9; // [K];
        //Transient
        this.Rc = 16.3e-6; // [ohm];
        this.Ra = 10e-6; // [ohm];
        this.fRhoEff = 1; // [1]; from TALES
        this.lTp = 10e-3; // [m];
        //Cable insulated
        this.wBare = 15e-3; // [m]; Roxie/Tales sheet 3 width (not sheet 4)
        this.hInBare = 1.5e-3; // [m];
        this.hOutBare = 1.5e-3; // [m];
        this.noOfStrands = 40;
        this.noOfStrandsPerLayer = 20;
        this.noOfLayers = 2;
        this.lTpStrand = 0.1; // [m];
        this.wCore = 0; // [m];
        this.hCore = 0; // [m];
        this.thetaTpStrand = Math.PI/5; // 36 [deg]
        this.C1 = 0; // [A];
        this.C2 = 0; // [A/T];
        this.fracHe = 0.0; //[%]
        this.fracFillInnerVoids = 1; //[-]
        this.fractFillOuterVoids = 1; //[-]

        this.criticalSurfaceFit = CriticalSurfaceFitEnum.Ic_TEST;
        this.insulationMaterial = MatDatabase.MAT_INSULATION_TEST;
        this.materialInnerVoids = MatDatabase.MAT_INSULATION_TEST;
        this.materialOuterVoids = MatDatabase.MAT_INSULATION_TEST;
        this.materialCore = MatDatabase.MAT_VOID;
        this.resitivityCopperFit = ResitivityCopperFitEnum.rho_Cu_TEST;
    }


}