package comsol.utils.models.cable.T1.withoutThetaTp;

import model.geometry.coil.Cable;
import model.materials.database.MatDatabase;

/**
 * Created by STEAM on 05/09/2016. TALES #492 and roxie_vs10.cadata as references
 */
public class Cable_T1_CopperResistivity extends Cable {

    public Cable_T1_CopperResistivity() {
        this.setCableParameters();
    }

    public void setCableParameters() {

        this.label = "cable_T0_CopperResistivity";

        //Insulation
        this.wInsulNarrow = 100e-6; // [m];
        this.wInsulWide = 100e-6; // [m];
        //Filament
        this.dFilament = 50.0e-6; // [m];
        //Strand
        this.dstrand = 0.75e-3; // [m];
        this.fracCu = 0.6;
        this.fracSc = 0.4;
        this.RRR = 200;
        this.TupRRR = 295; // [K];
        this.Top = 1.9; // [K];
        //Transient
        this.Rc = 16.3e-6; // [ohm];
        this.Ra = 10e-6; // [ohm];
        this.fRhoEff = 1; // [1]; from TALES
        this.lTp = 14e-3; // [m];
        //Cable insulated
        this.wBare = 15e-3; // [m]; Roxie/Tales sheet 3 width (not sheet 4)
        this.hInBare = 1.5e-3; // [m];
        this.hOutBare = 1.5e-3; // [m];
        this.noOfStrands = 40;
        this.noOfStrandsPerLayer = 20;
        this.noOfLayers = 2;
        this.lTpStrand = 0.1; // [m];
        this.wCore = 0; // [m];
        this.hCore = 0; // [m];
        this.thetaTpStrand = 0;
        this.C1 = 0; // [A];
        this.C2 = 0; // [A/T];
        this.fracHe = 0.0; //[%]
        this.fracFillInnerVoids = 1; //[-]
        this.fractFillOuterVoids = 1; //[-]

        this.criticalSurfaceFit = CriticalSurfaceFitEnum.Ic_TEST;
        this.insulationMaterial = MatDatabase.MAT_VOID;
        this.materialInnerVoids = MatDatabase.MAT_VOID;
        this.materialOuterVoids = MatDatabase.MAT_VOID;
        this.materialCore = MatDatabase.MAT_VOID;
        this.resitivityCopperFit = ResitivityCopperFitEnum.rho_Cu_NIST;
    }
}