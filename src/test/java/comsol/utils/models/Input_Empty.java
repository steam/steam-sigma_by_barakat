package comsol.utils.models;


import model.domains.Domain;
import input.UtilsUserInput;

public class Input_Empty extends UtilsUserInput {

    private final Domain[] domains;

    public Domain[] getDomains() {
        return domains.clone();
    }

    public Input_Empty() {
        domains = null;
    }
}
