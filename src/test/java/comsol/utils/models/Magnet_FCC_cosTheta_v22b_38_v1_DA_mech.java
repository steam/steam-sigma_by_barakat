package comsol.utils.models;

import model.geometry.Element;
import model.geometry.basic.*;

public class Magnet_FCC_cosTheta_v22b_38_v1_DA_mech {
    public static Element[] iron_yoke() {
//VARIABLES
        double ang = Math.PI / 180;
        double oradius = 300e-3;
        double padthick = 2e-3;
        double holerad = 52.5e-3;
        double apcenter = 102e-3;
        double holecent = 235e-3;
        double padx = 203.315e-3;
        double pady = 115e-3;

        //NODES
        Point kp1 = Point.ofCartesian(padx + padthick, 0);
        Point kp2 = Point.ofCartesian(oradius, 0);
        Point kp3 = Point.ofPolar(oradius, Math.atan((pady + padthick / 2) / (padx + padthick)));
        Point kp4 = Point.ofCartesian(padx + padthick, pady + padthick / 2);
        Point kp5 = Point.ofCartesian(0, pady + padthick);
        Point kp6 = Point.ofCartesian(padx + padthick / 2, pady + padthick);
        Point kp7 = Point.ofPolar(oradius, Math.atan((pady + padthick) / (padx + padthick / 2)));
        Point kp8 = Point.ofCartesian(0, oradius);
        Point kp9 = Point.ofCartesian(0, holecent + holerad);
        Point kp10 = Point.ofCartesian(0, holecent - holerad);
        Point kp11 = Point.ofCartesian(holerad, holecent);

        //LINES
        Line ln1 = Line.ofEndPoints(kp1, kp2);
        Arc ln2 = Arc.ofEndPointsRadius(kp3, kp2, oradius);
        Line ln3 = Line.ofEndPoints(kp4, kp3);
        Line ln4 = Line.ofEndPoints(kp1, kp4);
        Line ln5 = Line.ofEndPoints(kp5, kp6);
        Line ln6 = Line.ofEndPoints(kp6, kp7);
        Arc ln7 = Arc.ofEndPointsRadius(kp8, kp7, oradius);
        Line ln8 = Line.ofEndPoints(kp8, kp9);
        Arc ln9 = Arc.ofEndPointsRadius(kp9, kp11, holerad);
        Arc ln10 = Arc.ofEndPointsRadius(kp11, kp10, holerad);
        Line ln11 = Line.ofEndPoints(kp10, kp5);
        Circumference lnhx2 = Circumference.ofDiameterEndPoints(kp3,kp2);

        //AREAS
        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3, ln4});
        Area ar2 = Area.ofHyperLines(new HyperLine[]{ln5, ln6, ln7, ln8, ln9, ln10, ln11});

        //HOLES
        Circumference lnhx = Circumference.ofDiameterEndPoints(kp1,kp2);
        Area arhxhole = Area.ofHyperLines(new HyperLine[]{lnhx});
        Area arhxhole2 = Area.ofHyperLines(new HyperLine[]{lnhx2});


        //MESH
//        Lmesh(ln1,7);
//        Lmesh(ln7,15);

        // ELEMENTS
        Element el1 = new Element("ar2", ar2);
        Element el2 = new Element("ar1", ar1);
        Element el3 = new Element("arhxhole", arhxhole);
        Element el4 = new Element("arhxhole2", arhxhole2);

        Element[] elementList = new Element[4];
        elementList[0]=el1;
        elementList[1]=el2;
        elementList[2]=el3;
        elementList[3]=el4;

        return  elementList;
    }

}
