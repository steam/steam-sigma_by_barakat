package comsol.utils.models;


import input.UtilsUserInput;
import model.domains.Domain;
import model.domains.database.AirDomain;
import model.domains.database.AirFarFieldDomain;
import model.domains.database.CoilDomain;
import model.domains.database.IronDomain;
import model.geometry.Element;
import model.geometry.basic.*;
import model.geometry.coil.Cable;
import model.geometry.coil.Coil;
import model.geometry.coil.Pole;
import model.geometry.coil.Winding;
import model.materials.database.MatDatabase;

public class Input_T2_Iron_Test extends UtilsUserInput {


    private final Domain[] domains;

    public Domain[] getDomains() {
        return domains.clone();
    }

    public Input_T2_Iron_Test(Cable cableInput) {

        domains = new Domain[] {
                new AirDomain("airDomain", MatDatabase.MAT_AIR, air()),
                new AirFarFieldDomain("airFarFieldDomain", MatDatabase.MAT_AIR, airFarField()),
                new CoilDomain("C0", MatDatabase.MAT_COIL_TEST, coil(cableInput)),
                new IronDomain("ironYoke", MatDatabase.MAT_IRON1, ironYoke()),
       };

    }

    private Coil coil(Cable cableInput) {

        double x0 = 0.015;
        double y0 = 0.001;
        double w = 0.015;
        double h = 15e-4;
        double deltah = 5e-4;

        double x1 = 0.007;
        double y1 = 0.017;

        double n;
        double delta;


        Point kp11 = Point.ofCartesian(x0, y0+w);
        Point kp12 = Point.ofCartesian(x0, y0);
        Point kp13 = Point.ofCartesian(x0+h, y0);
        Point kp14 = Point.ofCartesian(x0+h, y0+w);

        Line ln11 = Line.ofEndPoints(kp11, kp14);
        Line ln12 = Line.ofEndPoints(kp12, kp11);
        Line ln13 = Line.ofEndPoints(kp12, kp13);
        Line ln14 = Line.ofEndPoints(kp13, kp14);

        Area ha11p = Area.ofHyperLines(new HyperLine[]{ln11, ln12, ln13, ln14});
        Area ha12p = ha11p.translate(h+deltah,0);
        Area ha13p = ha12p.translate(h+deltah,0);
        Area ha14p = ha13p.translate(h+deltah,0);
        Area ha15p = ha14p.translate(h+deltah,0);
        Area ha16p = ha15p.translate(h+deltah,0);
        Area ha17p = ha16p.translate(h+deltah,0);
        Area ha18p = ha17p.translate(h+deltah,0);
        Area ha19p = ha18p.translate(h+deltah,0);
        Area ha110p = ha19p.translate(h+deltah,0);
        Area ha111p = ha110p.translate(h+deltah,0);
        Area ha112p = ha111p.translate(h+deltah,0);
        Area ha113p = ha112p.translate(h+deltah,0);
        Area ha114p = ha113p.translate(h+deltah,0);

        Area ha11n = ha11p.mirrorY();
        Area ha12n = ha12p.mirrorY();
        Area ha13n = ha13p.mirrorY();
        Area ha14n = ha14p.mirrorY();
        Area ha15n = ha15p.mirrorY();
        Area ha16n = ha16p.mirrorY();
        Area ha17n = ha17p.mirrorY();
        Area ha18n = ha18p.mirrorY();
        Area ha19n = ha19p.mirrorY();
        Area ha110n = ha110p.mirrorY();
        Area ha111n = ha111p.mirrorY();
        Area ha112n = ha112p.mirrorY();
        Area ha113n = ha113p.mirrorY();
        Area ha114n = ha114p.mirrorY();

        //Second stack
        Area ha21p = ha11p.translate(x1-x0, y1-y0);
        Area ha22p = ha21p.translate(h+deltah,0);
        Area ha23p = ha22p.translate(h+deltah,0);
        Area ha24p = ha23p.translate(h+deltah,0);
        Area ha25p = ha24p.translate(h+deltah,0);
        Area ha26p = ha25p.translate(h+deltah,0);
        Area ha27p = ha26p.translate(h+deltah,0);
        Area ha28p = ha27p.translate(h+deltah,0);
        Area ha29p = ha28p.translate(h+deltah,0);
        Area ha210p = ha29p.translate(h+deltah,0);
        Area ha211p = ha210p.translate(h+deltah,0);
        Area ha212p = ha211p.translate(h+deltah,0);
        Area ha213p = ha212p.translate(h+deltah,0);
        Area ha214p = ha213p.translate(h+deltah,0);
        Area ha215p = ha214p.translate(h+deltah,0);
        Area ha216p = ha215p.translate(h+deltah,0);
        Area ha217p = ha216p.translate(h+deltah,0);
        Area ha218p = ha217p.translate(h+deltah,0);

        Area ha21n = ha21p.mirrorY();
        Area ha22n = ha22p.mirrorY();
        Area ha23n = ha23p.mirrorY();
        Area ha24n = ha24p.mirrorY();
        Area ha25n = ha25p.mirrorY();
        Area ha26n = ha26p.mirrorY();
        Area ha27n = ha27p.mirrorY();
        Area ha28n = ha28p.mirrorY();
        Area ha29n = ha29p.mirrorY();
        Area ha210n = ha210p.mirrorY();
        Area ha211n = ha211p.mirrorY();
        Area ha212n = ha212p.mirrorY();
        Area ha213n = ha213p.mirrorY();
        Area ha214n = ha214p.mirrorY();
        Area ha215n = ha215p.mirrorY();
        Area ha216n = ha216p.mirrorY();
        Area ha217n = ha217p.mirrorY();
        Area ha218n = ha218p.mirrorY();

        // windings:
        Winding w0 = Winding.ofAreas(new Area[]{ha11p,ha11n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w1 = Winding.ofAreas(new Area[]{ha12p,ha12n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w2 = Winding.ofAreas(new Area[]{ha13p,ha13n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w3 = Winding.ofAreas(new Area[]{ha14p,ha14n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w4 = Winding.ofAreas(new Area[]{ha15p,ha15n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w5 = Winding.ofAreas(new Area[]{ha16p,ha16n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w6 = Winding.ofAreas(new Area[]{ha17p,ha17n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w7 = Winding.ofAreas(new Area[]{ha18p,ha18n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w8 = Winding.ofAreas(new Area[]{ha19p,ha19n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w9 = Winding.ofAreas(new Area[]{ha110p,ha110n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w10 = Winding.ofAreas(new Area[]{ha111p,ha111n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w11 = Winding.ofAreas(new Area[]{ha112p,ha112n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w12 = Winding.ofAreas(new Area[]{ha113p,ha113n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w13 = Winding.ofAreas(new Area[]{ha114p,ha114n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w14 = Winding.ofAreas(new Area[]{ha21p,ha21n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w15 = Winding.ofAreas(new Area[]{ha22p,ha22n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w16 = Winding.ofAreas(new Area[]{ha23p,ha23n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w17 = Winding.ofAreas(new Area[]{ha24p,ha24n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w18 = Winding.ofAreas(new Area[]{ha25p,ha25n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w19 = Winding.ofAreas(new Area[]{ha26p,ha26n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w20 = Winding.ofAreas(new Area[]{ha27p,ha27n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w21 = Winding.ofAreas(new Area[]{ha28p,ha28n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w22 = Winding.ofAreas(new Area[]{ha29p,ha29n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w23 = Winding.ofAreas(new Area[]{ha210p,ha210n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w24 = Winding.ofAreas(new Area[]{ha211p,ha211n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w25 = Winding.ofAreas(new Area[]{ha212p,ha212n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w26 = Winding.ofAreas(new Area[]{ha213p,ha213n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w27 = Winding.ofAreas(new Area[]{ha214p,ha214n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w28 = Winding.ofAreas(new Area[]{ha215p,ha215n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w29 = Winding.ofAreas(new Area[]{ha216p,ha216n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w30 = Winding.ofAreas(new Area[]{ha217p,ha217n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w31 = Winding.ofAreas(new Area[]{ha218p,ha218n}, new int[]{+1,-1}, 1, 1, cableInput);



        // poles:
        Pole p1 = Pole.ofWindings(new Winding[]{w0,w1,w2,w3,w4,w5,w6,w7,w8,w9,w10,w11,w12,w13,w14,w15, w16, w17, w18, w19, w20, w21, w22, w23, w24, w25, w26, w27, w28, w29, w30, w31});
        Pole p2 = p1.mirrorX().reverseWindingDirection();

        // Coil:
        Coil c1 = Coil.ofPoles(new Pole[]{p1,p2});

        return c1;
    }

    private Element[] air() {
        // POINTS

        double r = 0.5;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r, 0);
        Point kp2 = Point.ofCartesian(0, r);

        // LINES
        Line ln1 = Line.ofEndPoints(kpc, kp1);
        Arc ln2 = Arc.ofEndPointsCenter(kp1, kp2, kpc);
        Line ln3 = Line.ofEndPoints(kp2, kpc);

        // AREAS
        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3});
        Area ar2 = ar1.mirrorY();
        Area ar3 = ar2.mirrorX();
        Area ar4 = ar1.mirrorX();

        // ELEMENTS
        Element el1 = new Element("AIR_El1", ar1);
        Element el2 = new Element("AIR_El2", ar2);
        Element el3 = new Element("AIR_El3", ar3);
        Element el4 = new Element("AIR_El4", ar4);

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
//        Element[] quad1 = {el1};
//        Element[] quad2 = {el2};
//        Element[] quad3 = {el3};
//        Element[] quad4 = {el4};
        Element[] elementsToBuild = new Element[]{el1,el2,el3,el4};
        return elementsToBuild;
    }

    private Element[] airFarField() {
        // POINTS

        double r = 0.5;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r, 0);
        Point kp2 = Point.ofCartesian(0, r);

        Point kp1_out = Point.ofCartesian(r * 1.05, 0);
        Point kp4_out = Point.ofCartesian(0, r * 1.05);

        // LINES
        Line ln1 = Line.ofEndPoints(kp1, kp1_out);
        Arc ln2 = Arc.ofEndPointsCenter(kp1_out, kp4_out, kpc);
        Line ln3 = Line.ofEndPoints(kp4_out, kp2);
        Arc ln4 = Arc.ofEndPointsCenter(kp2, kp1, kpc);

        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3, ln4});
        Area ar2 = ar1.mirrorY();
        Area ar3 = ar2.mirrorX();
        Area ar4 = ar1.mirrorX();

        Element el1 = new Element("FAR_El1", ar1);
        Element el2 = new Element("FAR_El2", ar2);
        Element el3 = new Element("FAR_El3", ar3);
        Element el4 = new Element("FAR_El4", ar4);

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
//        Element[] quad1 = {el1};
//        Element[] quad2 = {el2};
//        Element[] quad3 = {el3};
//        Element[] quad4 = {el4};
        Element[] elementsToBuild = new Element[]{el1,el2,el3,el4};
        return elementsToBuild;
    }

    private Element[] ironYoke() {
        double deg2rad = Math.PI / 180;
        double r_int = 0.06;
        double r_ext = 0.1;


        Point kp1 = Point.ofPolar(r_int, 0 * deg2rad);
        Point kp2 = Point.ofPolar(r_ext, 0 * deg2rad);
        Point kp3 = Point.ofPolar(r_ext, 90 * deg2rad);
        Point kp4 = Point.ofPolar(r_int, 90 * deg2rad);

        Point kp0 = Point.ofCartesian(0, 0);

        Arc ln1 = Arc.ofEndPointsCenter(kp1, kp4, kp0);
        Line ln2 = Line.ofEndPoints(kp1, kp2);
        Arc ln3 = Arc.ofEndPointsCenter(kp2, kp3, kp0);
        Line ln4 = Line.ofEndPoints(kp4, kp3);

        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3, ln4});
        Area ar2 = ar1.mirrorY();
        Area ar3 = ar2.mirrorX();
        Area ar4 = ar1.mirrorX();

        Element el1 = new Element("IY_El1", ar1);
        Element el2 = new Element("IY_El2", ar2);
        Element el3 = new Element("IY_El3", ar3);
        Element el4 = new Element("IY_El4", ar4);

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
//        Element[] quad1 = {el1};
//        Element[] quad2 = {el2};
//        Element[] quad3 = {el3};
//        Element[] quad4 = {el4};
        Element[] elementsToBuild = new Element[]{el1,el2,el3,el4};
        return elementsToBuild;
    }

}
