package comsol.utils.models;


import model.domains.Domain;
import model.domains.database.AirDomain;
import model.domains.database.AirFarFieldDomain;
import model.domains.database.CoilDomain;
import model.domains.database.IronDomain;
import model.geometry.Element;
import model.geometry.basic.*;
import model.geometry.coil.Cable;
import model.geometry.coil.Coil;
import model.geometry.coil.Pole;
import model.geometry.coil.Winding;
import model.materials.database.MatDatabase;
import input.UtilsUserInput;

public class Input_T0_Iron_Test extends UtilsUserInput {


    private final Domain[] domains;

    public Domain[] getDomains() {
        return domains.clone();
    }

    public Input_T0_Iron_Test(Cable cableInput) {

        domains = new Domain[] {
                new AirDomain("airDomain", MatDatabase.MAT_AIR, air()),
                new AirFarFieldDomain("airFarFieldDomain", MatDatabase.MAT_AIR, airFarField()),
                 new CoilDomain("C0", MatDatabase.MAT_COIL_TEST, coil(cableInput)),
               new IronDomain("ironYoke", MatDatabase.MAT_IRON1, ironYoke()),
       };

    }

    private Coil coil(Cable cableInput) {

        double x0 = 0.01;
        double y0 = 0.005;
        double w = 15e-3;
        double h = 1.5e-3;

        double n;
        double delta;


        Point kp11 = Point.ofCartesian(x0, y0+w);
        Point kp12 = Point.ofCartesian(x0, y0);
        Point kp13 = Point.ofCartesian(x0+h, y0);
        Point kp14 = Point.ofCartesian(x0+h, y0+w);

        Line ln11 = Line.ofEndPoints(kp11, kp14);
        Line ln12 = Line.ofEndPoints(kp12, kp11);
        Line ln13 = Line.ofEndPoints(kp12, kp13);
        Line ln14 = Line.ofEndPoints(kp13, kp14);

        Area ha11p = Area.ofHyperLines(new HyperLine[]{ln11, ln12, ln13, ln14});
        Area ha12p = ha11p.translate(2*h,0);
        Area ha13p = ha11p.translate(4*h,0);

        Area ha11n = ha11p.mirrorY();
        Area ha12n = ha12p.mirrorY();
        Area ha13n = ha13p.mirrorY();

        // windings:
        Winding w0 = Winding.ofAreas(new Area[]{ha11p,ha11n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w1 = Winding.ofAreas(new Area[]{ha12p,ha12n}, new int[]{+1,-1}, 1, 1, cableInput);
        Winding w2 = Winding.ofAreas(new Area[]{ha13p,ha13n}, new int[]{+1,-1}, 1, 1, cableInput);

        // poles:
        Pole p1 = Pole.ofWindings(new Winding[]{w0,w1,w2});
        Pole p2 = p1.mirrorX().reverseWindingDirection();

        // Coil:
        Coil c1 = Coil.ofPoles(new Pole[]{p1,p2});

        return c1;
    }

    private Element[] air() {
        // POINTS

        double r = 0.5;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r, 0);
        Point kp2 = Point.ofCartesian(0, r);

        // LINES
        Line ln1 = Line.ofEndPoints(kpc, kp1);
        Arc ln2 = Arc.ofEndPointsCenter(kp1, kp2, kpc);
        Line ln3 = Line.ofEndPoints(kp2, kpc);

        // AREAS
        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3});
        Area ar2 = ar1.mirrorY();
        Area ar3 = ar2.mirrorX();
        Area ar4 = ar1.mirrorX();

        // ELEMENTS
        Element el1 = new Element("AIR_El1", ar1);
        Element el2 = new Element("AIR_El2", ar2);
        Element el3 = new Element("AIR_El3", ar3);
        Element el4 = new Element("AIR_El4", ar4);

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
//        Element[] quad1 = {el1};
//        Element[] quad2 = {el2};
//        Element[] quad3 = {el3};
//        Element[] quad4 = {el4};
        Element[] elementsToBuild = new Element[]{el1,el2,el3,el4};
        return elementsToBuild;
    }

    private Element[] airFarField() {
        // POINTS

        double r = 0.5;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r, 0);
        Point kp2 = Point.ofCartesian(0, r);

        Point kp1_out = Point.ofCartesian(r * 1.05, 0);
        Point kp4_out = Point.ofCartesian(0, r * 1.05);

        // LINES
        Line ln1 = Line.ofEndPoints(kp1, kp1_out);
        Arc ln2 = Arc.ofEndPointsCenter(kp1_out, kp4_out, kpc);
        Line ln3 = Line.ofEndPoints(kp4_out, kp2);
        Arc ln4 = Arc.ofEndPointsCenter(kp2, kp1, kpc);

        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3, ln4});
        Area ar2 = ar1.mirrorY();
        Area ar3 = ar2.mirrorX();
        Area ar4 = ar1.mirrorX();

        Element el1 = new Element("FAR_El1", ar1);
        Element el2 = new Element("FAR_El2", ar2);
        Element el3 = new Element("FAR_El3", ar3);
        Element el4 = new Element("FAR_El4", ar4);

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
//        Element[] quad1 = {el1};
//        Element[] quad2 = {el2};
//        Element[] quad3 = {el3};
//        Element[] quad4 = {el4};
        Element[] elementsToBuild = new Element[]{el1,el2,el3,el4};
        return elementsToBuild;
    }

    private Element[] ironYoke() {
        double deg2rad = Math.PI / 180;
        double r_int = 0.04;
        double r_ext = 0.06;


        Point kp1 = Point.ofPolar(r_int, 0 * deg2rad);
        Point kp2 = Point.ofPolar(r_ext, 0 * deg2rad);
        Point kp3 = Point.ofPolar(r_ext, 90 * deg2rad);
        Point kp4 = Point.ofPolar(r_int, 90 * deg2rad);

        Point kp0 = Point.ofCartesian(0, 0);

        Arc ln1 = Arc.ofEndPointsCenter(kp1, kp4, kp0);
        Line ln2 = Line.ofEndPoints(kp1, kp2);
        Arc ln3 = Arc.ofEndPointsCenter(kp2, kp3, kp0);
        Line ln4 = Line.ofEndPoints(kp4, kp3);

        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3, ln4});
        Area ar2 = ar1.mirrorY();
        Area ar3 = ar2.mirrorX();
        Area ar4 = ar1.mirrorX();

        Element el1 = new Element("IY_El1", ar1);
        Element el2 = new Element("IY_El2", ar2);
        Element el3 = new Element("IY_El3", ar3);
        Element el4 = new Element("IY_El4", ar4);

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
//        Element[] quad1 = {el1};
//        Element[] quad2 = {el2};
//        Element[] quad3 = {el3};
//        Element[] quad4 = {el4};
        Element[] elementsToBuild = new Element[]{el1,el2,el3,el4};
        return elementsToBuild;
    }

}
