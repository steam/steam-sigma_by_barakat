package comsol.utils;

import org.junit.Assert;

import java.util.List;

/**
 * Created by STEAM on 09/02/2018.
 */
public class Util {


    public static void compareSignal(List<double[]> signalRefAsList, List<double[]> signalResAsList, double TOL_REL) {
        double TOL_ABS_MIN = 1e-10;

        // Compare time
        for (int val_i = 0; val_i < signalResAsList.size(); val_i++) {
            double expectedTime = signalRefAsList.get(val_i)[0];
            double actualTime = signalResAsList.get(val_i)[0];
            double toleranceTimeABS = Math.max(Math.abs(expectedTime * TOL_REL), TOL_ABS_MIN);
            Assert.assertEquals(expectedTime, actualTime, toleranceTimeABS);
        }

        // Compare value
        for (int val_i = 0; val_i < signalResAsList.size(); val_i++) {
            double expectedVal = signalRefAsList.get(val_i)[1];
            double actualVal = signalResAsList.get(val_i)[1];
            double toleranceValABS = Math.max(Math.abs(expectedVal * TOL_REL), TOL_ABS_MIN);
            Assert.assertEquals(expectedVal, actualVal, toleranceValABS);
        }
    }
}
