package comsol.functional;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by STEAM on 30/01/2018.
 */
public class FunctionalTestConfig {

    // test testLabel
    private String testLabel;

    // test flags and parameters
    private String[] flagName;
    private String[] flagValue;
    private String[] paramName;
    private String[] paramValue;

    // variables to be evaluated
    private String[] variableName;
    private String[] variableExpression;

    // study setup
    private String[] activePhysicsName;
    private Boolean[] activePhysicsBool;

    private String[] time;
    private String[] current;
    private String tStepMax;

    private String[] timeIntervals;
    private String[] timeSteps;

    // *****************************************************************************************************************
    // Constructor
    // *****************************************************************************************************************

    public static FunctionalTestConfig parseFileJSON(String arg) throws IOException {
        FunctionalTestConfig cfg = new FunctionalTestConfig();
        BufferedReader br = null;
        try {
            Gson gson = new Gson();
            br = new BufferedReader(new FileReader(arg));
            cfg = gson.fromJson(br, FunctionalTestConfig.class);
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                br.close();
            }
        }
        return cfg;
    }

    // *****************************************************************************************************************
    // Getters
    // *****************************************************************************************************************

    public String getTestLabel() {
        return testLabel;
    }

    public String[] getFlagName() {
        return flagName;
    }

    public String[] getFlagValue() {
        return flagValue;
    }

    public String[] getParamName() {
        return paramName;
    }

    public String[] getParamValue() {
        return paramValue;
    }

    public String[] getVariableName() {
        return variableName;
    }

    public String[] getVariableExpression() {
        return variableExpression;
    }

    public String[] getTime() {
        return time;
    }

    public String[] getCurrent() {
        return current;
    }

    public String gettStepMax() {
        return tStepMax;
    }

    public String[] getActivePhysicsName() {
        return activePhysicsName;
    }

    public Boolean[] getActivePhysicsBool() {
        return activePhysicsBool;
    }

    public String[] getTimeIntervals() {
        return timeIntervals;
    }

    public String[] getTimeSteps() {
        return timeSteps;
    }
}


