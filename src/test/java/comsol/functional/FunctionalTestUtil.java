package comsol.functional;

import com.comsol.model.Model;
import comsol.MagnetMPHBuilder;
import comsol.api.DefinitionsAPI;
import comsol.api.GlobalEquationsPhysicsAPI;
import comsol.api.PostProcessAPI;
import comsol.api.StudyAPI;
import comsol.constants.MPHC;
import comsol.definitions.utils.Variable;
import comsol.definitions.utils.VariableGroup;
import comsol.global.Parameters;
import comsol.utils.Util;
import config.ConfigSigma;
import model.domains.Domain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import server.MphSigmaServer;
import server.MphSigmaServerAPI;
import server.SigmaServer;
import server.TxtSigmaServer;
import utils.TextFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by STEAM on 06/02/2018.
 */
public class FunctionalTestUtil {
    private static final Logger LOGGER = LogManager.getLogger(FunctionalTestUtil.class);

    private static String LABEL_RESULTS_FOLDER = "results";
    private static String LABEL_REFERENCE_FOLDER = "reference";
    private static String EXTENSION_JSON = ".json";
    private static String EXTENSION_CSV = ".csv";

    private static String configPath;
    private static String inputDir;
    private static String outputDir;
    private static FunctionalTestConfig cfgFt;
    private static double TOL_REL;

    public FunctionalTestUtil(String rootDir, String testLabel, double tolerance){
        configPath = String.format("%s/%s/%s%s", rootDir, testLabel, testLabel, EXTENSION_JSON);
        inputDir = String.format("%s/%s/%s", rootDir, testLabel, LABEL_REFERENCE_FOLDER);
        outputDir = String.format("%s/%s/%s", rootDir, testLabel, LABEL_RESULTS_FOLDER);
        TOL_REL = tolerance;

        try {
            cfgFt = FunctionalTestConfig.parseFileJSON(configPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void executeMphFunctionalTest(String testLabel, double toleranceREL, String resourcePath, Domain[] domains, ConfigSigma cfg) throws IOException {
        // Construction of the test model with MphSigmaServer
        SigmaServer srv = new MphSigmaServer(cfg.getOutputModelPath());
        MagnetMPHBuilder magnet = new MagnetMPHBuilder(cfg, srv);
        magnet.connectToServer();
        magnet.prepareModelTemplate();
        magnet.buildMPH(domains);
        magnet.save();
        magnet.compile();

        // Analysis of the test model
        FunctionalTestUtil ft = new FunctionalTestUtil(resourcePath, testLabel, toleranceREL);
        ft.runStudy(srv);
        magnet.disconnectFromServer();
    }

    public static void executeTxtFunctionalTest(String testLabel, double toleranceREL, String resourcePath, Domain[] domains, ConfigSigma cfg) throws IOException {
        SigmaServer srv = new TxtSigmaServer(cfg.getJavaJDKPath(), cfg.getComsolBatchPath());
        MagnetMPHBuilder magnet = new MagnetMPHBuilder(cfg, srv);
        magnet.connectToServer();
        magnet.prepareModelTemplate();
        magnet.buildMPH(domains);
        magnet.save();
        magnet.compile();

        //Load model with an MphSigmaServer
        MphSigmaServer mphSrv = new MphSigmaServer(cfg.getOutputModelPath());
        mphSrv.connect(cfg.getComsolBatchPath());
        Model model = MphSigmaServerAPI.parseModelFromServerMPH(cfg.getOutputModelPath());
        mphSrv.setMph(model);

        // Analysis of the test model
        FunctionalTestUtil ft = new FunctionalTestUtil(resourcePath, testLabel, toleranceREL);
        ft.runStudy(mphSrv);
        magnet.disconnectFromServer();
    }

    public static String getResourcePath(String rootDir) {
        return FunctionalTestUtil.class.getClassLoader().getResource(rootDir).getFile();
    }

    public static void runStudy(SigmaServer srv) throws IOException {
        System.out.println("TEST: model preparation");

        // Set flags
        Parameters.setGlobalParameters(srv, cfgFt.getFlagName(), cfgFt.getFlagValue());

        // Set parameters
        Parameters.setGlobalParameters(srv, cfgFt.getParamName(), cfgFt.getParamValue());

        // Create control variable group for test
        createVariableGroup(srv, cfgFt.getTestLabel(), cfgFt.getVariableName(), cfgFt.getVariableExpression());

        // Set input stimula for every current lead in the magnet
        for (String currentLeadsLabel : getCurrentLeadsLabels(srv)) {
            String currentLeadsExtLabel = String.format("%s_EXT", currentLeadsLabel);
            DefinitionsAPI.createInterpolationTable(srv, currentLeadsExtLabel, DefinitionsAPI.getComponentNodeName(srv));
            DefinitionsAPI.addValToInterpolationTable(srv, currentLeadsExtLabel, cfgFt.getTime(), cfgFt.getCurrent(), "s", "A");
            String CLIQJunctionBoxLabel = MPHC.MARKER_JUNCTIONBOX + "_" + MPHC.MARKER_CLIQ;
            DefinitionsAPI.addVariable(srv, CLIQJunctionBoxLabel, currentLeadsLabel, currentLeadsExtLabel + "(t)");
        }

        // Set study
        String studyLabel = cfgFt.getTestLabel();
        String timeRange = String.format("range( %s, %s, %s )", cfgFt.getTime()[0], cfgFt.gettStepMax(), cfgFt.getTime()[cfgFt.getTime().length - 1]);
        StudyAPI.setNewTransientStudy(srv, studyLabel, timeRange);
        StudyAPI.setActivePhysics(srv, studyLabel, cfgFt.getActivePhysicsName(), cfgFt.getActivePhysicsBool());

        // Run study
        System.out.println("TEST: study execution");
        StudyAPI.runStudy(srv, studyLabel);

        //Compare results
        compare(srv, studyLabel);
    }

    public static void runMonolithicStudy(SigmaServer srv) throws IOException {
        System.out.println("TEST: model preparation");

        // Set flags
        Parameters.setGlobalParameters(srv, cfgFt.getFlagName(), cfgFt.getFlagValue());

        // Set parameters
        Parameters.setGlobalParameters(srv, cfgFt.getParamName(), cfgFt.getParamValue());

        // Create control variable group for test
        createVariableGroup(srv, cfgFt.getTestLabel(), cfgFt.getVariableName(), cfgFt.getVariableExpression());

        // Set input stimula for every current lead in the magnet
        String CLIQJunctionBoxLabel = MPHC.MARKER_JUNCTIONBOX + "_" + MPHC.MARKER_CLIQ;
        for (String currentLeadsLabel : getCurrentLeadsLabels(srv)) {
            DefinitionsAPI.addVariable(srv, CLIQJunctionBoxLabel, currentLeadsLabel, "I");
        }

        //Setting circuit params
        String[] circuitComp = new String[]{"I_0","R_dump", "Lc", "Li"};
        String[] circuitValue = new String[]{"18e3[A]","25e-3[ohm]", "10.11[m]", "9.20[m]"};
        clearVariableGroup(srv, MPHC.MARKER_CIRCUITPARAMS);
        writeVariableGroup(srv, MPHC.MARKER_CIRCUITPARAMS, circuitComp, circuitValue);
        writeWindingVoltage(srv);

        //Creating scaling factor for R_dump to simulate a switch
        String[] time = new String[]{"0","5e-3", "5.1e-3", "0.5"};
        String[] value = new String[]{"0","0", "1", "1"};
        DefinitionsAPI.createInterpolationTable(srv, "switchR", DefinitionsAPI.getComponentNodeName(srv));
        DefinitionsAPI.addValToInterpolationTable(srv, "switchR", time, value, "s", "1");

        setCircuitEquations(srv);
        setEnergyEquations(srv);

        // Set study
        String studyLabel = cfgFt.getTestLabel();
        String timeRange = buildTimeRangeString();
        StudyAPI.setNewMonolithicStudy(srv, studyLabel, timeRange);
        StudyAPI.setActivePhysics(srv, studyLabel, cfgFt.getActivePhysicsName(), cfgFt.getActivePhysicsBool());

        // Run study
        System.out.println("TEST: study execution");
        StudyAPI.runStudy(srv, studyLabel);

        //Compare results
        compare(srv, studyLabel);
    }

    public static void runMonolithicStudy(SigmaServer srv, String[] circuitComp, String[] circuitValue) throws IOException {
        System.out.println("TEST: model preparation");

        // Set flags
        Parameters.setGlobalParameters(srv, cfgFt.getFlagName(), cfgFt.getFlagValue());

        // Set parameters
        Parameters.setGlobalParameters(srv, cfgFt.getParamName(), cfgFt.getParamValue());

        // Create control variable group for test
        createVariableGroup(srv, cfgFt.getTestLabel(), cfgFt.getVariableName(), cfgFt.getVariableExpression());

        // Set input stimula for every current lead in the magnet
        String CLIQJunctionBoxLabel = MPHC.MARKER_JUNCTIONBOX + "_" + MPHC.MARKER_CLIQ;
        for (String currentLeadsLabel : getCurrentLeadsLabels(srv)) {
            DefinitionsAPI.addVariable(srv, CLIQJunctionBoxLabel, currentLeadsLabel, "I");
        }

        //Setting circuit params
        clearVariableGroup(srv, MPHC.MARKER_CIRCUITPARAMS);
        writeVariableGroup(srv, MPHC.MARKER_CIRCUITPARAMS, circuitComp, circuitValue);
        writeWindingVoltage(srv);

        //Creating scaling factor for R_dump to simulate a switch
        String[] time = new String[]{"0","5e-3", "5.1e-3", "0.5"};
        String[] value = new String[]{"0","0", "1", "1"};
        DefinitionsAPI.createInterpolationTable(srv, "switchR", DefinitionsAPI.getComponentNodeName(srv));
        DefinitionsAPI.addValToInterpolationTable(srv, "switchR", time, value, "s", "1");

        setCircuitEquations(srv);
        setEnergyEquations(srv);

        // Set study
        String studyLabel = cfgFt.getTestLabel();
        String timeRange = buildTimeRangeString();
        StudyAPI.setNewMonolithicStudy(srv, studyLabel, timeRange);
        StudyAPI.setActivePhysics(srv, studyLabel, cfgFt.getActivePhysicsName(), cfgFt.getActivePhysicsBool());

        // Run study
        System.out.println("TEST: study execution");
        StudyAPI.runStudy(srv, studyLabel);

        //Compare results
        compare(srv, studyLabel);
    }

    private static void compare(SigmaServer srv, String studyLabel) throws IOException {
        String[] varsToBeTested = cfgFt.getVariableName();
        for (int var_i = 0; var_i < varsToBeTested.length; var_i++) {

            // Get Results
            String varName = varsToBeTested[var_i];
            System.out.printf("TEST: analysis of test variable %s%n", varName);
            double[] time = PostProcessAPI.calculateTime(srv, studyLabel);
            double[] results = PostProcessAPI.calculateGlobalEvaluation(srv, varName, studyLabel);

            ArrayList<double[]> varResList = new ArrayList<>();
            ArrayList<String> varResTextList = new ArrayList<>();
            for (int val_i = 0; val_i < time.length; val_i++) {
                varResList.add(new double[]{time[val_i],results[val_i]});
                varResTextList.add(String.format("%s, %s", Double.toString(time[val_i]), Double.toString(results[val_i])));
            }

            // print to file
            String fileName = String.format("%s%s", cfgFt.getVariableName()[var_i], EXTENSION_CSV);
            TextFile.writeMultiLine(String.format("%s/%s", outputDir, fileName), varResTextList, false);

            // parse reference values
            List<double[]> varRefList = TextFile.readAsListOfDoubleArray(String.format("%s/%s.csv", inputDir, varName), ",");

            // compare
            Util.compareSignal(varRefList, varResList, TOL_REL);
        }
    }

    // *****************************************************************************************************************
    // Specific Methods
    // *****************************************************************************************************************

    private static List<String> getCurrentLeadsLabels(SigmaServer srv){
        List<String> currentLeadsLabels = new ArrayList<>();

        // Get the tags of every component node
        String[] allTags = srv.getTagsVariable();

        for (String tag : allTags){
            // Find the nodes that are junction boxes
            if (tag.contains(MPHC.MARKER_JUNCTIONBOX)){
                String[] tempCurrentLeadsLabels = srv.getVarNamesVariable(tag);
                // get the current leads labels in each junction box
                for (String leads : tempCurrentLeadsLabels) {
                    currentLeadsLabels.add(leads);
                }
            }
        }
        return currentLeadsLabels;
    }

    private static void writeWindingVoltage(SigmaServer srv) {
        String[] allTags = srv.getTagsVariable();

        for (String tag : allTags){
            if (tag.contains("WindingVoltage")) {
                String vTotLabel = "V_tot";
                StringBuilder vTotValue = new StringBuilder();
                String[] windings = srv.getVarNamesVariable(tag);

                for (String labelWindingVoltage : windings){
                    String labelIntOpWinding = labelWindingVoltage.replace("V_", "") + "_" + MPHC.LABEL_SURFACE_INTEGRATION;

                    String value = String.format(labelIntOpWinding + "(R_PE*Is_HT/surf_HT)*Lc + " + labelIntOpWinding + "(d(phi_PE,t)/surf_HT)*Li");
                    writeVariableGroup(srv, tag, labelWindingVoltage, value);
                    vTotValue.append(labelWindingVoltage).append("+");
                }
                String vTotEq = vTotValue.substring(0, vTotValue.length()-1);
                writeVariableGroup(srv, tag, vTotLabel, vTotEq);
            }

        }
    }

    private static void setCircuitEquations(SigmaServer srv) {
        srv.removeFeaturePhysics("ge", "ODE_kvl");

        String nodeLabel = MPHC.LABEL_PHYSICSGE_ODE_CLIQ;

        GlobalEquationsPhysicsAPI.buildVariableName(srv, nodeLabel, "I", 0);
        String eqBase = "V_tot";
        String eqExtra = "R_dump*I*switchR(t)";
        String equation = String.format("%s+%s", eqBase, eqExtra);
        GlobalEquationsPhysicsAPI.buildEquation(srv, nodeLabel, equation, 0);
        GlobalEquationsPhysicsAPI.buildInitialValue(srv, nodeLabel, "I_0", 0);
        GlobalEquationsPhysicsAPI.buildUnits(srv, nodeLabel,"V","A");
    }

    private static void setEnergyEquations(SigmaServer srv) {
        final String LABEL_PHYSICSGE_ODE_ENERGY = "ODE_Energy";
        srv.createLabelPhysics(MPHC.LABEL_PHYSICSGE_GE, LABEL_PHYSICSGE_ODE_ENERGY, "GlobalEquations", -1);

        srv.setLabelFeaturePhysics(MPHC.LABEL_PHYSICSGE_GE, LABEL_PHYSICSGE_ODE_ENERGY, LABEL_PHYSICSGE_ODE_ENERGY);

        GlobalEquationsPhysicsAPI.buildVariableName(srv, LABEL_PHYSICSGE_ODE_ENERGY, "E_dump", 0);
        String equation = "E_dumpt-I^2*R_dump*switchR(t)";
        GlobalEquationsPhysicsAPI.buildEquation(srv, LABEL_PHYSICSGE_ODE_ENERGY, equation, 0);
        GlobalEquationsPhysicsAPI.buildUnits(srv, LABEL_PHYSICSGE_ODE_ENERGY, "W", "J");
    }

    private static String buildTimeRangeString(){
        String[] timeIntervals = cfgFt.getTimeIntervals();
        String[] timeSteps = cfgFt.getTimeSteps();
        StringBuilder timeRange = new StringBuilder(String.format("range( %s, %s, %s )", timeIntervals[0], timeSteps[0], timeIntervals[1]));
        for (int i = 1; i < timeSteps.length; i++) {
            timeRange.append(String.format(",range( %s, %s, %s )", timeIntervals[i], timeSteps[i], timeIntervals[i + 1]));
        }
        return timeRange.toString();
    }

    private static void createVariableGroup(SigmaServer srv, String variableGroupLabel, String[] variableName, String[] variableExpression){
        ArrayList<Variable> varsList = new ArrayList<>();
        for (int i = 0; i < variableName.length; i++) {
            varsList.add(new Variable(variableName[i], variableExpression[i]));
        }
        new VariableGroup(variableGroupLabel, varsList).buildVariableGroup(srv,"");
    }

    private static void clearVariableGroup(SigmaServer srv, String variableGroupLabel) {
        // Command not compatible with COMSOL 52a
        // mph.variable(variableGroupLabel).clear();

        // It is supposed that the variable group is not domain specific
        DefinitionsAPI.removeVariableGroup(srv,variableGroupLabel);
        DefinitionsAPI.createVariableGroup(srv,variableGroupLabel);
    }

    private static void writeVariableGroup(SigmaServer srv, String variableGroupLabel, String[] variableName, String[] variableExpression){
        for (int i = 0; i < variableName.length; i++) {
            srv.setParamVariable(variableGroupLabel, variableName[i], variableExpression[i]);
        }
    }
    private static void writeVariableGroup(SigmaServer srv, String variableGroupLabel, String variableName, String variableExpression){
        srv.setParamVariable(variableGroupLabel, variableName, variableExpression);
    }

}
