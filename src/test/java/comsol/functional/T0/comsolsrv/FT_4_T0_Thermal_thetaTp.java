package comsol.functional.T0.comsolsrv;

import comsol.functional.FunctionalTestUtil;
import comsol.utils.models.Input_T0_Iron_Test;
import comsol.utils.models.cable.T0.withThetaTp.Cable_T0_Bare_thetaTp;
import comsol.utils.models.cable.T0.withThetaTp.Cable_T0_Conductor_thetaTp;
import comsol.utils.models.cable.T0.withThetaTp.Cable_T0_Insulated_thetaTp;
import config.ConfigSigma;
import org.junit.*;
import server.MphSigmaServerAPI;

import java.io.IOException;

/**
 * Created by STEAM on 22/01/2018.
 */
public class FT_4_T0_Thermal_thetaTp {

    private static final String JSON_PATH = "functional/T0/STEAMconfigTEST.json";
    private static final String TEST_ROOT_DIR = "functional/T0";
    private static ConfigSigma cfg;

    // *****************************************************************************************************************
    // Before-After
    // *****************************************************************************************************************

    @BeforeClass
    public static void executeBeforeClass() throws IOException {
        cfg = ConfigSigma.parseFileJSON(FunctionalTestUtil.getResourcePath(JSON_PATH));
    }

    @Before
    public void executeBeforeTest() {
        MphSigmaServerAPI.disconnect();
    }

    @After
    public void executeAfterTest() throws IOException {
        MphSigmaServerAPI.disconnect();
    }

    @AfterClass
    public static void executeAfterClass(){
    }

    // *****************************************************************************************************************
    // Tests
    // *****************************************************************************************************************

    @Test
    public void runTest_4A() throws IOException {
        String testLabel = "4A";
        double toleranceREL = 1e-2;
        System.out.println("TEST: Now running test " + testLabel+"   ************************************************");
        Input_T0_Iron_Test input = new Input_T0_Iron_Test(new Cable_T0_Conductor_thetaTp());

        FunctionalTestUtil.executeMphFunctionalTest(testLabel, toleranceREL, FunctionalTestUtil.getResourcePath(TEST_ROOT_DIR), input.getDomains(), cfg);
    }

    @Test
    public void runTest_4B() throws IOException {
        String testLabel = "4B";
        double toleranceREL = 1e-2;
        System.out.println("TEST: Now running test " + testLabel+"   ************************************************");
        Input_T0_Iron_Test input = new Input_T0_Iron_Test(new Cable_T0_Bare_thetaTp());

        FunctionalTestUtil.executeMphFunctionalTest(testLabel, toleranceREL, FunctionalTestUtil.getResourcePath(TEST_ROOT_DIR), input.getDomains(), cfg);
    }

    @Test
    public void runTest_4C() throws IOException {
        String testLabel = "4C";
        double toleranceREL = 1e-2;
        System.out.println("TEST: Now running test " + testLabel+"   ************************************************");
        Input_T0_Iron_Test input = new Input_T0_Iron_Test(new Cable_T0_Insulated_thetaTp());

        FunctionalTestUtil.executeMphFunctionalTest(testLabel, toleranceREL, FunctionalTestUtil.getResourcePath(TEST_ROOT_DIR), input.getDomains(), cfg);
    }
}
