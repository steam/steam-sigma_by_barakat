package comsol.functional.T0.comsolsrv;

import com.comsol.model.Model;
import comsol.functional.FunctionalTestUtil;
import comsol.utils.models.Input_T0_Iron_Test;
import comsol.utils.models.cable.T0.withoutThetaTp.Cable_T0_Electromagnetic;
import config.ConfigSigma;
import org.junit.*;
import server.MphSigmaServerAPI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by STEAM on 22/01/2018.
 */
public class FT_1_T0_ElectroMagnetic {

    private static final String JSON_PATH = "functional/T0/STEAMconfigTEST.json";
    private static final String TEST_ROOT_DIR = "functional/T0";
    private static ConfigSigma cfg;

    // *****************************************************************************************************************
    // Before-After
    // *****************************************************************************************************************

    @BeforeClass
    public static void executeBeforeClass() throws IOException {
        cfg = ConfigSigma.parseFileJSON(FunctionalTestUtil.getResourcePath(JSON_PATH));
    }

    @Before
    public void executeBeforeTest() {
        MphSigmaServerAPI.disconnect();
    }

    @After
    public void executeAfterTest() throws IOException {
        MphSigmaServerAPI.disconnect();
    }

    @AfterClass
    public static void executeAfterClass() {
    }

    // *****************************************************************************************************************
    // Tests
    // *****************************************************************************************************************

    @Test
    public void runTest_1A() throws IOException {
        String testLabel = "1A";
        double toleranceREL = 1e-2;
        System.out.println("TEST: Now running test " + testLabel+"   ************************************************");
        Input_T0_Iron_Test input = new Input_T0_Iron_Test(new Cable_T0_Electromagnetic());

        FunctionalTestUtil.executeMphFunctionalTest(testLabel, toleranceREL, FunctionalTestUtil.getResourcePath(TEST_ROOT_DIR), input.getDomains(), cfg);
    }

    @Test
    public void runTest_1B() throws IOException {
        String testLabel = "1B";
        double toleranceREL = 1e-2;
        System.out.println("TEST: Now running test " + testLabel+"   ************************************************");
        Input_T0_Iron_Test input = new Input_T0_Iron_Test(new Cable_T0_Electromagnetic());

        FunctionalTestUtil.executeMphFunctionalTest(testLabel, toleranceREL, FunctionalTestUtil.getResourcePath(TEST_ROOT_DIR), input.getDomains(), cfg);
    }

    @Test
    public void runTest_1C() throws IOException {
        String testLabel = "1C";
        double toleranceREL = 1e-2;
        System.out.println("TEST: Now running test " + testLabel+"   ************************************************");
        Input_T0_Iron_Test input = new Input_T0_Iron_Test(new Cable_T0_Electromagnetic());

        FunctionalTestUtil.executeMphFunctionalTest(testLabel, toleranceREL, FunctionalTestUtil.getResourcePath(TEST_ROOT_DIR), input.getDomains(), cfg);
    }

    @Test
    public void runTest_1D() throws IOException {
        String testLabel = "1D";
        double toleranceREL = 1e-2;
        System.out.println("TEST: Now running test " + testLabel+"   ************************************************");
        Input_T0_Iron_Test input = new Input_T0_Iron_Test(new Cable_T0_Electromagnetic());

        FunctionalTestUtil.executeMphFunctionalTest(testLabel, toleranceREL, FunctionalTestUtil.getResourcePath(TEST_ROOT_DIR), input.getDomains(), cfg);
    }

    // *****************************************************************************************************************
    // Specific Methods
    // *****************************************************************************************************************

    private static List<String> getCableLabels(Model mph, String cableLabel) {
        List<String> cableLabels = new ArrayList<>();

        // Get the tags of every component node
        String[] allTags = mph.variable().tags();
        for (String tag : allTags) {
            // Find the nodes that are junction boxes
            if (tag.contains(cableLabel)) {
                cableLabels.add(tag);
            }
        }
        return cableLabels;
    }
}
