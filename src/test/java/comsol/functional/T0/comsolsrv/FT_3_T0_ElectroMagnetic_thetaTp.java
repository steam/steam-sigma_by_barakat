package comsol.functional.T0.comsolsrv;

import com.comsol.model.util.ModelUtil;
import comsol.functional.FunctionalTestUtil;
import comsol.utils.models.Input_T0_Iron_Test;
import comsol.utils.models.cable.T0.withThetaTp.Cable_T0_Electromagnetic_thetaTp;
import config.ConfigSigma;
import org.junit.*;

import java.io.IOException;

/**
 * Created by STEAM on 22/01/2018.
 */
public class FT_3_T0_ElectroMagnetic_thetaTp {

    private static final String JSON_PATH = "functional/T0/STEAMconfigTEST.json";
    private static final String TEST_ROOT_DIR = "functional/T0";
    private static ConfigSigma cfg;

    // *****************************************************************************************************************
    // Before-After
    // *****************************************************************************************************************

    @BeforeClass
    public static void executeBeforeClass() throws IOException {
        cfg = ConfigSigma.parseFileJSON(FunctionalTestUtil.getResourcePath(JSON_PATH));
    }

    @Before
    public void executeBeforeTest() {
        ModelUtil.disconnect();
    }

    @After
    public void executeAfterTest() throws IOException {
        ModelUtil.disconnect();
    }

    @AfterClass
    public static void executeAfterClass(){
    }

    // *****************************************************************************************************************
    // Tests
    // *****************************************************************************************************************

    @Test
    public void runTest_3A() throws IOException {
        String testLabel = "3A";
        double toleranceREL = 1e-2;
        System.out.println("TEST: Now running test " + testLabel+"   ************************************************");
        Input_T0_Iron_Test input = new Input_T0_Iron_Test(new Cable_T0_Electromagnetic_thetaTp());

        FunctionalTestUtil.executeMphFunctionalTest(testLabel, toleranceREL, FunctionalTestUtil.getResourcePath(TEST_ROOT_DIR), input.getDomains(), cfg);
    }

    @Test
    //todo: I'm not convinced by the test. Losses are lower than test 1B, eve though there's more conductor! WTF
    public void runTest_3B() throws IOException {
        String testLabel = "3B";
        double toleranceREL = 1e-2;
        System.out.println("TEST: Now running test " + testLabel+"   ************************************************");
        Input_T0_Iron_Test input = new Input_T0_Iron_Test(new Cable_T0_Electromagnetic_thetaTp());

        FunctionalTestUtil.executeMphFunctionalTest(testLabel, toleranceREL, FunctionalTestUtil.getResourcePath(TEST_ROOT_DIR), input.getDomains(), cfg);
    }

    @Test
    public void runTest_3C() throws IOException {
        String testLabel = "3C";
        double toleranceREL = 1e-2;
        System.out.println("TEST: Now running test " + testLabel+"   ************************************************");
        Input_T0_Iron_Test input = new Input_T0_Iron_Test(new Cable_T0_Electromagnetic_thetaTp());

        FunctionalTestUtil.executeMphFunctionalTest(testLabel, toleranceREL, FunctionalTestUtil.getResourcePath(TEST_ROOT_DIR), input.getDomains(), cfg);
    }

    @Test
    public void runTest_3D() throws IOException {
        String testLabel = "3D";
        double toleranceREL = 1e-2;
        System.out.println("TEST: Now running test " + testLabel+"   ************************************************");
        Input_T0_Iron_Test input = new Input_T0_Iron_Test(new Cable_T0_Electromagnetic_thetaTp());

        FunctionalTestUtil.executeMphFunctionalTest(testLabel, toleranceREL, FunctionalTestUtil.getResourcePath(TEST_ROOT_DIR), input.getDomains(), cfg);
    }
}
