package comsol.functional.T1.txtsrv;

import comsol.functional.FunctionalTestUtil;
import comsol.utils.models.Input_T1_Iron_Test;
import comsol.utils.models.cable.T0.withoutThetaTp.Cable_T0_Bare;
import comsol.utils.models.cable.T0.withoutThetaTp.Cable_T0_Conductor;
import comsol.utils.models.cable.T0.withoutThetaTp.Cable_T0_Insulated;
import config.ConfigSigma;
import org.junit.*;
import server.MphSigmaServerAPI;

import java.io.IOException;

/**
 * Created by STEAM on 22/01/2018.
 */
public class FT_2_T1_Thermal {

    private static final String JSON_PATH = "functional/T1/STEAMconfigTEST.json";
    private static final String TEST_ROOT_DIR = "functional/T1";
    private static ConfigSigma cfg;

    // *****************************************************************************************************************
    // Before-After
    // *****************************************************************************************************************

    @BeforeClass
    public static void executeBeforeClass() throws IOException {
        cfg = ConfigSigma.parseFileJSON(FunctionalTestUtil.getResourcePath(JSON_PATH));
    }

    @Before
    public void executeBeforeTest() {
        MphSigmaServerAPI.disconnect();
    }

    @After
    public void executeAfterTest() throws IOException {
        MphSigmaServerAPI.disconnect();
        System.out.println("TEST: terminated.");
    }

    @AfterClass
    public static void executeAfterClass(){
    }

    // *****************************************************************************************************************
    // Tests
    // *****************************************************************************************************************
    @Test
    public void runTest_2A_Text() throws IOException {
        String testLabel = "2A";
        double toleranceREL = 1e-2;
        System.out.println("TEST: Now running test " + testLabel + " - ASCII Output ************************************************");
        Input_T1_Iron_Test input = new Input_T1_Iron_Test(new Cable_T0_Conductor());

        FunctionalTestUtil.executeTxtFunctionalTest(testLabel, toleranceREL, FunctionalTestUtil.getResourcePath(TEST_ROOT_DIR), input.getDomains(), cfg);
    }

    @Test
    public void runTest_2B_Text() throws IOException {
        String testLabel = "2B";
        double toleranceREL = 1e-2;
        System.out.println("TEST: Now running test " + testLabel + " - ASCII Output ************************************************");
        Input_T1_Iron_Test input = new Input_T1_Iron_Test(new Cable_T0_Bare());

        FunctionalTestUtil.executeTxtFunctionalTest(testLabel, toleranceREL, FunctionalTestUtil.getResourcePath(TEST_ROOT_DIR), input.getDomains(), cfg);
    }

    @Test
    public void runTest_2C_Text() throws IOException {
        String testLabel = "2C";
        double toleranceREL = 1e-2;
        System.out.println("TEST: Now running test " + testLabel + " - ASCII Output ************************************************");
        Input_T1_Iron_Test input = new Input_T1_Iron_Test(new Cable_T0_Insulated());

        FunctionalTestUtil.executeTxtFunctionalTest(testLabel, toleranceREL, FunctionalTestUtil.getResourcePath(TEST_ROOT_DIR), input.getDomains(), cfg);
    }
}
