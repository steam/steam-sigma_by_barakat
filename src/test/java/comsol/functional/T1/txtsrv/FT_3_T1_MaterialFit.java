package comsol.functional.T1.txtsrv;

import com.comsol.model.Model;
import comsol.MagnetMPHBuilder;
import comsol.constants.MPHC;
import comsol.functional.FunctionalTestUtil;
import comsol.utils.models.Input_T1_Iron_Test;
import comsol.utils.models.cable.T1.withoutThetaTp.Cable_T1_CopperResistivity;
import config.ConfigSigma;
import org.junit.*;
import server.MphSigmaServer;
import server.MphSigmaServerAPI;
import server.SigmaServer;
import server.TxtSigmaServer;

import java.io.IOException;

/**
 * Created by STEAM on 22/01/2018.
 */
public class FT_3_T1_MaterialFit {

    private static final String JSON_PATH = "functional/T1/STEAMconfigTEST.json";
    private static final String TEST_ROOT_DIR = "functional/T1";
    private static ConfigSigma cfg;
    private SigmaServer srv;

    // *****************************************************************************************************************
    // Before-After
    // *****************************************************************************************************************

    @BeforeClass
    public static void executeBeforeClass() throws IOException {
        String resourcePath = FT_3_T1_MaterialFit.class.getClassLoader().getResource(JSON_PATH).getFile();
        cfg = ConfigSigma.parseFileJSON(resourcePath);
    }

    @Before
    public void executeBeforeTest() {
        srv = null;
        MphSigmaServerAPI.disconnect();
    }

    @After
    public void executeAfterTest() throws IOException {
        MphSigmaServerAPI.disconnect();
    }

    @AfterClass
    public static void executeAfterClass(){
    }

    // *****************************************************************************************************************
    // Tests
    // *****************************************************************************************************************
    @Test
    public void runTest_3Aa_Text() throws IOException {
        String testLabel = "3Aa";
        double toleranceREL = 1e-2;

        System.out.println("TEST: Now running test " + testLabel + " - ASCII Output ************************************************");
        Input_T1_Iron_Test input = new Input_T1_Iron_Test(new Cable_T1_CopperResistivity());

        FunctionalTestUtil.executeTxtFunctionalTest(testLabel, toleranceREL, FunctionalTestUtil.getResourcePath(TEST_ROOT_DIR), input.getDomains(), cfg);
    }

    @Test
    public void runTest_3Ab_Text() throws IOException {
        String testLabel = "3Ab";
        double toleranceREL = 1e-2;
        System.out.println("TEST: Now running test " + testLabel + " - ASCII Output ************************************************");
        Input_T1_Iron_Test input = new Input_T1_Iron_Test(new Cable_T1_CopperResistivity());

        // Construction of the test model
        srv = new TxtSigmaServer(cfg.getJavaJDKPath(), cfg.getComsolBatchPath());
        MagnetMPHBuilder magnet = new MagnetMPHBuilder(cfg, srv);
        magnet.connectToServer();
        magnet.prepareModelTemplate();
        magnet.buildMPH(input.getDomains());
        magnet.save();
        magnet.compile();

        //Load model with an MphSigmaServer
        MphSigmaServer mphSrv = new MphSigmaServer(cfg.getOutputModelPath());
        mphSrv.connect(cfg.getComsolBatchPath());
        Model model = MphSigmaServerAPI.parseModelFromServerMPH(cfg.getOutputModelPath());
        mphSrv.setMph(model);

        //Analysis of the test model
        setInitialTemp(mphSrv, "30[K]");

        String resourcePath = FT_3_T1_MaterialFit.class.getClassLoader().getResource(TEST_ROOT_DIR).getFile();
        FunctionalTestUtil ft = new FunctionalTestUtil(resourcePath, testLabel,toleranceREL);
        ft.runStudy(mphSrv);
    }

    @Test
    public void runTest_3Ac_Text() throws IOException {
        String testLabel = "3Ac";
        double toleranceREL = 1e-2;
        System.out.println("TEST: Now running test " + testLabel + " - ASCII Output ************************************************");
        Input_T1_Iron_Test input = new Input_T1_Iron_Test(new Cable_T1_CopperResistivity());

        // Construction of the test model
        srv = new TxtSigmaServer(cfg.getJavaJDKPath(), cfg.getComsolBatchPath());
        MagnetMPHBuilder magnet = new MagnetMPHBuilder(cfg, srv);
        magnet.connectToServer();
        magnet.prepareModelTemplate();
        magnet.buildMPH(input.getDomains());
        magnet.save();
        magnet.compile();

        //Load model with an MphSigmaServer
        MphSigmaServer mphSrv = new MphSigmaServer(cfg.getOutputModelPath());
        mphSrv.connect(cfg.getComsolBatchPath());
        Model model = MphSigmaServerAPI.parseModelFromServerMPH(cfg.getOutputModelPath());
        mphSrv.setMph(model);

        //Analysis of the test model
        setInitialTemp(mphSrv, "100[K]");

        String resourcePath = FT_3_T1_MaterialFit.class.getClassLoader().getResource(TEST_ROOT_DIR).getFile();
        FunctionalTestUtil ft = new FunctionalTestUtil(resourcePath, testLabel,toleranceREL);
        ft.runStudy(mphSrv);
    }

    // *****************************************************************************************************************
    // Specific Methods
    // *****************************************************************************************************************

    private static void setInitialTemp(SigmaServer srv, String value){
        srv.setParamFeaturePhysics(MPHC.LABEL_PHYSICSTH, "init1", "Tinit", value);
    }

}
