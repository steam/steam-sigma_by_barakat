package comsol.functional.T1.txtsrv;

import comsol.functional.FunctionalTestUtil;
import comsol.utils.models.Input_T1_Iron_Test;
import comsol.utils.models.Input_T1_NoIron_Test;
import comsol.utils.models.cable.T0.withoutThetaTp.Cable_T0_Electromagnetic;
import config.ConfigSigma;
import org.junit.*;
import server.MphSigmaServerAPI;

import java.io.IOException;


/**
 * Created by STEAM on 22/01/2018.
 */

//todo: CSV referece values sometimes are truncated to the second decimal (thanks EXCEL), enforcing the relative precision to 1%.
//todo: update all the reference values with full precision digit

public class FT_0_T1_Geometry {

    private static final String JSON_PATH = "functional/T1/STEAMconfigTEST.json";
    private static final String TEST_ROOT_DIR = "functional/T1";
    private static ConfigSigma cfg;

    // *****************************************************************************************************************
    // Before-After
    // *****************************************************************************************************************

    @BeforeClass
    public static void executeBeforeClass() throws IOException {
        cfg = ConfigSigma.parseFileJSON(FunctionalTestUtil.getResourcePath(JSON_PATH));
    }

    @Before
    public void executeBeforeTest() {
        MphSigmaServerAPI.disconnect();

    }

    @After
    public void executeAfterTest() throws IOException {
        MphSigmaServerAPI.disconnect();
    }

    @AfterClass
    public static void executeAfterClass(){
    }

    // *****************************************************************************************************************
    // Tests
    // *****************************************************************************************************************
    @Test
    public void runTest_0A_Text() throws IOException {
        String testLabel = "0A";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel + " - ASCII Output ************************************************");
        Input_T1_NoIron_Test input = new Input_T1_NoIron_Test(new Cable_T0_Electromagnetic());

        FunctionalTestUtil.executeTxtFunctionalTest(testLabel, toleranceREL, FunctionalTestUtil.getResourcePath(TEST_ROOT_DIR), input.getDomains(), cfg);
    }

    @Test
    public void runTest_0B_Text() throws IOException {
        String testLabel = "0B";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel + " - ASCII Output ************************************************");
        Input_T1_Iron_Test input = new Input_T1_Iron_Test(new Cable_T0_Electromagnetic());

        FunctionalTestUtil.executeTxtFunctionalTest(testLabel, toleranceREL, FunctionalTestUtil.getResourcePath(TEST_ROOT_DIR), input.getDomains(), cfg);
    }
}
