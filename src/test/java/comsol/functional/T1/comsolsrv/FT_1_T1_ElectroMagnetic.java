package comsol.functional.T1.comsolsrv;

import com.comsol.model.util.ModelUtil;
import comsol.MagnetMPHBuilder;
import comsol.constants.MPHC;
import comsol.functional.FunctionalTestUtil;
import comsol.utils.models.Input_T1_Iron_Test;
import comsol.utils.models.cable.T0.withoutThetaTp.Cable_T0_Electromagnetic;
import config.ConfigSigma;
import org.junit.*;
import server.MphSigmaServer;
import server.SigmaServer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by STEAM on 22/01/2018.
 */
public class FT_1_T1_ElectroMagnetic {

    private static final String JSON_PATH = "functional/T1/STEAMconfigTEST.json";
    private static final String TEST_ROOT_DIR = "functional/T1";
    private static ConfigSigma cfg;
    private SigmaServer srv;

    // *****************************************************************************************************************
    // Before-After
    // *****************************************************************************************************************

    @BeforeClass
    public static void executeBeforeClass() throws IOException {
        String resourcePath = FT_1_T1_ElectroMagnetic.class.getClassLoader().getResource(JSON_PATH).getFile();
        cfg = ConfigSigma.parseFileJSON(resourcePath);
    }

    @Before
    public void executeBeforeTest() {
        ModelUtil.disconnect();
    }

    @After
    public void executeAfterTest() throws IOException {
        ModelUtil.disconnect();
    }

    @AfterClass
    public static void executeAfterClass() {
    }

    // *****************************************************************************************************************
    // Tests
    // *****************************************************************************************************************

    @Test
    public void runTest_1A() throws IOException {
        String testLabel = "1A";
        double toleranceREL = 1e-2;
        System.out.println("TEST: Now running test " + testLabel+"   ************************************************");

        // Construction of the test model
        Cable_T0_Electromagnetic cable = new Cable_T0_Electromagnetic();
        Input_T1_Iron_Test input = new Input_T1_Iron_Test(cable);
        srv = new MphSigmaServer(cfg.getOutputModelPath());
        MagnetMPHBuilder magnet = new MagnetMPHBuilder(cfg, srv);
        magnet.connectToServer();
        magnet.prepareModelTemplate();
        magnet.buildMPH(input.getDomains());

        // Analysis of the test model
        setContTauIFCC(srv, "1e-3", cable.getLabel());

        String resourcePath = FT_1_T1_ElectroMagnetic.class.getClassLoader().getResource(TEST_ROOT_DIR).getFile();
        FunctionalTestUtil ft = new FunctionalTestUtil(resourcePath, testLabel,toleranceREL);
        ft.runStudy(srv);
    }

    // *****************************************************************************************************************
    // Specific Methods
    // *****************************************************************************************************************

    private static void setContTauIFCC(SigmaServer srv, String value, String cableLabel){
        List<String> variableNames = getCableLabels(srv, cableLabel);
        for (String variableName : variableNames){
            srv.setParamVariable(variableName, MPHC.LABEL_CABLE_TAU_IFCC, value);
        }
    }

    private static List<String> getCableLabels(SigmaServer srv, String cableLabel) {
        List<String> cableLabels = new ArrayList<>();

        // Get the tags of every component node
        String[] allTags = srv.getTagsVariable();

        for (String tag : allTags) {
            // Find the nodes that are junction boxes
            if (tag.contains(cableLabel)) {
                cableLabels.add(tag);
            }
        }
        return cableLabels;
    }
}
