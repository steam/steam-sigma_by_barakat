package comsol.functional.T1.comsolsrv;

import comsol.MagnetMPHBuilder;
import comsol.constants.MPHC;
import comsol.functional.FunctionalTestUtil;
import comsol.utils.models.Input_T1_Iron_Test;
import comsol.utils.models.cable.T0.withoutThetaTp.Cable_T0_Electromagnetic;
import comsol.utils.models.cable.T1.withoutThetaTp.Cable_T1_CopperResistivity;
import comsol.utils.models.cable.T1.withoutThetaTp.Cable_T1_HeatCapacity;
import config.ConfigSigma;
import org.junit.*;
import server.MphSigmaServer;
import server.MphSigmaServerAPI;
import server.SigmaServer;

import java.io.IOException;


/**
 * Created by STEAM on 22/01/2018.
 */

//todo: CSV referece values sometimes are truncated to the second decimal (thanks EXCEL), enforcing the relative precision to 1%.
//todo: update all the reference values with full precision digit

public class FT_4_T1_CircuitCoupling {

    private static final String JSON_PATH = "functional/T1/STEAMconfigTEST.json";
    private static final String TEST_ROOT_DIR = "functional/T1";
    private static ConfigSigma cfg;
    private SigmaServer srv;

    // *****************************************************************************************************************
    // Before-After
    // *****************************************************************************************************************

    @BeforeClass
    public static void executeBeforeClass() throws IOException {
        String resourcePath = FT_4_T1_CircuitCoupling.class.getClassLoader().getResource(JSON_PATH).getFile();
        cfg = ConfigSigma.parseFileJSON(resourcePath);
    }

    @Before
    public void executeBeforeTest() {
        srv = null;
        MphSigmaServerAPI.disconnect();

    }

    @After
    public void executeAfterTest() throws IOException {
        MphSigmaServerAPI.disconnect();
    }

    @AfterClass
    public static void executeAfterClass(){
    }

    // *****************************************************************************************************************
    // Tests
    // *****************************************************************************************************************

    @Test
    public void runTest_4A() throws IOException {
        String testLabel = "4A";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel+"   ************************************************");

        // Construction of the test model
        Input_T1_Iron_Test input = new Input_T1_Iron_Test(new Cable_T0_Electromagnetic());
        srv = new MphSigmaServer(cfg.getOutputModelPath());
        MagnetMPHBuilder magnet = new MagnetMPHBuilder(cfg, srv);
        magnet.connectToServer();
        magnet.prepareModelTemplate();
        magnet.buildMPH(input.getDomains());

        // Analysis of the test model
        setInitialTemp(srv, "4.5[K]");

        String resourcePath = FT_4_T1_CircuitCoupling.class.getClassLoader().getResource(TEST_ROOT_DIR).getFile();
        FunctionalTestUtil ft = new FunctionalTestUtil(resourcePath, testLabel,toleranceREL);
        ft.runMonolithicStudy(srv);
        magnet.disconnectFromServer();
    }

    @Test
    public void runTest_4B() throws IOException {
        String testLabel = "4B";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel+"   ************************************************");

        // Construction of the test model
        Input_T1_Iron_Test input = new Input_T1_Iron_Test(new Cable_T1_CopperResistivity());
        srv = new MphSigmaServer(cfg.getOutputModelPath());
        MagnetMPHBuilder magnet = new MagnetMPHBuilder(cfg, srv);
        magnet.connectToServer();
        magnet.prepareModelTemplate();
        magnet.buildMPH(input.getDomains());

        // Analysis of the test model
        setInitialTemp(srv, "4.5[K]");

        String resourcePath = FT_4_T1_CircuitCoupling.class.getClassLoader().getResource(TEST_ROOT_DIR).getFile();
        FunctionalTestUtil ft = new FunctionalTestUtil(resourcePath, testLabel,toleranceREL);
        ft.runMonolithicStudy(srv);
        magnet.disconnectFromServer();
    }

    @Test
    public void runTest_4C() throws IOException {
        String testLabel = "4C";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel+"   ************************************************");

        // Construction of the test model
        Input_T1_Iron_Test input = new Input_T1_Iron_Test(new Cable_T1_CopperResistivity());
        srv = new MphSigmaServer(cfg.getJavaJDKPath());
        MagnetMPHBuilder magnet = new MagnetMPHBuilder(cfg, srv);
        magnet.connectToServer();
        magnet.prepareModelTemplate();
        magnet.buildMPH(input.getDomains());

        // Analysis of the test model
        setInitialTemp(srv, "4.5[K]");

        String resourcePath = FT_4_T1_CircuitCoupling.class.getClassLoader().getResource(TEST_ROOT_DIR).getFile();
        FunctionalTestUtil ft = new FunctionalTestUtil(resourcePath, testLabel,toleranceREL);
        ft.runMonolithicStudy(srv);
        magnet.disconnectFromServer();
    }

    @Test
    public void runTest_4D() throws IOException {
        String testLabel = "4D";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel+"   ************************************************");

        // Construction of the test model
        Input_T1_Iron_Test input = new Input_T1_Iron_Test(new Cable_T1_HeatCapacity());
        srv = new MphSigmaServer(cfg.getOutputModelPath());
        MagnetMPHBuilder magnet = new MagnetMPHBuilder(cfg, srv);
        magnet.connectToServer();
        magnet.prepareModelTemplate();
        magnet.buildMPH(input.getDomains());

        // Analysis of the test model
        setInitialTemp(srv, "4.5[K]");

        String resourcePath = FT_4_T1_CircuitCoupling.class.getClassLoader().getResource(TEST_ROOT_DIR).getFile();
        FunctionalTestUtil ft = new FunctionalTestUtil(resourcePath, testLabel,toleranceREL);
        ft.runMonolithicStudy(srv);
        magnet.disconnectFromServer();
    }

    @Test
    public void runTest_4E() throws IOException {
        String testLabel = "4E";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel+"   ************************************************");

        // Construction of the test model
        Input_T1_Iron_Test input = new Input_T1_Iron_Test(new Cable_T1_HeatCapacity());
        srv = new MphSigmaServer(cfg.getOutputModelPath());
        MagnetMPHBuilder magnet = new MagnetMPHBuilder(cfg, srv);
        magnet.connectToServer();
        magnet.prepareModelTemplate();
        magnet.buildMPH(input.getDomains());

        // Analysis of the test model
        setInitialTemp(srv, "4.5[K]");

        String resourcePath = FT_4_T1_CircuitCoupling.class.getClassLoader().getResource(TEST_ROOT_DIR).getFile();
        FunctionalTestUtil ft = new FunctionalTestUtil(resourcePath, testLabel,toleranceREL);
        ft.runMonolithicStudy(srv);
        magnet.disconnectFromServer();
    }

    // *****************************************************************************************************************
    // Specific Methods
    // *****************************************************************************************************************

    private static void setInitialTemp(SigmaServer srv, String value){
        srv.setParamFeaturePhysics(MPHC.LABEL_PHYSICSTH, "init1", "Tinit", value);
    }
}
