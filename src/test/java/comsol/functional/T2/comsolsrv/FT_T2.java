package comsol.functional.T2.comsolsrv;

import comsol.functional.FunctionalTestUtil;
import comsol.utils.models.Input_T2_Iron_Test;
import comsol.utils.models.cable.T0.withoutThetaTp.Cable_T0_Electromagnetic;
import config.ConfigSigma;
import org.junit.*;
import server.MphSigmaServerAPI;

import java.io.IOException;

public class FT_T2 {
    private static final String JSON_PATH = "functional/T2/STEAMconfigTEST.json";
    private static final String TEST_ROOT_DIR = "functional/T2";
    private static ConfigSigma cfg;

    // *****************************************************************************************************************
    // Before-After
    // *****************************************************************************************************************

    @BeforeClass
    public static void executeBeforeClass() throws IOException {
        String resourcePath = FT_T2.class.getClassLoader().getResource(JSON_PATH).getFile();
        cfg = ConfigSigma.parseFileJSON(resourcePath);
    }

    @Before
    public void executeBeforeTest() {
        MphSigmaServerAPI.disconnect();

    }

    @After
    public void executeAfterTest() throws IOException {
        MphSigmaServerAPI.disconnect();
    }

    @AfterClass
    public static void executeAfterClass(){
    }

    // *****************************************************************************************************************
    // Tests
    // *****************************************************************************************************************

    @Test
    public void runTest_5A() throws IOException {
        String testLabel = "5A";
        double toleranceREL = 1e-3;
        System.out.println("TEST: Now running test " + testLabel+"   ************************************************");
        Input_T2_Iron_Test input = new Input_T2_Iron_Test(new Cable_T0_Electromagnetic());

        FunctionalTestUtil.executeMphFunctionalTest(testLabel, toleranceREL, FunctionalTestUtil.getResourcePath(TEST_ROOT_DIR), input.getDomains(), cfg);
    }
}
