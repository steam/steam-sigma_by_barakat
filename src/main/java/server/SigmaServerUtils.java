package server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.lang.model.SourceVersion;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Paths;

/**
 * Class represents a set of utils for SigmaServer
 */
public class SigmaServerUtils {
    private static final Logger LOGGER = LogManager.getLogger(SigmaServerUtils.class);
    /**
     * Method executes a batch file with a given execution arguments
     *
     * @param compileExecutionArguments an array of input execution arguments
     * @throws IOException in case of I/O problems, e.g., path to a file to be executed does not exist
     */
    public static void executeBatch(String[] compileExecutionArguments) throws IOException {
        Process process = Runtime.getRuntime().exec(compileExecutionArguments);
        // Read process stream
        BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream(), Charset.defaultCharset()));
        String line;
        while ((line = input.readLine()) != null) {
            LOGGER.info(line);
        }
    }

    /**
     * Method returns a file name from an input path
     *
     * @param path input path with file name
     * @return file name
     */
    public static String getFileNameFromPath(String path) {
        String fileNameWithExtension = Paths.get(path).getFileName().toString();
        return fileNameWithExtension.replaceFirst("[.][^.]+$", "");
    }

    /**
     * Method checks whether a provided class name is a syntactically valid Java class name
     *
     * @param className input class name to be chacked
     */
    public static void checkClassNameValidity(String className) {
        //https://docs.oracle.com/javase/specs/jls/se8/html/jls-3.html#jls-3.8ds
        if (!SourceVersion.isName(className)) {
            throw new RuntimeException("Invalid class name: " + className);
        }
    }

    /**
     * Method parses input path and returns file directory path
     * @param path input path to be parsed
     * @return file directory
     */
    public static String getFileDirectoryFromPath(String path) {
        return Paths.get(path).getParent().toString();
    }
}
