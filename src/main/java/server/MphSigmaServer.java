//package server;
//
//import com.comsol.model.Model;
//import com.comsol.model.util.ModelUtil;
//import comsol.constants.MPHC;
//
//import java.io.File;
//import java.io.IOException;
//
///**
// * Created by mmacieje on 01/08/2018.
// */
//public class MphSigmaServer implements SigmaServer {
//
//    Model mph;
//
//    private final String pathOutput;
//
//    public MphSigmaServer(String pathOutput) {
//        this.pathOutput = pathOutput;
//    }
//
//    public MphSigmaServer setMph(Model mph) {
//        this.mph = mph;
//        return this;
//    }
//
//    @Override
//    public void createModelNode(String labelComp) {
//        mph.modelNode().create(labelComp);
//    }
//
//    @Override
//    public void clearFile() {
//        mph.file().clear();
//    }
//
//    @Override
//    public void createGeom(String labelGeom, int dimensionality) {
//        mph.geom().create(labelGeom, dimensionality);
//    }
//
//    @Override
//    public void setGeomLabel(String labelGeom, String labelGeom2) {
//        mph.geom(labelGeom).label(labelGeom2);
//    }
//
//    @Override
//    public void setGeomRepairTol(String labelGeom, double repairTol) {
//        mph.geom(labelGeom).repairTol(repairTol);
//    }
//
//    @Override
//    public void createPhysicsNode(String labelPhysics, String typePhysics, String labelGeom) {
//        mph.physics().create(labelPhysics, typePhysics, labelGeom);
//    }
//
//    @Override
//    public void setPropertyPhysics(String labelPhysics, String nameProperty, String key, String value) {
//        mph.physics(labelPhysics).prop(nameProperty).set(key, value);
//    }
//
//    @Override
//    public void setPropertyPhysics(String labelPhysics, String nameProperty, String key, Boolean value) {
//        mph.physics(labelPhysics).prop(nameProperty).set(key, value);
//    }
//
//    @Override
//    public void setIndexFeaturePhysics(String labelPhysics, String nameFeature, String nameIndex, String key, int value) {
//        mph.physics(labelPhysics).feature(nameFeature).setIndex(nameIndex, key, value);
//    }
//
//    @Override
//    public void setFeaturePhysics(String labelPhysics, String nameFeature, String key, String value) {
//        mph.physics(labelPhysics).feature(nameFeature).set(key, value);
//    }
//
//    @Override
//    public void createTagFeaturePhysics(String labelPhysics, String labelFeature, String tag) {
//        mph.physics(labelPhysics).feature(labelFeature).tag(tag);
//    }
//
//    @Override
//    public void setLabelFeaturePhysics(String labelPhysics, String labelFeature, String label) {
//        mph.physics(labelPhysics).feature(labelFeature).label(label);
//    }
//
//    public void setParam(String parameterName, String parameterValue) {
//        mph.param().set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void createFunctionNode(String nodeName, String functionType) {
//        mph.func().create(nodeName, functionType);
//    }
//
//    @Override
//    public void setFunctionNodeLabel(String nodeName, String labelNode) {
//        mph.func(nodeName).label(labelNode);
//    }
//
//    @Override
//    public void setFunctionParam(String nodeName, String paramName, String paramValue) {
//        mph.func(nodeName).set(paramName, paramValue);
//    }
//
//    @Override
//    public void setFunctionParam(String nodeName, String paramName, String[][] paramValue) {
//        mph.func(nodeName).set(paramName, paramValue);
//    }
//
//    @Override
//    public void createSelectionGeometry(String labelGeom, String label, String labelSelection) {
//        mph.geom(labelGeom).selection().create(label, labelSelection);
//    }
//
//    @Override
//    public void setLabelSelectionGeometry(String labelGeom, String selection, String label) {
//        mph.geom(labelGeom).selection(selection).label(label);
//    }
//
//    @Override
//    public void createLabelGeometry(String labelGeom, String labelKey, String labelValue) {
//        mph.geom(MPHC.LABEL_GEOM).create(labelKey, labelValue);
//    }
//
//    @Override
//    public void setLabelFeatureGeometry(String labelGeom, String feature, String label) {
//        mph.geom(labelGeom).feature(feature).label(label);
//    }
//
//    @Override
//    public void setParamFeatureGeometry(String labelGeom, String label, String parameterName, String parameterValue) {
//        mph.geom(labelGeom).feature(label).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void setParamFeatureGeometry(String labelGeom, String label, String parameterName, String[] parameterValue) {
//        mph.geom(labelGeom).feature(label).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void createGeom(String labelGeom, String label, String value) {
//        mph.geom(labelGeom).create(label, value);
//    }
//
//    @Override
//    public void setSelectionFeatureGeom(String labelGeom, String label, String selection, String[] inputs) {
//        mph.geom(labelGeom).feature(label).selection(selection).set(inputs);
//    }
//
//    @Override
//    public void setIndexFeatureGeometry(String labelGeom, String label, String tableName, String value, int row, int col) {
//        mph.geom(labelGeom).feature(label).setIndex(tableName, value, row, col);
//    }
//
//    @Override
//    public void setParamFeatureGeometry(String labelGeom, String label, String parameterName, int[] parameterValue) {
//        mph.geom(labelGeom).feature(label).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void runLabelGeometry(String labelGeom, String label) {
//        mph.geom(labelGeom).run(label);
//    }
//
//    @Override
//    public void runPreLabelGeometry(String labelGeom, String label) {
//        mph.geom(labelGeom).runPre(label);
//    }
//
//    @Override
//    public void setParamFeatureGeometry(String labelGeom, String label, String parameterName, boolean parameterValue) {
//        mph.geom(labelGeom).feature(label).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void initSelectionFeatureGeometry(String labelGeom, String label, String selection, int value) {
//        mph.geom(labelGeom).feature(label).selection(selection).init(value);
//    }
//
//    @Override
//    public void setParamSelectionFeatureGeom(String labelGeom, String label, String selection, String parameterName, int[] parameterValue) {
//        mph.geom(labelGeom).feature(label).selection(selection).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void createHideEntitiesView(String labelView, String nodeName) {
//        mph.view(labelView).hideEntities().create(nodeName);
//    }
//
//    @Override
//    public void setGeomHideEntitiesView(String labelView, String entitiesName, String parameterName, int parameterValue) {
//        mph.view(labelView).hideEntities(entitiesName).geom(parameterName, parameterValue);
//    }
//
//    @Override
//    public void setNamedHideEntitiesView(String labelView, String entitiesName, String compName) {
//        mph.view(labelView).hideEntities(entitiesName).named(compName);
//    }
//
//    @Override
//    public void createLabelVariable(String label) {
//        mph.variable().create(label);
//    }
//
//    @Override
//    public void setLabelVariable(String variable, String label) {
//        mph.variable(variable).label(label);
//    }
//
//    @Override
//    public void setModelVariable(String label, String model) {
//        mph.variable(label).model(model);
//    }
//
//    @Override
//    public void setParamVariable(String groupLabel, String parameterName, String parameterValue) {
//        mph.variable(groupLabel).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void setGlobalSelectionVariable(String label) {
//        mph.variable(label).selection().global();
//    }
//
//    @Override
//    public void setParamGeomSelectionVariable(String label, String parameterName, int parameterValue) {
//        mph.variable(label).selection().geom(parameterName, parameterValue);
//    }
//
//    @Override
//    public void setNamedSelectionVariable(String label, String selection) {
//        mph.variable(label).selection().named(selection);
//    }
//
//    @Override
//    public void createCpl(String label, String type, String labelGeom) {
//        mph.cpl().create(label, type, labelGeom);
//    }
//
//    @Override
//    public void setLabelCpl(String cpl, String label) {
//        mph.cpl(cpl).label(label);
//    }
//
//    @Override
//    public void setParamCpl(String label, String parameterName, String parameterValue) {
//        mph.cpl(label).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void setNamedSelectionCpl(String label, String selectionLabel) {
//        mph.cpl(label).selection().named(selectionLabel);
//    }
//
//    @Override
//    public void createLabelMaterial(String label, String type, String labelComp) {
//        mph.material().create(label,type, labelComp);
//    }
//
//    @Override
//    public void setLabelMaterial(String material, String label) {
//        mph.material(material).label(label);
//    }
//
//    @Override
//    public void setNamedSelectionMaterial(String material, String selectionLabel) {
//        mph.material(material).selection().named(selectionLabel);
//    }
//
//    @Override
//    public void setParamPropertyGroupMaterial(String label, String propertyGroup, String parameterName, String parameterValue) {
//        mph.material(label).propertyGroup(propertyGroup).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void setParamPropertyGroupMaterial(String label, String propertyGroup, String parameterName, String[] parameterValue) {
//        mph.material(label).propertyGroup(propertyGroup).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void createPropertyGroupMaterial(String material, String label, String description) {
//        mph.material(material).propertyGroup().create(label, description);
//    }
//
//    @Override
//    public void createFunctionPropertyGroupMaterial(String material, String propertyGroup, String label, String type) {
//        mph.material(material).propertyGroup(propertyGroup).func().create(label, type);
//    }
//
//    @Override
//    public void setParamFuncPropertyGroupMaterial(String material, String propertyGroup, String label, String parameterName, String parameterValue) {
//        mph.material(material).propertyGroup(propertyGroup).func(label).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void setParamFuncPropertyGroupMaterial(String material, String propertyGroup, String label, String parameterName, String[][] parameterValue) {
//        mph.material(material).propertyGroup(propertyGroup).func(label).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void setIndexFeaturePhysics(String labelPhysics, String feature, String tableName, String value, int row, int col) {
//        mph.physics(labelPhysics).feature(feature).setIndex(tableName, value, row, col);
//    }
//
//    @Override
//    public void setParamFeaturePhysics(String labelPhysics, String feature, String parameterName, String parameterValue) {
//        mph.physics(labelPhysics).feature(feature).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void removeFunc(String nodeName) {
//        mph.func().remove(nodeName);
//    }
//
//    @Override
//    public void setModelFunction(String nodeName, String model) {
//        mph.func(nodeName).model(model);
//    }
//
//    @Override
//    public void setLabelFunction(String nodeName, String label) {
//        mph.func(nodeName).label(label);
//    }
//
//    @Override
//    public void setParamFunction(String nodeName, String parameterName, String parameterValue) {
//        mph.func(nodeName).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void setIndexFunction(String nodeName, String tableName, String value, int row, int col) {
//        mph.func(nodeName).setIndex(tableName, value, row, col);
//    }
//
//    @Override
//    public void setActiveVariable(String labelGroup, boolean isActive) {
//        mph.variable(labelGroup).active(isActive);
//    }
//
//    @Override
//    public void createMesh(String labelMesh, String labelGeom) {
//        mph.mesh().create(labelMesh, labelGeom);
//    }
//
//    @Override
//    public void runMesh(String labelMesh) {
//        mph.mesh(labelMesh).run();
//    }
//
//    @Override
//    public void save(String modelPath) throws IOException {
//        try {
//            File file = new File(modelPath);
//            file.delete();
//            mph.save(modelPath);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public String[] getTagsVariable() {
//        return mph.variable().tags();
//    }
//
//    @Override
//    public String[] getVarNamesVariable(String tag) {
//        return mph.variable(tag).varnames();
//    }
//
//    @Override
//    public String getTagsModelNode(int index) {
//        return mph.modelNode().tags()[index];
//    }
//
//    @Override
//    public void removeStudy(String studyName) {
//        mph.study().remove(studyName);
//    }
//
//    @Override
//    public void createStudy(String studyName) {
//        mph.study().create(studyName);
//    }
//
//    @Override
//    public void setLabelStudy(String studyName, String label) {
//        mph.study(studyName).label(label);
//    }
//
//    @Override
//    public void createStudyType(String studyName, String label, String type) {
//        mph.study(studyName).create(label, type);
//    }
//
//    @Override
//    public void setParamFeatureStudy(String studyName, String feature, String parameterName, String parameterValue) {
//        mph.study(studyName).feature(feature).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void createSol(String solutionName) {
//        mph.sol().create(solutionName);
//    }
//
//    @Override
//    public void setStudySol(String solutionName, String studyName) {
//        mph.sol(solutionName).study(studyName);
//    }
//
//    @Override
//    public void createSol(String solutionName, String feature, String type) {
//        mph.sol(solutionName).create(feature, type);
//    }
//
//    @Override
//    public void setParamFeatureSol(String solutionName, String feature, String parameterName, String parameterValue) {
//        mph.sol(solutionName).feature(feature).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void setParamFeatureSol(String solutionName, String feature, String parameterName, String[] parameterValue) {
//        mph.sol(solutionName).feature(feature).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void setParamFeatureSol(String solutionName, String feature, String parameterName, double parameterValue) {
//        mph.sol(solutionName).feature(feature).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void setParamFeatureSol(String solutionName, String feature, String parameterName, int parameterValue) {
//        mph.sol(solutionName).feature(feature).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void createFeatureSol(String solutionName, String feature, String label, String type) {
//        mph.sol(solutionName).feature(feature).create(label, type);
//    }
//
//    @Override
//    public void attachSol(String solutionName, String studyName) {
//        mph.sol(solutionName).attach(studyName);
//    }
//
//    @Override
//    public void activateFeatureStudy(String studyName, String feature, String physicsName, Boolean isActive) {
//        mph.study(studyName).feature(feature).activate(physicsName, isActive);
//    }
//
//    @Override
//    public void removeNumericalResult(String expression) {
//        mph.result().numerical().remove(expression);
//    }
//
//    @Override
//    public void createNumericalResult(String expression, String evalGlobal) {
//        mph.result().numerical().create(expression, evalGlobal);
//    }
//
//    @Override
//    public void setLabelNumericalResult(String numerical, String expression) {
//        mph.result().numerical(numerical).label(expression);
//    }
//
//    @Override
//    public void setParamNumericalResult(String numerical, String parameterName, String parameterValue) {
//        mph.result().numerical(numerical).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void setParamNumericalResult(String numerical, String parameterName, String[] parameterValue) {
//        mph.result().numerical(numerical).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void setIndexNumericalResult(String numerical, String table, String expression, int index) {
//        mph.result().numerical(numerical).setIndex(table, expression, index);
//    }
//
//    @Override
//    public double[][] getRealNumericalResult(String numerical) {
//        return mph.result().numerical(numerical).getReal();
//    }
//
//    @Override
//    public void removeFeaturePhysics(String physics, String feature) {
//        mph.physics(physics).feature().remove(feature);
//    }
//
//    @Override
//    public void createLabelPhysics(String labelPhysics, String label, String globalEquations, int index) {
//        mph.physics(labelPhysics).create(label, globalEquations, index);
//    }
//
//    @Override
//    public void removeVariable(String label) {
//        mph.variable().remove(label);
//    }
//
//    @Override
//    public void setParamFeatureStudy(String studyName, String feature, String parameterName, boolean parameterValue) {
//        mph.study(studyName).feature(feature).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void setParamFeatureStudy(String studyName, String feature, String parameterName, int parameterValue) {
//        mph.study(studyName).feature(feature).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void setParamFeatureFeatureSol(String solutionName, String feature1, String feature2, String parameterName, String parameterValue) {
//        mph.sol(solutionName).feature(feature1).feature(feature2).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void setParamFeatureFeatureSol(String solutionName, String feature1, String feature2, String parameterName, double parameterValue) {
//        mph.sol(solutionName).feature(feature1).feature(feature2).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void removeFeatureFeatureSol(String solutionName, String feature, String label) {
//        mph.sol(solutionName).feature(feature).feature().remove(label);
//    }
//
//    @Override
//    public void removeDatasetResult(String datasetName) {
//        mph.result().dataset().remove(datasetName);
//    }
//
//    @Override
//    public void showProgressModelUtil(boolean isShown) {
//        ModelUtil.showProgress(isShown);
//    }
//
//    @Override
//    public void runAllSol(String solutionName) {
//        mph.sol(solutionName).runAll();
//    }
//
//    @Override
//    public void createDatasetResult(String datasetName, String solutionName) {
//        mph.result().dataset().create(datasetName, solutionName);
//    }
//
//    @Override
//    public void setLabelDatasetResult(String datasetName, String label) {
//        mph.result().dataset(datasetName).label(label);
//    }
//
//    @Override
//    public void setParamDatasetResult(String datasetName, String parameterName, String parameterValue) {
//        mph.result().dataset(datasetName).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void createPlotFunc(String funName, String plotName) {
//        mph.func(funName).createPlot(plotName);
//    }
//
//    @Override
//    public void setIndexFunction(String nodeName, String tableName, double value, int row, int col) {
//        mph.func(nodeName).setIndex(tableName, value, row, col);
//    }
//
//    @Override
//    public void runResult(String plotName) {
//        mph.result(plotName).run();
//    }
//
//    @Override
//    public void createExportResult(String plotName, String plotGroup, String label, String comment) {
//        mph.result().export().create(plotName, plotGroup, label, comment);
//    }
//
//    @Override
//    public void setParamExportResult(String export, String parameterName, boolean parameterValue) {
//        mph.result().export(export).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void setParamExportResult(String export, String parameterName, String parameterValue) {
//        mph.result().export(export).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void runExportResult(String export) {
//        mph.result().export(export).run();
//    }
//
//    @Override
//    public void setNamedSelectionFeaturePhysics(String labelPhysics, String nodeLabel, String selectionLabel) {
//        mph.physics(labelPhysics).feature(nodeLabel).selection().named(selectionLabel);
//    }
//
//    @Override
//    public void createCoordSystem(String nodeLabel, String labelGeom, String elementType) {
//        mph.coordSystem().create(nodeLabel, labelGeom, elementType);
//    }
//
//    @Override
//    public void setLabelCoordSystem(String coordSystem, String nodeLabel) {
//        mph.coordSystem(coordSystem).label(nodeLabel);
//    }
//
//    @Override
//    public void setParamCoordSystem(String nodeLabel, String parameterName, String parameterValue) {
//        mph.coordSystem(nodeLabel).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void setNamedSelectionCoordSystem(String nodeLabel, String selectionLabel) {
//        mph.coordSystem(nodeLabel).selection().named(selectionLabel);
//    }
//
//    @Override
//    public void setFeaturePhysics(String labelPhysics, String nameFeature, String key, boolean value) {
//        mph.physics(labelPhysics).feature(nameFeature).set(key, value);
//    }
//
//    @Override
//    public void setFeaturePhysics(String labelPhysics, String nameFeature, String parameterName, String[] parameterValue) {
//        mph.physics(labelPhysics).feature(nameFeature).set(parameterName, parameterValue);
//    }
//
//    @Override
//    public void setNamedSelectionFeaturePhysics(String labelPhysics, String selectionLabel) {
//        mph.physics(labelPhysics).selection().named(selectionLabel);
//    }
//
//    @Override
//    public void setPropPhysics(String labelPhysics, String prop, String key, String value) {
//        mph.physics(labelPhysics).prop(prop).set(key, value);
//    }
//
//    @Override
//    public void setIndexPropPhysics(String labelPhysics, String prop, String table, String value, int row, int col) {
//        mph.physics(labelPhysics).prop(prop).setIndex(table, value, row, col);
//    }
//
//    @Override
//    public void setFieldFieldPhysics(String labelPhysics, String field1, String field2) {
//        mph.physics(labelPhysics).field(field1).field(field2);
//    }
//
//    @Override
//    public void setComponentFieldPhysics(String labelPhysics, String field, int index, String value) {
//        mph.physics(labelPhysics).field(field).component(index, value);
//    }
//
//    @Override
//    public void setParamFeatureGeometry(String labelGeom, String feature, String key, double value) {
//        mph.geom(labelGeom).feature(feature).set(key, value);
//    }
//
//    @Override
//    public void createFeaturePhysics(String labelPhysics, String feature, String label, String key, int value) {
//        mph.physics(labelPhysics).feature(feature).create(label, key, value);
//    }
//
//    @Override
//    public void setFeatureFeaturePhysics(String labelPhysics, String parentNode, String nodeLabel, String key, String value) {
//        mph.physics(labelPhysics).feature(parentNode).feature(nodeLabel).set(key, value);
//    }
//
//    @Override
//    public void setMaterial(String label, String family, String value) {
//        mph.material(label).set(family, value);
//    }
//
//    @Override
//    public MphSigmaServer prepareModelTemplate() {
//        mph = MphSigmaServerAPI.initializeModel(this.pathOutput);
//        mph.hist().disable();
//        mph.hist().disable();
//        mph.disableUpdates(true);
//        return this;
//    }
//
//    @Override
//    public void disconnect() {
//        MphSigmaServerAPI.disconnect();
//    }
//
//    /**
//     * Method builds a model
//     */
//    @Override
//    public void build(String fileName) {
//
//    }
//
//    /**
//     * Method compiles a model
//     */
//    @Override
//    public void compile(String modelPath) {
//
//    }
//
//    public void connect(String COMSOLBatchPath) throws IOException {
//        MphSigmaServerAPI.connect(COMSOLBatchPath);
//    }
//}
