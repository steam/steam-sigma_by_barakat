package server;

/**
 * Class provides static utils for the text-based sigma server
 */
public class TxtSigmaServerUtils {
    protected static String convertArrayToStringExpression(String[] inputs) {
        if (inputs.length == 0) {
            return "new String[]{}";
        }
        StringBuilder sb = new StringBuilder("new String[]{");
        for (String s : inputs) {
            sb.append(String.format("\"%s\", ", s));
        }
        String inputsAsString = sb.toString();
        inputsAsString = inputsAsString.substring(0, inputsAsString.length() - 2);

        return String.format("%s}", inputsAsString);
    }

    protected static String convertArrayToStringExpression(int[] inputs) {
        if (inputs.length == 0) {
            return "new int[]{}";
        }
        StringBuilder sb = new StringBuilder("new int[]{");
        for (int i : inputs) {
            sb.append(String.format("%d, ", i));
        }
        String inputsAsString = sb.toString();
        inputsAsString = inputsAsString.substring(0, inputsAsString.length() - 2);
        return String.format("%s}", inputsAsString);
    }

    protected static String convert2DStringArrayToString(String[][] array) {
        StringBuilder sb = new StringBuilder("new String[][]{");
        for (int row = 0; row < array.length; row++) {
            sb.append('{');
            for (int col = 0; col < array[0].length - 1; col++) {
                sb.append("\"" + array[row][col] + "\"");
                sb.append(", ");
            }
            sb.append("\"" + array[row][array[0].length - 1] + "\"");
            if (row < array.length - 1) {
                sb.append("}, ");
            } else {
                sb.append('}');
            }
        }
        sb.append('}');
        return sb.toString();
    }
}
