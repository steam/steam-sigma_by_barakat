//package server;
//
//import com.comsol.model.Model;
//import com.comsol.model.util.ModelUtil;
//
//import java.io.BufferedReader;
//import java.io.File;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.nio.file.Paths;
//
///**
// * Created by STEAM on 03/02/2017.
// */
//public class MphSigmaServerAPI {
//    private static final long DELAY_TO_START_MPH_SERVER = 1500; // [ms]
//    private static final long DELAY_AFTER_STOP_MPH_SERVER = 1500; // [ms]
//    private static final int PORT_LOCAL_HOST = 2036; // [ms]
//
//    private static final String TASKLIST = "tasklist";
//    private static final String KILL = "taskkill /F /IM ";
//
//    public static boolean isProcessRunning(String serviceName) throws Exception {
//        Process p = Runtime.getRuntime().exec(TASKLIST);
//        try (BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
//            String line;
//            while ((line = reader.readLine()) != null) {
//                if (line.contains(serviceName)) {
//                    System.out.println(line);
//                    return true;
//                }
//            }
//        }
//        return false;
//    }
//
//    public static void killProcess(String serviceName) throws Exception {
//        Runtime.getRuntime().exec(KILL + serviceName);
//    }
//
//    public static void connect(String comsolBatchPath) throws IOException {
//        System.out.println("SERVER: connection...");
//        connectToServer(comsolBatchPath);
//        boolean portIsFound = findPort();
//        while (!portIsFound) {
//            connectToServer(comsolBatchPath);
//            portIsFound = findPort();
//            System.out.println("SERVER: port not found, waiting 1 minute to try again.");
//            try {
//                Thread.sleep(40*DELAY_TO_START_MPH_SERVER);
//            } catch (InterruptedException ex) {
//                Thread.currentThread().interrupt();
//            }
//        }
//        if (portIsFound) {
//            System.out.println("SERVER: connected.");
//        } else {
//            System.out.println("SERVER ERROR: no ports available for connection.");
//            System.exit(0);
//        }
//    }
//
//    public static void connectToServer(String comsolBatchPath) {
//        System.out.println("SERVER: initialization...");
//        try {
//            String processName = "comsolmphserver.exe";
//
//            try {
//                if (isProcessRunning(processName)) {
//                    killProcess(processName);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            try {
//                Thread.sleep(5*DELAY_TO_START_MPH_SERVER);
//            } catch (InterruptedException ex) {
//                Thread.currentThread().interrupt();
//            }
//
//            Runtime.getRuntime ().exec(Paths.get(comsolBatchPath, "comsolmphserver") + ".exe");
//        } catch (IOException e) {
//            System.out.println("SERVER ERROR: initialization failed.");
//            e.printStackTrace();
//        }
//
//        try {
//            Thread.sleep(DELAY_TO_START_MPH_SERVER);
//        } catch (InterruptedException ex) {
//            Thread.currentThread().interrupt();
//        }
//    }
//
//
//    private static boolean findPort() {
//        boolean portIsFound = false;
//        for (int port = PORT_LOCAL_HOST; port < PORT_LOCAL_HOST + 10; port++) {
//            if (!portIsFound) {
//                try {
//                    System.out.println("SERVER: knocking port " + port + " for connection...");
//                    ModelUtil.connect("localhost", port);
//                    portIsFound = true;
//                    break;
//                } catch(Exception IO) {
//                    continue;
//                }
//            }
//        }
//        return portIsFound;
//    }
//
//    public static Model initializeModel(String modelPath) {
//        Model model = ModelUtil.create("Model");
//        model.modelPath(modelPath);
//        System.out.println("SERVER: new model is created.");
//        return model;
//    }
//
//    public static Model parseModelFromServerMPH(String modelPath) {
//        System.out.println("SERVER: Model parsing..");
//        Model model = ModelUtil.create("Model");
//        try {
//            model = ModelUtil.load("Model", modelPath);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        System.out.println("SERVER: Model parsed.");
//        return model;
//    }
//
//    public static void saveModel(Model mph, String modelPath) {
//        try {
//            File file = new File(modelPath);
//            file.delete();
//            mph.save(modelPath);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void disconnect() {
//        ModelUtil.disconnect();
//        try {
//            Thread.sleep(DELAY_AFTER_STOP_MPH_SERVER);
//            System.out.println("SERVER: Disconnected!");
//        } catch (InterruptedException ex) {
//            Thread.currentThread().interrupt();
//        }
//    }
//
//
//}
