package server;

import java.io.IOException;

/**
 * SigmaServer interface providing a set of COMSOL API methods to be either executed directly in code or to generate a text output
 */
public interface SigmaServer {
    /**
     * Method creates a model node. Model node is the most fundamental entity of a COMSOL mode
     */
    void createModelNode();

    /**
     * Method creates a component node
     */
    void createComponentNode(String labelComp);

    /**
     * Method clears a COMSOL file prior to creation of the model.
     */
    void clearFile();

    /**
     * Method creates a geometry in a COMSOL model of a given dimensionality.
     *
     * @param labelGeom      - label of geometry necessary to identify this particular node in the model tree
     * @param dimensionality - model dimensionality (0,1,2,3)
     */
    void createGeom(String labelGeom, int dimensionality);

    /**
     * Method sets geometry label and tag
     *
     * @param labelGeom  - label of geometry necessary to identify this particular node in the model tree
     * @param labelGeom2 - the same as above; COMSOL has a redundant naming system (label, name tag). We assume that label and tag are the same
     */
    void setGeomLabel(String labelGeom, String labelGeom2);

    /**
     * Method removes geometry nodes which are closer than the user-specified repair tolerance.
     * As a result, the mesh of the model should not blow-up in number of elements.
     *
     * @param labelGeom - lebal of geometry subject to repair
     * @param repairTol - value indicating the repair tolerance for a model
     */
    void setGeomRepairTol(String labelGeom, double repairTol);

    /**
     * Method creates a physics node in the model for a given physics type and geometric entity
     *
     * @param labelPhysics - label of a physics node
     * @param typePhysics  - type of physics (e.g., mf - magnetic field, ht - heat transfer)
     * @param labelGeom    - label of the geometric entity for which the physics node is create
     */
    void createPhysicsNode(String labelPhysics, String typePhysics, String labelGeom);

    /**
     * Method sets a property for a given physics label for a triplet (name, key, value)
     *
     * @param labelPhysics - label of a physics node for which property is set
     * @param nameProperty - name of a property which is set
     * @param key          - key of the property which is set
     * @param value        - String value of the parameter corresponding to key
     */
    void setPropertyPhysics(String labelPhysics, String nameProperty, String key, String value);

    /**
     * Method sets a property for a given physics label for a triplet (name, key, value)
     *
     * @param labelPhysics - label of a physics node for which property is set
     * @param nameProperty - name of a property which is set
     * @param key          - key of the property which is set
     * @param value        - Boolean value of the parameter corresponding to key
     */
    void setPropertyPhysics(String labelPhysics, String nameProperty, String key, Boolean value);

    /**
     * Method sets a property for a given physics label for a triplet (name, key, value)
     *
     * @param labelPhysics - label of a physics node for which property is set
     * @param nameFeature  - name of a feature which is set
     * @param nameIndex    - index of a feature which is set
     * @param key          - key of the property which is set
     * @param value        - int value of the parameter corresponding to key
     */
    void setIndexFeaturePhysics(String labelPhysics, String nameFeature, String nameIndex, String key, int value);

    /**
     * Method sets a feature (e.g. source term in a heat transfer physics) for a given physics present in a model.
     * The feature is set on the basis of a triplet (name, key, value)
     *
     * @param labelPhysics - label identifying physics for which a feature is set
     * @param nameFeature  - name of a property which is set
     * @param key          - key of the property which is set
     * @param value        - String value of the parameter corresponding to key
     */
    void setFeaturePhysics(String labelPhysics, String nameFeature, String key, String value);

    /**
     * Method creates a tag for a given physics feature
     *
     * @param labelPhysics - label identifying physics for which the tag is created
     * @param labelFeature - name of a property which the tag is created
     * @param tag          - String value of the tag to be created
     */
    void createTagFeaturePhysics(String labelPhysics, String labelFeature, String tag);

    /**
     * Method sets a label for a given physics feature
     *
     * @param labelPhysics - label identifying physics for which the label is set
     * @param labelFeature - name of a property which the label is set
     * @param label        - String value of the label to be set
     */
    void setLabelFeaturePhysics(String labelPhysics, String labelFeature, String label);

    /**
     * Method sets a parameter in the global definition of a model as a (name, value) pair
     *
     * @param parameterName         - name of the parameter to be set
     * @param parameterValue        - value of the parameter to be set (may include unit)
     * @param parameterDescription  - description of parameter
     */
    void setParam(String parameterName, String parameterValue, String parameterDescription);
    //void setParam(String parameterName, String parameterValue);

    /**
     * Method creates a function node in the global definition of a model
     *
     * @param nodeName     - name of the function node
     * @param functionType - function type (e.g., external C/MATLAB function, look-up table, etc.)
     */
    void createFunctionNode(String nodeName, String functionType);

    /**
     * Method sets a label for a given function node in the global definition of a model
     *
     * @param nodeName  - name of the function node
     * @param labelNode - label to be assigned to the function node
     */
    void setFunctionNodeLabel(String nodeName, String labelNode);

    /**
     * Method sets scalar parameters (e.g., function arguments, path to a C-function, plotting arguments)
     * for a function node in the global definition of a model
     *
     * @param nodeName   - name of the function node
     * @param paramName  - name of the parameter to be set
     * @param paramValue - value of the parameter to be set
     */
    void setFunctionParam(String nodeName, String paramName, String paramValue);

    /**
     * Method sets parameters (e.g., function arguments, path to a C-function, plotting arguments) in a form of a 2D array
     * for a function node in the global definition of a model
     *
     * @param nodeName   - name of the function node
     * @param paramName  - name of the parameter to be set
     * @param paramValue - value of the parameter to be set
     */
    void setFunctionParam(String nodeName, String paramName, String[][] paramValue);

    /**
     * Method creates a selection for a given geometry node
     *
     * @param labelGeom      - label of the geometry node
     * @param label          - label of the geometric entity for which the selection is created
     * @param labelSelection - label representing the type of the selection (e.g. CumulativeSelection)
     */
    void createSelectionGeometry(String labelGeom, String label, String labelSelection);

    /**
     * Method sets label for the selection
     *
     * @param labelGeom - label of the geometry node in the model
     * @param selection - name of the selection
     * @param label     - label of the selection to be set
     */
    void setLabelSelectionGeometry(String labelGeom, String selection, String label);

    /**
     * Method creates a label in a given geometry node of the model
     *
     * @param labelGeom  - label of the geometry node in the model
     * @param labelKey   - key corresponding to the label
     * @param labelValue - value corresponding to the label
     */
    void createLabelGeometry(String labelGeom, String labelKey, String labelValue);
    //void createLabelGeometry2(String compLabel, String labelGeom, String labelKey, String labelValue);

    /**
     * Method sets a label for a feature in a given geometry of the model
     *
     * @param labelGeom - label of the geometry node in the model
     * @param feature   - selected feature to be assigned a label
     * @param label     - label to be set
     */
    void setLabelFeatureGeometry(String labelGeom, String feature, String label);

    /**
     * Method sets a scalar parameter for a feature in the model geometry
     *
     * @param labelGeom      - label of the geometry node in the model
     * @param label          - label of the feature for which a parameter is set
     * @param parameterName  - name of the parameter to be set
     * @param parameterValue - value of the parameter to be set
     */
    void setParamFeatureGeometry(String labelGeom, String label, String parameterName, String parameterValue);

    /**
     * Method sets parameter as an array for a feature in the model geometry
     *
     * @param labelGeom      - label of the geometry node in the model
     * @param label          - label of the feature for which a parameter is set
     * @param parameterName  - name of the parameter to be set
     * @param parameterValue - value of the parameter to be set
     */
    void setParamFeatureGeometry(String labelGeom, String label, String parameterName, String[] parameterValue);

    // maybe this doesnt work
    void setParamFeatureGeometry(String labelGeom, String label, String parameterName, String[][] parameterValue);




    /**
     * Method creates a geometry label of a given value
     *
     * @param labelGeom - label of the geometry node in the model
     * @param label     - label to be created
     * @param value     - value of the label (e.g., ConvertToSolid)
     */
    void createGeom(String labelGeom, String label, String value);

    /**
     * Method sets inputs for a geometric selection
     *
     * @param labelGeom - label of the geometry node in the model
     * @param label     - label representing a feature
     * @param selection - label representing selection
     * @param inputs    - inputs to be set for a given selection and label
     */
    void setSelectionFeatureGeom(String labelGeom, String label, String selection, String[] inputs);

    /**
     * Method sets a value for a given index of a feature table
     *
     * @param labelGeom - label of the geometry node in the model
     * @param label     - label representing a feature
     * @param tableName - name of the feature table
     * @param value     - value to be assigned
     * @param row       - row of an index
     * @param col       - column of an index
     */
    void setIndexFeatureGeometry(String labelGeom, String label, String tableName, String value, int row, int col);

    /**
     * Method sets a parameter as an int array for a feature in the model geometry
     *
     * @param labelGeom      - label of the geometry node in the model
     * @param label          - label of the feature for which a parameter is set
     * @param parameterName  - name of the parameter to be set
     * @param parameterValue - value of the parameter to be set
     */
    void setParamFeatureGeometry(String labelGeom, String label, String parameterName, int[] parameterValue);

    /**
     * Method runs a given label of the geometry node
     *
     * @param labelGeom - label of the geometry node in the model
     * @param label     - label of the feature for which a parameter is set
     */
    void runLabelGeometry(String labelGeom, String label);

    /**
     * Method pre-runs a given label of the geometry node
     *
     * @param labelGeom - label of the geometry node in the model
     * @param label     - label of the feature for which a parameter is set
     */
    void runPreLabelGeometry(String labelGeom, String label);

    /**
     * Method sets a scalar, boolean parameter for a feature in the model geometry
     *
     * @param labelGeom      - label of the geometry node in the model
     * @param label          - label of the feature for which a parameter is set
     * @param parameterName  - name of the parameter to be set
     * @param parameterValue - value of the parameter to be set
     */
    void setParamFeatureGeometry(String labelGeom, String label, String parameterName, boolean parameterValue);

    /**
     * Method initializes a geometry selection with a given input value
     *
     * @param labelGeom - label of the geometry node in the model
     * @param label     - label of the feature for which an input is set
     * @param selection - label of the selection
     * @param value     - input value
     */
    void initSelectionFeatureGeometry(String labelGeom, String label, String selection, int value);

    /**
     * Method creates Explicit Selection node inside Definitions node:
     * @param compLabel - Label for the name of the component to which Explicit Selections must be assigned
     * @param nodeName  - Label for the name of the Explicit Selections node
     * @param nodeLabel - Label for the Explicit Selections node
     */
    void createExplicitSelectionNode(String compLabel, String nodeName, String nodeLabel);

    /**
     * Method assigns Explicit Selection to Definitions with Geoemetry node as reference:
     * @param compLabel - Label for the name of the component to which Explicit Selections must be assigned
     * @param nodeName  - Label for the name of the Explicit Selections node
     * @param depth     - 0 = domain, 1 = boundary, 2 = point
     */
    void setExplicitSelectionNodetoGeometry(String compLabel, String nodeName, int depth);

    /**
     * Method takes the boundaries for which an Explicit Selection must be made:
     * @param compLabel - Label for the name of the component to which Explicit Selections must be assigned
     * @param selLabel  - Label for the selection of boundaries
     * @param input     - Input of boundaries
     */
    void setExplicitSelectionBoundary(String compLabel, String selLabel, String input);

    /**
     * Method sets parameter as a (key, value) pair for a given feature selection in a geometry node of the model
     *
     * @param labelGeom      - label of the geometry node in the model
     * @param label          - label of the feature for which an input is set
     * @param selection      - label of the selection
     * @param parameterName  - name of the parameter to be set
     * @param parameterValue - input value of the parameter
     */
    void setParamSelectionFeatureGeom(String labelGeom, String label, String selection, String parameterName, int[] parameterValue);

    /**
     * Method sets parameter as a (key, value) pair for a given feature selection in a geometry node of the model
     *
     * @param labelGeom      - label of the geometry node in the model
     * @param label          - label of the feature for which an input is set
     * @param selection      - label of the selection
     * @param parameterValue - input value of the parameter
     */
    void setParamSelectionFeatureGeom(String labelGeom, String label, String selection, String parameterValue);


    /**
     * Method creates a view that hides geometric entities in the model
     *
     * @param labelView - label of a view containing hidden entities
     * @param nodeName  - name of the node with hidden entities
     */
    void createHideEntitiesView(String labelView, String nodeName);

    /**
     * Method sets the hide view to a given geometric node in the model
     *
     * @param labelView      - label of a view containing hidden entities
     * @param entitiesName   - name of the node with hidden entities
     * @param parameterName  - parameter name, name of the geometry node
     * @param parameterValue - parameter value
     */
    void setGeomHideEntitiesView(String labelView, String entitiesName, String parameterName, int parameterValue);

    /**
     * Method sets named geometric entities to the hidden view
     *
     * @param labelView    - label of a view containing hidden entities
     * @param entitiesName - name of the node with hidden entities
     * @param compName     - name of components to be hidden
     */
    void setNamedHideEntitiesView(String labelView, String entitiesName, String compName);

    /**
     * Method creates a variable with a given name
     *
     * @param label - variable name
     */
    void createLabelVariable(String label);
    //void createLabelAnalyticFunction(String label); //************************************************************************** analytic function

    /**
     * Method sets a COMSOL label to a variable name
     *
     * @param variable - variable name
     * @param label    - variable label
     */
    void setLabelVariable(String variable, String label);
    //void setLabelAnalyticFunction(String variable, String label);

    /**
     * Method sets variable with a given label to a model component
     *
     * @param label - variable name
     * @param model - name of the model component
     */
    void setModelVariable(String label, String model); // parameter added
    //void setModelAnalyticFunction(String label, String model); // parameter added

    //void createComponentTag(String labelComp, String componentTag);


    /**
     * Method sets a parameter as a (key,value) pair for a variable
     *
     * @param groupLabel     - variable name
     * @param parameterName  - input parameter name
     * @param parameterValue - input parameter value
     */
    void setParamVariable(String groupLabel, String parameterName, String parameterValue);

    /**
     * Method sets a variable to a global model selection
     *
     * @param label - variable name
     */
    void setGlobalSelectionVariable(String label);

    /**
     * Method sets variable to a geometric selection
     *
     * @param label          - variable name
     * @param parameterName  - geomatric label (usually main geometry node)
     * @param parameterValue - parameter value (dimensionality)
     */
    void setParamGeomSelectionVariable(String label, String parameterName, int parameterValue);

    /**
     * Method sets a variable for a named selection
     *
     * @param label     - variable name
     * @param selection - selection name
     */
    void setNamedSelectionVariable(String label, String selection);

    /**
     * Method creates component coupling of a given type and assigns it to a geometric entity of the model
     *
     * @param label     - component coupling name
     * @param type      - component coupling type
     * @param labelGeom - label of the geometric entity for which the component coupling is created
     */
    void createCpl(String label, String type, String labelGeom);

    /**
     * Method sets a label for a component coupling
     *
     * @param cpl   - component coupling name
     * @param label - component coupling label
     */
    void setLabelCpl(String cpl, String label);

    /**
     * Method sets a parameter given as a (key, value) pair to a component coupling
     *
     * @param label          - component coupling name
     * @param parameterName  - input parameter name
     * @param parameterValue - input parameter value
     */
    void setParamCpl(String label, String parameterName, String parameterValue);

    /**
     * Method sets a named selection to a component coupling
     *
     * @param label          - component coupling label
     * @param selectionLabel - named selection label
     */
    void setNamedSelectionCpl(String label, String selectionLabel);

    /**
     * Method creates material label of a given type to a model component
     *
     * @param label     - material name
     * @param type      - material type
     * @param labelComp - component name
     */
    void createLabelMaterial(String label, String type, String labelComp);

    /**
     * Method sets a label for a material
     *
     * @param material - material name
     * @param label    - material label
     */
    void setLabelMaterial(String material, String label);

    /**
     * Method sets material for a named selection
     *
     * @param material       - material label
     * @param selectionLabel - named selection label
     */
    void setNamedSelectionMaterial(String material, String selectionLabel);

    /**
     * Method sets a scalar parameter as a (key, value) pair to a property group of a material
     *
     * @param label          - material label
     * @param propertyGroup  - name of the property group
     * @param parameterName  - parameter name
     * @param parameterValue - parameter value
     */
    void setParamPropertyGroupMaterial(String label, String propertyGroup, String parameterName, String parameterValue);

    /**
     * Method sets a 2D parameter as a (key, value) pair to a property group of a material
     *
     * @param label          - material label
     * @param propertyGroup  - name of the property group
     * @param parameterName  - parameter name
     * @param parameterValue - parameter value
     */
    void setParamPropertyGroupMaterial(String label, String propertyGroup, String parameterName, String[] parameterValue);

    /**
     * Method creates a property group for a material
     *
     * @param material    - material label
     * @param label       - property group label
     * @param description - property group description
     */
    void createPropertyGroupMaterial(String material, String label, String description);

    /**
     * Method creates a function property group for a material
     *
     * @param material      - material label
     * @param propertyGroup - property group label
     * @param label         - function label
     * @param type          - function type
     */
    void createFunctionPropertyGroupMaterial(String material, String propertyGroup, String label, String type);

    /**
     * Method sets a scalar parameter as a (key, value) pair for a function property group of a material
     *
     * @param material       - material label
     * @param propertyGroup  - property group label
     * @param label          - function label
     * @param parameterName  - parameter name
     * @param parameterValue - parameter value
     */
    void setParamFuncPropertyGroupMaterial(String material, String propertyGroup, String label, String parameterName, String parameterValue);

    /**
     * Method sets a 2D parameter as a (key, value) pair for a function property group of a material
     *
     * @param material       - material label
     * @param propertyGroup  - property group label
     * @param label          - function label
     * @param parameterName  - parameter name
     * @param parameterValue - parameter value
     */
    void setParamFuncPropertyGroupMaterial(String material, String propertyGroup, String label, String parameterName, String[][] parameterValue);

    /**
     * Method sets a value of a parameter in a table for a physics feature
     *
     * @param labelPhysics - physics label
     * @param feature      - feature label
     * @param tableName    - table name
     * @param value        - input parameter value
     * @param row          - table row index
     * @param col          - table column index
     */
    void setIndexFeaturePhysics(String labelPhysics, String feature, String tableName, String value, int row, int col);

    /**
     * Method sets a scalar parameter as a (key, value) pair to a property group of a physics
     *
     * @param labelPhysics   - physics label
     * @param feature        - feature label
     * @param parameterName  - parameter name
     * @param parameterValue - parameter value
     */
    void setParamFeaturePhysics(String labelPhysics, String feature, String parameterName, String parameterValue);

    /**
     * Method removes a node with a given function name
     *
     * @param nodeName - function node name
     */
    void removeFunc(String nodeName);

    /**
     * Method sets a function to a model component
     *
     * @param nodeName - function node name
     * @param model    - name of a component node in the model
     */
    void setModelFunction(String nodeName, String model);

    /**
     * Method sets label for a function node
     *
     * @param nodeName - function node name
     * @param label    - function label
     */
    void setLabelFunction(String nodeName, String label);

    /**
     * Method sets a parameter as a (key, value) pair for a component function
     *
     * @param nodeName       - function node name
     * @param parameterName  - input parameter name
     * @param parameterValue - input parameter value
     */
    void setParamFunction(String nodeName, String parameterName, String parameterValue);

    /**
     * Method sets a value of a parameter in a table for a component function
     *
     * @param nodeName  - function node name
     * @param tableName - table name
     * @param value     - input parameter value
     * @param row       - table row index
     * @param col       - table column index
     */
    void setIndexFunction(String nodeName, String tableName, String value, int row, int col);

    /**
     * Method sets a flag to activate/deactivate function given by a function label
     * @param functionLabel - Label for the function to be (de)activated
     * @param isActive      - Boolean pass value (true-active, false-inactive)
     */
    void setActiveFunction(String functionLabel, boolean isActive);
    /**
     * Method sets a flag to activate/deactivate variable given by a label group
     *
     * @param labelGroup - variable group label
     * @param isActive   - boolean flag (true-active, false-inactive)
     */
    void setActiveVariable(String labelGroup, boolean isActive);

    /**
     * Method creates a mesh for a geometry entity
     *
     * @param labelMesh - label of a mesh to be created
     * @param labelGeom - label of geometric entity for which mesh is created
     */
    void createMesh(String labelMesh, String labelGeom);

    /**
     * Method creates a new mesh Feature in an existing Mesh node:
     * @param labelMesh     - Label for the Mesh node
     * @param labelFeature  - Label for the Feature to which a new feature is to be assigned
     * @param labelType     - Label for the new Fature
     * @param type          - Name of the new feature (COMSOL Recognized)
     */
    void createMeshFeature(String labelMesh, String labelFeature, String labelType, String type);

    /**
     * Method sets the name and label for a mesh node
     * @param labelMesh - label and name for mesh node
     */
    void setLabelMesh(String labelMesh);

    /**
     * Method creates a userdefined mesh type to an existing mesh node
     * @param labelMesh - Label for the Mesh node
     * @param labelType - Label for the type of Mesh to be defined
     * @param type      - Type of the label to be added (COMSOL recognized)
     */
    void  setMeshType(String labelMesh, String labelType, String type);

    /**
     * Method creates a Mesh Feature
     * @param labelMesh         - Label for the Mesh node
     * @param labelFeature      - Label for the Mesh Feature
     * @param labelSubFeature   - Label for the Mesh subfeature
     * @param labelType         - Feature to be added
     * @param value             - value
     */
    void setMeshFeature(String labelMesh, String labelFeature, String labelSubFeature, String labelType, int value);

    /**
     * Method assigns a Mesh feature to a selection
     * @param labelMesh         - Label for the Mesh node
     * @param labelFeature      - Label for the Mesh Feasture
     * @param labelSubFeature   - Label for the Mesh subfeature
     * @param labelSelection    - Label for the selection for which a feature is to be assigned
     */
    void assignMeshFeaturetoSelection(String labelMesh, String labelFeature, String labelSubFeature, String labelSelection);

    /**
     * Method assigns a mesh feature to a geometric level entity of a certain depth:
     * @param labelMesh     - Label for the Mesh node
     * @param labelFeature  - Label for the mesh feature to be assigned to a geometric level entity
     * @param labelGeom     - Label for the geometry node
     * @param depth         - 0 ("Remaining"), 1 ("Entire Geometry"), 2 ("Domain")
     */
    void setMeshGeometricLevel(String labelMesh, String labelFeature, String labelGeom, int depth);

    /**
     * Method assigns a Mesh feature to a named selection
     * @param labelMesh      - Label for the Mesh node
     * @param labelFeature   - Label for the mesh feature to be assigned to a selection
     * @param labelSelection - Label for the selection
     */
    void assignMeshtoNamedSelection(String labelMesh, String labelFeature, String labelSelection);

    /**
     * Method sets mesh size
     * @param labelMesh  - Label for the Mesh node
     * @param MeshSize   - Label for the size:
     *      MeshSize = 1    => Extremely fine mesh
     *      MeshSize = 2    => Extra fine mesh
     *      MeshSize = 3    => Finer mesh
     *      MeshSize = 4    => Fine mesh
     *      MeshSize = 5    => Normal mesh
     *      MeshSize = 6    => Coarse mesh
     *      MeshSize = 7    => Coarser mesh
     *      MeshSize = 8    => Extra coarse mesh
     *      MeshSize = 9    => Extremely coarse mesh
     */
    void setMeshSize(String labelMesh, int MeshSize);

    /**
     * Method runs mesh (builds mesh for a model)
     *
     * @param labelMesh - label of a mesh to be built
     */
    void runMesh(String labelMesh);

    /**
     * Method save a model at a given absolute path
     *
     * @param modelPath - an absolute path to an output model
     * @throws IOException - thrown in case of I/O problems, e.g., path does not exist, insufficient rights, etc.
     */
    void save(String modelPath) throws IOException;

    /**
     * Method returns all tags for a given component variable
     *
     * @return - an array of tags for a given component variable
     */
    String[] getTagsVariable();

    /**
     * Method returns an array of variable names for a given tag
     *
     * @param tag - input tag
     * @return - an array of variable names
     */
    String[] getVarNamesVariable(String tag);

    /**
     * Method returns the tag of the model node at a given index
     *
     * @param index - index of a tag
     * @return - value of the tag at a given index
     */
    String getTagsModelNode(int index);

    /**
     * Method removes a study of a given name
     *
     * @param studyName - study name
     */
    void removeStudy(String studyName);

    /**
     * Method creates a study with a given name
     *
     * @param studyName - study name
     */
    void createStudy(String studyName);

    /**
     * Method sets a label for a study
     *
     * @param studyName - study name
     * @param label     - study label
     */
    void setLabelStudy(String studyName, String label);

    /**
     * Method creates a study of a given type
     *
     * @param studyName - study name
     * @param label     - study label
     * @param type      - study type
     */
    void createStudyType(String studyName, String label, String type);

    /**
     * Method sets a parameter given as a (key, value) pair for a study feature
     *
     * @param studyName      - study name
     * @param feature        - feature label
     * @param parameterName  - input parameter name
     * @param parameterValue - input parameter value
     */
    void setParamFeatureStudy(String studyName, String feature, String parameterName, String parameterValue);

    /**
     * Method creates a solution
     *
     * @param solutionName - solution name
     */
    void createSol(String solutionName);

    /**
     * Method sets a solution to a study
     *
     * @param solutionName - name of a solution
     * @param studyName    - name of a study
     */
    void setStudySol(String solutionName, String studyName);

    /**
     * Method creates a feature of a given type for a solution
     *
     * @param solutionName - solution name
     * @param feature      - feature label
     * @param type         - feature type
     */
    void createSol(String solutionName, String feature, String type);

    /**
     * Method sets a scalar parameter given as a (key, value) pair to a feature of a solution
     *
     * @param solutionName   - solution name
     * @param feature        - feature label
     * @param parameterName  - input parameter name
     * @param parameterValue - input parameter value
     */
    void setParamFeatureSol(String solutionName, String feature, String parameterName, String parameterValue);

    /**
     * Method sets a 2D parameter given as a (key, value) pair to a feature of a solution
     *
     * @param solutionName   - solution name
     * @param feature        - feature label
     * @param parameterName  - input parameter name
     * @param parameterValue - input parameter value
     */
    void setParamFeatureSol(String solutionName, String feature, String parameterName, String[] parameterValue);

    /**
     * Method sets a scalar parameter given as a (key, value) pair to a feature of a solution
     *
     * @param solutionName   - solution name
     * @param feature        - feature label
     * @param parameterName  - input parameter name
     * @param parameterValue - input parameter value
     */
    void setParamFeatureSol(String solutionName, String feature, String parameterName, double parameterValue);

    /**
     * Method sets a scalar parameter given as a (key, value) pair to a feature of a solution
     *
     * @param solutionName   - solution name
     * @param feature        - feature label
     * @param parameterName  - input parameter name
     * @param parameterValue - input parameter value
     */
    void setParamFeatureSol(String solutionName, String feature, String parameterName, int parameterValue);

    /**
     * Method creates a parameter, of a given label and type, for a feature of a solution
     *
     * @param solutionName - solution name
     * @param feature      - feature label
     * @param label        - parameter label
     * @param type         - parameter type
     */
    void createFeatureSol(String solutionName, String feature, String label, String type);

    /**
     * Method attaches a solution to a study
     *
     * @param solutionName - solution label
     * @param studyName    - study label
     */
    void attachSol(String solutionName, String studyName);

    /**
     * Method activates a physics for a feature of a study
     *
     * @param studyName   - study label
     * @param feature     - feature label
     * @param physicsName - name of a physics
     * @param isActive    - input boolean flag (true - active, false - inactive)
     */
    void activateFeatureStudy(String studyName, String feature, String physicsName, Boolean isActive);

    /**
     * Method removes a numerical result
     *
     * @param expression - expression corresponding to a numerical result to be removed
     */
    void removeNumericalResult(String expression);

    /**
     * Method creates a numerical result fo a given type
     *
     * @param expression - expression corresponding to a numerical result
     * @param evalGlobal - type of a numerical result
     */
    void createNumericalResult(String expression, String evalGlobal);

    /**
     * Method sets a label for a numerical result
     *
     * @param numerical  - label of the numerical result
     * @param expression - expression of the numerical result
     */
    void setLabelNumericalResult(String numerical, String expression);

    /**
     * Method sets a scalar parameter for a numerical result
     *
     * @param numerical      - label of a numerical result
     * @param parameterName  - input parameter name
     * @param parameterValue - input parameter value
     */
    void setParamNumericalResult(String numerical, String parameterName, String parameterValue);

    /**
     * Method sets a 2D parameter for a numerical result
     *
     * @param numerical      - label of a numerical result
     * @param parameterName  - input parameter name
     * @param parameterValue - input parameter value
     */
    void setParamNumericalResult(String numerical, String parameterName, String[] parameterValue);

    /**
     * Method sets a value of a table of numerical result at a given index
     *
     * @param numerical  - label of a numerical result
     * @param table      - name of a table
     * @param expression - expression of a numerical result to be set
     * @param index      - index in the table
     */
    void setIndexNumericalResult(String numerical, String table, String expression, int index);

    /**
     * Method returns a numerical result
     *
     * @param expression - expression of a numerical result to be returned
     * @return - 2D array with values corresponding to the numerical result
     */
    double[][] getRealNumericalResult(String expression);

    /**
     * Method removes a feature from a physics
     *
     * @param physics - physics label
     * @param feature - feature label
     */
    void removeFeaturePhysics(String physics, String feature);

    /**
     * Method creates a label for a physics feature with a given name at a given index
     *
     * @param labelPhysics    - physics label
     * @param label           - feature label
     * @param globalEquations - label to be created
     * @param index           - index of the label
     */
    void createLabelPhysics(String labelPhysics, String label, String globalEquations, int index);

    /**
     * Method removes a variable with a given label
     *
     * @param label - label of the variable to be removed
     */
    void removeVariable(String label);

    /**
     * Method sets a scalar parameter given as a (key, value) pair for a feature of a study
     *
     * @param studyName      - name of a study
     * @param feature        - feature label
     * @param parameterName  - input parameter name
     * @param parameterValue - input parameter value
     */
    void setParamFeatureStudy(String studyName, String feature, String parameterName, boolean parameterValue);

    /**
     * Method sets a scalar parameter given as a (key, value) pair for a feature of a study
     *
     * @param studyName      - name of a study
     * @param feature        - feature label
     * @param parameterName  - input parameter name
     * @param parameterValue - input parameter value
     */
    void setParamFeatureStudy(String studyName, String feature, String parameterName, int parameterValue);

    /**
     * Method sets a scalar parameter given as a (key, value) pair for a double nested feature of a solution
     *
     * @param solutionName   - solution label
     * @param feature1       - feature label (1st level)
     * @param feature2       - feature label (2nd level)
     * @param parameterName  - input parameter name
     * @param parameterValue - input parameter value
     */
    void setParamFeatureFeatureSol(String solutionName, String feature1, String feature2, String parameterName, String parameterValue);

    /**
     * Method sets a scalar parameter given as a (key, value) pair for a double nested feature of a solution
     *
     * @param solutionName   - solution label
     * @param feature1       - feature label (1st level)
     * @param feature2       - feature label (2nd level)
     * @param parameterName  - input parameter name
     * @param parameterValue - input parameter value
     */
    void setParamFeatureFeatureSol(String solutionName, String feature1, String feature2, String parameterName, double parameterValue);

    /**
     * Method removes a double nested feature from a solution
     *
     * @param solutionName - solution label
     * @param feature      - feature label (1st level)
     * @param label        - feature label (2nd level)
     */
    void removeFeatureFeatureSol(String solutionName, String feature, String label);

    //******************************************************************************************************************

    /**
     * Method creates a Result node
     * @param resultLabel - Label for the result node
     * @param resultType - Label for the result node to be created
     */
    void createResult(String resultLabel, String resultType);

    /**
     * Method creates a sub result and assigns it to a result node
     * @param resultLabel        - Existing result node, to which subresult must be assigned
     * @param resultFeatureLabel - label for the sub result to be created
     * @param resultFeature - the type of sub result to be created
     */
    void setResultType(String resultLabel, String resultFeatureLabel, String resultFeature);

    /**
     * Method sets label for a Result node
     * @param resultLabel - Result node label
     */
    void setResultLabel(String resultLabel);

    /**
     * Method sets feature at index for a result node
     * @param resultLabel - label for the result node, to which feature is to be added
     * @param featureLabel - label for the feature to be added
     * @param featureType - COMSOL feature label for the feature to be added
     * @param featureExpression - the expression to be added
     * @param depth - location of the feature to be added
     */
    void setResultFeatureIndex(String resultLabel, String featureLabel, String featureType, String featureExpression, int depth);

    /**
     * Method sets a result feature for a given result node
     * @param resultLabel   - label for result note for which feature is set
     * @param featureLabel - label for the feature to be set
     * @param featureType - COMSOL feature to be set
     * @param featureExpression - expression of the feature to be set
     */
    void setResultFeature(String resultLabel, String featureLabel, String featureType, String featureExpression);

    /**
     * Method sets a result feature for a given result node
     * @param resultLabel   - label for result note for which feature is set
     * @param featureLabel - label for the feature to be set
     * @param featureType - COMSOL feature to be set
     * @param isShown - true or false
     */
    void setResultFeature(String resultLabel, String featureLabel, String featureType, boolean isShown);

    /**
     * Method sets a result feature for a given result node
     * @param resultLabel   - label for result note for which feature is set
     * @param featureLabel - label for the feature to be set
     * @param featureType - COMSOL feature to be set
     * @param Value - Value of the feature to be added
     */
    void setResultFeature(String resultLabel, String featureLabel, String featureType, int Value);

    /**
     * Method sets result
     * @param resultLabel - Result node label
     * @param featureLabel - label for the feature
     * @param resultFeature - the feature
     */
    void setResult(String resultLabel, String featureLabel, String resultFeature);

    /**
     * Assigns results feature to all domains as selection
     * @param resultLabel - label for the result nde
     * @param featureLabel - label for the feature
     */
    void assignResultSelectiontoAll(String resultLabel, String featureLabel);


    //******************************************************************************************************************
    /**
     * Method removes a dataset result from a study
     *
     * @param datasetName - name of a dataset to be removed
     */
    void removeDatasetResult(String datasetName);

    /**
     * Method shows a progress bar during a model simulation
     *
     * @param isShown - boolean flag controlling the visibility of the progress bar (true-visible, false-invisible)
     */
    void showProgressModelUtil(boolean isShown);

    /**
     * Method runs all solutions
     *
     * @param solutionName - name of a solution to be executed
     */
    void runAllSol(String solutionName);

    /**
     * Method creates a dataset result with a given name for a given study
     *
     * @param datasetName  - name of a dataset to be created
     * @param solutionName - solution name for which the results are extracted
     */
    void createDatasetResult(String datasetName, String solutionName);

    /**
     * Method sets a label to a dataset result
     *
     * @param datasetName - name of a dataset
     * @param label       - label to be assigned to a dataset
     */
    void setLabelDatasetResult(String datasetName, String label);

    /**
     * Method sets a scalar parameter given as a (name, value) pair to a dataset result
     *
     * @param datasetName    - name of a dataset
     * @param parameterName  - input parameter name
     * @param parameterValue - input parameter value
     */
    void setParamDatasetResult(String datasetName, String parameterName, String parameterValue);

    /**
     * Method creates a plot function
     *
     * @param funName  - function name
     * @param plotName - plot name
     */
    void createPlotFunc(String funName, String plotName);

    /**
     * Method sets a value for a function table at a given index (row and column)
     *
     * @param nodeName  - function node name
     * @param tableName - table name
     * @param value     - input value to be set
     * @param row       - row index
     * @param col       - column index
     */
    void setIndexFunction(String nodeName, String tableName, double value, int row, int col);

    /**
     * Method runs a plot group
     *
     * @param plotName - name of the plot to be run (generated)
     */
    void runResult(String plotName);

    /**
     * Method creates an export result
     *
     * @param plotName   - name of a plot to be created
     * @param plotGroup  - name of a plot group to which the plot corresponds
     * @param label      - label for the plot
     * @param expression - expression to be evaluated
     */
    void createExportResult(String plotName, String plotGroup, String label, String expression);

    /**
     * Method sets a parameter given as a (name, value) pair for an export result
     *
     * @param export         - label of the export result
     * @param parameterName  - input parameter name
     * @param parameterValue - input parameter value
     */
    void setParamExportResult(String export, String parameterName, boolean parameterValue);

    /**
     * Method sets a parameter given as a (name, value) pair for an export result
     *
     * @param export         - label of the export result
     * @param parameterName  - input parameter name
     * @param parameterValue - input parameter value
     */
    void setParamExportResult(String export, String parameterName, String parameterValue);

    /**
     * Method runs export results for a model
     *
     * @param export - label of the export result
     */
    void runExportResult(String export);

    /**
     * Method sets a named selection for a physics feature
     *
     * @param labelPhysics   - label of physics
     * @param nodeLabel      - feature label
     * @param selectionLabel - named selection label
     */
    void setNamedSelectionFeaturePhysics(String labelPhysics, String nodeLabel, String selectionLabel);

    /**
     * Method creates a coordinate system for a geometric entity
     *
     * @param nodeLabel   - coordinate system label
     * @param labelGeom   - geometry label
     * @param elementType - element type (e.g., InfiniteElement)
     */
    void createCoordSystem(String nodeLabel, String labelGeom, String elementType);

    /**
     * Method sets a label for the coordinate system
     *
     * @param coordSystem - coordinate system name
     * @param nodeLabel   - input coordinate system label
     */
    void setLabelCoordSystem(String coordSystem, String nodeLabel);

    /**
     * Method sets a parameter given as a (name, value) pair for a coordinate system
     *
     * @param nodeLabel      - label of a coordinate system node
     * @param parameterName  - input parameter name
     * @param parameterValue - input parameter value
     */
    void setParamCoordSystem(String nodeLabel, String parameterName, String parameterValue);

    /**
     * Method sets coordinate system for a named selection
     *
     * @param nodeLabel      - label of a coordinate system node
     * @param selectionLabel - named selection label
     */
    void setNamedSelectionCoordSystem(String nodeLabel, String selectionLabel);

    /**
     * Method sets a feature for a physics as a (key, value) pair
     *
     * @param labelPhysics - physics label
     * @param nameFeature  - feature label
     * @param key          - input feature key
     * @param value        - input feature scalar value
     */
    void setFeaturePhysics(String labelPhysics, String nameFeature, String key, boolean value);

    /**
     * Method sets a feature for a physics as a (key, value) pair
     *
     * @param labelPhysics   - physics label
     * @param nodeLabel      - feature label
     * @param parameterName  - input feature key
     * @param parameterValue - input feature 1D value
     */
    void setFeaturePhysics(String labelPhysics, String nodeLabel, String parameterName, String[] parameterValue);

    /**
     * Method sets a physics feature for a named selection
     *
     * @param labelPhysics   - physics label
     * @param selectionLabel - named selection label
     */
    void setNamedSelectionFeaturePhysics(String labelPhysics, String selectionLabel);

    /**
     * Method sets a scalar property given as a (key, value) pair of a physics
     *
     * @param labelPhysics - physics label
     * @param prop         - property label
     * @param key          - input property key
     * @param value        - input property value
     */
    void setPropPhysics(String labelPhysics, String prop, String key, String value);

    /**
     * Method sets a value for a given index of a physics property table
     *
     * @param labelPhysics - physics label
     * @param prop         - property label
     * @param table        - name of the feature table
     * @param value        - value to be assigned
     * @param row          - row of an index
     * @param col          - column of an index
     */
    void setIndexPropPhysics(String labelPhysics, String prop, String table, String value, int row, int col);

    /**
     * Method sets a double nested field to a physics node
     *
     * @param labelPhysics - physics label
     * @param field1       - field label (1st level)
     * @param field2       - field label (2nd level)
     */
    void setFieldFieldPhysics(String labelPhysics, String field1, String field2);

    /**
     * Method sets a component for a physics field
     *
     * @param labelPhysics - physics label
     * @param field        - field label
     * @param index        - index at which component is set
     * @param value        - input component value
     */
    void setComponentFieldPhysics(String labelPhysics, String field, int index, String value);

    /**
     * Method sets a scalar parameter given as a (key, value) pair for a geometry feature
     *
     * @param labelGeom - geometry label
     * @param feature   - feature label
     * @param key       - input parameter key
     * @param value     - input parameter value
     */
    void setParamFeatureGeometry(String labelGeom, String feature, String key, double value);


    /**
     * Method creates a feature given by a triplet (label, key, value) for a physics node
     *
     * @param labelPhysics - physics label
     * @param feature      - feature label
     * @param label        - input feature label
     * @param key          - input feature key
     * @param value        - input feature value
     */
    void createFeaturePhysics(String labelPhysics, String feature, String label, String key, int value);

    /**
     * Method sets a double nested feature given as a (key, value) pair for a physics
     *
     * @param labelPhysics - physics label
     * @param parentNode   - feature label (1st level)
     * @param nodeLabel    - feature label (2nd level)
     * @param key          - input feature key
     * @param value        - input feature value
     */
    void setFeatureFeaturePhysics(String labelPhysics, String parentNode, String nodeLabel, String key, String value);

    /**
     * Method sets a label for a material family, which is helpful for graphical rendering in COMSOL
     *
     * @param label  - material label
     * @param family - input parameter key (family label)
     * @param value  - input parameter value (material name)
     */
    void setMaterial(String label, String family, String value);

    /**
     * Method connects to a SIGMA Server
     * Only for internal use with COMSOL API, for text output, the method is empty
     *
     * @param mphServerPath - an absolute path to the COMSOL server
     * @throws IOException - thrown in case of IO problems in connecting with the server
     */
    void connect(String mphServerPath) throws IOException;

    /**
     * Method prepares a model template for model generation
     *
     * @return - SigmaServer object with updated model template
     */
    SigmaServer prepareModelTemplate();

    /**
     * Method disconnects from the SIGMA server
     * Only for internal use with COMSOL API, for text output, the method is empty
     */
    void disconnect();

    /**
     * Method builds a model
     */
    void build(String fileName);

    /**
     * Method compiles a model
     */
    void compile(String modelPath) throws IOException;
}