package server;

import comsol.constants.MPHC;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utils.TextFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of the SigmaServer class for text output to be compiled by Java
 */
public class TxtSigmaServer implements SigmaServer {
    private static final Logger LOGGER = LogManager.getLogger(TxtSigmaServer.class);

    public static final String METHOD_NOT_SUPPORTED_IN_THE_TEXT_MODE = "Method not supported in the text mode!";

    private static final int MAX_LINES_PER_METHOD = 300;
    private final List<String> txt = new ArrayList();
    private final String javaJDKPath;
    private final String comsolBatchPath;
    private List<String> output = new ArrayList<>();

    /**
     * Constructs a TxtSigmaServer object with input paths
     * @param javaJDKPath absolute path to a directory with local instalation of the Java JDK
     * @param comsolBatchPath absolute path to a directory with COMSOl Batch executables
     */
    public TxtSigmaServer(String javaJDKPath, String comsolBatchPath) {
        this.javaJDKPath = javaJDKPath;
        this.comsolBatchPath = comsolBatchPath;
        createModelNode();  // An empty model note is created, when 'TxtSigmaServer' runs in main file
    }

    /**
     * Method creates an empty model node:
     */
    @Override
    public void createModelNode() {
        String expression = genExprModelCreate("Model");
        this.txt.add(expression);
    }

    protected static String genExprModelCreate(String modelName) {
        return String.format("Model mph = ModelUtil.create(\"%s\");", modelName);
    }

    /**
     * Method creates an empty component node
     * @param labelComp - label for name of the component
     */
    @Override
    public void createComponentNode(String labelComp) {
        String expression = genExprCreateModelNode(labelComp);
        this.txt.add(expression);
        String expression2 = genExprCreateComponentTag(labelComp);
        this.txt.add(expression2);
    }

    protected static String genExprCreateModelNode(String labelComp) {
        return String.format("mph.modelNode().create(\"%s\");", labelComp);
    }

    protected static String genExprCreateComponentTag(String labelComp) {
        return String.format("mph.component(\"%s\").label(\"%s\");", labelComp, labelComp);
    }

                                            // ORIGINAL CODES....
//******************************************************************************************************************
/*
    @Override
    public void createModelNode(String labelComp) {
        String expression = genExprModelCreate("Model");
        this.txt.add(expression);
        String expression = genExprCreateModelNode(labelComp);
        this.txt.add(expression);
    }

    protected static String genExprModelCreate(String modelName) {
        return String.format("Model mph = ModelUtil.create(\"%s\");", modelName);
    }

    protected static String genExprCreateModelNode(String labelComp) {
        return String.format("mph.modelNode().create(\"%s\");", labelComp);
    }
 */

    /*
    //@Override
    public void createQHModelNode(String QHlabelComp) {

        String expression = genQHExprCreateModelNode(QHlabelComp);
        this.txt.add(expression);
    }

    protected static String genQHExprModelCreate(String QHmodelName) {
        return String.format("Model mph = ModelUtil.create(\"%s\");", QHmodelName);
    }

    protected static String genQHExprCreateModelNode(String QHlabelComp) {
        return String.format("mph.modelNode().create(\"%s\");", QHlabelComp);
    }
    */

    /*
    protected static String genQHExprModelCreate(String QHmodelName) {
        return String.format("Model mph = ModelUtil.create(\"%s\");", QHmodelName);
    }

    protected static String genQHExprCreateModelNode(String QHlabelComp) {
        return String.format("mph.modelNode().create(\"%s\");", QHlabelComp);
    }
    */

    /*
    /**
     * Method creates an empty component node
     * @param labelComp - label for name of the component
     */
    /*
    //@Override
    public void createComponentTag(String labelComp, String componentTag) {
        String expression = genExprCreateComponentTag(labelComp);
        this.txt.add(expression);
    }

    protected static String genExprCreateComponentTag(String labelComp) {
        return String.format("mph.component(\"%s\").tag(\"%s\");", labelComp, labelComp);
    }
    */

    //******************************************************************************************************************




    @Override
    public void clearFile() {
        String expression = genExprClearFile();
        this.txt.add(expression);
    }

    protected static String genExprClearFile() {
        return "mph.file().clear();";
    }

    @Override
    public void createGeom(String labelGeom, int dimensionality) {
        String expression = genExprCreateGeom(labelGeom, dimensionality);
        this.txt.add(expression);
    }

    protected static String genExprCreateGeom(String labelGeom, int dimensionality) {
        return String.format("mph.geom().create(\"%s\", %d);", labelGeom, dimensionality);
    }


    @Override
    public void setGeomLabel(String labelGeom, String labelGeom2) {
        String expression = genExprSetGeomLabel(labelGeom, labelGeom2);
        this.txt.add(expression);
    }

    protected static String genExprSetGeomLabel(String labelGeom, String labelGeom2) {
        return String.format("mph.geom(\"%s\").label(\"%s\");", labelGeom, labelGeom2);
    }


    @Override
    public void setGeomRepairTol(String labelGeom, double repairTol) {
        String expression = genExprSetGeomRepairTol(labelGeom, repairTol);
        this.txt.add(expression);
    }

    protected static String genExprSetGeomRepairTol(String labelGeom, double repairTol) {
        return String.format("mph.geom(\"%s\").repairTol(%f);", labelGeom, repairTol);
    }


    @Override
    public void createPhysicsNode(String labelPhysics, String typePhysics, String labelGeom) {
        String expression = genExprCreatePhysicsNode(labelPhysics, typePhysics, labelGeom);
        this.txt.add(expression);
    }

    protected static String genExprCreatePhysicsNode(String labelPhysics, String typePhysics, String labelGeom) {
        return String.format("mph.physics().create(\"%s\", \"%s\", \"%s\");", labelPhysics, typePhysics, labelGeom);
    }


    @Override
    public void setPropertyPhysics(String labelPhysics, String nameProperty, String key, String value) {
        String expression = genExprSetPropertyPhysics(labelPhysics, nameProperty, key, value);
        this.txt.add(expression);
    }

    protected static String genExprSetPropertyPhysics(String labelPhysics, String nameProperty, String key, String value) {
        return String.format("mph.physics(\"%s\").prop(\"%s\").set(\"%s\", \"%s\");", labelPhysics, nameProperty, key, value);
    }


    @Override
    public void setPropertyPhysics(String labelPhysics, String nameProperty, String key, Boolean value) {
        String expression = genExprSetPropertyPhysics(labelPhysics, nameProperty, key, value);
        this.txt.add(expression);
    }

    protected static String genExprSetPropertyPhysics(String labelPhysics, String nameProperty, String key, boolean value) {
        return String.format("mph.physics(\"%s\").prop(\"%s\").set(\"%s\", %b);", labelPhysics, nameProperty, key, value);
    }


    @Override
    public void setIndexFeaturePhysics(String labelPhysics, String nameFeature, String nameIndex, String key, int value) {
        String expression = genExprSetIndexFeaturePhysics(labelPhysics, nameFeature, nameIndex, key, value);
        this.txt.add(expression);
    }

    protected static String genExprSetIndexFeaturePhysics(String labelPhysics, String nameFeature, String nameIndex, String key, int value) {
        return String.format("mph.physics(\"%s\").feature(\"%s\").setIndex(\"%s\", \"%s\", %d);", labelPhysics, nameFeature, nameIndex, key, value);
    }


    @Override
    public void setFeaturePhysics(String labelPhysics, String nameFeature, String key, String value) {
        String expression = genExprSetFeaturePhysics(labelPhysics, nameFeature, key, value);
        this.txt.add(expression);
    }

    protected static String genExprSetFeaturePhysics(String labelPhysics, String nameFeature, String key, String value) {
        return String.format("mph.physics(\"%s\").feature(\"%s\").set(\"%s\", \"%s\");", labelPhysics, nameFeature, key, value);
    }


    @Override
    public void createTagFeaturePhysics(String labelPhysics, String labelFeature, String tag) {
        String expression = genExprCreateTagFeaturePhysics(labelPhysics, labelFeature, tag);
        this.txt.add(expression);
    }

    protected static String genExprCreateTagFeaturePhysics(String labelPhysics, String labelFeature, String tag) {
        return String.format("mph.physics(\"%s\").feature(\"%s\").tag(\"%s\");", labelPhysics, labelFeature, tag);
    }


    @Override
    public void setLabelFeaturePhysics(String labelPhysics, String labelFeature, String label) {
        String expression = genExprSetLabelFeaturePhysics(labelPhysics, labelFeature, label);
        this.txt.add(expression);
    }

    protected static String genExprSetLabelFeaturePhysics(String labelPhysics, String labelFeature, String label) {
        return String.format("mph.physics(\"%s\").feature(\"%s\").label(\"%s\");", labelPhysics, labelFeature, label);
    }

    @Override
    public void setParam(String parameterName, String parameterValue, String parameterDescription) {
        String expression = genExprSetParam(parameterName, parameterValue, parameterDescription);
        this.txt.add(expression);
    }

    protected static String genExprSetParam(String parameterName, String parameterValue, String parameterDescription) {
        return String.format("mph.param().set(\"%s\", \"%s\", \"%s\");", parameterName, parameterValue, parameterDescription);
    }
//**********************************************************************************************************************
// Old code to add additional parameters to global parameters:
//    @Override
//    public void setParam(String parameterName, String parameterValue) {
//        String expression = genExprSetParam(parameterName, parameterValue);
//        this.txt.add(expression);
//    }

//    protected static String genExprSetParam(String parameterName, String parameterValue) {
//        return String.format("mph.param().set(\"%s\", \"%s\");", parameterName, parameterValue);
//    }

    /*
        public void createexplicitnodenameExpr(String compLabel, String selLabel) {
        String expression = String.format("mph.component(\"%s\").selection().create(\"%s\", \"Explicit\");", compLabel, selLabel);
        this.txt.add(expression);
    }

        public void createexplicitgeoExpr(String compLabel, String selLabel, String geoLabel) {
        //String expression = String.format("mph.component(\"%s\").selection(\"%s\").geom(\"%s\");", compLabel, selLabel, geoLabel);
        String expression = String.format("mph.component(\"%s\").selection(\"%s\").geom(1);", compLabel, selLabel);
        this.txt.add(expression);


        public void createbndExpr(String compLabel, String selLabel, String myinput) {
        String expression = String.format("mph.component(\"%s\").selection(\"%s\").set(new int[]{%s});", compLabel, selLabel, myinput);
        this.txt.add(expression);
    }
    }
     */

    @Override
    public void createExplicitSelectionNode(String compLabel, String nodeName, String nodeLabel) {
        String expression = genExprcreateExplicitSelectionNode(compLabel, nodeName);
        this.txt.add(expression);
        String expression2 = genExprcreateExplicitSelectionNodeLabel(compLabel, nodeName, nodeLabel);
        this.txt.add(expression2);
    }

    protected static String genExprcreateExplicitSelectionNode(String compLabel, String nodeName) {
        return String.format("mph.component(\"%s\").selection().create(\"%s\", \"Explicit\");", compLabel, nodeName);
    }


    protected static String genExprcreateExplicitSelectionNodeLabel(String compLabel, String nodeName, String nodeLabel) {
        return String.format("mph.component(\"%s\").selection(\"%s\").label(\"%s\");", compLabel, nodeName, nodeLabel);
    }


    @Override
    public void setExplicitSelectionNodetoGeometry(String compLabel, String selLabel, int depth) {
        String expression = genExprsetExplicitSelectionNodetoGeometry(compLabel, selLabel,  depth);
        this.txt.add(expression);
    }

    protected static String genExprsetExplicitSelectionNodetoGeometry(String compLabel, String nodeName, int depth) {
        //return String.format("mph.component(\"%s\").selection(\"%s\").geom(%s);", compLabel, selLabel, depth);
        return String.format("mph.component(\"%s\").selection(\"%s\").geom(%s);", compLabel, nodeName, depth);
    }

    @Override
    public void setExplicitSelectionBoundary(String compLabel, String selLabel, String input) {
        String expression = genExprsetExplicitSelectionBoundary(compLabel, selLabel, input);
        this.txt.add(expression);
    }

    protected static String genExprsetExplicitSelectionBoundary(String compLabel, String selLabel, String input) {
        return String.format("mph.component(\"%s\").selection(\"%s\").set(new int[]{%s});", compLabel, selLabel, input);
    }
//**********************************************************************************************************************






    @Override
    public void createFunctionNode(String nodeName, String functionType) {
        String expression = genExprCreateFunctionNode(nodeName, functionType);
        this.txt.add(expression);
    }

    protected static String genExprCreateFunctionNode(String nodeName, String functionType) {
        return String.format("mph.func().create(\"%s\", \"%s\");", nodeName, functionType);
    }


    @Override
    public void setFunctionNodeLabel(String nodeName, String labelNode) {
        String expression = genExprSetFunctionNodeLabel(nodeName, labelNode);
        this.txt.add(expression);
    }

    protected static String genExprSetFunctionNodeLabel(String nodeName, String labelNode) {
        return String.format("mph.func(\"%s\").label(\"%s\");", nodeName, labelNode);
    }


    @Override
    public void setFunctionParam(String nodeName, String paramName, String paramValue) {
        String expression = genExprSetFunctionParam(nodeName, paramName, paramValue);
        this.txt.add(expression);
    }

    protected static String genExprSetFunctionParam(String nodeName, String paramName, String paramValue) {
        return String.format("mph.func(\"%s\").set(\"%s\", \"%s\");", nodeName, paramName, paramValue);
    }


    @Override
    public void setFunctionParam(String nodeName, String paramName, String[][] paramValue) {
        String expression = genExprSetFunctionParam(nodeName, paramName, paramValue);
        this.txt.add(expression);
    }

    protected static String genExprSetFunctionParam(String nodeName, String paramName, String[][] paramValue) {
        return String.format("mph.func(\"%s\").set(\"%s\", %s);", nodeName, paramName, TxtSigmaServerUtils.convert2DStringArrayToString(paramValue));
    }


    @Override
    public void createSelectionGeometry(String labelGeom, String label, String labelSelection) {
        String expression = genExprCreateSelectionGeometry(labelGeom, label, labelSelection);
        this.txt.add(expression);
    }

    protected static String genExprCreateSelectionGeometry(String labelGeom, String label, String labelSelection) {
        return String.format("mph.geom(\"%s\").selection().create(\"%s\", \"%s\");", labelGeom, label, labelSelection);
    }


    @Override
    public void setLabelSelectionGeometry(String labelGeom, String selection, String label) {
        String expression = genExprSetLabelSelectionGeometry(labelGeom, selection, label);
        this.txt.add(expression);
    }

    protected static String genExprSetLabelSelectionGeometry(String labelGeom, String selection, String label) {
        return String.format("mph.geom(\"%s\").selection(\"%s\").label(\"%s\");", labelGeom, selection, label);
    }


    @Override
    public void createLabelGeometry(String labelGeom, String labelKey, String labelValue) {
        String expression = genExprCreateLabelGeometry(labelGeom, labelKey, labelValue);
        this.txt.add(expression);
    }

    protected static String genExprCreateLabelGeometry(String labelGeom, String labelKey, String labelValue) {
        return String.format("mph.geom(\"%s\").create(\"%s\", \"%s\");", labelGeom, labelKey, labelValue);
    }


    @Override
    public void setLabelFeatureGeometry(String labelGeom, String feature, String label) {
        String expression = genExprSetLabelFeatureGeometry(labelGeom, feature, label);
        this.txt.add(expression);
    }

    protected static String genExprSetLabelFeatureGeometry(String labelGeom, String feature, String label) {
        return String.format("mph.geom(\"%s\").feature(\"%s\").label(\"%s\");", labelGeom, feature, label);
    }


    @Override
    public void setParamFeatureGeometry(String labelGeom, String label, String parameterName, String parameterValue) {
        String expression = genExprSetParamFeatureGeometry(labelGeom, label, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamFeatureGeometry(String labelGeom, String label, String parameterName, String parameterValue) {
        return String.format("mph.geom(\"%s\").feature(\"%s\").set(\"%s\", \"%s\");", labelGeom, label, parameterName, parameterValue);
    }


    @Override
    public void setParamFeatureGeometry(String labelGeom, String label, String parameterName, String[] parameterValue) {
        String expression = genExprSetParamFeatureGeometry(labelGeom, label, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamFeatureGeometry(String labelGeom, String label, String parameterName, String[] parameterValue) {
        return String.format("mph.geom(\"%s\").feature(\"%s\").set(\"%s\", %s);",
                labelGeom,
                label,
                parameterName,
                TxtSigmaServerUtils.convertArrayToStringExpression(parameterValue));
    }

    //******************************************************************************************************************
    @Override
    public void setParamFeatureGeometry(String labelGeom, String label, String parameterName, String[][] parameterValue) {
        String expression = genExprSetParamFeatureGeometry(labelGeom, label, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamFeatureGeometry(String labelGeom, String label, String parameterName, String[][] parameterValue) {
        return String.format("mph.geom(\"%s\").feature(\"%s\").set(\"%s\", %s);",
                labelGeom,
                label,
                parameterName,
                TxtSigmaServerUtils.convert2DStringArrayToString(parameterValue));
    }
    //******************************************************************************************************************

    @Override
    public void createGeom(String labelGeom, String label, String value) {
        String expression = genExprCreateGeom(labelGeom, label, value);
        this.txt.add(expression);
    }

    protected static String genExprCreateGeom(String labelGeom, String label, String value) {
        return String.format("mph.geom(\"%s\").create(\"%s\", \"%s\");", labelGeom, label, value);
    }


    @Override
    public void setSelectionFeatureGeom(String labelGeom, String label, String selection, String[] inputs) {
        String expression = genExprSetSelectionFeatureGeom(labelGeom, label, selection, inputs);
        this.txt.add(expression);
    }

    protected static String genExprSetSelectionFeatureGeom(String labelGeom, String label, String selection, String[] inputs) {
        return String.format("mph.geom(\"%s\").feature(\"%s\").selection(\"%s\").set(%s);",
                labelGeom,
                label,
                selection,
                TxtSigmaServerUtils.convertArrayToStringExpression(inputs));
    }


    @Override
    public void setIndexFeatureGeometry(String labelGeom, String label, String tableName, String value, int row, int col) {
        String expression = genExprSetIndexFeatureGeometry(labelGeom, label, tableName, value, row, col);
        this.txt.add(expression);
    }

    protected static String genExprSetIndexFeatureGeometry(String labelGeom, String label, String tableName, String value, int row, int col) {
        return String.format("mph.geom(\"%s\").feature(\"%s\").setIndex(\"%s\", \"%s\", %d, %d);", labelGeom, label, tableName, value, row, col);
    }


    @Override
    public void setParamFeatureGeometry(String labelGeom, String label, String parameterName, int[] parameterValue) {
        String expression = genExprSetParamFeatureGeometry(labelGeom, label, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamFeatureGeometry(String label, String propertyGroup, String parameterName, int[] parameterValue) {
        return String.format("mph.geom(\"%s\").feature(\"%s\").set(\"%s\", %s);",
                label,
                propertyGroup,
                parameterName,
                TxtSigmaServerUtils.convertArrayToStringExpression(parameterValue));
    }


    @Override
    public void runLabelGeometry(String labelGeom, String label) {
        String expression = genExprRunLabelGeometry(labelGeom, label);
        this.txt.add(expression);
    }

    protected static String genExprRunLabelGeometry(String labelGeom, String label) {
        return String.format("mph.geom(\"%s\").run(\"%s\");", labelGeom, label);
    }


    @Override
    public void runPreLabelGeometry(String labelGeom, String label) {
        String expression = genExprRunPreLabelGeometry(labelGeom, label);
        this.txt.add(expression);
    }

    protected static String genExprRunPreLabelGeometry(String labelGeom, String label) {
        return String.format("mph.geom(\"%s\").runPre(\"%s\");", labelGeom, label);
    }


    @Override
    public void setParamFeatureGeometry(String labelGeom, String label, String parameterName, boolean parameterValue) {
        String expression = genExprSetParamFeatureGeometry(labelGeom, label, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamFeatureGeometry(String labelGeom, String label, String parameterName, boolean parameterValue) {
        return String.format("mph.geom(\"%s\").feature(\"%s\").set(\"%s\", %b);", labelGeom, label, parameterName, parameterValue);
    }

    @Override
    public void initSelectionFeatureGeometry(String labelGeom, String label, String selection, int value) {
        String expression = genExprInitSelectionFeatureGeometry(labelGeom, label, selection, value);
        this.txt.add(expression);
    }

    protected static String genExprInitSelectionFeatureGeometry(String labelGeom, String label, String selection, int value) {
        return String.format("mph.geom(\"%s\").feature(\"%s\").selection(\"%s\").init(%d);", labelGeom, label, selection, value);
    }


    @Override
    public void setParamSelectionFeatureGeom(String labelGeom, String label, String selection, String parameterName, int[] parameterValue) {
        String expression = genExprSetParamSelectionFeatureGeom(labelGeom, label, selection, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamSelectionFeatureGeom(String labelGeom, String label, String selection, String parameterName, int[] parameterValue) {
        return String.format("mph.geom(\"%s\").feature(\"%s\").selection(\"%s\").set(\"%s\", %s);",
                labelGeom,
                label,
                selection,
                parameterName,
                TxtSigmaServerUtils.convertArrayToStringExpression(parameterValue));
    }
    //******************************************************************************************************************
    @Override
    public void setParamSelectionFeatureGeom(String labelGeom, String label, String selection, String selectionLabel) {
        String expression = genExprSetParamSelectionFeatureGeom2(labelGeom, label, selection, selectionLabel);
        this.txt.add(expression);
    }

    protected static String genExprSetParamSelectionFeatureGeom2(String labelGeom, String label, String selection, String selectionLabel) {
        return String.format("mph.geom(\"%s\").feature(\"%s\").selection(\"%s\").set(new String[] {\"%s\"});",
                labelGeom,
                label,
                selection,
                selectionLabel);
    }
    //******************************************************************************************************************

    @Override
    public void createHideEntitiesView(String labelView, String nodeName) {
        String expression = genExprCreateHideEntitiesView(labelView, nodeName);
        this.txt.add(expression);
    }

    protected static String genExprCreateHideEntitiesView(String labelView, String nodeName) {
        return String.format("mph.view(\"%s\").hideEntities().create(\"%s\");", labelView, nodeName);
    }


    @Override
    public void setGeomHideEntitiesView(String labelView, String entitiesName, String parameterName, int parameterValue) {
        String expression = genExprSetGeomHideEntitiesView(labelView, entitiesName, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetGeomHideEntitiesView(String labelView, String entitiesName, String parameterName, int parameterValue) {
        return String.format("mph.view(\"%s\").hideEntities(\"%s\").geom(\"%s\", %d);", labelView, entitiesName, parameterName, parameterValue);
    }


    @Override
    public void setNamedHideEntitiesView(String labelView, String entitiesName, String compName) {
        String expression = genExprSetNamedHideEntitiesView(labelView, entitiesName, compName);
        this.txt.add(expression);
    }

    protected static String genExprSetNamedHideEntitiesView(String labelView, String entitiesName, String compName) {
        return String.format("mph.view(\"%s\").hideEntities(\"%s\").named(\"%s\");", labelView, entitiesName, compName);

    }


    @Override
    public void createLabelVariable(String label) {
        String expression = genExprCreateLabelVariable(label);
        this.txt.add(expression);
    }

    protected static String genExprCreateLabelVariable(String label) {
        return String.format("mph.variable().create(\"%s\");", label);
    }

    /*
    // API for analytic function
    //******************************************************************************************************************
    //@Override
    public void createLabelAnalyticFunction(String label) {
        String expression = genExprCreateLabelAnalyticFunction(label);
        this.txt.add(expression);
    }

    protected static String genExprCreateLabelAnalyticFunction(String label) {
        return String.format("mph.func().create(\"%s\", \"Analytic\");", label);
    }


    //@Override
    public void setLabelAnalyticFunction(String variable, String label) {
        String expression = genExprSetLabelAnalyticFunction(variable, label);
        this.txt.add(expression);
    }

    protected static String genExprSetLabelAnalyticFunction(String variable, String label) {
        return String.format("mph.func(\"%s\").label(\"%s\");", variable, label);
    }

    //@Override
    public void setModelAnalyticFunction(String label, String model) {
        String expression = genExprSetModelAnalyticFunction(label, model);
        this.txt.add(expression);
    }

    protected static String genExprSetModelAnalyticFunction(String label, String model) {
        return String.format("mph.func(\"%s\").model(\"%s\");", label, model);
    }
    //******************************************************************************************************************
    */

    @Override
    public void setLabelVariable(String variable, String label) {
        String expression = genExprSetLabelVariable(variable, label);
        this.txt.add(expression);
    }

    protected static String genExprSetLabelVariable(String variable, String label) {
        return String.format("mph.variable(\"%s\").label(\"%s\");", variable, label);
    }


    @Override
    public void setModelVariable(String label, String labelComp) {
        String expression = genExprSetModelVariable(label, labelComp);
        this.txt.add(expression);
    }

    protected static String genExprSetModelVariable(String label, String labelComp) {
        return String.format("mph.variable(\"%s\").model(\"%s\");", label, labelComp);
    }


    @Override
    public void setParamVariable(String groupLabel, String parameterName, String parameterValue) {
        String expression = genExprSetParamVariable(groupLabel, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamVariable(String groupLabel, String parameterName, String parameterValue) {
        return String.format("mph.variable(\"%s\").set(\"%s\", \"%s\");", groupLabel, parameterName, parameterValue);
    }

    /*
    //******************************************************************************************************************
    // Analytic Function: creates the label:
    //@Override
    public void setParamAnalyticFunction(String groupLabel, String parameterName, String parameterValue) {
        String expression = genExprSetParamAnalyticFunction(groupLabel, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamAnalyticFunction(String groupLabel, String parameterName, String parameterValue) {
        return String.format("mph.func(\"%s\").set(\"%s\", \"%s\");", groupLabel, parameterName, parameterValue);
    }
    //******************************************************************************************************************
    */

    @Override
    public void setGlobalSelectionVariable(String label) {
        String expression = genExprSetGlobalSelectionVariable(label);
        this.txt.add(expression);
    }

    protected static String genExprSetGlobalSelectionVariable(String label) {
        return String.format("mph.variable(\"%s\").selection().global();", label);
    }


    @Override
    public void setParamGeomSelectionVariable(String label, String parameterName, int parameterValue) {
        String expression = genExprSetParamGeomSelectionVariable(label, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamGeomSelectionVariable(String label, String parameterName, int parameterValue) {
        return String.format("mph.variable(\"%s\").selection().geom(\"%s\", %d);", label, parameterName, parameterValue);
    }


    @Override
    public void setNamedSelectionVariable(String label, String selection) {
        String expression = genExprSetNamedSelectionVariable(label, selection);
        this.txt.add(expression);
    }

    protected static String genExprSetNamedSelectionVariable(String label, String selection) {
        return String.format("mph.variable(\"%s\").selection().named(\"%s\");", label, selection);
    }


    @Override
    public void createCpl(String label, String type, String labelGeom) {
        String expression = genExprCreateCpl(label, type, labelGeom);
        this.txt.add(expression);
    }

    protected static String genExprCreateCpl(String label, String type, String labelGeom) {
        return String.format("mph.cpl().create(\"%s\", \"%s\", \"%s\");", label, type, labelGeom);
    }


    @Override
    public void setLabelCpl(String cpl, String label) {
        String expression = genExprSetLabelCpl(cpl, label);
        this.txt.add(expression);
    }

    protected static String genExprSetLabelCpl(String cpl, String label) {
        return String.format("mph.cpl(\"%s\").label(\"%s\");", cpl, label);
    }


    @Override
    public void setParamCpl(String label, String parameterName, String parameterValue) {
        String expression = genExprSetParamCpl(label, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamCpl(String label, String parameterName, String parameterValue) {
        return String.format("mph.cpl(\"%s\").set(\"%s\", \"%s\");", label, parameterName, parameterValue);
    }


    @Override
    public void setNamedSelectionCpl(String label, String selectionLabel) {
        String expression = genExprSetNamedSelectionCpl(label, selectionLabel);
        this.txt.add(expression);
    }

    protected static String genExprSetNamedSelectionCpl(String label, String selectionLabel) {
        return String.format("mph.cpl(\"%s\").selection().named(\"%s\");", label, selectionLabel);
    }


    @Override
    public void createLabelMaterial(String label, String type, String labelComp) {
        String expression = genExprCreateLabelMaterial(label, type, labelComp);
        this.txt.add(expression);
    }

    protected static String genExprCreateLabelMaterial(String label, String type, String labelComp) {
        return String.format("mph.material().create(\"%s\",\"%s\", \"%s\");", label, type, labelComp);
    }


    @Override
    public void setLabelMaterial(String material, String label) {
        String expression = genExprSetLabelMaterial(material, label);
        this.txt.add(expression);
    }

    protected static String genExprSetLabelMaterial(String material, String label) {
        return String.format("mph.material(\"%s\").label(\"%s\");", material, label);
    }


    @Override
    public void setNamedSelectionMaterial(String material, String selectionLabel) {
        String expression = genExprSetNamedSelectionMaterial(material, selectionLabel);
        this.txt.add(expression);
    }

    protected static String genExprSetNamedSelectionMaterial(String material, String selectionLabel) {
        return String.format("mph.material(\"%s\").selection().named(\"%s\");", material, selectionLabel);
    }


    @Override
    public void setParamPropertyGroupMaterial(String label, String propertyGroup, String parameterName, String parameterValue) {
        String expression = genExprSetParamPropertyGroupMaterial(label, propertyGroup, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamPropertyGroupMaterial(String label, String propertyGroup, String parameterName, String parameterValue) {
        return String.format("mph.material(\"%s\").propertyGroup(\"%s\").set(\"%s\", \"%s\");", label, propertyGroup, parameterName, parameterValue);
    }


    @Override
    public void setParamPropertyGroupMaterial(String label, String propertyGroup, String parameterName, String[] parameterValue) {
        String expression = genExprSetParamPropertyGroupMaterial(label, propertyGroup, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamPropertyGroupMaterial(String label, String propertyGroup, String parameterName, String[] parameterValue) {
        return String.format("mph.material(\"%s\").propertyGroup(\"%s\").set(\"%s\", %s);",
                label,
                propertyGroup,
                parameterName,
                TxtSigmaServerUtils.convertArrayToStringExpression(parameterValue));
    }


    @Override
    public void createPropertyGroupMaterial(String material, String label, String description) {
        String expression = genExprCreatePropertyGroupMaterial(material, label, description);
        this.txt.add(expression);
    }

    protected static String genExprCreatePropertyGroupMaterial(String material, String label, String description) {
        return String.format("mph.material(\"%s\").propertyGroup().create(\"%s\", \"%s\");", material, label, description);
    }


    @Override
    public void createFunctionPropertyGroupMaterial(String material, String propertyGroup, String label, String type) {
        String expression = genExprCreateFunctionPropertyGroupMaterial(material, propertyGroup, label, type);
        this.txt.add(expression);
    }

    protected static String genExprCreateFunctionPropertyGroupMaterial(String material, String propertyGroup, String label, String type) {
        return String.format("mph.material(\"%s\").propertyGroup(\"%s\").func().create(\"%S\", \"%s\");", material, propertyGroup, label, type);
    }


    @Override
    public void setParamFuncPropertyGroupMaterial(String material, String propertyGroup, String label, String parameterName, String parameterValue) {
        String expression = genExprSetParamFuncPropertyGroupMaterial(material, propertyGroup, label, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamFuncPropertyGroupMaterial(String material,
                                                                     String propertyGroup,
                                                                     String label,
                                                                     String parameterName,
                                                                     String parameterValue) {
        return String.format("mph.material(\"%s\").propertyGroup(\"%s\").func(\"%s\").set(\"%s\", \"%s\");",
                material,
                propertyGroup,
                label,
                parameterName,
                parameterValue);
    }


    @Override
    public void setParamFuncPropertyGroupMaterial(String material, String propertyGroup, String label, String parameterName, String[][] parameterValue) {
        String expression = genExprSetParamFuncPropertyGroupMaterial(material, propertyGroup, label, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamFuncPropertyGroupMaterial(String material,
                                                                     String propertyGroup,
                                                                     String label,
                                                                     String parameterName,
                                                                     String[][] parameterValue) {
        return String.format("mph.material(\"%s\").propertyGroup(\"%s\").func(\"%s\").set(\"%s\", %s);",
                material,
                propertyGroup,
                label,
                parameterName,
                TxtSigmaServerUtils.convert2DStringArrayToString(parameterValue));
    }


    @Override
    public void setIndexFeaturePhysics(String labelPhysics, String feature, String tableName, String value, int row, int col) {
        String expression = genExprSetIndexFeaturePhysics(labelPhysics, feature, tableName, value, row, col);
        this.txt.add(expression);
    }

    protected static String genExprSetIndexFeaturePhysics(String labelPhysics, String feature, String tableName, String value, int row, int col) {
        return String.format("mph.physics(\"%s\").feature(\"%s\").setIndex(\"%s\", \"%s\", %d, %d);", labelPhysics, feature, tableName, value, row, col);
    }


    @Override
    public void setParamFeaturePhysics(String labelPhysics, String feature, String parameterName, String parameterValue) {
        String expression = genExprSetParamFeaturePhysics(labelPhysics, feature, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamFeaturePhysics(String labelPhysics, String feature, String parameterName, String parameterValue) {
        return String.format("mph.physics(\"%s\").feature(\"%s\").set(\"%s\", \"%s\");", labelPhysics, feature, parameterName, parameterValue);
    }


    @Override
    public void removeFunc(String nodeName) {
        String expression = genExprRemoveFunc(nodeName);
        this.txt.add(expression);
    }

    protected static String genExprRemoveFunc(String nodeName) {
        return String.format("mph.func().remove(\"%s\");", nodeName);
    }


    @Override
    public void setModelFunction(String nodeName, String model) {
        String expression = genExprSetModelFunction(nodeName, model);
        this.txt.add(expression);
    }

    protected static String genExprSetModelFunction(String nodeName, String model) {
        return String.format("mph.func(\"%s\").model(\"%s\");", nodeName, model);
    }


    @Override
    public void setLabelFunction(String nodeName, String label) {
        String expression = genExprSetLabelFunction(nodeName, label);
        this.txt.add(expression);
    }

    protected static String genExprSetLabelFunction(String nodeName, String label) {
        return String.format("mph.func(\"%s\").label(\"%s\");", nodeName, label);
    }


    @Override
    public void setParamFunction(String nodeName, String parameterName, String parameterValue) {
        String expression = genExprSetParamFunction(nodeName, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamFunction(String nodeName, String parameterName, String parameterValue) {
        return String.format("mph.func(\"%s\").set(\"%s\", \"%s\");", nodeName, parameterName, parameterValue);
    }


    @Override
    public void setIndexFunction(String nodeName, String tableName, String value, int row, int col) {
        String expression = genExprSetIndexFunction(nodeName, tableName, value, row, col);
        this.txt.add(expression);
    }

    protected static String genExprSetIndexFunction(String nodeName, String tableName, String value, int row, int col) {
        return String.format("mph.func(\"%s\").setIndex(\"%s\", \"%s\", %d, %d);", nodeName, tableName, value, row, col);
    }

    @Override
    public void setActiveFunction(String functionLabel, boolean isActive) {
        String expression = genExprsetActiveFunction(functionLabel, isActive);
        this.txt.add(expression);
    }

    protected static String genExprsetActiveFunction(String functionLabel, boolean isActive) {
        return String.format("mph.func(\"%s\").active(%b);", functionLabel, isActive);
    }


    @Override
    public void setActiveVariable(String labelGroup, boolean isActive) {
        String expression = genExprSetActiveVariable(labelGroup, isActive);
        this.txt.add(expression);
    }

    protected static String genExprSetActiveVariable(String labelGroup, boolean isActive) {
        return String.format("mph.variable(\"%s\").active(%b);", labelGroup, isActive);
    }


    @Override
    public void createMesh(String labelMesh, String labelGeom) {
        String expression = genExprCreateMesh(labelMesh, labelGeom);
        this.txt.add(expression);
    }

    protected static String genExprCreateMesh(String labelMesh, String labelGeom) {
        return String.format("mph.mesh().create(\"%s\", \"%s\");", labelMesh, labelGeom);

    }

    //******************************************************************************************************************
    @Override
    public void setLabelMesh(String labelMesh) {
        String expression = genExprsetLabelMesh(labelMesh);
        this.txt.add(expression);
    }

    protected static String genExprsetLabelMesh(String labelMesh) {
        return String.format("mph.mesh(\"%s\").label(\"%s\");", labelMesh, labelMesh);

    }

    @Override
    public void createMeshFeature(String labelMesh, String labelFeature, String labelType, String type) {
        String expression = genExprcreateMeshFeature(labelMesh, labelFeature, labelType, type);
        this.txt.add(expression);
    }

    protected static String genExprcreateMeshFeature(String labelMesh, String labelFeature, String labelType, String type) {
        return String.format("mph.mesh(\"%s\").feature(\"%s\").create(\"%s\", \"%s\");", labelMesh, labelFeature, labelType, type);

    }

    @Override
    public void setMeshFeature(String labelMesh, String labelFeature, String labelSubFeature, String labelType, int value) {
        String expression = genExprsetMeshFeature(labelMesh, labelFeature, labelSubFeature, labelType, value);
        this.txt.add(expression);
    }

    protected static String genExprsetMeshFeature(String labelMesh, String labelFeature, String labelSubFeature, String labelType, int value) {
        return String.format("mph.mesh(\"%s\").feature(\"%s\").feature(\"%s\").set(\"%s\", \"%s\");", labelMesh, labelFeature, labelSubFeature, labelType, value);

    }

    @Override
    public void assignMeshFeaturetoSelection(String labelMesh, String labelFeature, String labelSubFeature, String labelSelection) {
        String expression = genExprassignMeshFeaturetoSelection(labelMesh, labelFeature, labelSubFeature, labelSelection);
        this.txt.add(expression);
    }

    protected static String genExprassignMeshFeaturetoSelection(String labelMesh, String labelFeature, String labelSubFeature, String labelSelection) {
        return String.format("mph.mesh(\"%s\").feature(\"%s\").feature(\"%s\").selection().named(\"%s\");", labelMesh, labelFeature, labelSubFeature, labelSelection);

    }


    @Override
    public void setMeshType(String labelMesh, String labelType, String type) {
        String expression = genExprsetMeshType(labelMesh, labelType, type);
        this.txt.add(expression);
    }

    protected static String genExprsetMeshType(String labelMesh, String labelType, String type) {
        return String.format("mph.mesh(\"%s\").create(\"%s\", \"%s\");", labelMesh, labelType, type);

    }

    @Override
    public void setMeshGeometricLevel(String labelMesh, String labelFeature, String labelGeom, int depth) {
        String expression = genExprsetMeshGeometricLevel(labelMesh, labelFeature, labelGeom, depth);
        this.txt.add(expression);
    }

    protected static String genExprsetMeshGeometricLevel(String labelMesh, String labelFeature, String labelGeom, int depth) {
        return String.format("mph.mesh(\"%s\").feature(\"%s\").selection().geom(\"%s\", %d);", labelMesh, labelFeature, labelGeom, depth);

    }

    @Override
    public void assignMeshtoNamedSelection(String labelMesh, String labelFeature, String labelSelection) {
        String expression = genExprassignMeshtoNamedSelection(labelMesh, labelFeature, labelSelection);
        this.txt.add(expression);
    }

    protected static String genExprassignMeshtoNamedSelection(String labelMesh, String labelFeature, String labelSelection) {
        return String.format("mph.mesh(\"%s\").feature(\"%s\").selection().named(\"%s\");", labelMesh, labelFeature, labelSelection);

    }

    @Override
    public void setMeshSize(String labelMesh, int MeshSize) {
        String expression = genExprsetMeshSize(labelMesh, MeshSize);
        this.txt.add(expression);
    }

    protected static String genExprsetMeshSize(String labelMesh, int MeshSize) {
        return String.format("mph.mesh(\"%s\").autoMeshSize(%d);", labelMesh, MeshSize);

    }



    //******************************************************************************************************************




    @Override
    public void runMesh(String labelMesh) {
        String expression = genExprRunMesh(labelMesh);
        this.txt.add(expression);
    }

    protected static String genExprRunMesh(String labelMesh) {
        return String.format("mph.mesh(\"%s\").run();", labelMesh);
    }


    @Override
    public void save(String modelPath) throws IOException {
        //Write instructions to a .java file
        LOGGER.info("Stuff to be written away:");

        for (int i = 0; i < output.size(); i++) {
            LOGGER.info(output.get(i));
        }

        TextFile.writeMultiLine(modelPath, output, false);
    }

    protected List<String> createComsolJavaModel(String fileName) {
        List<String> output = new ArrayList();
        //Add first comment
        output.add("/*");
        output.add(String.format(" * %s.java", fileName));
        output.add("*/");
        output.add("");
        //Add imports
        output.add("import com.comsol.model.*;");
        output.add("import com.comsol.model.util.*;");
        output.add("");
        output.add(String.format("public class %s {", fileName));
        output.add("");
        //Due to the java limit of maximum lines per method, the code is divided into MAX_LINES_PER_METHOD methods
        int noOfMethods = (int) Math.ceil((double) txt.size() / MAX_LINES_PER_METHOD);
        for (int m = 0; m < noOfMethods; m++) {
            //Add method
            output.add(createMethodDefinition(m));

            int startLine = m * MAX_LINES_PER_METHOD;
            int stopLine = Math.min(txt.size(), (m + 1) * MAX_LINES_PER_METHOD);
            for (int l = startLine; l < stopLine; l++) {
                output.add("\t\t" + txt.get(l));
            }

            output.add("\t\treturn mph;");
            output.add("\t}");
            output.add("");
        }

        //Call the created method in order
        output.add("\tpublic static void main(String[] args) {");

        for (int m = 0; m < noOfMethods; m++) {
            output.add("\t\t" + createMethodCall(m));
        }

        output.add("\t}");

        //Closing curly bracket for the class definition
        output.add("}");
        return output;
    }

    private String createMethodCall(int methodCount) {
        if (methodCount == 0) {
            return "Model mph = run();";
        } else {
            return String.format("mph = run%s(mph);", methodCount + 1);
        }
    }

    private String createMethodDefinition(int methodCount) {
        if (methodCount == 0) {
            return "\tpublic static Model run() {";
        } else {
            return String.format("\tpublic static Model run%s(Model mph) {", methodCount + 1);
        }
    }


    @Override
    public String[] getTagsVariable() {
        throw new RuntimeException(METHOD_NOT_SUPPORTED_IN_THE_TEXT_MODE);
    }


    @Override
    public String[] getVarNamesVariable(String tag) {
        throw new RuntimeException(METHOD_NOT_SUPPORTED_IN_THE_TEXT_MODE);
    }


    @Override
    public String getTagsModelNode(int index) {
        throw new RuntimeException(METHOD_NOT_SUPPORTED_IN_THE_TEXT_MODE);
    }


    @Override
    public void removeStudy(String studyName) {
        String expression = genExprRemoveStudy(studyName);
        this.txt.add(expression);
    }

    protected static String genExprRemoveStudy(String studyName) {
        return String.format("mph.study().remove(\"%s\");", studyName);
    }


    @Override
    public void createStudy(String studyName) {
        String expression = genExprCreateStudy(studyName);
        this.txt.add(expression);
    }

    protected static String genExprCreateStudy(String studyName) {
        return String.format("mph.study().create(\"%s\");", studyName);
    }


    @Override
    public void setLabelStudy(String studyName, String label) {
        String expression = genExprSetLabelStudy(studyName, label);
        this.txt.add(expression);
    }

    protected static String genExprSetLabelStudy(String studyName, String label) {
        return String.format("mph.study(\"%s\").label(\"%s\");", studyName, label);
    }


    @Override
    public void createStudyType(String studyName, String label, String type) {
        String expression = genExprCreateStudyType(studyName, label, type);
        this.txt.add(expression);
    }

    protected static String genExprCreateStudyType(String studyName, String label, String type) {
        return String.format("mph.study(\"%s\").create(\"%s\", \"%s\");", studyName, label, type);
    }


    @Override
    public void setParamFeatureStudy(String studyName, String feature, String parameterName, String parameterValue) {
        String expression = genExprSetParamFeatureStudy(studyName, feature, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamFeatureStudy(String studyName, String feature, String parameterName, String parameterValue) {
        return String.format("mph.study(\"%s\").feature(\"%s\").set(\"%s\", \"%s\");", studyName, feature, parameterName, parameterValue);
    }


    @Override
    public void createSol(String solutionName) {
        String expression = genExprCreateSol(solutionName);
        this.txt.add(expression);
    }

    protected static String genExprCreateSol(String solutionName) {
        return String.format("mph.sol().create(\"%s\");", solutionName);
    }


    @Override
    public void setStudySol(String solutionName, String studyName) {
        String expression = genExprSetStudySol(solutionName, studyName);
        this.txt.add(expression);
    }

    protected static String genExprSetStudySol(String solutionName, String studyName) {
        return String.format("mph.sol(\"%s\").study(\"%s\");", solutionName, studyName);
    }


    @Override
    public void createSol(String solutionName, String feature, String type) {
        String expression = genExprCreateSol(solutionName, feature, type);
        this.txt.add(expression);
    }

    protected static String genExprCreateSol(String solutionName, String feature, String type) {
        return String.format("mph.sol(\"%s\").create(\"%s\", \"%s\");", solutionName, feature, type);
    }


    @Override
    public void setParamFeatureSol(String solutionName, String feature, String parameterName, String parameterValue) {
        String expression = genExprSetParamFeatureSol(solutionName, feature, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamFeatureSol(String solutionName, String feature, String parameterName, String parameterValue) {
        return String.format("mph.sol(\"%s\").feature(\"%s\").set(\"%s\", \"%s\");", solutionName, feature, parameterName, parameterValue);
    }


    @Override
    public void setParamFeatureSol(String solutionName, String feature, String parameterName, String[] parameterValue) {
        String expression = genExprSetParamFeatureSol(solutionName, feature, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamFeatureSol(String solutionName, String feature, String parameterName, String[] parameterValue) {
        return String.format("mph.sol(\"%s\").feature(\"%s\").set(\"%s\", %s);",
                solutionName,
                feature,
                parameterName,
                TxtSigmaServerUtils.convertArrayToStringExpression(parameterValue));
    }

    @Override
    public void setParamFeatureSol(String solutionName, String feature, String parameterName, double parameterValue) {
        String expression = genExprSetParamFeatureSol(solutionName, feature, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamFeatureSol(String solutionName, String feature, String parameterName, double parameterValue) {
        return String.format("mph.sol(\"%s\").feature(\"%s\").set(\"%s\", %f);", solutionName, feature, parameterName, parameterValue);
    }

    @Override
    public void setParamFeatureSol(String solutionName, String feature, String parameterName, int parameterValue) {
        String expression = genExprSetParamFeatureSol(solutionName, feature, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamFeatureSol(String solutionName, String feature, String parameterName, int parameterValue) {
        return String.format("mph.sol(\"%s\").feature(\"%s\").set(\"%s\", %d);", solutionName, feature, parameterName, parameterValue);
    }


    @Override
    public void createFeatureSol(String solutionName, String feature, String label, String type) {
        String expression = genExprCreateFeatureSol(solutionName, feature, label, type);
        this.txt.add(expression);
    }

    protected static String genExprCreateFeatureSol(String solutionName, String feature, String label, String type) {
        return String.format("mph.sol(\"%s\").feature(\"%s\").create(\"%s\", \"%s\");", solutionName, feature, label, type);
    }


    @Override
    public void attachSol(String solutionName, String studyName) {
        String expression = genExprAttachSol(solutionName, studyName);
        this.txt.add(expression);
    }

    protected static String genExprAttachSol(String solutionName, String studyName) {
        return String.format("mph.sol(\"%s\").attach(\"%s\");", solutionName, studyName);
    }


    @Override
    public void activateFeatureStudy(String studyName, String feature, String physicsName, Boolean isActive) {
        String expression = genExprActivateFeature(studyName, feature, physicsName, isActive);
        this.txt.add(expression);
    }

    protected static String genExprActivateFeature(String studyName, String feature, String physicsName, Boolean isActive) {
        return String.format("mph.study(\"%s\").feature(\"%s\").activate(\"%s\", %b);", studyName, feature, physicsName, isActive);
    }


    @Override
    public void removeNumericalResult(String expression) {
        String expr = genExprRemoveNumericalResult(expression);
        this.txt.add(expr);
    }

    protected static String genExprRemoveNumericalResult(String expression) {
        return String.format("mph.result().numerical().remove(\"%s\");", expression);
    }


    @Override
    public void createNumericalResult(String expression, String evalGlobal) {
        String expr = genExprCreateNumericalResult(expression, evalGlobal);
        this.txt.add(expr);
    }

    protected static String genExprCreateNumericalResult(String expression, String evalGlobal) {
        return String.format("mph.result().numerical().create(\"%s\", \"%s\");", expression, evalGlobal);
    }


    @Override
    public void setLabelNumericalResult(String numerical, String expression) {
        String expr = genExprSetLabelNumericalResult(numerical, expression);
        this.txt.add(expr);
    }

    protected static String genExprSetLabelNumericalResult(String numerical, String expression) {
        return String.format("mph.result().numerical(\"%s\").label(\"%s\");", numerical, expression);
    }


    @Override
    public void setParamNumericalResult(String numerical, String parameterName, String parameterValue) {
        String expr = genExprSetParamNumericalResult(numerical, parameterName, parameterValue);
        this.txt.add(expr);
    }

    protected static String genExprSetParamNumericalResult(String numerical, String parameterName, String parameterValue) {
        return String.format("mph.result().numerical(\"%s\").set(\"%s\", \"%s\");", numerical, parameterName, parameterValue);
    }


    @Override
    public void setParamNumericalResult(String numerical, String parameterName, String[] parameterValue) {
        String expression = genExprSetParamNumericalResult(numerical, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamNumericalResult(String label, String parameterName, String[] parameterValue) {
        return String.format("mph.result().numerical(\"%s\").set(\"%s\", %s);",
                label,
                parameterName,
                TxtSigmaServerUtils.convertArrayToStringExpression(parameterValue));
    }


    @Override
    public void setIndexNumericalResult(String numerical, String table, String expression, int index) {
        String expr = genExprSetIndexNumericalResult(numerical, table, expression, index);
        this.txt.add(expr);
    }

    protected static String genExprSetIndexNumericalResult(String numerical, String table, String expression, int index) {
        return String.format("mph.result().numerical(\"%s\").setIndex(\"%s\", \"%s\", %d);", numerical, table, expression, index);
    }


    @Override
    public double[][] getRealNumericalResult(String expression) {
        throw new RuntimeException(METHOD_NOT_SUPPORTED_IN_THE_TEXT_MODE);
    }


    @Override
    public void removeFeaturePhysics(String physics, String feature) {
        String expression = genExprRemoveFeaturePhysics(physics, feature);
        this.txt.add(expression);
    }

    protected static String genExprRemoveFeaturePhysics(String physics, String feature) {
        return String.format("mph.physics(\"%s\").feature().remove(\"%s\");", physics, feature);
    }


    @Override
    public void createLabelPhysics(String labelPhysics, String label, String globalEquations, int index) {
        String expr = genExprCreateLabelPhysics(labelPhysics, label, globalEquations, index);
        this.txt.add(expr);
    }

    protected static String genExprCreateLabelPhysics(String labelPhysics, String label, String globalEquations, int index) {
        return String.format("mph.physics(\"%s\").create(\"%s\", \"%s\", %d);", labelPhysics, label, globalEquations, index);
    }


    @Override
    public void removeVariable(String label) {
        String expr = genExprRemoveVariable(label);
        this.txt.add(expr);
    }

    protected static String genExprRemoveVariable(String label) {
        return String.format("mph.variable().remove(\"%s\");", label);
    }


    @Override
    public void setParamFeatureStudy(String studyName, String feature, String parameterName, boolean parameterValue) {
        String expression = genExprSetParamFeatureStudy(studyName, feature, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamFeatureStudy(String studyName, String feature, String parameterName, boolean parameterValue) {
        return String.format("mph.study(\"%s\").feature(\"%s\").set(\"%s\", %b);", studyName, feature, parameterName, parameterValue);
    }


    @Override
    public void setParamFeatureStudy(String studyName, String feature, String parameterName, int parameterValue) {
        String expression = genExprSetParamFeatureStudy(studyName, feature, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamFeatureStudy(String studyName, String feature, String parameterName, int parameterValue) {
        return String.format("mph.study(\"%s\").feature(\"%s\").set(\"%s\", %d);", studyName, feature, parameterName, parameterValue);
    }


    @Override
    public void setParamFeatureFeatureSol(String solutionName, String feature1, String feature2, String parameterName, String parameterValue) {
        String expression = genExprSetParamFeatureSol(solutionName, feature1, feature2, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamFeatureSol(String solutionName, String feature1, String feature2, String parameterName, String parameterValue) {
        return String.format("mph.sol(\"%s\").feature(\"%s\").feature(\"%s\").set(\"%s\", \"%s\");",
                solutionName,
                feature1,
                feature2,
                parameterName,
                parameterValue);
    }


    @Override
    public void setParamFeatureFeatureSol(String solutionName, String feature1, String feature2, String parameterName, double parameterValue) {
        String expression = genExprSetParamFeatureSol(solutionName, feature1, feature2, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamFeatureSol(String solutionName, String feature1, String feature2, String parameterName, double parameterValue) {
        return String.format("mph.sol(\"%s\").feature(\"%s\").feature(\"%s\").set(\"%s\", %f);",
                solutionName,
                feature1,
                feature2,
                parameterName,
                parameterValue);
    }


    @Override
    public void removeFeatureFeatureSol(String solutionName, String feature, String label) {
        String expr = genExprRemoveFeatureFeatureSol(solutionName, feature, label);
        this.txt.add(expr);
    }

    protected static String genExprRemoveFeatureFeatureSol(String solutionName, String feature, String label) {
        return String.format("mph.sol(\"%s\").feature(\"%s\").feature().remove(\"%s\");", solutionName, feature, label);
    }

    //******************************************************************************************************************
    @Override
    public void createResult(String resultLabel, String resultType) {
        String expr = genExprcreateResult(resultLabel, resultType);
        this.txt.add(expr);
    }

    protected static String genExprcreateResult(String resultLabel, String resultFeature) {
        return String.format("mph.result().create(\"%s\", \"%s\");", resultLabel, resultFeature);
    }

    @Override
    public void setResultType(String resultLabel, String resultFeatureLabel, String resultFeature) {
        String expr = genExprsetResultType(resultLabel, resultFeatureLabel, resultFeature);
        this.txt.add(expr);
    }

    protected static String genExprsetResultType(String resultLabel, String resultFeatureLabel, String resultFeature) {
        return String.format("mph.result(\"%s\").create(\"%s\", \"%s\");", resultLabel, resultFeatureLabel, resultFeature);
    }


    @Override
    public void setResultLabel(String resultLabel) {
        String expr = genExprsetResultLabel(resultLabel);
        this.txt.add(expr);
    }

    protected static String genExprsetResultLabel(String resultLabel) {
        return String.format("mph.result(\"%s\").label(\"%s\");", resultLabel, resultLabel);
    }

    @Override
    public void setResultFeatureIndex(String resultLabel, String featureLabel, String featureType, String featureExpression, int depth) {
        String expr = genExprsetResultFeatureIndex(resultLabel, featureLabel, featureType, featureExpression, depth);
        this.txt.add(expr);
    }

    protected static String genExprsetResultFeatureIndex(String resultLabel, String featureLabel, String featureType, String featureExpression, int depth) {
        return String.format("mph.result(\"%s\").feature(\"%s\").setIndex(\"%s\", \"%s\", %d);", resultLabel, featureLabel, featureType, featureExpression, depth);
    }

    @Override
    public void setResultFeature(String resultLabel, String featureLabel, String featureType, String featureExpression) {
        String expr = genExprsetResultFeature(resultLabel, featureLabel, featureType, featureExpression);
        this.txt.add(expr);
    }

    protected static String genExprsetResultFeature(String resultLabel, String featureLabel, String featureType, String featureExpression) {
        return String.format("mph.result(\"%s\").feature(\"%s\").set(\"%s\", \"%s\");", resultLabel, featureLabel, featureType, featureExpression);
    }

    @Override
    public void setResultFeature(String resultLabel, String featureLabel, String featureType, boolean isShown) {
        String expr = genExprsetResultFeature(resultLabel, featureLabel, featureType, isShown);
        this.txt.add(expr);
    }

    protected static String genExprsetResultFeature(String resultLabel, String featureLabel, String featureType, boolean isShown) {
        return String.format("mph.result(\"%s\").feature(\"%s\").set(\"%s\", %b);", resultLabel, featureLabel, featureType, isShown);
    }

    @Override
    public void setResultFeature(String resultLabel, String featureLabel, String featureType, int Value) {
        String expr = genExprsetResultFeature(resultLabel, featureLabel, featureType, Value);
        this.txt.add(expr);
    }

    protected static String genExprsetResultFeature(String resultLabel, String featureLabel, String featureType, int Value) {
        return String.format("mph.result(\"%s\").feature(\"%s\").set(\"%s\", %d);", resultLabel, featureLabel, featureType, Value);
    }

    @Override
    public void setResult(String resultLabel, String featureLabel, String resultFeature) {
        String expr = genExprsetResult(resultLabel, featureLabel, resultFeature);
        this.txt.add(expr);
    }

    protected static String genExprsetResult(String resultLabel, String featureLabel, String resultFeature) {
        return String.format("mph.result(\"%s\").set(\"%s\", \"%s\");", resultLabel, featureLabel, resultFeature);
    }

    @Override
    public void assignResultSelectiontoAll(String resultLabel, String featureLabel) {
        String expr = genExprassignResultSelectiontoAll(resultLabel, featureLabel);
        this.txt.add(expr);
    }

    protected static String genExprassignResultSelectiontoAll(String resultLabel, String featureLabel) {
        return String.format("mph.result(\"%s\").feature(\"%s\").selection().all();", resultLabel, featureLabel);
    }
    //******************************************************************************************************************


    @Override
    public void removeDatasetResult(String datasetName) {
        String expr = genExprRemoveDatasetResult(datasetName);
        this.txt.add(expr);
    }

    protected static String genExprRemoveDatasetResult(String datasetName) {
        return String.format("mph.result().dataset().remove(\"%s\");", datasetName);
    }


    @Override
    public void showProgressModelUtil(boolean isShown) {
        throw new RuntimeException(METHOD_NOT_SUPPORTED_IN_THE_TEXT_MODE);
    }


    @Override
    public void runAllSol(String solutionName) {
        String expr = genExprRunAllSol(solutionName);
        this.txt.add(expr);
    }

    protected static String genExprRunAllSol(String solutionName) {
        return String.format("mph.sol(\"%s\").runAll();", solutionName);
    }


    @Override
    public void createDatasetResult(String datasetName, String solutionName) {
        String expr = genExprCreateDatasetResult(datasetName, solutionName);
        this.txt.add(expr);
    }

    protected static String genExprCreateDatasetResult(String datasetName, String solutionName) {
        return String.format("mph.result().dataset().create(\"%s\", \"%s\");", datasetName, solutionName);
    }


    @Override
    public void setLabelDatasetResult(String datasetName, String label) {
        String expr = genExprSetLabelDatasetResult(datasetName, label);
        this.txt.add(expr);
    }

    protected static String genExprSetLabelDatasetResult(String datasetName, String label) {
        return String.format("mph.result().dataset().create(\"%s\", \"%s\");", datasetName, label);
    }


    @Override
    public void setParamDatasetResult(String datasetName, String parameterName, String parameterValue) {
        String expr = genExprSetParamDatasetResult(datasetName, parameterName, parameterValue);
        this.txt.add(expr);
    }

    protected static String genExprSetParamDatasetResult(String datasetName, String parameterName, String parameterValue) {
        return String.format("mph.result().dataset(\"%s\").set(\"%s\", \"%s\");", datasetName, parameterName, parameterValue);
    }


    @Override
    public void createPlotFunc(String funName, String plotName) {
        String expr = genExprCreatePlotFunc(funName, plotName);
        this.txt.add(expr);
    }

    protected static String genExprCreatePlotFunc(String funName, String plotName) {
        return String.format("mph.func(\"%s\").createPlot(\"%s\");", funName, plotName);
    }


    @Override
    public void setIndexFunction(String nodeName, String tableName, double value, int row, int col) {
        String expression = genExprSetIndexFunction(nodeName, tableName, value, row, col);
        this.txt.add(expression);
    }

    protected static String genExprSetIndexFunction(String nodeName, String tableName, double value, int row, int col) {
        return String.format("mph.func(\"%s\").setIndex(\"%s\", %f, %d, %d);", nodeName, tableName, value, row, col);
    }


    @Override
    public void runResult(String plotName) {
        String expression = genExprRunResult(plotName);
        this.txt.add(expression);
    }

    protected static String genExprRunResult(String plotName) {
        return String.format("mph.result(\"%s\").run();", plotName);
    }


    @Override
    public void createExportResult(String plotName, String plotGroup, String label, String expression) {
        String expr = genExprCreateExportResult(plotName, plotGroup, label, expression);
        this.txt.add(expr);
    }

    protected static String genExprCreateExportResult(String plotName, String plotGroup, String label, String expression) {
        return String.format("mph.result().export().create(\"%s\", \"%s\", \"%s\", \"%s\");", plotName, plotGroup, label, expression);
    }


    @Override
    public void setParamExportResult(String export, String parameterName, boolean parameterValue) {
        String expression = genExprSetParamExportResult(export, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamExportResult(String export, String parameterName, boolean parameterValue) {
        return String.format("mph.result().export(\"%s\").set(\"%s\", %b);", export, parameterName, parameterValue);
    }


    @Override
    public void setParamExportResult(String export, String parameterName, String parameterValue) {
        String expression = genExprSetParamExportResult(export, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamExportResult(String export, String parameterName, String parameterValue) {
        return String.format("mph.result().export(\"%s\").set(\"%s\", \"%s\");", export, parameterName, parameterValue);
    }


    @Override
    public void runExportResult(String export) {
        String expression = genExprRunExportResult(export);
        this.txt.add(expression);
    }

    protected static String genExprRunExportResult(String export) {
        return String.format("mph.result().export(\"%s\").run();", export);
    }


    @Override
    public void setNamedSelectionFeaturePhysics(String labelPhysics, String nodeLabel, String selectionLabel) {
        String expression = genExprSetNamedSelectionFeaturePhysics(labelPhysics, nodeLabel, selectionLabel);
        this.txt.add(expression);
    }

    protected static String genExprSetNamedSelectionFeaturePhysics(String labelPhysics, String nodeLabel, String selectionLabel) {
        return String.format("mph.physics(\"%s\").feature(\"%s\").selection().named(\"%s\");", labelPhysics, nodeLabel, selectionLabel);
    }


    @Override
    public void createCoordSystem(String nodeLabel, String labelGeom, String elementType) {
        String expression = genExprCreateCoordSystem(nodeLabel, labelGeom, elementType);
        this.txt.add(expression);
    }

    protected static String genExprCreateCoordSystem(String nodeLabel, String labelGeom, String elementType) {
        return String.format("mph.coordSystem().create(\"%s\", \"%s\", \"%s\");", nodeLabel, labelGeom, elementType);
    }


    @Override
    public void setLabelCoordSystem(String coordSystem, String nodeLabel) {
        String expression = genExprSetLabelCoordSystem(coordSystem, nodeLabel);
        this.txt.add(expression);
    }

    protected static String genExprSetLabelCoordSystem(String coordSystem, String nodeLabel) {
        return String.format("mph.coordSystem(\"%s\").label(\"%s\");", coordSystem, nodeLabel);
    }


    @Override
    public void setParamCoordSystem(String nodeLabel, String parameterName, String parameterValue) {
        String expression = genExprSetParamCoordSystem(nodeLabel, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetParamCoordSystem(String nodeLabel, String parameterName, String parameterValue) {
        return String.format("mph.coordSystem(\"%s\").set(\"%s\", \"%s\");", nodeLabel, parameterName, parameterValue);
    }


    @Override
    public void setNamedSelectionCoordSystem(String nodeLabel, String selectionLabel) {
        String expression = genExprSetNamedSelectionCoordSystem(nodeLabel, selectionLabel);
        this.txt.add(expression);
    }

    protected static String genExprSetNamedSelectionCoordSystem(String nodeLabel, String selectionLabel) {
        return String.format("mph.coordSystem(\"%s\").selection().named(\"%s\");", nodeLabel, selectionLabel);
    }


    @Override
    public void setFeaturePhysics(String labelPhysics, String nameFeature, String key, boolean value) {
        String expression = genExprSetFeaturePhysics(labelPhysics, nameFeature, key, value);
        this.txt.add(expression);
    }

    protected static String genExprSetFeaturePhysics(String labelPhysics, String nameFeature, String key, boolean value) {
        return String.format("mph.physics(\"%s\").feature(\"%s\").set(\"%s\", %b);", labelPhysics, nameFeature, key, value);
    }


    @Override
    public void setFeaturePhysics(String labelPhysics, String nodeLabel, String parameterName, String[] parameterValue) {
        String expression = genExprSetFeaturePhysics(labelPhysics, nodeLabel, parameterName, parameterValue);
        this.txt.add(expression);
    }

    protected static String genExprSetFeaturePhysics(String labelPhysics, String nodeLabel, String parameterName, String[] parameterValue) {
        return String.format("mph.physics(\"%s\").feature(\"%s\").set(\"%s\", %s);",
                labelPhysics,
                nodeLabel,
                parameterName,
                TxtSigmaServerUtils.convertArrayToStringExpression(parameterValue));
    }


    @Override
    public void setNamedSelectionFeaturePhysics(String labelPhysics, String selectionLabel) {
        String expression = genExprSetNamedSelectionFeaturePhysics(labelPhysics, selectionLabel);
        this.txt.add(expression);
    }

    protected static String genExprSetNamedSelectionFeaturePhysics(String labelPhysics, String selectionLabel) {
        return String.format("mph.physics(\"%s\").selection().named(\"%s\");", labelPhysics, selectionLabel);
    }


    @Override
    public void setPropPhysics(String labelPhysics, String prop, String key, String value) {
        String expression = genExprSetPropPhysics(labelPhysics, prop, key, value);
        this.txt.add(expression);
    }

    protected static String genExprSetPropPhysics(String labelPhysics, String prop, String key, String value) {
        return String.format("mph.physics(\"%s\").prop(\"%s\").set(\"%s\", \"%s\");", labelPhysics, prop, key, value);
    }


    @Override
    public void setIndexPropPhysics(String labelPhysics, String prop, String table, String value, int row, int col) {
        String expression = genExprSetIndexPropPhysics(labelPhysics, prop, table, value, row, col);
        this.txt.add(expression);
    }

    protected static String genExprSetIndexPropPhysics(String labelPhysics, String prop, String table, String value, int row, int col) {
        return String.format("mph.physics(\"%s\").prop(\"%s\").setIndex(\"%s\", \"%s\", %d, %d);", labelPhysics, prop, table, value, row, col);
    }


    @Override
    public void setFieldFieldPhysics(String labelPhysics, String field1, String field2) {
        String expression = genExprSetFieldFieldPhysics(labelPhysics, field1, field2);
        this.txt.add(expression);
    }

    protected static String genExprSetFieldFieldPhysics(String labelPhysics, String field1, String field2) {
        return String.format("mph.physics(\"%s\").field(\"%s\").field(\"%s\");", labelPhysics, field1, field2);
    }


    @Override
    public void setComponentFieldPhysics(String labelPhysics, String field, int index, String value) {
        String expression = genExprSetComponentFieldPhysics(labelPhysics, field, index, value);
        this.txt.add(expression);
    }

    protected static String genExprSetComponentFieldPhysics(String labelPhysics, String field, int index, String value) {
        return String.format("mph.physics(\"%s\").field(\"%s\").component(%d, \"%s\");", labelPhysics, field, index, value);
    }


    @Override
    public void setParamFeatureGeometry(String labelGeom, String feature, String key, double value) {
        String expression = genExprSetParamFeatureGeometry(labelGeom, feature, key, value);
        this.txt.add(expression);
    }

    protected static String genExprSetParamFeatureGeometry(String labelGeom, String feature, String key, double value) {
        return String.format("mph.geom(\"%s\").feature(\"%s\").set(\"%s\", %f);", labelGeom, feature, key, value);
    }


    @Override
    public void createFeaturePhysics(String labelPhysics, String feature, String label, String key, int value) {
        String expression = genExprSetParamFeatureGeometry(labelPhysics, feature, label, key, value);
        this.txt.add(expression);
    }

    protected static String genExprSetParamFeatureGeometry(String labelPhysics, String feature, String label, String key, int value) {
        return String.format("mph.physics(\"%s\").feature(\"%s\").create(\"%s\", \"%s\", %d);", labelPhysics, feature, label, key, value);
    }


    @Override
    public void setFeatureFeaturePhysics(String labelPhysics, String parentNode, String nodeLabel, String key, String value) {
        String expression = genExprSetFeatureFeaturePhysics(labelPhysics, parentNode, nodeLabel, key, value);
        this.txt.add(expression);
    }

    protected static String genExprSetFeatureFeaturePhysics(String labelPhysics, String parentNode, String nodeLabel, String key, String value) {
        return String.format("mph.physics(\"%s\").feature(\"%s\").feature(\"%s\").set(\"%s\", \"%s\");", labelPhysics, parentNode, nodeLabel, key, value);
    }


    @Override
    public void setMaterial(String label, String family, String value) {
        String expression = genExprSetMaterial(label, family, value);
        this.txt.add(expression);
    }


    @Override
    public void connect(String mphServerPath) throws IOException {
        /*
        This method is not needed for the text-based server and, therefore, not supported.
         */
    }


    @Override
    public TxtSigmaServer prepareModelTemplate() {
        return this;
    }


    @Override
    public void disconnect() {
        /*
        This method is not needed for the text-based server and, therefore, not supported.
         */
    }

    /**
     * Method builds a model
     */
    @Override
    public void build(String modelPath) {
        String fileName = SigmaServerUtils.getFileNameFromPath(modelPath);
        output = createComsolJavaModel(fileName);
    }

    /**
     * Method compiles a model
     */
    @Override
    public void compile(String modelPath) throws IOException {
        String fileDirectory = SigmaServerUtils.getFileDirectoryFromPath(modelPath);
        String fileName = SigmaServerUtils.getFileNameFromPath(modelPath);
        //Check if name is correct Java identifier
        SigmaServerUtils.checkClassNameValidity(fileName);

        //Write raw instructions as a txt file
        TextFile.writeMultiLine(Paths.get(fileDirectory, fileName) + ".txt", this.txt, false);

        //Write instructions to a .java file
        TextFile.writeMultiLine(Paths.get(fileDirectory, fileName) + ".java", output, false);

        //execute compile process
        String javaFileName = Paths.get(fileDirectory, fileName) + ".java";
        String classFileName = Paths.get(fileDirectory, fileName) + ".class";
        String mphFileName = Paths.get(fileDirectory, fileName) + ".mph";

        LOGGER.info(String.format("COMSOL: Compiling %s into %s", javaFileName, classFileName));
        String[] compileExecutionArguments = {Paths.get(this.comsolBatchPath, "comsolcompile") + ".exe",
                "-jdkroot",
                this.javaJDKPath,
                javaFileName};
        SigmaServerUtils.executeBatch(compileExecutionArguments);

        LOGGER.info(String.format("COMSOL: Opening %s and saving as %s", classFileName, mphFileName));
        String[] batchExecutionArguments = {Paths.get(this.comsolBatchPath, "comsolbatch") + ".exe",
                "-inputfile",
                Paths.get(fileDirectory, fileName) + ".class",
                "-outputfile",
                Paths.get(fileDirectory, fileName) + ".mph"};
        SigmaServerUtils.executeBatch(batchExecutionArguments);
        //COMSOL adds a suffix _Model to the outputfile name, so it is renamed to the original one
        LOGGER.info(String.format("Renaming file: From %s to %s", fileName + "_Model.mph", fileName + ".mph"));
        Path source = Paths.get(fileDirectory, fileName + "_Model.mph");
        Files.move(source, source.resolveSibling(fileName + ".mph"), StandardCopyOption.REPLACE_EXISTING);
    }

    protected static String genExprSetMaterial(String label, String family, String value) {
        return String.format("mph.material(\"%s\").set(\"%s\", \"%s\");", label, family, value);
    }
}
