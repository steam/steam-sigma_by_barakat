package comsol;

import comsol.constants.MPHC;
import comsol.definitions.utils.Variable;
import comsol.definitions.utils.VariableGroup;
import config.ConfigSigma;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import server.SigmaServer;
import comsol.api.*;

import java.util.ArrayList;
import java.util.List;

public class QuenchHeaterMPHBuilder {
    // Declaration of variables:
    private static final Logger LOGGER = LogManager.getLogger(QuenchHeaterMPHBuilder.class);
    private final SigmaServer srv;
    public static int indexHtGlobal;
    private final String pathCLib;
    private final String pathOutput;
    private final String comsolBatchPath;

    // Declaration of Quench Heater variables and labels:
    public static int numOfQHs;
    private final String[] QHinput;
    private final String QHLabel;
    private final String QHOperatorNameLabel;

    //******************************************************************************************************************
    //                                                  CONSTRUCTOR
    //******************************************************************************************************************
    public QuenchHeaterMPHBuilder(ConfigSigma cfg, SigmaServer srv) {
        // Initialization of variables:
        this.indexHtGlobal = 0;
        this.pathCLib = String.format("%s\\%s", cfg.getExternalCFunLibPath(), MPHC.LABEL_MATLIBRARY_VERSION);
        this.pathOutput = cfg.getOutputModelPath();
        this.comsolBatchPath = cfg.getComsolBatchPath();
        this.srv = srv;

        // Initialization of Quench Heater variables and labels:
        this.numOfQHs = 2;
        this.QHinput = new String[this.numOfQHs];
        this.QHLabel = MPHC.LABEL_QH;
        this.QHOperatorNameLabel = "aveop1";
    }

    //******************************************************************************************************************
    //                                  BUILD EVERYTHING RELATED TO QUENCH HEATERS
    //******************************************************************************************************************
    public void buildQuenchHeaterMPH() {
        long startTime = System.currentTimeMillis();

        LOGGER.info("COMSOL: Working on Quench Heaters");
        DefinitionsAPI.createComponentNode(srv, MPHC.LABEL_COMP_QH);
        GeometryAPI.createGeometryNode(srv, MPHC.LABEL_GEOM_QH, GeometryAPI.COMPONENT_DIMENSION_1D);
        HeatTransferInSolidsPhysicsAPI.addNode(srv, MPHC.LABEL_PHYSICSTH_QH, MPHC.LABEL_GEOM_QH);
        buildQHDefinitions(srv);
        buildQHMesh(srv);
        buildStandardSimulationSettings(srv);
        ResultsAPI.createMonolithicResultsNode(srv);

        LOGGER.info("COMSOL: Build model");
        srv.build(pathOutput);

        LOGGER.info("COMSOL: Done!");

        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        LOGGER.info("Elapsed time " + elapsedTime / 1e3 + " [s]");
    }

    //******************************************************************************************************************
    //                                                THE BIG COLLECTOR
    //******************************************************************************************************************

    /**
     * Method builds all definitions for Quench heaters (variable groups, functions, geometry, heat transfer, materials)
     *
     * @param srv - input SigmaServer instance
     */
    private void buildQHDefinitions(SigmaServer srv) {
        buildQHGlobalDefinitions(srv);
        buildQHGeometryNodes(srv);
        buildQHExpressions(srv);
        buildQHMaterials(srv);
        buildQHHeatTransferNodes(srv);
    }

    //******************************************************************************************************************
    //                                          THE MINOR COLLECTORS
    //******************************************************************************************************************

    /**
     * Method collects all methods related to Quench Heater geometry (the actual geometry, averaging component couplings
     * and selections)
     *
     * @param srv - input SigmaServer instance
     */
    private void buildQHGeometryNodes(SigmaServer srv) {
        buildQHGeometry(srv);
        buildQHExplicitSelections(srv);
        buildQHAverageComponentCouplings(srv);
    }

    /**
     * Method collects all expressions (variable groups, analytic functions) for the Quench Heaters:
     *
     * @param srv - input SigmaServer instance
     */
    private void buildQHExpressions(SigmaServer srv) {
        buildQuenchHeaterProperties(srv);
        buildresSteelFunction(srv);
        buildIQHFunction(srv);
        buildHeaterDomainVariables(srv);
        buildTemperatureStabilization(srv);
    }

    /**
     * Method collects all methods related to Quench Heater heat transfer (thin layer, heat source, heat flux
     * and temperature nodes)
     *
     * @param srv - input SigmaServer instance
     */
    private void buildQHHeatTransferNodes(SigmaServer srv) {
        buildQHTemperature(srv);
        buildQHHeatSources(srv);
        buildQHHeatFluxNodes(srv);
        buildQHThinLayers(srv);
    }

    //******************************************************************************************************************
    //                                           METHOD FOR GLOBAL PARAMETERS
    //******************************************************************************************************************

    /**
     * The method builds the Quench Heater global parameters
     */
    private void buildQHGlobalDefinitions(SigmaServer srv) {
        // Global Quench Heater parameters independant of number of Quench Heaters:
        GlobalDefinitionsAPI.setParameter(srv, MPHC.LABEL_TRIGGER_TIME_QH, String.valueOf(0.004).concat("[s]"), "Triggering time of QH's");
        GlobalDefinitionsAPI.setParameter(srv, MPHC.LABEL_OPERATIONAL_TEMPERATUR, (MPHC.VALUE_T_OPERATIONAL), "Operational temperature");
        GlobalDefinitionsAPI.setParameter(srv, MPHC.LABEL_INSULATION_THICKNESS_QH_TO_COIL, String.valueOf(MPHC.VALUE_INSULATION_THICKNESS_QH_TO_COIL).concat("[m]"), "Thickness of QH insulation between QH strip and coil");
        GlobalDefinitionsAPI.setParameter(srv, MPHC.LABEL_INSULATION_THICKNESS_TO_COIL, String.valueOf(MPHC.VALUE_INSULATION_THICKNESS_TO_COIL).concat("[m]"), "Thickness of QH insulation to the coil");
        GlobalDefinitionsAPI.setParameter(srv, MPHC.LABEL_INSULATION_THICKNESS_QH_TO_BATH, String.valueOf(MPHC.VALUE_INSULATION_THICKNESS_QH_TO_BATH).concat("[m]"), "Thickness of QH insulation between QH strip and bath");
        GlobalDefinitionsAPI.setParameter(srv, MPHC.LABEL_EXPONENTIAL_TIME_CONSTANT_DECAY, String.valueOf(MPHC.VALUE_EXPONENTIAL_TIME_CONSTANT_DECAY).concat("[m]"), "Exponential time constant decay");
        GlobalDefinitionsAPI.setParameter(srv, MPHC.LABEL_NUMBER_OF_QH_SUBDIVISIONS, String.valueOf(MPHC.VALUE_NUMBER_OF_QH_SUBDIVISIONS), "Number of subdivisions within QH");
        GlobalDefinitionsAPI.setParameter(srv, MPHC.LABEL_INITIAL_QH_CURRENT, String.valueOf(MPHC.VALUE_INITIAL_QH_CURRENT).concat("[A]"), "Initial QH current");
        GlobalDefinitionsAPI.setParameter(srv, MPHC.LABEL_INSULATION_THICKNESS_QH_STRIP, String.valueOf(MPHC.VALUE_INSULATION_THICKNESS_QH_STRIP).concat("[m]"), "Thickess of Quench Heater Steel Strip");

        // Global Quench Heater parameters dependant of number of Quench Heaters:
        for (int i = 0; i < numOfQHs; i++) {
            GlobalDefinitionsAPI.setParameter(srv, MPHC.LABEL_WIDTH_QH + String.format("%d", i + 1), String.valueOf(20e-3).concat("[m]"), "Width of QH " + String.format("%d", i + 1));
            GlobalDefinitionsAPI.setParameter(srv, this.QHLabel + String.format("%d", i + 1) + MPHC.LABEL_FRACTION_OF_QH_STATION, String.valueOf(0.2), "Fraction of the heater stations for QH " + String.format("%d", i + 1));
        }
    }

    //******************************************************************************************************************
    //                                           METHODS FOR GEOMETRY
    //******************************************************************************************************************

    /**
     * Method creates the Geometry of all Quench Heaters:
     *
     * @param srv - input SigmaServer instance
     */
    private void buildQHGeometry(SigmaServer srv) {
        // Initialization of input strings for creation of Union Selections (parameters not equal to 'numOfQHs' corresponds to the number of differen Quench Heater intervals):
        String[] BathTemperatureSelection = new String[numOfQHs];
        String[] HeatFluxSelection = new String[numOfQHs];
        String[] QuenchHeaterUnionSelection = new String[4];
        String[] KaptonDomainIndividualSelection = new String[2];
        String[] HeatertoBathThinLayerSelection = new String[numOfQHs];
        String[] HeatertoCoilThinLayerSelection = new String[numOfQHs];
        String[] CoilInsulationThinLayerSelection = new String[numOfQHs];

        for (int i = 0; i < numOfQHs; i++) {
        // Construction of geometry and its contribution to selection:
            // Bath temperature point:
            String BathTemperaturePointLabel = String.format("%s%d%s", this.QHLabel, i + 1, MPHC.LABEL_BATH_TEMPERATURE_POINT);
            String BathTemperaturePointExpr = String.format("%s*%d+0[m]", MPHC.VALUE_QUENCH_HEATER_DISPLACEMENT, i);
            String BathTemperatureSelectionLabel = String.format("%s%d%s", this.QHLabel, i + 1, MPHC.LABEL_BATH_TEMPERATURE_SELECTION);
            GeometryAPI.createPointGeometry(srv, MPHC.LABEL_GEOM_QH, BathTemperaturePointLabel, BathTemperaturePointLabel, BathTemperaturePointExpr);
            GeometryAPI.contributeGeometrytoSelection(srv, MPHC.LABEL_GEOM_QH, BathTemperatureSelectionLabel, BathTemperaturePointLabel);

            // Heater to bath points:
            // Build first initial point:
            String HeatertoBathInitPointLabel = String.format("%s%d%s", this.QHLabel, i + 1, MPHC.LABEL_HEATER_TO_BATH_INS_POINT_INIT);
            String HeatertoBathInitPointExpr = String.format("%s*%d+0.5*%s/%s", MPHC.VALUE_QUENCH_HEATER_DISPLACEMENT, i, MPHC.LABEL_INSULATION_THICKNESS_QH_TO_BATH, MPHC.LABEL_NUMBER_OF_QH_SUBDIVISIONS);
            String HeatertoBathInitPointSelectionLabel = String.format("%s%d%s", this.QHLabel, i + 1, MPHC.LABEL_HEATER_TO_BATH_INS_POINT_SELECTION);
            GeometryAPI.createPointGeometry(srv, MPHC.LABEL_GEOM_QH, HeatertoBathInitPointLabel, HeatertoBathInitPointLabel, HeatertoBathInitPointExpr);
            GeometryAPI.contributeGeometrytoSelection(srv, MPHC.LABEL_GEOM_QH, HeatertoBathInitPointSelectionLabel, HeatertoBathInitPointLabel);

            // Transform the second point:
            String HeatertoBathTransformLabel = String.format("%s%d%s", this.QHLabel, i + 1, MPHC.LABEL_HEATER_TO_BATH_INS_LAYER);
            String HeatertoBathTransformSize = MPHC.LABEL_NUMBER_OF_QH_SUBDIVISIONS;
            String HeatertoBathTransformDispl = String.format("%s/%s", MPHC.LABEL_INSULATION_THICKNESS_QH_TO_BATH, MPHC.LABEL_NUMBER_OF_QH_SUBDIVISIONS);
            String HeatertoBathTransformSelectionLabel = String.format("%s%d%s", this.QHLabel, i + 1, MPHC.LABEL_HEATER_TO_BATH_INS_TRANSFORM_SELECTION);
            GeometryAPI.createArrayGeometry(srv, HeatertoBathTransformLabel, HeatertoBathTransformLabel, HeatertoBathInitPointLabel, HeatertoBathTransformSize, HeatertoBathTransformDispl);
            GeometryAPI.contributeGeometrytoSelection(srv, MPHC.LABEL_GEOM_QH, HeatertoBathTransformSelectionLabel, HeatertoBathTransformLabel);
            //HeatertoBathThinLayerSelection[i] = HeatertoBathTransformSelectionLabel;

            // Heater to coil insulation points:
            // Build first initial point:
            String HeatertoCoilInitPointLabel = String.format("%s%d%s", this.QHLabel, i + 1, MPHC.LABEL_HEATER_TO_COIL_INS_POINT_INIT);
            String HeatertoCoilInitPntExpr = String.format("%s*%d+%s+%s+0.5*%s/%s", MPHC.VALUE_QUENCH_HEATER_DISPLACEMENT, i, MPHC.LABEL_INSULATION_THICKNESS_QH_TO_BATH, MPHC.LABEL_INSULATION_THICKNESS_QH_STRIP, MPHC.LABEL_INSULATION_THICKNESS_QH_TO_COIL, MPHC.LABEL_NUMBER_OF_QH_SUBDIVISIONS);
            String HeatertoCoilInitPointSelectionLabel = String.format("%s%d%s", this.QHLabel, i + 1, MPHC.LABEL_HEATER_TO_COIL_INS_POINT_SELECTION);
            GeometryAPI.createPointGeometry(srv, MPHC.LABEL_GEOM_QH, HeatertoCoilInitPointLabel, HeatertoCoilInitPointLabel, HeatertoCoilInitPntExpr);
            GeometryAPI.contributeGeometrytoSelection(srv, MPHC.LABEL_GEOM_QH, HeatertoCoilInitPointSelectionLabel, HeatertoCoilInitPointLabel);

            // Transform the first initial point:
            String HeatertoCoilTransformLabel = String.format("%s%d%s", this.QHLabel, i + 1, MPHC.LABEL_HEATER_TO_COIL_INS_LAYER);
            String HeatertoCoilTransformSize = MPHC.LABEL_NUMBER_OF_QH_SUBDIVISIONS;
            String HeatertoCoilTransformDispl = String.format("%s/%s", MPHC.LABEL_INSULATION_THICKNESS_QH_TO_COIL, MPHC.LABEL_NUMBER_OF_QH_SUBDIVISIONS);
            String HeatertoCoilTransformSelectionLabel = String.format("%s%d%s", this.QHLabel, i + 1, MPHC.LABEL_HEATER_TO_COIL_INS_TRANSFORM_SELECTION);
            GeometryAPI.createArrayGeometry(srv, HeatertoCoilTransformLabel, HeatertoCoilTransformLabel, HeatertoCoilInitPointLabel, HeatertoCoilTransformSize, HeatertoCoilTransformDispl);
            GeometryAPI.contributeGeometrytoSelection(srv, MPHC.LABEL_GEOM_QH, HeatertoCoilTransformSelectionLabel, HeatertoCoilTransformLabel);
            //HeatertoCoilThinLayerSelection[i] = HeatertoCoilTransformSelectionLabel;

            // Coil insulation points:
            // Build first initial point:
            String CoilInsulationInitPointLabel = String.format("%s%d%s", this.QHLabel, i + 1, MPHC.LABEL_COIL_INS_POINT_INIT);
            String CoilInsulationInitPointExpr = String.format("%s*%d+%s+%s+%s+0.5*%s/%s", MPHC.VALUE_QUENCH_HEATER_DISPLACEMENT, i, MPHC.LABEL_INSULATION_THICKNESS_QH_TO_BATH, MPHC.LABEL_INSULATION_THICKNESS_QH_STRIP, MPHC.LABEL_INSULATION_THICKNESS_QH_TO_COIL, MPHC.LABEL_INSULATION_THICKNESS_TO_COIL, MPHC.LABEL_NUMBER_OF_QH_SUBDIVISIONS);
            String CoilInsulationInitPointSelectionLabel = String.format("%s%d%s", this.QHLabel, i + 1, MPHC.LABEL_COIL_INS_POINT_SELECTION);
            GeometryAPI.createPointGeometry(srv, MPHC.LABEL_GEOM_QH, CoilInsulationInitPointLabel, CoilInsulationInitPointLabel, CoilInsulationInitPointExpr);
            GeometryAPI.contributeGeometrytoSelection(srv, MPHC.LABEL_GEOM_QH, CoilInsulationInitPointSelectionLabel, CoilInsulationInitPointLabel);

            // Transform the first initial point:
            String CoilInsulationTransformLabel = String.format("%s%d%s", this.QHLabel, i + 1, MPHC.LABEL_COIL_INS_LAYER);
            String CoilInsulationTransformSize = MPHC.LABEL_NUMBER_OF_QH_SUBDIVISIONS;
            String CoilInsulationTransformDispl = String.format("%s/%s", MPHC.LABEL_INSULATION_THICKNESS_TO_COIL, MPHC.LABEL_NUMBER_OF_QH_SUBDIVISIONS);
            String CoilInsulationTransformSelectionLabel = String.format("%s%d%s", this.QHLabel, i + 1, MPHC.LABEL_COIL_INS_TRANSFORM_SELECTION);
            GeometryAPI.createArrayGeometry(srv, CoilInsulationTransformLabel, CoilInsulationTransformLabel, CoilInsulationInitPointLabel, CoilInsulationTransformSize, CoilInsulationTransformDispl);
            GeometryAPI.contributeGeometrytoSelection(srv, MPHC.LABEL_GEOM_QH, CoilInsulationTransformSelectionLabel, CoilInsulationTransformLabel);
            //CoilInsulationThinLayerSelection[i] = CoilInsulationTransformSelectionLabel;

            // Build intervals:
            // Heater to bath interval:
            String HeatertoBathIntervalLabel = String.format("%s%d%s", this.QHLabel, i + 1, MPHC.LABEL_HEATER_TO_BATH_INTERVAL);
            String HeatertoBathIntervalLeft = String.format("%s*%d+0[m]", MPHC.VALUE_QUENCH_HEATER_DISPLACEMENT, i);
            String HeatertoBathIntervalRight = String.format("%s*%d+%s", MPHC.VALUE_QUENCH_HEATER_DISPLACEMENT, i, MPHC.LABEL_INSULATION_THICKNESS_QH_TO_BATH);
            String HeatertoBathIntervalSelectionLabel = String.format("%s%d%s", this.QHLabel, i + 1, MPHC.LABEL_HEATER_TO_BATH_INTERVAL_SELECTION);
            GeometryAPI.createIntervalGeometry(srv, HeatertoBathIntervalLabel, HeatertoBathIntervalLabel, HeatertoBathIntervalLeft, HeatertoBathIntervalRight);
            GeometryAPI.contributeGeometrytoSelection(srv, MPHC.LABEL_GEOM_QH, HeatertoBathIntervalSelectionLabel, HeatertoBathIntervalLabel);

            // Steel strip interval:
            String SteelStripIntervalLabel = String.format("%s%d%s", this.QHLabel, i + 1, MPHC.LABEL_STEEL_STRIP_INTERVAL);
            String SteelStripIntervalLeft = HeatertoBathIntervalRight;
            String SteelStripIntervalRight = HeatertoBathIntervalRight + "+" + MPHC.LABEL_INSULATION_THICKNESS_QH_STRIP;
            String SteelStripSelectionLabel = String.format("%s%d%s", this.QHLabel, i + 1, MPHC.LABEL_STEEL_STRIP_INTERVAL_SELECTION);
            GeometryAPI.createIntervalGeometry(srv, SteelStripIntervalLabel, SteelStripIntervalLabel, SteelStripIntervalLeft, SteelStripIntervalRight);
            GeometryAPI.contributeGeometrytoSelection(srv, MPHC.LABEL_GEOM_QH, SteelStripSelectionLabel, SteelStripIntervalLabel);
            //HeatFluxSelection[i] = SteelStripSelectionLabel;

            // Heater to coil interval:
            String HeatertoCoilIntervalLabel = String.format("%s%d%s", this.QHLabel, i + 1, MPHC.LABEL_HEATER_TO_COIL_INTERVAL);
            String HeatertoCoilIntervalLeft = SteelStripIntervalRight;
            String HeatertoCoilIntervalRight = SteelStripIntervalRight + "+" + MPHC.LABEL_INSULATION_THICKNESS_QH_TO_COIL;
            String HeatertoCoilIntervalSelectionLabel = String.format("%s%d%s", this.QHLabel, i + 1, MPHC.LABEL_HEATER_TO_COIL_INTERVAL_SELECTION);
            GeometryAPI.createIntervalGeometry(srv, HeatertoCoilIntervalLabel, HeatertoCoilIntervalLabel, HeatertoCoilIntervalLeft, HeatertoCoilIntervalRight);
            GeometryAPI.contributeGeometrytoSelection(srv, MPHC.LABEL_GEOM_QH, HeatertoCoilIntervalSelectionLabel, HeatertoCoilIntervalLabel);

            // Coil insulation interval:
            String CoilInsulationIntervalLabel = String.format("%s%d%s", this.QHLabel, i + 1, MPHC.LABEL_COIL_INSULATION_INTERVAL);
            String CoilInsulationIntervalLeft = HeatertoCoilIntervalRight;
            String CoilInsulationIntervalRight = HeatertoCoilIntervalRight + "+" + MPHC.LABEL_INSULATION_THICKNESS_TO_COIL;
            String CoilInsulationIntervalSelectionLabel = String.format("%s%d%s", this.QHLabel, i + 1, MPHC.LABEL_COIL_INS_INTERVAL_SELECTION);
            GeometryAPI.createIntervalGeometry(srv, CoilInsulationIntervalLabel, CoilInsulationIntervalLabel, CoilInsulationIntervalLeft, CoilInsulationIntervalRight);
            GeometryAPI.contributeGeometrytoSelection(srv, MPHC.LABEL_GEOM_QH, CoilInsulationIntervalSelectionLabel, CoilInsulationIntervalLabel);

            // Coil temperature points:
            String CoilTemperaturePointLabel = String.format("%s%d%s", this.QHLabel, i + 1, MPHC.LABEL_COIL_TEMPERATURE_POINT);
            String CoilTemperaturePointExpr = CoilInsulationIntervalRight;
            String CoilTemperatureSelectionLabel = String.format("%s%d%s", this.QHLabel, i + 1, MPHC.LABEL_COIL_TEMPERATURE_SELECTION);
            GeometryAPI.createPointGeometry(srv, MPHC.LABEL_GEOM_QH, CoilTemperaturePointLabel, CoilTemperaturePointLabel, CoilTemperaturePointExpr);
            GeometryAPI.contributeGeometrytoSelection(srv, MPHC.LABEL_GEOM_QH, CoilTemperatureSelectionLabel, CoilTemperaturePointLabel);


        // Creation of String inputs for Union Selections from contributed geometry:
            // Bath Temperature selection input:
            BathTemperatureSelection[i] = BathTemperatureSelectionLabel;

            // Heat Flux selection input:
            HeatFluxSelection[i] = SteelStripSelectionLabel;

            // Heater to Bath Thin Layer Selection input:
            HeatertoBathThinLayerSelection[i] = HeatertoBathTransformSelectionLabel;

            // Heater to Coil Thin Layer selection input:
            HeatertoCoilThinLayerSelection[i] = HeatertoCoilTransformSelectionLabel;

            // Coil Insulation Thin Layer selection input:
            CoilInsulationThinLayerSelection[i] = CoilInsulationTransformSelectionLabel;

            // Creation of Union Selection input and Union Selection from all domains of every quench heater:
            QuenchHeaterUnionSelection[0] = HeatertoBathIntervalSelectionLabel;
            QuenchHeaterUnionSelection[1] = SteelStripSelectionLabel;
            QuenchHeaterUnionSelection[2] = HeatertoCoilIntervalSelectionLabel;
            QuenchHeaterUnionSelection[3] = CoilInsulationIntervalSelectionLabel;
            String QuenchHeaterUnionSelectionLabel = String.format("%s%d_Union_Selection", this.QHLabel, i + 1);
            GeometryAPI.createUnionSelectionDomain(srv, MPHC.LABEL_GEOM_QH, QuenchHeaterUnionSelectionLabel, QuenchHeaterUnionSelection);

            // Creation of Union Selection input and Union Selection for all kapton domains of every quench heater:
            KaptonDomainIndividualSelection[0] = HeatertoBathIntervalSelectionLabel;
            KaptonDomainIndividualSelection[1] = HeatertoCoilIntervalSelectionLabel;
            String KaptonDomainIndividualSelectionLabel = String.format("%s%d%s", this.QHLabel, i+1, MPHC.LABEL_KAPTON_DOMAIN_SELECTION);
            GeometryAPI.createUnionSelectionDomain(srv, MPHC.LABEL_GEOM_QH, KaptonDomainIndividualSelectionLabel, KaptonDomainIndividualSelection);
        }

        // Creation of Union Selection for Bath Temperature Selection:
        GeometryAPI.createUnionSelectionBoundary(srv, MPHC.LABEL_GEOM_QH, this.QHLabel + "Bath_Temperature_Selection", BathTemperatureSelection);

        // Creation of Union Selection for Heat Flux Selection:
        GeometryAPI.createUnionSelectionDomain(srv, MPHC.LABEL_GEOM_QH, this.QHLabel + "Heat_Flux_Selection", HeatFluxSelection);

        // Creation of Union Selection for Heater to Bath Thin Layer:
        GeometryAPI.createUnionSelectionBoundary(srv, MPHC.LABEL_GEOM_QH, this.QHLabel + "Heater_to_Bath_Thin_Layer_Selection", HeatertoBathThinLayerSelection);

        // Creation of Union Selection for Heater to Coil Thin Layer:
        GeometryAPI.createUnionSelectionBoundary(srv, MPHC.LABEL_GEOM_QH, this.QHLabel + "Heater_to_Coil_Thin_Layer_Selection", HeatertoCoilThinLayerSelection);

        // Creation of Union Selection for Coil Insulation Thin Layer:
        GeometryAPI.createUnionSelectionBoundary(srv, MPHC.LABEL_GEOM_QH, this.QHLabel + "Coil_Insulation_Layer_Selection",  CoilInsulationThinLayerSelection);

        // Build the final geometry with selections:
        GeometryAPI.finalizeGeometry(srv, MPHC.LABEL_GEOM_QH, "fin");
    }

    /**
     * Method builds Explicit Selections for the Quench Heaters:
     *
     * @param srv - input SigmaServer instance
     */
    private void buildQHExplicitSelections(SigmaServer srv) {
        for (int i = 0; i < numOfQHs; i++) {
            QHinput[i] = String.format("%d", i + 1);
            String ExplicitSelectionLabel = String.format("%s%d_Selection", this.QHLabel, i + 1);
            GeometryAPI.createExplicitSelectioninDefinitions(srv, MPHC.LABEL_COMP, ExplicitSelectionLabel, ExplicitSelectionLabel, 1, QHinput[i]);
        }
    }

    /**
     * Method builds Averaging Component Coupling for the Quench Heaters:
     *
     * @param srv - input SigmaServer instance
     */
    private void buildQHAverageComponentCouplings(SigmaServer srv) {
        for (int i = 0; i < numOfQHs; i++) {
            // Creates Average component coupling for each Quench Heater:
            String AverageTemperatureLabel = String.format("%s%d%s", this.QHLabel, i + 1, MPHC.LABEL_AVERAGE_TEMPERATURE);
            String OperatorLabel = String.format("%s%d_%s", this.QHLabel, i + 1, this.QHOperatorNameLabel);
            DefinitionsAPI.createComponentCoupling(srv, AverageTemperatureLabel, OperatorLabel, DefinitionsAPI.ComponentCouplingType.Average, MPHC.LABEL_GEOM);

            // Assigns each Averaging component couplings to Explicit Selections:
            String ExplicitSelectionLabel = String.format("%s%d_Selection", this.QHLabel, i + 1);
            DefinitionsAPI.assignSelection(srv, AverageTemperatureLabel, ExplicitSelectionLabel);
        }
    }

    //******************************************************************************************************************
    //                             METHODS FOR EXPRESSIONS (VARIABLES, FUNCTIONS ETC.)
    //******************************************************************************************************************

    /**
     * Method creates Quench Heater Properties variable group
     *
     * @param srv - Input SigmaServer instance
     */
    private void buildQuenchHeaterProperties(SigmaServer srv) {
        String labelGroup = String.format(this.QHLabel + "%s", MPHC.LABEL_QUENCH_HEATER_PROPERTY);
        List<Variable> varList = new ArrayList<>();
        for (int i = 0; i < numOfQHs; i++) {

            // T_QH(i) is added:
            String TQHLabel = String.format("T_%s%d", this.QHLabel, i + 1);
            String THQExpressionsLabel = String.format("%s%d_%s(T)", this.QHLabel, i + 1, this.QHOperatorNameLabel);
            varList.add(createQHPropertyVariable(TQHLabel, THQExpressionsLabel));

            // Q_QH(i) is added:
            String QQHLabel = String.format("Q_%s%d", this.QHLabel, i + 1);
            String netHeatRateLabel = String.format("%s.ht2.%s%d_Coil_Temperature.ntfluxInt[1/m^2]*%s%d%s", MPHC.LABEL_COMP_QH, this.QHLabel, i + 1, this.QHLabel, i + 1, MPHC.LABEL_FRACTION_OF_QH_STATION);
            varList.add(createQHPropertyVariable(QQHLabel, netHeatRateLabel));
        }
        VariableGroup varGroup = new VariableGroup(labelGroup, varList);
        varGroup.buildVariableGroup(srv, MPHC.LABEL_COMP, MPHC.LABEL_GEOM_QH, "", 2);
    }

    private static Variable createQHPropertyVariable(String varName, String varExpression) {
        return new Variable(varName, varExpression);
    }

    /**
     * Method defines Analytic Function for the resistivity of steel:
     *
     * @param srv - input SigmaServer instance
     */
    private void buildresSteelFunction(SigmaServer srv) {
        String nodeName = this.QHLabel + MPHC.LABEL_RESISTIVITY_OF_STEEL;
        String expressionNameLabel = this.QHLabel + MPHC.LABEL_RESISTIVITY_OF_STEEL;
        String expressionLabel = "5.45e-7+(6.9e-7-5.45e-7)/300*T";
        String argumentLabel = "T";
        String argumentUnit = "K";
        String functionUnit = "ohm*m";
        DefinitionsAPI.createAnalyticFunctioninGlobal(srv, nodeName, expressionNameLabel, expressionLabel, argumentLabel, argumentUnit, functionUnit);
    }

    /**
     * Method defines Analytic Function for the Quench Heater Current:
     *
     * @param srv - input SigmaServer instance
     */
    private void buildIQHFunction(SigmaServer srv) {
        String nodeName = MPHC.LABEL_QH_CURRENT;
        String expressionNameLabel = MPHC.LABEL_QH_CURRENT;
        String expressionLabel = String.format("%s*exp(-(t-%s)/%s)*(t>%s)",
                MPHC.LABEL_INITIAL_QH_CURRENT,
                MPHC.LABEL_TRIGGER_TIME_QH,
                MPHC.LABEL_EXPONENTIAL_TIME_CONSTANT_DECAY,
                MPHC.LABEL_TRIGGER_TIME_QH);
        String argumentLabel = "t";
        String argumentUnit = "s";
        String functionUnit = "A";
        DefinitionsAPI.createAnalyticFunctioninGlobal(
                srv,
                nodeName,
                expressionNameLabel,
                expressionLabel,
                argumentLabel,
                argumentUnit,
                functionUnit);
    }

    /**
     * Method creates a user-defined number (numOfQHs) of heater domains for quench heater:
     *
     * @param srv - input SigmaServer instance
     */
    private void buildHeaterDomainVariables(SigmaServer srv) {
        for (int i = 0; i < numOfQHs; i++) {
            List<Variable> varList = new ArrayList<>();
            String labelGroup = String.format("%s%d%s", this.QHLabel, i + 1, MPHC.LABEL_HEATER_DOMAIN);
            String selectionLabel = String.format("%s_%s%d_Union_Selection", MPHC.LABEL_GEOM_QH, this.QHLabel, i + 1);

            String label = this.QHLabel + String.format("%d", i + 1) + "_Pheater";
            String value = String.format("%s%s(%s)/(%s*%s%d)^2*%s(t)^2",
                    this.QHLabel,
                    MPHC.LABEL_RESISTIVITY_OF_STEEL,
                    MPHC.LABEL_QUENCH_HEATER_TEMPERATURE,
                    MPHC.LABEL_INSULATION_THICKNESS_QH_STRIP,
                    MPHC.LABEL_WIDTH_QH,
                    i+1,
                    MPHC.LABEL_QH_CURRENT);

            varList.add(new Variable(label, value));                                                                    // Adds every variable name and expression to the variable list
            VariableGroup varGroup = new VariableGroup(labelGroup, varList);                                            // Variable node and the variable list is defined
            varGroup.buildVariableGroup(srv, MPHC.LABEL_COMP_QH, MPHC.LABEL_GEOM_QH, selectionLabel, 0);
        }
    }

    /**
     * This method creates a heater domain variable for each quench heater in the model:
     *
     * @param srv - input SigmaServer instance
     */
    private void buildTemperatureStabilization(SigmaServer srv) {
        List<Variable> varList = new ArrayList<>();
        String labelGroup = String.format(this.QHLabel + "%s", MPHC.LABEL_TEMPERATURE_STABILIZATION);
        String label = String.format("%s", MPHC.LABEL_QUENCH_HEATER_TEMPERATURE);
        String value = String.format("max(%s,%s)", MPHC.LABEL_PHYSICSTH_T, MPHC.LABEL_OPERATIONAL_TEMPERATUR);
        varList.add(new Variable(label, value));

        VariableGroup varGroup = new VariableGroup(labelGroup, varList);
        varGroup.buildVariableGroup(srv, MPHC.LABEL_COMP_QH, MPHC.LABEL_GEOM_QH, "", 2);
    }

    //******************************************************************************************************************
    //                                      METHODS FOR HEAT TRANSFER NODES
    //******************************************************************************************************************

    /**
     * Method builds Temperature nodes in the Heat Transfer node
     *
     * @param srv - input SigmaServer instance
     */
    private void buildQHTemperature(SigmaServer srv) {
        // Bath temperature:
        String BathTemperatureLabel = this.QHLabel + "Bath_Temperature";
        String BathTemperatureSelectionLabel = MPHC.LABEL_GEOM_QH + "_" + this.QHLabel + "Bath_Temperature_Selection";
        String BathTemperatureExpression = MPHC.LABEL_OPERATIONAL_TEMPERATUR;
        HeatTransferInSolidsPhysicsAPI.addQHNodeTemperature(
                srv,
                BathTemperatureLabel,
                BathTemperatureSelectionLabel,
                BathTemperatureExpression,
                "unidirectionalVar");

        // Coil temperature:
        for (int i = 0; i < numOfQHs; i++) {
            String CoilTemperatureLabel = this.QHLabel + String.format("%d", i + 1) + "_Coil_Temperature";
            String CoilTemperatureSelectionLabel = MPHC.LABEL_GEOM_QH + "_" + this.QHLabel + String.format("%d", i + 1) + "_Coil_Temperature_Selection_bnd";
            String CoilTemperatureExpression = MPHC.LABEL_COMP + "." + this.QHLabel + String.format("%d_", i + 1) + this.QHOperatorNameLabel + "(T)";
            HeatTransferInSolidsPhysicsAPI.addQHNodeTemperature(
                    srv,
                    CoilTemperatureLabel,
                    CoilTemperatureSelectionLabel,
                    CoilTemperatureExpression,
                    "unidirectionalVar");
        }
    }

    /**
     * Method builds Quench Heater Heat Sources in the Heat Transfer node:
     *
     * @param srv - input SigmaServer instance
     */
    private void buildQHHeatSources(SigmaServer srv) {
        for (int i = 0; i < numOfQHs; i++) {
            String HeatSourceLabel = this.QHLabel + String.format("%d", i + 1) + "_Heater_Steel_Strip";
            String HeatSourceSelection = MPHC.LABEL_GEOM_QH + "_" + this.QHLabel + String.format("%d", i + 1) + "_Steel_Strip_Interval_Selection_dom";
            String Q0Label = this.QHLabel + String.format("%d", i + 1) + "_Pheater";
            HeatTransferInSolidsPhysicsAPI.addQHNodeHeatSource(srv, HeatSourceLabel, HeatSourceSelection, Q0Label);
        }
    }

    /**
     * Method creates Heat Flux node inside the Heater Transfer node of the Quench Heater component:
     *
     * @param srv - input SigmaServer instance
     */
    private void buildQHHeatFluxNodes(SigmaServer srv) {
        for (int i = 0; i < numOfQHs; i++) {
            // Node name labes for heat flux nodes:
            String ExplicitSelectionLabel = String.format("%s%d_Selection", this.QHLabel, i + 1);
            String QHHeatFluxLabel = "Heat_Flux_QH_" + String.format("%d", i + 1);
            String q0ValueLabel = String.format("Q_%s%d", this.QHLabel, i + 1);

            // Heat flux in Heat Transfer node:
            HeatTransferInSolidsPhysicsAPI.addQHHeatFluxNode(srv, QHHeatFluxLabel, ExplicitSelectionLabel, q0ValueLabel);
        }
    }

    /**
     * Method builds Thin Layer nodes in the Heat Transfer node in component 2:
     *
     * @param srv - input
     */
    private void buildQHThinLayers(SigmaServer srv) {

        // Heater to bath thin layer:
        String HeatertoBathThinLayerLabel = MPHC.LABEL_HEATER_TO_BATH_THIN_LAYER;
        String HeatertoBathThinLayerSelectionLabel = String.format("%s_%sHeater_to_Bath_Thin_Layer_Selection", MPHC.LABEL_GEOM_QH, this.QHLabel);
        String HeatertoBathThinLayerdsLabel = String.format("%s/%s", MPHC.LABEL_INSULATION_THICKNESS_QH_TO_BATH, MPHC.LABEL_NUMBER_OF_QH_SUBDIVISIONS);
        String HeatertoBathThinLayerksLabel = String.format("%s(TQH[1/K])[W/m/K]", MPHC.LABEL_CFUN_K_KAPTON);
        HeatTransferInSolidsPhysicsAPI.addQHNodeThinLayer(
                srv,
                HeatertoBathThinLayerLabel,
                HeatertoBathThinLayerSelectionLabel,
                HeatertoBathThinLayerdsLabel,
                HeatertoBathThinLayerksLabel);

        // Heater to coil thin layer:
        String HeatertoCoilThinLayerLabel = MPHC.LABEL_HEATER_TO_COIL_THIN_LAYER;
        String HeatertoCoilThinLayerSelectionLabel = String.format("%s_%sHeater_to_Coil_Thin_Layer_Selection", MPHC.LABEL_GEOM_QH, this.QHLabel);
        String HeatertoCoilThinLayerdsLabel = String.format("%s/%s", MPHC.LABEL_INSULATION_THICKNESS_QH_TO_COIL, MPHC.LABEL_NUMBER_OF_QH_SUBDIVISIONS);
        String HeatertoCoilThinLayerksLabel = String.format("%s(TQH[1/K])[W/m/K]", MPHC.LABEL_CFUN_K_KAPTON);
        HeatTransferInSolidsPhysicsAPI.addQHNodeThinLayer(
                srv,
                HeatertoCoilThinLayerLabel,
                HeatertoCoilThinLayerSelectionLabel,
                HeatertoCoilThinLayerdsLabel,
                HeatertoCoilThinLayerksLabel);

        // Coil insulation thin layer:
        String CoilInsulationThinLayerLabel = MPHC.LABEL_COIL_INSULATION_THIN_LAYER;
        String CoilInsulationThinLayerSelectionLabel = String.format("%s_%sCoil_Insulation_Layer_Selection", MPHC.LABEL_GEOM_QH, this.QHLabel);
        String CoilInsulationThinLayerdsLabel = String.format("%s/%s", MPHC.LABEL_INSULATION_THICKNESS_TO_COIL, MPHC.LABEL_NUMBER_OF_QH_SUBDIVISIONS);
        String CoilInsulationThinLayerksLabel = "CFUN_kG10(TQH[1/K])[W/m/K]";
        HeatTransferInSolidsPhysicsAPI.addQHNodeThinLayer(
                srv,
                CoilInsulationThinLayerLabel,
                CoilInsulationThinLayerSelectionLabel,
                CoilInsulationThinLayerdsLabel,
                CoilInsulationThinLayerksLabel);
    }

    //******************************************************************************************************************
    //                                      METHOD FOR QUENCH HEATER MATERIALS
    //******************************************************************************************************************

    /**
     * This method builds the Quench Heater materials inside the Materials node of the Quench Heater component:
     *
     * @param srv - input SigmaServer instance
     */
    private void buildQHMaterials(SigmaServer srv) {
        for (int i = 0; i < numOfQHs; i++) {
            // Kapton:
            String KaptonLabel = String.format("%s%d_Kapton", this.QHLabel, i+1);
            String KaptonSelectionLabel = String.format("%s_%s%d%s", MPHC.LABEL_GEOM_QH, this.QHLabel, i+1, MPHC.LABEL_KAPTON_DOMAIN_SELECTION);
            String KaptoncpLabel = String.format("%s(TQH[1/K])[J/kg/K]", MPHC.LABEL_CFUN_CV_KAPTON);
            MaterialsAPI.createBasicQHMaterial(srv,MPHC.LABEL_COMP_QH, KaptonLabel, "1", KaptoncpLabel, "1e6", KaptonSelectionLabel);

            // G10:
            String G10Label = String.format("%s%d_G10", this.QHLabel, i+1);
            String G10SelectionLabel = String.format("%s_%s%d%s_dom", MPHC.LABEL_GEOM_QH, this.QHLabel, i + 1, MPHC.LABEL_COIL_INS_INTERVAL_SELECTION);
            String G10cpLabel = String.format("%s(TQH[1/K])[J/kg/K]", MPHC.LABEL_CFUN_CV_G10);
            MaterialsAPI.createBasicQHMaterial(srv,MPHC.LABEL_COMP_QH, G10Label, "1", G10cpLabel, "1e6", G10SelectionLabel);

            // Steel:
            String SteelLabel = String.format("%s%d_Steel", this.QHLabel, i+1);
            String SteelSelectionLabel = String.format("%s_%s%d%s_dom", MPHC.LABEL_GEOM_QH, this.QHLabel, i + 1, MPHC.LABEL_STEEL_STRIP_INTERVAL_SELECTION);
            String SteelcpLabel = String.format("%s(min(TQH,299)[1/K])[J/kg/K]", MPHC.LABEL_CFUN_CV_STEEL);
            MaterialsAPI.createBasicQHMaterial(srv,MPHC.LABEL_COMP_QH, SteelLabel, "1", SteelcpLabel, "1e6", SteelSelectionLabel);
        }
    }

    //******************************************************************************************************************
    //                                           METHOD FOR DEFAULT STUDY SETTINGS
    //******************************************************************************************************************

    /**
     * Method defines Standard Study Simulation settings: A Stationary and a Transient study:
     *
     * @param srv - input SigmaServer instance
     */
    private void buildStandardSimulationSettings(SigmaServer srv) {
        String timeRange = "range(0,0.0002,0.005) range(0.005,0.0005,0.05),range(0.05,0.01,0.25)";
        StudyAPI.setNewMonolithicStudy(srv, "Default_Study", timeRange);
    }

    private void buildQHMesh(SigmaServer srv) {
        MeshAPI.createPhysicsControlledMesh(srv, MPHC.LABEL_QH_MESH, MPHC.LABEL_GEOM_QH, 9);
    }
}

//**********************************************************************************************************************
//      TRASHED CODE        TRASHED CODE        TRASHED CODE        TRASHED CODE        TRASHED CODE
//**********************************************************************************************************************
    /*
    public static final String LABEL_INSULATION_THICKNESS_QH_TO_COIL    = "thQHtoCoilIns";
    public static final String LABEL_INSULATION_THICKNESS_TO_COIL       = "thCoilIns";
    public static final String LABEL_INSULATION_THICKNESS_QH_TO_BATH    = "thQHtoBathIns";
    public static final String LABEL_INSULATION_THICKNESS_QH_STRIP      = "thQHSteelStrip";
    public static final String LABEL_EXPONENTIAL_TIME_CONSTANT_DECAY    = "tauQH";
    public static final String LABEL_NUMBER_OF_QH_SUBDIVISIONS          = "NumQHDivisions";

    // Input selection domains for materials:
        //String kaptonDomain = "1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57";
        //String G10Domain = "24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68";
        //String steelDomain = "12, 46";

        //MaterialsAPI.createQHMaterial2(srv, MPHC.LABEL_COMP_QH, "kapton", "mat1", "1", "CFUN_Cvkapton(TQH[1/K])[J/kg/K]", "1e6", kaptonDomain);
        // Definitions of materials node and properties:
        //MaterialsAPI.createQHMaterial(srv, MPHC.LABEL_COMP_QH, "kapton", "mat1", "1", "CFUN_Cvkapton(TQH[1/K])[J/kg/K]", "1e6", kaptonDomain);
        //MaterialsAPI.createQHMaterial(srv, MPHC.LABEL_COMP_QH, "G10", "mat2", "1", "CFUN_CvG10(TQH[1/K])[J/kg/K]", "1e6", G10Domain);
        //MaterialsAPI.createQHMaterial(srv, MPHC.LABEL_COMP_QH, "steel", "mat3", "1", "CFUN_CvSteel(min(TQH,299)[1/K])[J/kg/K]", "1e6", steelDomain);

        String inputHeatertoCoil = "14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58";
        String dsLabelHeatertoCoil = MPHC.LABEL_INSULATION_THICKNESS_QH_TO_COIL + "/" + MPHC.LABEL_NUMBER_OF_QH_SUBDIVISIONS;
        String ksLabelHeatertoCoil = "CFUN_kkapton(TQH[1/K])[W/m/K]";
        HeatTransferInSolidsPhysicsAPI.addQHNodeThinLayer(
                srv,
                "tl2",
                "QH_Heater-to-Bath-insulation",
                inputHeatertoCoil,
                dsLabelHeatertoCoil,
                ksLabelHeatertoCoil);

        String inputCoilInsulation = "25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69";
        String dsLabelCoilInsulation = MPHC.LABEL_INSULATION_THICKNESS_TO_COIL + "/" + MPHC.LABEL_NUMBER_OF_QH_SUBDIVISIONS;
        String ksLabelCoilInsulation = "CFUN_kG10(TQH[1/K])[W/m/K]";
        HeatTransferInSolidsPhysicsAPI.addQHNodeThinLayer(
                srv,
                "tl3",
                "Coil-insulation, G10",
                inputCoilInsulation,
                dsLabelCoilInsulation,
                ksLabelCoilInsulation);


                for (int i = 0; i < numOfQHs; i++) {
            String inputCoilTemp = "35";
            String tempCoilTemp = MPHC.LABEL_COMP + "." + this.aveopLabel + String.format("%d", i + 1) + "(T)";
            HeatTransferInSolidsPhysicsAPI.addQHNodeTemperature(
                    srv,

                    "unidirectionalVar");
        }

        String inputCoilTemp1 = "35";
        String tempCoilTemp1 = MPHC.LABEL_COMP + "." + this.aveopLabel + "1(T)";
        HeatTransferInSolidsPhysicsAPI.addQHNodeTemperature(
                srv,
                this.TemperatureLabelQH1,
                this.QHLabel + "1_Coil_Temperature",
                inputCoilTemp1,
                tempCoilTemp1,
                "unidirectionalVar");

        String inputCoilTemp2 = "70";
        String tempCoilTemp2 = MPHC.LABEL_COMP + "." + this.aveopLabel + "2(T)";
        HeatTransferInSolidsPhysicsAPI.addQHNodeTemperature(
                srv,
                this.TemperatureLabelQH2,
                this.QHLabel + "2_Coil_Temperature",
                inputCoilTemp2,
                tempCoilTemp2,
                "unidirectionalVar");

                        String inputBathTemperature = "1, 36";
        String tempLabelBath = MPHC.LABEL_OPERATIONAL_TEMPERATUR;
        HeatTransferInSolidsPhysicsAPI.addQHNodeTemperature(
                srv,
                "tempnode3",
                this.QHLabel + "Bath_Temperature",
                inputBathTemperature,
                tempLabelBath,
                null);

        GeometryAPI.createArrayGeometry(srv, "Heater-to-Bath-insulation_Layer", "arr1", "pt1", MPHC.LABEL_NUMBER_OF_QH_SUBDIVISIONS, MPHC.LABEL_INSULATION_THICKNESS_QH_TO_BATH + "/" + MPHC.LABEL_NUMBER_OF_QH_SUBDIVISIONS);

        GeometryAPI.createPointGeometry(srv, MPHC.LABEL_GEOM_QH, "Heater-to-Coil-insulation_Point", "pt2", MPHC.LABEL_INSULATION_THICKNESS_QH_TO_BATH + "+" + MPHC.LABEL_INSULATION_THICKNESS_QH_STRIP + "+" + "0.5*" + MPHC.LABEL_INSULATION_THICKNESS_QH_TO_COIL + "/" + MPHC.LABEL_NUMBER_OF_QH_SUBDIVISIONS);
        GeometryAPI.createArrayGeometry(srv, "Heater-to-Coil-insulation_Layer", "arr2", "pt2", MPHC.LABEL_NUMBER_OF_QH_SUBDIVISIONS, MPHC.LABEL_INSULATION_THICKNESS_QH_TO_COIL + "/" + MPHC.LABEL_NUMBER_OF_QH_SUBDIVISIONS);

        GeometryAPI.createPointGeometry(srv, MPHC.LABEL_GEOM_QH, "Coil-insulation_Point", "pt3", MPHC.LABEL_INSULATION_THICKNESS_QH_TO_BATH + "+" + MPHC.LABEL_INSULATION_THICKNESS_QH_STRIP + "+" + MPHC.LABEL_INSULATION_THICKNESS_QH_TO_COIL + "+" + "0.5*" + MPHC.LABEL_INSULATION_THICKNESS_TO_COIL + "/" + MPHC.LABEL_NUMBER_OF_QH_SUBDIVISIONS);
        GeometryAPI.createArrayGeometry(srv, "Coil-insulation_Layer", "arr3", "pt3", MPHC.LABEL_NUMBER_OF_QH_SUBDIVISIONS, MPHC.LABEL_INSULATION_THICKNESS_TO_COIL + "/" + MPHC.LABEL_NUMBER_OF_QH_SUBDIVISIONS);

 private void buildHeaterDomainVariable(SigmaServer srv, String labelQH) {
        List<Variable> varList = new ArrayList<>();
        String labelGroup = String.format("%s", labelQH);

        // 'Heater_Domain_QHx' is defined and added:
        varList.add(createHeaterDomainVariable(MPHC.LABEL_HEATER_DOMAIN_1, MPHC.LABEL_WIDTH_QH));

        // The created variables are added to variable group:
        VariableGroup varGroup = new VariableGroup(labelGroup, varList);
        varGroup.buildVariableGroup(srv, MPHC.LABEL_COMP_QH, "");
    }

        String functionUnit = "A";
        DefinitionsAPI.createAnalyticFunction(srv, componentLabel, nodeName, expressionNameLabel, expressionLabel, argumentLabel, argumentUnit, functionUnit);
    }

        // Intervals:
        GeometryAPI.createIntervalGeometry(srv, "Heater-to-Bath-insulation", "i1", "0[m]", MPHC.LABEL_INSULATION_THICKNESS_QH_TO_BATH);
        GeometryAPI.createIntervalGeometry(srv, "Steel-strip", "i2", MPHC.LABEL_INSULATION_THICKNESS_QH_TO_BATH, MPHC.LABEL_INSULATION_THICKNESS_QH_TO_BATH + "+" + MPHC.LABEL_INSULATION_THICKNESS_QH_STRIP);
        GeometryAPI.createIntervalGeometry(srv, "Heater-to-Coil-insulation", "i3", MPHC.LABEL_INSULATION_THICKNESS_QH_TO_BATH + "+" + MPHC.LABEL_INSULATION_THICKNESS_QH_STRIP, MPHC.LABEL_INSULATION_THICKNESS_QH_TO_BATH + "+" + MPHC.LABEL_INSULATION_THICKNESS_QH_STRIP + "+" + MPHC.LABEL_INSULATION_THICKNESS_QH_TO_COIL);
        GeometryAPI.createIntervalGeometry(srv, "Coil-insulation", "i4", MPHC.LABEL_INSULATION_THICKNESS_QH_TO_BATH + "+" + MPHC.LABEL_INSULATION_THICKNESS_QH_STRIP + "+" + MPHC.LABEL_INSULATION_THICKNESS_QH_TO_COIL, MPHC.LABEL_INSULATION_THICKNESS_QH_TO_BATH + "+" + MPHC.LABEL_INSULATION_THICKNESS_QH_STRIP + "+" + MPHC.LABEL_INSULATION_THICKNESS_QH_TO_COIL + "+" + MPHC.LABEL_INSULATION_THICKNESS_TO_COIL);

        // Copy QH geometry:
        //GeometryAPI.createCopyGeometry(srv, "QH_Geometry_Copy", "copy1", ("arr1\", \"arr2\", \"arr3\", \"i1\", \"i2\", \"i3\", \"i4"), "1e-3");
        for (int i = 0; i < numOfQHs - 1; i++) {
            // Copy QH geometry:
            GeometryAPI.createCopyGeometry(srv, "QH_Geometry_Copy_" + String.format("%d", i + 1), "copy" + String.format("%d", i + 1), ("arr1\", \"arr2\", \"arr3\", \"i1\", \"i2\", \"i3\", \"i4"), "1e-3*" + String.format("%d", i + 1));
        }

        GeometryAPI.finalizeGeometry(srv, MPHC.LABEL_GEOM_QH, "fin");

        ???????????????????????????????
                    // Creation of Heater to Bath Insulation Layer for all Quench Heaters:
            String HeatertoBathInsLayName   = this.QHLabel + String.format("%d", i + 1) + "_Heater_to_Bath_insulation_Layer";
            String HeatertoBathInsLayLabel  = this.QHLabel + String.format("%d", i + 1) + "_Layer_1";
            String HeatertoBathInsLayExpr   = MPHC.LABEL_INSULATION_THICKNESS_QH_TO_BATH + "/" + MPHC.LABEL_NUMBER_OF_QH_SUBDIVISIONS;
            GeometryAPI.createArrayGeometry(
                    srv,
                    HeatertoBathInsLayName,
                    HeatertoBathInsLayLabel,
                    HeatertoBathInsPointLabel,
                    MPHC.LABEL_NUMBER_OF_QH_SUBDIVISIONS,
                    HeatertoBathInsLayExpr);

                     private void buildHeaterDomainVariable(SigmaServer srv, String labelQH) {
        List<Variable> varList = new ArrayList<>();
        String labelGroup = String.format("%s", labelQH);

        // 'Heater_Domain_QHx' is defined and added:
        varList.add(createHeaterDomainVariable(MPHC.LABEL_HEATER_DOMAIN_1, MPHC.LABEL_WIDTH_QH));

        // The created variables are added to variable group:
        VariableGroup varGroup = new VariableGroup(labelGroup, varList);
        varGroup.buildVariableGroup(srv, MPHC.LABEL_COMP_QH, "");
    }

        String functionUnit = "A";
        DefinitionsAPI.createAnalyticFunction(srv, componentLabel, nodeName, expressionNameLabel, expressionLabel, argumentLabel, argumentUnit, functionUnit);
    }

        ????????????????????????????????
                    // Creation of Heater to Bath Insulation Point for all Quench Heaters:
            String HeatertoBathInsPntName       = this.QHLabel + String.format("%d", i + 1) + "_Heater_to_Bath_insulation_Point";
            String HeatertoBathInsPointLabel    = this.QHLabel + String.format("%d", i + 1) + "_Point_1";
            String HeatertoBathInsPointExpr     = MPHC.VALUE_QUENCH_HEATER_DISPLACEMENT+"*"+String.format("%d", i) + "+0.5*" + MPHC.LABEL_INSULATION_THICKNESS_QH_TO_BATH + "/" + MPHC.LABEL_NUMBER_OF_QH_SUBDIVISIONS;
            GeometryAPI.createPointGeometry(srv, MPHC.LABEL_GEOM_QH, HeatertoBathInsPntName, HeatertoBathInsPointLabel, HeatertoBathInsPointExpr);

            // Creation of selection for all 'Heater to Bath Insulation Points' for all quench heaters:
            String HeatertoBathInsPntSelLabel   = this.QHLabel + String.format("%d", i + 1) + "_Heater_to_Bath_insulation_Point_Selection" ;
            GeometryAPI.contributeGeometrytoUnionSelection(
                    srv,
                    MPHC.LABEL_GEOM_QH,
                    HeatertoBathInsPntSelLabel,
                    HeatertoBathInsPointLabel,
                    HeatertoBathInsPntSelLabel,
                    "pnt");
            // Creation of selection for all 'Heater to Bath Insulation Layers' for all Quench Heaters:
        */

    /*
    //******************************************************************************************************************
    //                                          GETTER & SETTER METHODS
    //******************************************************************************************************************
      //Getter method for the number of quench heaters
      //@return - numOfQHs

public static int getnumOfQHs() {
    return numOfQHs;
}

 private void buildHeaterDomainVariable(SigmaServer srv, String labelQH) {
        List<Variable> varList = new ArrayList<>();
        String labelGroup = String.format("%s", labelQH);

        // 'Heater_Domain_QHx' is defined and added:
        varList.add(createHeaterDomainVariable(MPHC.LABEL_HEATER_DOMAIN_1, MPHC.LABEL_WIDTH_QH));

        // The created variables are added to variable group:
        VariableGroup varGroup = new VariableGroup(labelGroup, varList);
        varGroup.buildVariableGroup(srv, MPHC.LABEL_COMP_QH, "");
    }

        String functionUnit = "A";
        DefinitionsAPI.createAnalyticFunction(srv, componentLabel, nodeName, expressionNameLabel, expressionLabel, argumentLabel, argumentUnit, functionUnit);
    }

    //
    // Getter method for operator name label for average temperature component coupling nodes
    // @return - aveopLabel
    //
    public static String getaveopLabel() {
        return aveopLabel;
    }
}
    private Variable createHeaterDomainVariable(String label, String labelQHwidth) {
        for (int i=1; i <= numOfQHs; i++) {
            String value = String.format("%s(%s)/(%s*%s_" + i +  ")^2*%s(t)^2",
                    MPHC.LABEL_RESISTIVITY_OF_STEEL,
                    MPHC.LABEL_QUENCH_HEATER_TEMPERATURE,
                    MPHC.LABEL_INSULATION_THICKNESS_QH_STRIP,
                    labelQHwidth,
                    MPHC.LABEL_QH_CURRENT);
            return new Variable(label, value);
        }
    }

    private void buildHeaterDomainVariable(SigmaServer srv, String labelQH) {
        List<Variable> varList = new ArrayList<>();
        String labelGroup = String.format("%s", labelQH);

        // 'Heater_Domain_QHx' is defined and added:
        varList.add(createHeaterDomainVariable(MPHC.LABEL_HEATER_DOMAIN_1, MPHC.LABEL_WIDTH_QH));

        // The created variables are added to variable group:
        VariableGroup varGroup = new VariableGroup(labelGroup, varList);
        varGroup.buildVariableGroup(srv, MPHC.LABEL_COMP_QH, "");
    }

        String functionUnit = "A";
        DefinitionsAPI.createAnalyticFunction(srv, componentLabel, nodeName, expressionNameLabel, expressionLabel, argumentLabel, argumentUnit, functionUnit);
    }

    //******************************************************************************************************************
    //                                      EVERYTHING RELATED TO SELECTIONS
    //******************************************************************************************************************
    /**
     * Method contains everything related to selections and component couplings for the Quench Heaters:
     * @param srv           - input SigmaServer instance
     * @param coilLabel     - Label for the coil
     *      @subparam depth - 0 = domain, 1 = boundary, 2 = point
     */

    /*
    private static void buildQHSelections(SigmaServer srv, String coilLabel) {

        // Labels for QH explicit selections and average component couplings:
        String QH1ExplicitSelectionLabel  = String.format(coilLabel+"_QH1_Selection");
        String QH2ExplicitSelectionLabel  = String.format(coilLabel+"_QH2_Selection");
        String QH1AverageTemperatureLabel = String.format(coilLabel+"_Average_Temperature_QH1");
        String QH2AverageTemperatureLabel = String.format(coilLabel+"_Average_Temperature_QH2");

        // Creates explicit selection nodes in MagnetComp:
        GeometryAPI.createExplicitSelectioninDefinitions(srv, MPHC.LABEL_COMP, QH1ExplicitSelectionLabel, QH1ExplicitSelectionLabel, 1, "106, 116, 130, 140");
        GeometryAPI.createExplicitSelectioninDefinitions(srv, MPHC.LABEL_COMP, QH2ExplicitSelectionLabel, QH2ExplicitSelectionLabel, 1, "2, 12, 26, 36");

 private void buildHeaterDomainVariable(SigmaServer srv, String labelQH) {
        List<Variable> varList = new ArrayList<>();
        String labelGroup = String.format("%s", labelQH);

        // 'Heater_Domain_QHx' is defined and added:
        varList.add(createHeaterDomainVariable(MPHC.LABEL_HEATER_DOMAIN_1, MPHC.LABEL_WIDTH_QH));

        // The created variables are added to variable group:
        VariableGroup varGroup = new VariableGroup(labelGroup, varList);
        varGroup.buildVariableGroup(srv, MPHC.LABEL_COMP_QH, "");
    }

        String functionUnit = "A";
        DefinitionsAPI.createAnalyticFunction(srv, componentLabel, nodeName, expressionNameLabel, expressionLabel, argumentLabel, argumentUnit, functionUnit);
    }



    private void buildHeaterDomainVariable(SigmaServer srv, String labelQH) {
        List<Variable> varList = new ArrayList<>();
        String labelGroup = String.format("%s", labelQH);

        // 'Heater_Domain_QHx' is defined and added:
        varList.add(createHeaterDomainVariable(MPHC.LABEL_HEATER_DOMAIN_1, MPHC.LABEL_WIDTH_QH));

        // The created variables are added to variable group:
        VariableGroup varGroup = new VariableGroup(labelGroup, varList);
        varGroup.buildVariableGroup(srv, MPHC.LABEL_COMP_QH, "");
    }

        String functionUnit = "A";
        DefinitionsAPI.createAnalyticFunction(srv, componentLabel, nodeName, expressionNameLabel, expressionLabel, argumentLabel, argumentUnit, functionUnit);
    }

        // Creates Average component coupling in MagnetComp:
        DefinitionsAPI.createComponentCoupling(srv, QH1AverageTemperatureLabel, DefinitionsAPI.ComponentCouplingType.Average, MPHC.LABEL_GEOM);
        DefinitionsAPI.assignSelection(srv, QH1AverageTemperatureLabel, QH1ExplicitSelectionLabel);

        // Assigns explicit selections to Average component coupling in MagnetComp:
        DefinitionsAPI.createComponentCoupling(srv, QH2AverageTemperatureLabel, DefinitionsAPI.ComponentCouplingType.Average, MPHC.LABEL_GEOM);
        DefinitionsAPI.assignSelection(srv, QH2AverageTemperatureLabel, QH2ExplicitSelectionLabel);
    }
        public int getnumOfQHs() {
            return numOfQHs;
        }

        public void setnumOfQHs(int value) {
            this.numOfQHs = value;
        }

// Method:
// Define new quench heater geometry

// Method:
// Define new quench heater selections

// Method:
// Getter methods

        DefinitionsAPI.createComponentNode(srv,MPHC.LABEL_COMP_QH);
        GeometryAPI.createGeometryNode(srv);
        MagneticFieldsPhysicsAPI.addNode(srv);
        HeatTransferInSolidsPhysicsAPI.addNode(srv);
        DodePhysicsAPI.addNode(srv);
        GlobalEquationsPhysicsAPI.addNode(srv);

*/