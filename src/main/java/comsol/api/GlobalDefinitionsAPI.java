package comsol.api;

import server.SigmaServer;

/**
 * Class contains a set of static API functions to deal with geometry in COMSOL
 */
public class GlobalDefinitionsAPI {
    private GlobalDefinitionsAPI() {
    }

    /**
     * Method builds a node for an external function under global definitions of COMSOL
     *
     * @param srv           - input SigmaServer instance
     * @param cFunLibPath   - an absolute path to the library with C functions with material properties
     * @param nodeName      - function node name
     * @param functions     - function name
     * @param plotArguments - plotting arguments
     */
    public static void buildNodeExternalFunction(SigmaServer srv, String cFunLibPath, String nodeName, String[][] functions, String[][] plotArguments) {
        srv.createFunctionNode(nodeName, "External");
        srv.setFunctionNodeLabel(nodeName, nodeName);

        String cFunAbsPath = cFunLibPath + "\\" + nodeName + ".dll";
        srv.setFunctionParam(nodeName, "path", cFunAbsPath.replace("\\", "\\\\"));
        srv.setFunctionParam(nodeName, "funcs", functions);
        srv.setFunctionParam(nodeName, "plotargs", plotArguments);
        srv.setFunctionParam(nodeName, "threadsafe", "on");
    }

    /**
     * Method sets a parameter given as a (name, value) pair for a global definition
     *
     * @param srv                   - input SigmaServer instance
     * @param parameterName         - input parameter name
     * @param parameterValue        - input parameter value
     * @param parameterDescription  - input parameter value
     */
    public static void setParameter(SigmaServer srv, String parameterName, String parameterValue, String parameterDescription) {
        srv.setParam(parameterName, parameterValue, parameterDescription);
    }






//    /**
//     * Method sets a parameter given as a (name, value) pair for a global definition
//     *
//     * @param srv            - input SigmaServer instance
//     * @param parameterName  - input parameter name
//     * @param parameterValue - input parameter value
//     */
//    public static void setParameter(SigmaServer srv, String parameterName, String parameterValue) {
//        srv.setParam(parameterName, parameterValue);
//    }
}
