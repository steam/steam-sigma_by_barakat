package comsol.api;

import comsol.constants.MPHC;
import server.SigmaServer;

/**
 * Class contains a set of static API functions to deal with component definitions in COMSOL
 */
public class DefinitionsAPI {

    private DefinitionsAPI() {
    }

    /**
     * Enum provides selection of different component coupling types (operations executed over a domain in COMSOL)
     */
    public enum ComponentCouplingType {
        Minimum,
        Average,
        Maximum,
        Integration
    }

    /**
     * Method initializes a COMSOL model by creating a component node (root of a COMSOL model)
     *
     * @param srv - input SigmaServer instance
     */
    public static void createComponentNode(SigmaServer srv, String componentLabel) {
        srv.createComponentNode(componentLabel);
        srv.clearFile();
    }

    /**
     * Method returns the name of a model component node
     *
     * @param srv - input SigmaServer instance
     * @return - component node name
     */
    public static String getComponentNodeName(SigmaServer srv) {
        return srv.getTagsModelNode(0);
    }

    /**
     * Method creates a component coupling of a given type:
     *
     * @param srv   - input SigmaServer instance
     * @param label - component coupling label
     * @param type  - component coupling type
     */
    public static void createComponentCoupling(SigmaServer srv, String label, String operatorLabel, ComponentCouplingType type, String geoLabel) {
        srv.createCpl(label, type.toString(), geoLabel);
        srv.setLabelCpl(label, label);
        srv.setParamCpl(label, "opname", operatorLabel);
    }

/*
    public static void createComponentCoupling(SigmaServer srv, String label, ComponentCouplingType type) {
    srv.createCpl(label, type.toString(), MPHC.LABEL_GEOM);
    srv.setLabelCpl(label, label);
    srv.setParamCpl(label, "opname", label);
}
 */

    /**
     * Method assigns a named selection to a label
     *
     * @param srv      - input SigmaServer instance
     * @param label    - selection label
     * @param selLabel - named selection label
     */
    public static void assignSelection(SigmaServer srv, String label, String selLabel) {
        srv.setNamedSelectionCpl(label, selLabel);
    }

    /**
     * Method creates a variable group in a component
     *
     * @param srv   - input SigmaServer instance
     * @param label - variable group label
     */
    public static void createVariableGroup(SigmaServer srv, String label, String labelComp) {
        srv.createLabelVariable(label);
        srv.setLabelVariable(label, label);
        srv.setModelVariable(label, labelComp);
    }

    /**
     * Method removes a variable group of a component
     *
     * @param srv   - input SigmaServer instance
     * @param label - variable group label
     */
    public static void removeVariableGroup(SigmaServer srv, String label) {
        srv.removeVariable(label);
    }

    /**
     * Method assigns a variable group to a selection (variables will be only present in that selection)
     *
     * @param srv      - input SigmaServer instance
     * @param label    - variable group label
     * @param selLabel - named selection label
     */
    public static void assignVariableGroupToSelection(SigmaServer srv, String labelGeom, String label, String selLabel, int entityDim) {
        srv.setParamGeomSelectionVariable(label, labelGeom, entityDim);
        srv.setNamedSelectionVariable(label, selLabel);
    }


    /**
     * Method assigns a variable group to global selection
     *
     * @param srv   - input SigmaServer instance
     * @param label - variable group label
     */
    public static void assignVariableGroupToGlobal(SigmaServer srv, String label) {
        srv.setGlobalSelectionVariable(label);
    }

    /**
     * Method adds a variable to a variable group
     *
     * @param srv        - input SigmaServer instance
     * @param groupLabel - variable group label
     * @param label      - variable label
     * @param value      - variable value
     */
    public static void addVariable(SigmaServer srv, String groupLabel, String label, String value) {
        srv.setParamVariable(groupLabel, label, value);
    }

    /**
     * Method creates an interpolation table in a component (e.g., external current for the magnet)
     *
     * @param srv      - input SigmaServer instance
     * @param nodeName - function node name
     * @param compName - component node name
     */
    public static void createInterpolationTable(SigmaServer srv, String nodeName, String compName) {
        srv.removeFunc(nodeName);
        srv.createFunctionNode(nodeName, "Interpolation");
        srv.setModelFunction(nodeName, compName);
        srv.setLabelFunction(nodeName, nodeName);
        srv.setParamFunction(nodeName, "funcname", nodeName);
        srv.setParamFunction(nodeName, "extrap", "linear");
    }

    /**
     * Method adds entries to an interpolation table
     *
     * @param srv      - input SigmaServer instance
     * @param nodeName - function node name
     * @param x        - first value
     * @param y        - second value
     * @param xUnit    - unit of the first value
     * @param yUnit    - unit of the second value
     */
    public static void addValToInterpolationTable(SigmaServer srv, String nodeName, String[] x, String[] y, String xUnit, String yUnit) {
        srv.setParamFunction(nodeName, "argunit", xUnit);
        srv.setParamFunction(nodeName, "fununit", yUnit);

        for (int var_i = 0; var_i < x.length; var_i++) {
            srv.setIndexFunction(nodeName, "table", x[var_i], var_i, 0);
            srv.setIndexFunction(nodeName, "table", y[var_i], var_i, 1);
        }
    }

    /**
     * This method creates an analytic function node (with expression, argument and units) inside a component
     *
     * @param srv                 - input SigmaServer instance
     * @param nodeName            - Label for name of analytic function
     * @param expressionNameLabel - Label for the name of the function
     * @param expressionLabel     - Label for the expression of the analytic function
     * @param argumentLabel       - Label for the arguments of the analytic function
     * @param argumentUnit        - Label for the units of the arguments of the analytic function
     * @param functionUnit        - Label for the units of the function of the analytic function
     */
    public static void createAnalyticFunctioninGlobal(SigmaServer srv, String nodeName, String expressionNameLabel, String expressionLabel, String argumentLabel, String argumentUnit, String functionUnit) {
        srv.removeFunc(nodeName);
        srv.createFunctionNode(nodeName, "Analytic");         // Assigns to a component
        srv.setLabelFunction(nodeName, nodeName);                                         // nodeName
        srv.setParamFunction(nodeName, "funcname", expressionNameLabel);    // functionNameLabel
        srv.setParamFunction(nodeName, "expr", expressionLabel);            // functionLabel
        srv.setParamFunction(nodeName, "args", argumentLabel);              // Unit of argument
        srv.setParamFunction(nodeName, "argunit", argumentUnit);            // Unit of argument
        srv.setParamFunction(nodeName, "fununit", functionUnit);            // Unit of function
    }

    /**
     * Method creates hidden entities
     *
     * @param srv      - input SigmaServer instance
     * @param nodeName - name of the hidden entity view
     * @param compName - component name
     */
    public static void createHideEntities(SigmaServer srv, String nodeName, String compName) {
        srv.createHideEntitiesView(MPHC.LABEL_VIEW, nodeName);
        srv.setGeomHideEntitiesView(MPHC.LABEL_VIEW, nodeName, MPHC.LABEL_GEOM, 1);
        srv.setNamedHideEntitiesView(MPHC.LABEL_VIEW, nodeName, compName);
    }
}