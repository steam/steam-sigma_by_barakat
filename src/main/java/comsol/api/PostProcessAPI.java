package comsol.api;

import comsol.constants.MPHC;
import server.SigmaServer;


/**
 * Class contains a set of static API functions to deal with post processing in COMSOL
 */
public class PostProcessAPI {

    private PostProcessAPI() {
    }
    /**
     * Method calculates (actually extracts from a solution) time
     *
     * @param srv          - input SigmaServer instance
     * @param datasetLabel - label of a dataset from which time is extracted
     * @return - 1D array with time
     */
    public static double[] calculateTime(SigmaServer srv, String datasetLabel) {
        return calculateGlobalEvaluation(srv, MPHC.LABEL_TIME, datasetLabel);
    }

    /**
     * Method calculates a single global evaluation from a dataset
     *
     * @param srv          - input SigmaServer instance
     * @param expression   - expression to be evaluated as a global evaluation
     * @param dataSetLabel - label of a dataset from which signal is extracted
     * @return - 1D array with signal value
     */
    public static double[] calculateGlobalEvaluation(SigmaServer srv, String expression, String dataSetLabel) {
        srv.removeNumericalResult(expression);
        srv.createNumericalResult(expression, "EvalGlobal");
        srv.setLabelNumericalResult(expression, expression);
        srv.setParamNumericalResult(expression, "data", dataSetLabel);

        // Clean the table from the cursed comsol automatic fill
        srv.setParamNumericalResult(expression, "expr", new String[]{});
        srv.setParamNumericalResult(expression, "descr", new String[]{});
        srv.setIndexNumericalResult(expression, "expr", expression, 0);

        // Because of COMSOL: double[][] even though value is a single array!
        return srv.getRealNumericalResult(expression)[0];
    }

    /**
     * Method calculates multiple global evaluations from a dataset
     *
     * @param srv                   - input SigmaServer instance
     * @param varNames              - expressions to be evaluated as a global evaluation
     * @param datasetLabel          - label of a dataset from which signal is extracted
     * @param globalEvaluationLabel - label of a global evaluation to which expressions are assigned
     * @return - 2D array with values of signals
     */
    public static double[][] calculateGlobalEvaluationWithMultipleExpr(SigmaServer srv, String varNames[], String datasetLabel, String globalEvaluationLabel) {
        srv.removeNumericalResult(globalEvaluationLabel);
        srv.createNumericalResult(globalEvaluationLabel, "EvalGlobal");
        srv.setLabelNumericalResult(globalEvaluationLabel, globalEvaluationLabel);
        srv.setParamNumericalResult(globalEvaluationLabel, "data", datasetLabel);

        // Clean the table from the cursed comsol automatic fill
        srv.setParamNumericalResult(globalEvaluationLabel, "expr", new String[]{});
        srv.setParamNumericalResult(globalEvaluationLabel, "descr", new String[]{});

        for (int i = 0; i < varNames.length; i++) {
            srv.setIndexNumericalResult(globalEvaluationLabel, "expr", varNames[i], i);
        }

        // Because of COMSOL: double[][] even though value is a single array!
        return srv.getRealNumericalResult(globalEvaluationLabel);
    }
}
