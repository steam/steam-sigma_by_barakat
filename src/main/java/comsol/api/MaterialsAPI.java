package comsol.api;

import comsol.constants.MPHC;
import server.SigmaServer;

/**
 * Class contains a set of static API functions to deal with magnetic fields physics in COMSOL
 */
public class MaterialsAPI {

    private MaterialsAPI() {
    }
    /**
     * Method adds a material with a given label to a component
     *
     * @param srv   - input SigmaServer instance
     * @param label - material label
     */

    public static void add(SigmaServer srv, String label) {
        srv.createLabelMaterial(label, "Common", MPHC.LABEL_COMP);
        srv.setLabelMaterial(label, label);
    }

    /**
     * Method adds a material family (for graphical rendering reasons)
     *
     * @param srv   - input SigmaServer instance
     * @param label - material label
     */
    // familyLabel = label of material family (only for graphical rendering within COMSOL)
    public static void addFamily(SigmaServer srv, String label) {
        srv.setMaterial(label, "family", "elem_air");
    }

    /**
     * Method adds relative permittivity to a material
     *
     * @param srv   - input SigmaServer instance
     * @param label - material label
     * @param value - scalar relative permittivity value
     */
    public static void addEpsilonr(SigmaServer srv, String label, String value) {
        srv.setParamPropertyGroupMaterial(label, "def", "relpermittivity", value);
    }

    /**
     * Method adds electric conductivity to a material
     *
     * @param srv   - input SigmaServer instance
     * @param label - material label
     * @param value - vectorial electric conductivity value
     */
    public static void addSigma(SigmaServer srv, String label, String[] value) {
        srv.setParamPropertyGroupMaterial(label, "def", "electricconductivity", value);
    }

    /**
     * Method adds heat capacity to a material
     *
     * @param srv   - input SigmaServer instance
     * @param label - material label
     * @param value - scalar heat capacity value
     */
    public static void addCp(SigmaServer srv, String label, String value) {
        srv.setParamPropertyGroupMaterial(label, "def", "heatcapacity", value);
    }

    /**
     * Method adds density to a material
     *
     * @param srv   - input SigmaServer instance
     * @param label - material label
     * @param value - scalar density value
     */
    public static void addRho(SigmaServer srv, String label, String value) {
        srv.setParamPropertyGroupMaterial(label, "def", "density", value);
    }

    /**
     * Method adds thermal conductivity to a material
     *
     * @param srv   - input SigmaServer instance
     * @param label - material label
     * @param value - scalar thermal conductivity value
     */
    public static void addK(SigmaServer srv, String label, String value) {
        srv.setParamPropertyGroupMaterial(label, "def", "thermalconductivity", value);
    }

    /**
     * Method adds relative permeability to a material
     *
     * @param srv   - input SigmaServer instance
     * @param label - material label
     * @param value - scalar relative permeability value
     */
    public static void addMur(SigmaServer srv, String label, String value) {
        srv.setParamPropertyGroupMaterial(label, "def", "relpermeability", value);
    }

    /**
     * Method adds HB curve to a material
     *
     * @param srv     - input SigmaServer instance
     * @param label   - material label
     * @param curveHB - 2D HB curve
     */
    public static void addHB(SigmaServer srv, String label, String[][] curveHB) {
        srv.createPropertyGroupMaterial(label, "HBCurve", "HB curve");
        srv.setParamPropertyGroupMaterial(label, "HBCurve", "normH", "HB(normB[1/T])[A/m]");
        srv.createFunctionPropertyGroupMaterial(label, "HBCurve", "HB", "Interpolation");
        srv.setParamFuncPropertyGroupMaterial(label, "HBCurve", "HB", "sourcetype", "user");
        srv.setParamFuncPropertyGroupMaterial(label, "HBCurve", "HB", "source", "table");
        srv.setParamFuncPropertyGroupMaterial(label, "HBCurve", "HB", "funcname", "HB");
        srv.setParamFuncPropertyGroupMaterial(label, "HBCurve", "HB", "table", curveHB);
        srv.setParamFuncPropertyGroupMaterial(label, "HBCurve", "HB", "interp", "piecewisecubic");
        srv.setParamFuncPropertyGroupMaterial(label, "HBCurve", "HB", "extrap", "linear");
    }

    /**
     * Method assigns material to a named selection
     *
     * @param srv      - input SigmaServer instance
     * @param label    - material label
     * @param selLabel - named selection label
     */
    public static void assignToSelection(SigmaServer srv, String label, String selLabel) {
        srv.setNamedSelectionMaterial(label, selLabel);
    }

    /**
     * Method creates a userdefined component with density, specific heat capacity and thermal conductivity:
     * @param srv       - input SigmaServer instance
     * @param compLabel - Label of component to which the material should be assigned
     * @param label - Label for the materiale node
     * @param rhoLabel  - Label for the density of the material
     * @param cpLabel   - Label for the specific heat capacity of the material
     * @param lambdaLabel - - Label for the thermal conductivity of the material
     */
    public static void createBasicQHMaterial(SigmaServer srv, String compLabel, String label, String rhoLabel, String cpLabel, String lambdaLabel, String selectionLabel) {
        srv.createLabelMaterial(label, "Common", compLabel);
        srv.setLabelMaterial(label, label);
        srv.setParamPropertyGroupMaterial(label, "def", "density", rhoLabel);
        srv.setParamPropertyGroupMaterial(label, "def", "heatcapacity", cpLabel);
        srv.setParamPropertyGroupMaterial(label, "def", "thermalconductivity", lambdaLabel);
        srv.setNamedSelectionMaterial(label, selectionLabel);
    }
}
