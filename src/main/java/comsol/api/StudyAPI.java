package comsol.api;

import comsol.constants.MPHC;
import server.SigmaServer;

/**
 * Class contains a set of static API functions to deal with studies in COMSOL
 */
public class StudyAPI {

    private StudyAPI() {
    }
    // *****************************************************************************************************************
    // Solver settings: edit them at your own risk
    // *****************************************************************************************************************
    // Stationary Solver:
    private static final int MAX_ITER_STATIONARY = 100;
    private static final String TOL_FACTOR_STATIONARY = "1e-3";

    // Time domain Solver
    private static final double ABSOLUTE_TOLERANCE_GLOBAL = 0.001;
    private static final int BDF_ORDER = 5;
    // Newton method
    private static final double DAMP_FACTOR_NEWTON = 1;
    private static final int MAX_ITER_NEWTON = 10;
    private static final String TOL_FACTOR_NEWTON = "1e-2";

    // *****************************************************************************************************************
    // Enums
    // *****************************************************************************************************************

    private enum jacobianUpdate {minimal, once, onevery}

    private enum outputSteps {tlist, tsteps} // list of steps (tlist), or for each time step (tsteps)

    private enum timeStepping {free, intermediate, strict}

    private enum algebraicError {exclude, include}

    // *****************************************************************************************************************
    // Methods
    // *****************************************************************************************************************

    /**
     * Method sets a new transient study
     *
     * @param srv             - input SigmaServer instance
     * @param studyNameIn     - input study name
     * @param timeRangeString - simulation time range, i.e., start time, maximum time step, end time
     */
    public static void setNewTransientStudy(SigmaServer srv, String studyNameIn, String timeRangeString) {
        String studyName = studyNameIn;
        String solutionName = studyNameIn;

        // Transient study creation
        // Reset
        srv.removeStudy(studyName);

        // Set
        srv.createStudy(studyName);
        srv.setLabelStudy(studyName, studyName);
        srv.createStudyType(studyName, "time", "Transient");
        srv.setParamFeatureStudy(studyName, "time", "tlist", timeRangeString);
        srv.setParamFeatureStudy(studyName, "time", "tunit", "s");
        srv.createSol(solutionName);
        srv.setStudySol(solutionName, studyName);
        srv.createSol(solutionName, "st1", "StudyStep");
        srv.setParamFeatureSol(solutionName, "st1", "study", studyName);
        srv.setParamFeatureSol(solutionName, "st1", "studystep", "time");
        srv.createSol(solutionName, "v1", "Variables");
        srv.setParamFeatureSol(solutionName, "v1", "control", "time");
        srv.createSol(solutionName, "t1", "Time");
        srv.setParamFeatureSol(solutionName, "t1", "tlist", timeRangeString);
        srv.setParamFeatureSol(solutionName, "t1", "plot", "off");
        srv.setParamFeatureSol(solutionName, "t1", "plotgroup", "Default");
        srv.setParamFeatureSol(solutionName, "t1", "plotfreq", "tout");
        srv.setParamFeatureSol(solutionName, "t1", "probesel", "all");
        srv.setParamFeatureSol(solutionName, "t1", "probes", new String[]{});
        srv.setParamFeatureSol(solutionName, "t1", "probefreq", "tsteps");
        srv.setParamFeatureSol(solutionName, "t1", "atolglobalmethod", "scaled");
        srv.setParamFeatureSol(solutionName, "t1", "atolglobal", ABSOLUTE_TOLERANCE_GLOBAL);
        srv.setParamFeatureSol(solutionName, "t1", "tstepsbdf", timeStepping.intermediate.toString());
        srv.setParamFeatureSol(solutionName, "t1", "estrat", algebraicError.include.toString());
        srv.setParamFeatureSol(solutionName, "t1", "maxorder", BDF_ORDER);
        srv.setParamFeatureSol(solutionName, "t1", "control", "time");
        srv.setParamFeatureSol(solutionName, "t1", "tlist", timeRangeString);
        srv.createFeatureSol(solutionName, "t1", "d1", "Direct");
        srv.setParamFeatureFeatureSol(solutionName, "t1", "d1", "linsolver", "mumps");
        srv.createFeatureSol(solutionName, "t1", "fc1", "FullyCoupled");
        srv.setParamFeatureFeatureSol(solutionName, "t1", "fc1", "linsolver", "d1");
        srv.setParamFeatureFeatureSol(solutionName, "t1", "fc1", "damp", DAMP_FACTOR_NEWTON);
        srv.setParamFeatureFeatureSol(solutionName, "t1", "fc1", "jtech", jacobianUpdate.onevery.toString());
        srv.setParamFeatureFeatureSol(solutionName, "t1", "fc1", "maxiter", MAX_ITER_NEWTON);
        srv.setParamFeatureFeatureSol(solutionName, "t1", "fc1", "ntolfact", TOL_FACTOR_NEWTON);
        srv.attachSol(solutionName, studyName);
    }

    /**
     * Method sets a new monolithic study composed of transient study with stationary and transient studies
     *
     * @param srv             - input SigmaServer instance
     * @param studyNameIn     - input study name
     * @param timeRangeString - simulation time range, i.e., start time, maximum time step, end time
     */
    public static void setNewMonolithicStudy(SigmaServer srv, String studyNameIn, String timeRangeString) {
        String studyName = studyNameIn;
        String solutionName = studyNameIn;

        // Transient study creation
        // Reset
        srv.removeStudy(studyName);

        // Set
        srv.createStudy(studyName);
        srv.setLabelStudy(studyName, studyName);

        // creates stationary study:
        srv.createStudyType(studyName, "stat", "Stationary");
        srv.activateFeatureStudy(studyName, "stat", "mf", true);
        srv.activateFeatureStudy(studyName, "stat", "ht", false);
        srv.activateFeatureStudy(studyName, "stat", MPHC.LABEL_PHYSICSTH_QH, false);
        srv.activateFeatureStudy(studyName, "stat", "dode", false);
        srv.activateFeatureStudy(studyName, "stat", "ge", false);

        // creates transient study study:
        srv.createStudyType(studyName, "time", "Transient");
        srv.setParamFeatureStudy(studyName, "time", "tlist", timeRangeString);
        srv.setParamFeatureStudy(studyName, "time", "tunit", "s");
        srv.setParamFeatureStudy(studyName, "time", "useinitsol", true);
        srv.setParamFeatureStudy(studyName, "time", "initmethod", "sol");
        srv.setParamFeatureStudy(studyName, "time", "initstudy", studyName);
        srv.createSol(solutionName);
        srv.setStudySol(solutionName, studyName);
        srv.setParamFeatureStudy(studyName, "stat", "notlistsolnum", 1);
        srv.setParamFeatureStudy(studyName, "stat", "notsolnum", "1");
        srv.setParamFeatureStudy(studyName, "stat", "listsolnum", 1);
        srv.setParamFeatureStudy(studyName, "stat", "solnum", "1");
        srv.setParamFeatureStudy(studyName, "stat", "notlistsolnum", 1);
        srv.setParamFeatureStudy(studyName, "stat", "notsolnum", "auto");
        srv.setParamFeatureStudy(studyName, "stat", "listsolnum", 1);
        srv.setParamFeatureStudy(studyName, "stat", "solnum", "auto");

        //Stationary
        srv.createSol(solutionName, "st1", "StudyStep");
        srv.setParamFeatureSol(solutionName, "st1", "study", studyName);
        srv.setParamFeatureSol(solutionName, "st1", "studystep", "stat");
        srv.createSol(solutionName, "v1", "Variables");
        srv.setParamFeatureSol(solutionName, "v1", "control", "stat");
        srv.createSol(solutionName, "s1", "Stationary");
        srv.createFeatureSol(solutionName, "s1", "fc1", "FullyCoupled");
        srv.setParamFeatureFeatureSol(solutionName, "s1", "fc1", "linsolver", "dDef");

        srv.setParamFeatureFeatureSol(solutionName, "s1", "fc1", "maxiter", MAX_ITER_STATIONARY);
        srv.setParamFeatureFeatureSol(solutionName, "s1", "fc1", "ntolfact", TOL_FACTOR_STATIONARY);

        srv.removeFeatureFeatureSol(solutionName, "s1", "fcDef");
        srv.createSol(solutionName, "su1", "StoreSolution");

        //Time dependent
        srv.createSol(solutionName, "st2", "StudyStep");
        srv.setParamFeatureSol(solutionName, "st2", "study", studyName);
        srv.setParamFeatureSol(solutionName, "st2", "studystep", "time");
        srv.createSol(solutionName, "v2", "Variables");
        srv.setParamFeatureSol(solutionName, "v2", "initmethod", "sol");
        srv.setParamFeatureSol(solutionName, "v2", "initsol", solutionName);
        srv.setParamFeatureSol(solutionName, "v2", "initsoluse", "su1");
        srv.setParamFeatureSol(solutionName, "v2", "notsolmethod", "sol");
        srv.setParamFeatureSol(solutionName, "v2", "notsol", solutionName);
        srv.setParamFeatureSol(solutionName, "v2", "control", "time");
        srv.createSol(solutionName, "t1", "Time");
        srv.setParamFeatureSol(solutionName, "t1", "tlist", timeRangeString);
        srv.setParamFeatureSol(solutionName, "t1", "plot", "off");
        srv.setParamFeatureSol(solutionName, "t1", "plotgroup", "Default");
        srv.setParamFeatureSol(solutionName, "t1", "plotfreq", "tout");
        srv.setParamFeatureSol(solutionName, "t1", "probesel", "all");
        srv.setParamFeatureSol(solutionName, "t1", "probes", new String[]{});
        srv.setParamFeatureSol(solutionName, "t1", "probefreq", "tsteps");
        srv.setParamFeatureSol(solutionName, "t1", "atolglobalmethod", "scaled");
        srv.setParamFeatureSol(solutionName, "t1", "atolglobal", ABSOLUTE_TOLERANCE_GLOBAL);
        srv.setParamFeatureSol(solutionName, "t1", "tstepsbdf", timeStepping.strict.toString());
        srv.setParamFeatureSol(solutionName, "t1", "estrat", algebraicError.include.toString());
        srv.setParamFeatureSol(solutionName, "t1", "maxorder", BDF_ORDER);
        srv.setParamFeatureSol(solutionName, "t1", "control", "time");
        srv.setParamFeatureSol(solutionName, "t1", "tout", outputSteps.tlist.toString());
        srv.createFeatureSol(solutionName, "t1", "d1", "Direct");
        srv.setParamFeatureFeatureSol(solutionName, "t1", "d1", "linsolver", "pardiso");
        srv.createFeatureSol(solutionName, "t1", "fc1", "FullyCoupled");
        srv.setParamFeatureFeatureSol(solutionName, "t1", "fc1", "linsolver", "d1");
        srv.setParamFeatureFeatureSol(solutionName, "t1", "fc1", "damp", DAMP_FACTOR_NEWTON);
        srv.setParamFeatureFeatureSol(solutionName, "t1", "fc1", "jtech", jacobianUpdate.onevery.toString());
        srv.setParamFeatureFeatureSol(solutionName, "t1", "fc1", "ntermconst", "itertol");
        srv.setParamFeatureFeatureSol(solutionName, "t1", "fc1", "niter", MAX_ITER_NEWTON);
        srv.setParamFeatureFeatureSol(solutionName, "t1", "fc1", "ntolfact", TOL_FACTOR_NEWTON);

        srv.attachSol(solutionName, studyName);
        srv.setParamFeatureStudy(studyName, "time", "initsol", solutionName);
        srv.setParamFeatureStudy(studyName, "time", "initsoluse", "sol1");
    }

    /**
     * Method sets active physics for a study
     *
     * @param srv               - input SigmaServer instance
     * @param studyName         - name of a study for which the settting is made
     * @param activePhysicsName - names of physics to be activated
     * @param activePhysicsBool - flag corresponding to each physics (true - active, false - inactive)
     */
    public static void setActivePhysics(SigmaServer srv, String studyName, String[] activePhysicsName, Boolean[] activePhysicsBool) {
        String feature = "time";
        for (int i = 0; i < activePhysicsName.length; i++) {
            srv.activateFeatureStudy(studyName, feature, activePhysicsName[i], activePhysicsBool[i]);
        }
    }

    /**
     * Method runs a study
     *
     * @param srv         - input SigmaServer instance
     * @param studyNameIn - name of a study to be run
     */
    public static void runStudy(SigmaServer srv, String studyNameIn) {
        String solutionName = studyNameIn;
        String datasetName = studyNameIn;
        srv.removeDatasetResult(datasetName);
        srv.showProgressModelUtil(true);
        srv.runAllSol(solutionName);
        srv.showProgressModelUtil(false);

        // Doesn't matter what API I use, the previous command will always generate a dset1 by default
        srv.removeDatasetResult("dset1");
        srv.createDatasetResult(datasetName, "Solution");
        srv.setLabelDatasetResult(datasetName, datasetName);
        srv.setParamDatasetResult(datasetName, "solution", solutionName);
    }
}
