package comsol.api;

import comsol.constants.MPHC;
import server.SigmaServer;

/**
 * Class contains a set of static API functions to deal with global equations physics in COMSOL
 */
public class HeatTransferInSolidsPhysicsAPI {
    private HeatTransferInSolidsPhysicsAPI() {
    }

    /**
     * Method adds a node in the heat transfer in solids physics
     * @param srv           - Input SigmaServer instance
     * @param nodeName      - Label for the name of the heat transfer node
     * @param geometryLabel - Label for the corresponding geometry node of the heat transer node
     */
    public static void addNode(SigmaServer srv, String nodeName, String geometryLabel) {
        srv.createPhysicsNode(nodeName, "HeatTransfer", geometryLabel);
        srv.setPropertyPhysics(MPHC.LABEL_PHYSICSTH, "PhysicalModelProperty", "dz", "1");
        srv.setPropertyPhysics(nodeName, "PhysicalModelProperty", "IsothermalDomain", true);
        srv.setIndexFeaturePhysics(MPHC.LABEL_PHYSICSTH, "solid1", "minput_magneticfluxdensity_src", "userdef", 0);
        srv.setFeaturePhysics(nodeName, "init1", "Tinit", MPHC.VALUE_T_OPERATIONAL);
        srv.setPropertyPhysics(nodeName, "ShapeProperty", "order_temperature", "1");
        srv.setFieldFieldPhysics(nodeName, "temperature", MPHC.LABEL_PHYSICSTH_T);
    }

    /**
     * Method sets node selection from the geometry for the physics
     *
     * @param srv - input SigmaServer instance
     */
    public static void setNodeSelection(SigmaServer srv) {
        srv.setNamedSelectionFeaturePhysics(MPHC.LABEL_PHYSICSTH, MPHC.LABEL_GEOM + "_" + MPHC.LABEL_PHYSICSTH_SELECTION + "_dom");
    }

    /**
     * Method builds a heat source
     *
     * @param srv            - input SigmaServer instance
     * @param nodeLabel      - physics node label
     * @param selectionLabel - named selection label
     * @param q0             - input heat source term
     */
    public static void buildHeatSource(SigmaServer srv, String nodeLabel, String selectionLabel, String q0) {
        String nodeLocalLabel = nodeLabel + "_heatSource";
        HeatTransferInSolidsPhysicsAPI.addNodeHeatSource(srv, nodeLocalLabel, q0);
        HeatTransferInSolidsPhysicsAPI.assignToSelection(srv, nodeLocalLabel, selectionLabel);
    }

    /**
     * Method builds a general thin layer
     *
     * @param srv            - input SigmaServer instance
     * @param nodeLabel      - physics node label
     * @param selectionLabel - named selection label
     * @param material       - input layer materials
     * @param thickness      - input layer thiknesses
     */
    public static void buildGeneralThinLayer(SigmaServer srv, String nodeLabel, String selectionLabel, String[] material, String[] thickness) {
        String nodeLocalLabel = nodeLabel + "_thinLayer";
        HeatTransferInSolidsPhysicsAPI.addNodeGeneralThinLayer(srv, nodeLocalLabel, material, thickness);
        HeatTransferInSolidsPhysicsAPI.assignToSelection(srv, nodeLocalLabel, selectionLabel);
    }

    /**
     * Method builds a thick layer
     *
     * @param srv            - input SigmaServer instance
     * @param nodeLabel      - physics node label
     * @param selectionLabel - named selection label
     * @param material       - input layer materials
     * @param thickness      - input layer thiknesses
     */
    public static void buildThickLayer(SigmaServer srv, String nodeLabel, String selectionLabel, String[] material, String[] thickness) {
        String nodeLocalLabel = nodeLabel + "_thickLayer";
        HeatTransferInSolidsPhysicsAPI.addNodeThickLayer(srv, nodeLocalLabel, material, thickness);
        HeatTransferInSolidsPhysicsAPI.assignToSelection(srv, nodeLocalLabel, selectionLabel);
    }

    /**
     * Method builds a thick layer
     *
     * @param srv            - input SigmaServer instance
     * @param nodeLabel      - physics node label
     * @param selectionLabel - named selection label
     * @param thickness      - input layer thiknesses
     */
    public static void buildThickLayer(SigmaServer srv, String nodeLabel, String selectionLabel, String[] thickness) {
        String nodeLocalLabel = nodeLabel + "_thickLayer";
        HeatTransferInSolidsPhysicsAPI.addNodeThickLayer(srv, nodeLocalLabel, thickness);
        HeatTransferInSolidsPhysicsAPI.assignToSelection(srv, nodeLocalLabel, selectionLabel);
    }

    /**
     * Method builds quench heaters (to be deprecated)
     *
     * @param srv            - input SigmaServer instance
     * @param nodeLabel      - physics node label
     * @param selectionLabel - named selection label
     * @param material       - input layer materials
     * @param thickness      - input layer thiknesses
     * @param qs             - heat source term
     */
    public static void buildQH(SigmaServer srv, String nodeLabel, String selectionLabel, String[] material, String[] thickness, String[] qs) {
        String localNodeThinLayerLabel = nodeLabel + "_thinLayer";
        String localNodeQHLabel = nodeLabel + "_QH";
        HeatTransferInSolidsPhysicsAPI.addNodeGeneralThinLayer(srv, localNodeThinLayerLabel, material, thickness);
        HeatTransferInSolidsPhysicsAPI.addNodeHeatSourceToGeneralThinLayer(srv, localNodeThinLayerLabel, localNodeQHLabel, qs);
        HeatTransferInSolidsPhysicsAPI.assignToSelection(srv, localNodeThinLayerLabel, selectionLabel);
    }

    private static void addNodeHeatSource(SigmaServer srv, String nodeLabel, String q0) {
        srv.createLabelPhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, "HeatSource", 2);
        srv.setLabelFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, nodeLabel);
        srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, "Q0", q0);
    }

    private static void addNodeGeneralThinLayer(SigmaServer srv, String nodeLabel, String[] material, String[] thickness) {
        srv.createLabelPhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, "ThinLayer", 1);
        srv.setLabelFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, nodeLabel);
        srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, "LayerType", "General");
        srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, "NumberOfLayers", Integer.toString(material.length));

        for (int layer_i = 0; layer_i < material.length; layer_i++) {
            String materialInternalLabel = "SolidMaterial" + Integer.toString(layer_i + 1);
            String thicknessInternalLabel = "ds" + Integer.toString(layer_i + 1);
            srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, materialInternalLabel, material[layer_i]);
            srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, thicknessInternalLabel, thickness[layer_i]);
        }
    }

    private static void addNodeThickLayer(SigmaServer srv, String nodeLabel, String[] material, String[] thickness) {
        srv.createLabelPhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, "ThinLayer", 1);
        srv.setLabelFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, nodeLabel);
        srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, "LayerType", "Resistive");
        srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, "MultipleLayersStructure", true);
        srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, "NumberOfLayers", Integer.toString(material.length));

        for (int layer_i = 0; layer_i < material.length; layer_i++) {
            String materialInternalLabel = "SolidMaterial" + Integer.toString(layer_i + 1);
            String thicknessInternalLabel = "ds" + Integer.toString(layer_i + 1);
            String ksMaterial = "ks" + Integer.toString(layer_i + 1) + "_mat";
            srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, materialInternalLabel, material[layer_i]);
            srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, thicknessInternalLabel, thickness[layer_i]);
            srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, ksMaterial, "from_mat");
        }
    }

    private static void addNodeThickLayer(SigmaServer srv, String nodeLabel, String[] thickness) {
        srv.createLabelPhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, "ThinLayer", 1);
        srv.setLabelFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, nodeLabel);
        srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, "LayerType", "Resistive");
        srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, "MultipleLayersStructure", true);
        srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, "NumberOfLayers", Integer.toString(thickness.length));

        for (int layer_i = 0; layer_i < thickness.length; layer_i++) {
            String materialInternalLabel = "SolidMaterial" + Integer.toString(layer_i + 1);
            String thicknessInternalLabel = "ds" + Integer.toString(layer_i + 1);
            srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, materialInternalLabel, "dommat");
            srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, thicknessInternalLabel, thickness[layer_i]);

            String kMaterial = "ks" + Integer.toString(layer_i + 1) + "_mat";
            String rhoMaterial = "rhos" + Integer.toString(layer_i + 1) + "_mat";
            String cpMaterial = "Cp_s" + Integer.toString(layer_i + 1) + "_mat";

            // Define the entry of the drop-down menu
            srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, kMaterial, "from_mat");
            srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, rhoMaterial, "from_mat");
            srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, cpMaterial, "userdef");

            // Define the userdef values
            srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, "Cp_s" + Integer.toString(layer_i + 1), "eps[J/kg/K]");
        }
    }

    private static void addNodeHeatSourceToGeneralThinLayer(SigmaServer srv, String parentNode, String nodeLabel, String[] qs) {
        srv.createFeaturePhysics(MPHC.LABEL_PHYSICSTH, parentNode, nodeLabel, "LayerHeatSource", 1);
        srv.setFeatureFeaturePhysics(MPHC.LABEL_PHYSICSTH, parentNode, nodeLabel, "Qs1", qs[0]);
        srv.setFeatureFeaturePhysics(MPHC.LABEL_PHYSICSTH, parentNode, nodeLabel, "Qs2", qs[1]);
        srv.setFeatureFeaturePhysics(MPHC.LABEL_PHYSICSTH, parentNode, nodeLabel, "Qs3", qs[2]);
    }

    private static void assignToSelection(SigmaServer srv, String nodeLabel, String selectionLabel) {
        if (selectionLabel != null) {
            srv.setNamedSelectionFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, selectionLabel);
        }
    }

    /**
     * Method adds a Thin Layer node to the Heat Transfer node in the Quench Heater component:
     * @param srv       - input SigmaServer instance
     * @param label     - Label for the Thin Layer node
     * @param input     - Input of domains (selection)
     * @param dsLabel   - Value of ds (layer thickness)
     * @param ksLabel   - Value of ks (layer thermal conductivity)
     */
    public static void addQHNodeThinLayer(SigmaServer srv, String label, String input, String dsLabel, String ksLabel) {
        srv.createLabelPhysics(MPHC.LABEL_PHYSICSTH_QH, label, "ThinLayer", 0);
        srv.setLabelFeaturePhysics(MPHC.LABEL_PHYSICSTH_QH, label, label);
        srv.setNamedSelectionFeaturePhysics(MPHC.LABEL_PHYSICSTH_QH, label, input);
        srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH_QH, label, "ds", dsLabel);
        srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH_QH, label, "ks_mat", "userdef");
        srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH_QH, label, "ks", ksLabel);
        srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH_QH, label, "Cp_s_mat", "userdef");
        srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH_QH, label, "rhos_mat", "userdef");
        srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH_QH, label, "rhos", "1");
    }

    /**
     * Method adds a Temperature node to the Heat Transfer node in the Quench Heater component:
     * @param srv            - input SigmaServer instance
     * @param label          - Label for the Temperature node
     * @param selectionLabel - Input of domains (selection)
     * @param tempValue      - Value of the Temperature
     * @param constraintType - pass 'null' as default. if not empty, it creates 'internally symmetric' in COMSOL
     */
    public static void addQHNodeTemperature(SigmaServer srv, String label, String selectionLabel, String tempValue, String constraintType) {
        srv.createLabelPhysics(MPHC.LABEL_PHYSICSTH_QH, label, "TemperatureBoundary", 0);
        srv.setLabelFeaturePhysics(MPHC.LABEL_PHYSICSTH_QH, label, label);
        srv.setNamedSelectionFeaturePhysics(MPHC.LABEL_PHYSICSTH_QH, label, selectionLabel);
        srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH_QH, label, "T0", tempValue);

        if (constraintType != null) {
            srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH_QH, label, "constraintType", "unidirectionalVar");
        }
        else {

        }
    }

    /**
     * Method adds a Heat Source node to the Heat Transfer node in the Quench Heater component:
     * @param srv            - input SigmaServer instance
     * @param label          - Label for the Heat Source node
     * @param selectionLabel - Input of domains (selection)
     * @param Q0Label        - Value of the Q0
     */
    public static void addQHNodeHeatSource(SigmaServer srv, String label, String selectionLabel, String Q0Label) {
        srv.createLabelPhysics(MPHC.LABEL_PHYSICSTH_QH, label, "HeatSource", 1);
        srv.setLabelFeaturePhysics(MPHC.LABEL_PHYSICSTH_QH, label, label);
        srv.setNamedSelectionFeaturePhysics(MPHC.LABEL_PHYSICSTH_QH, label, selectionLabel);
        srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH_QH, label, "Q0", Q0Label);
    }

    /**
     * Method builds a Heat Flux node in the heat transfer node in component 1
     * @param srv             - input SigmaServer instance
     * @param nodeLabel       - Label for the name of the Heat Flux node
     * @param selectionLabel  - Label for the selection, to which a Heat Flux node must be assigned
     * @param q0Value         - String value for the q0 inside the Heat Flux node
     */
    public static void addQHHeatFluxNode(SigmaServer srv, String nodeLabel, String selectionLabel, String q0Value) {
        srv.createLabelPhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, "HeatFluxBoundary", 1);
        srv.setLabelFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, nodeLabel);
        srv.setIndexFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, "materialType", "solid", 0);
        srv.setFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, "q0", q0Value);
        srv.setNamedSelectionFeaturePhysics(MPHC.LABEL_PHYSICSTH, nodeLabel, selectionLabel);
    }
}
