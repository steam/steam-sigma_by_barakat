package comsol.api;

import comsol.constants.MPHC;
import server.SigmaServer;

public class ResultsAPI {

    private ResultsAPI() {

    }

    public static void createMonolithicResultsNode(SigmaServer srv) {
        // Plot magnetic flux density:
        String MagnetFluxLabel = "Magnetic Flux Density";
        srv.createResult(MagnetFluxLabel, "PlotGroup2D");
        srv.setResultLabel(MagnetFluxLabel);
        srv.setResultType(MagnetFluxLabel, "Surf1", "Surface");
        srv.setResultType(MagnetFluxLabel, "Cont1", "Contour");
        srv.setResultFeature(MagnetFluxLabel, "Cont1", "number", 15);
        srv.setResultFeature(MagnetFluxLabel, "Cont1", "coloring", "uniform");
        srv.setResultFeature(MagnetFluxLabel, "Cont1", "color", "gray");
        srv.setResultFeature(MagnetFluxLabel, "Cont1", "expr", "mf.Az");
        srv.setResultFeature(MagnetFluxLabel, "Cont1", "colorlegend", false);
        srv.setResultFeature(MagnetFluxLabel, "Cont1", "titletype", "none");
        srv.runResult(MagnetFluxLabel);

        // Plot currents (I, I_cliq and I + I_cliq):
        String Currentlabel = "Currents";
        srv.createResult(Currentlabel, "PlotGroup1D");
        srv.setResultLabel(Currentlabel);
        srv.setResultType(Currentlabel, Currentlabel, "Global");
        srv.setResultFeatureIndex(Currentlabel, Currentlabel, "expr","I+I_cliq", 0);
        srv.setResultFeatureIndex(Currentlabel, Currentlabel, "descr","I+I_cliq", 0);
        srv.setResultFeatureIndex(Currentlabel, Currentlabel, "descr","I", 1);
        srv.setResultFeatureIndex(Currentlabel, Currentlabel, "descr","I_cliq", 2);
        srv.runResult(Currentlabel);

        // Plot Quench Heater temperatures as Line Graph:
        String QHTemplabel = "QH Temperature (Line)";
        srv.createResult(QHTemplabel, "PlotGroup1D");
        srv.setResultLabel(QHTemplabel);
        srv.setResultType(QHTemplabel, "lngr1", "LineGraph");
        srv.setResult(QHTemplabel,"data", "dset2");
        srv.assignResultSelectiontoAll(QHTemplabel, "lngr1");
        srv.setResultFeature(QHTemplabel, "lngr1", "expr", "TQH");
        srv.setResultFeature(QHTemplabel, "lngr1", "xdata", "expr");
        srv.setResultFeature(QHTemplabel, "lngr1", "xdataexpr", "x");
        srv.runResult(QHTemplabel);
    }
}
