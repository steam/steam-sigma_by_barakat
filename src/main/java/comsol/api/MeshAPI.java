package comsol.api;

import comsol.constants.MPHC;
import model.domains.Domain;
import model.domains.DomainType;
import server.SigmaServer;

/**
 * Class contains a set of static API functions to deal with default mesh in COMSOL
 */
public class MeshAPI {

    private MeshAPI() {
    }
    /**
     * Method creates the default mesh in COMSOL
     * @param srv - input SigmaServer instance
     */
    public static void createDefaultMesh(SigmaServer srv) {
        srv.createMesh(MPHC.LABEL_MESH, MPHC.LABEL_GEOM);
        srv.runMesh(MPHC.LABEL_MESH);
    }

    /**
     * Method creates a Mesh node inside component 1, with a Mapped mesh (incl. 2 Distribution) and Free Triangular Mesh
     * @param srv       - input SigmaServer instance
     * @param domains   - input domains
     */
    public static void createMappedMeshwithFreeTriangular(SigmaServer srv, Domain[] domains) {
        for (Domain dom : domains) {
            if (dom.getDomainType() == DomainType.COIL) {
                // Create empty Mesh node:
                srv.createMesh(MPHC.LABEL_MESH, MPHC.LABEL_GEOM);

                // Add "Mapped" Mesh node:
                srv.setMeshType(MPHC.LABEL_MESH, MPHC.LABEL_MAPPED_MESH, "Map");
                srv.setMeshGeometricLevel(MPHC.LABEL_MESH, MPHC.LABEL_MAPPED_MESH, MPHC.LABEL_GEOM, 2);
                srv.assignMeshtoNamedSelection(
                        MPHC.LABEL_MESH,
                        MPHC.LABEL_MAPPED_MESH,
                        String.format("%s_%s_dom",
                                MPHC.LABEL_GEOM,
                                dom.getLabel()));

                // Add "Distribution" Mesh node to "Mapped" Mesh node:
                srv.createMeshFeature(MPHC.LABEL_MESH, MPHC.LABEL_MAPPED_MESH, "dis1", "Distribution");
                srv.setMeshFeature(MPHC.LABEL_MESH, MPHC.LABEL_MAPPED_MESH, "dis1", "numelem", 4);
                srv.assignMeshFeaturetoSelection(
                        MPHC.LABEL_MESH,
                        MPHC.LABEL_MAPPED_MESH,
                        "dis1",
                        String.format("%s_%s%s%s",
                                MPHC.LABEL_GEOM,
                                dom.getLabel(),
                                MPHC.MARKER_SELECTION,
                                MPHC.MARKER_WIDEBEZIER));

                // Add "Distribution" Mesh node to "Mapped" Mesh node:
                srv.createMeshFeature(MPHC.LABEL_MESH, MPHC.LABEL_MAPPED_MESH, "dis2", "Distribution");
                srv.setMeshFeature(MPHC.LABEL_MESH, MPHC.LABEL_MAPPED_MESH, "dis2", "numelem", 1);
                srv.assignMeshFeaturetoSelection(
                        MPHC.LABEL_MESH,
                        MPHC.LABEL_MAPPED_MESH,
                        "dis2",
                        String.format("%s_%s%s%s",
                                MPHC.LABEL_GEOM,
                                dom.getLabel(),
                                MPHC.MARKER_SELECTION,
                                MPHC.MARKER_NARROWBEZIER));

                // Add "Free Triangular" Mesh node:
                srv.setMeshType(MPHC.LABEL_MESH, MPHC.LABEL_TRIANGULAR_MESH, "FreeTri");

                // Runs mesh
                srv.runMesh(MPHC.LABEL_MESH);
            }
        }
    }

    /**
     * Method creates a Mesh node inside component 1, with a Mapped mesh (incl. 2 Distribution) and Free Triangular Mesh
     * @param srv       - input SigmaServer instance
     */
    public static void createPhysicsControlledMesh(SigmaServer srv, String labelMesh, String labelGeom, int MeshSize) {
        srv.createMesh(labelMesh, labelGeom);
        srv.setLabelMesh(labelMesh);
        srv.setMeshSize(labelMesh, MeshSize);
        srv.runMesh(labelMesh);
    }
}
