package comsol.api;

import comsol.constants.MPHC;
import server.SigmaServer;

/**
 * Class contains a set of static API functions to deal with global equations physics in COMSOL
 */
public class GlobalEquationsPhysicsAPI {
    private GlobalEquationsPhysicsAPI() {
    }

    /**
     * Method adds a node in the global equation physics
     *
     * @param srv - input SigmaServer instance
     */
    public static void addNode(SigmaServer srv) {
        srv.createPhysicsNode(MPHC.LABEL_PHYSICSGE_GE, "GlobalEquations", MPHC.LABEL_GEOM);
        srv.createTagFeaturePhysics(MPHC.LABEL_PHYSICSGE_GE, "ge1", MPHC.LABEL_PHYSICSGE_ODE_CLIQ);
        srv.setLabelFeaturePhysics(MPHC.LABEL_PHYSICSGE_GE, MPHC.LABEL_PHYSICSGE_ODE_CLIQ, MPHC.LABEL_PHYSICSGE_ODE_CLIQ);
        srv.createLabelPhysics(MPHC.LABEL_PHYSICSGE_GE, MPHC.LABEL_PHYSICSGE_ODE_KVL, "GlobalEquations", -1);
        srv.setLabelFeaturePhysics(MPHC.LABEL_PHYSICSGE_GE, MPHC.LABEL_PHYSICSGE_ODE_KVL, MPHC.LABEL_PHYSICSGE_ODE_KVL);
    }

    private static void setIndex(SigmaServer srv, String nodeLabel, String operation, String value, int lineNumber) {
        srv.setIndexFeaturePhysics(MPHC.LABEL_PHYSICSGE_GE, nodeLabel, operation, value, lineNumber, 0);
    }

    /**
     * Method builds a variable with a given name at a given line index
     *
     * @param srv        - input SigmaServer instance
     * @param nodeLabel  - feature node label
     * @param value      - input value
     * @param lineNumber - input line index
     */
    public static void buildVariableName(SigmaServer srv, String nodeLabel, String value, int lineNumber) {
        setIndex(srv, nodeLabel, "name", value, lineNumber);
    }

    /**
     * Method builds equation in the global equations physics
     *
     * @param srv        - input SigmaServer instance
     * @param nodeLabel  - feature node label
     * @param value      - input value
     * @param lineNumber - input line index
     */
    public static void buildEquation(SigmaServer srv, String nodeLabel, String value, int lineNumber) {
        setIndex(srv, nodeLabel, "equation", value, lineNumber);
    }

    /**
     * Method builds initial value node in the global equations physics
     *
     * @param srv        - input SigmaServer instance
     * @param nodeLabel  - feature node label
     * @param value      - input value
     * @param lineNumber - input line index
     */
    public static void buildInitialValue(SigmaServer srv, String nodeLabel, String value, int lineNumber) {
        setIndex(srv, nodeLabel, "initialValueU", value, lineNumber);
    }

    /**
     * Method builds CLIQ equations in the global equations physics
     *
     * @param srv          - input SigmaServer instance
     * @param varaibleName - variable name
     * @param equation     - input equation
     * @param initialValue - initial value
     * @param lineNumber   - input line index
     */
    public static void buildCLIQEquations(SigmaServer srv, String varaibleName, String equation, String initialValue, int lineNumber) {
        buildVariableName(srv, MPHC.LABEL_PHYSICSGE_ODE_CLIQ, varaibleName, lineNumber);
        buildEquation(srv, MPHC.LABEL_PHYSICSGE_ODE_CLIQ, equation, lineNumber);
        buildInitialValue(srv, MPHC.LABEL_PHYSICSGE_ODE_CLIQ, initialValue, lineNumber);
    }

    /**
     * Method builds KVL equations in the global equations physics
     *
     * @param srv          - input SigmaServer instance
     * @param varaibleName - variable name
     * @param equation     - input equation
     * @param initialValue - initial value
     * @param lineNumber   - input line index
     */
    public static void buildKVLEquations(SigmaServer srv, String varaibleName, String equation, String initialValue, int lineNumber) {
        buildVariableName(srv, MPHC.LABEL_PHYSICSGE_ODE_KVL, varaibleName, lineNumber);
        buildEquation(srv, MPHC.LABEL_PHYSICSGE_ODE_KVL, equation, lineNumber);
        buildInitialValue(srv, MPHC.LABEL_PHYSICSGE_ODE_KVL, initialValue, lineNumber);
    }

    /**
     * Method builds units for the equations used in the global equations physics
     *
     * @param srv       - input SigmaServer instance
     * @param nodeLabel - global equations node label
     * @param input     - input unit
     * @param output    - output unit
     */
    public static void buildUnits(SigmaServer srv, String nodeLabel, String input, String output) {
        srv.setParamFeaturePhysics(MPHC.LABEL_PHYSICSGE_GE, nodeLabel, "CustomDependentVariableUnit", "1");
        srv.setParamFeaturePhysics(MPHC.LABEL_PHYSICSGE_GE, nodeLabel, "DependentVariableQuantity", "none");
        srv.setIndexFeaturePhysics(MPHC.LABEL_PHYSICSGE_GE, nodeLabel, "CustomDependentVariableUnit", output, 0, 0);
        srv.setParamFeaturePhysics(MPHC.LABEL_PHYSICSGE_GE, nodeLabel, "CustomSourceTermUnit", "1");
        srv.setParamFeaturePhysics(MPHC.LABEL_PHYSICSGE_GE, nodeLabel, "SourceTermQuantity", "none");
        srv.setIndexFeaturePhysics(MPHC.LABEL_PHYSICSGE_GE, nodeLabel, "CustomSourceTermUnit", input, 0, 0);
    }
}
