package comsol.api;

import comsol.constants.MPHC;
import server.SigmaServer;

/**
 * Class contains a set of static API functions to deal with domain ordinary equations (dode) in COMSOL
 */
public class DodePhysicsAPI {

    private DodePhysicsAPI() {
    }

    /**
     * Method adds a node to the dode physics
     *
     * @param srv - input SigmaServer instance
     */
    public static void addNode(SigmaServer srv) {
        srv.createPhysicsNode(MPHC.LABEL_PHYSICSDODE_DODE, "DomainODE", MPHC.LABEL_GEOM);
    }

    /**
     * Method builds a dode node
     *
     * @param srv            - input SigmaServer instance
     * @param selectionLabel - selection label to which dode is assigned
     */
    public static void buildDode(SigmaServer srv, String selectionLabel) {
        DodePhysicsAPI.assignSelection(srv, selectionLabel);
        DodePhysicsAPI.buildParamsAndUnits(srv);
        DodePhysicsAPI.setSourceTerm(srv, MPHC.LABEL_QUENCHSTATE);
        DodePhysicsAPI.setFirstOrderCoeff(srv, "1");
        DodePhysicsAPI.setSecondOrderCoeff(srv, "0");
    }

    private static void assignSelection(SigmaServer srv, String selectionLabel) {
        srv.setNamedSelectionFeaturePhysics(MPHC.LABEL_PHYSICSDODE_DODE, selectionLabel);
    }

    private static void setSourceTerm(SigmaServer srv, String sourceTerm) {
        srv.setIndexFeaturePhysics(MPHC.LABEL_PHYSICSDODE_DODE, "dode1", "f", sourceTerm, 0);
    }

    private static void setFirstOrderCoeff(SigmaServer srv, String coeff) {
        srv.setIndexFeaturePhysics(MPHC.LABEL_PHYSICSDODE_DODE, "dode1", "da", coeff, 0);
    }

    private static void setSecondOrderCoeff(SigmaServer srv, String coeff) {
        srv.setIndexFeaturePhysics(MPHC.LABEL_PHYSICSDODE_DODE, "dode1", "ea", coeff, 0);
    }

    private static void buildParamsAndUnits(SigmaServer srv) {
        srv.setPropPhysics(MPHC.LABEL_PHYSICSDODE_DODE, "Units", "CustomDependentVariableUnit", "1");
        srv.setPropPhysics(MPHC.LABEL_PHYSICSDODE_DODE, "Units", "DependentVariableQuantity", "none");
        srv.setIndexPropPhysics(MPHC.LABEL_PHYSICSDODE_DODE, "Units", "CustomDependentVariableUnit", "s", 0, 0);
        srv.setFieldFieldPhysics(MPHC.LABEL_PHYSICSDODE_DODE, "dimensionless", "t_star");
        srv.setComponentFieldPhysics(MPHC.LABEL_PHYSICSDODE_DODE, "dimensionless", 1, "t_star");
    }
}
