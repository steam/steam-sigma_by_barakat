package comsol.api;

/**
 * Class defines custom exception thrown in case of COMSOL API issues
 */
public class ComsolApiException extends RuntimeException {
    private final String message;
    /**
     * Constructs GeometryException object with a given message
     * @param message input exception message
     */
    public ComsolApiException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}