package comsol.api;


import comsol.constants.MPHC;
import server.SigmaServer;

/**
 * Class contains a set of static API functions to deal with magnetic fields physics in COMSOL
 */
public class MagneticFieldsPhysicsAPI {

    private MagneticFieldsPhysicsAPI() {
    }

    /**
     * Method adds a node in the heat transfer in solids physics
     *
     * @param srv - input SigmaServer instance
     */
    public static void addNode(SigmaServer srv) {
        srv.createPhysicsNode(MPHC.LABEL_PHYSICSMF, "InductionCurrents", MPHC.LABEL_GEOM);
        srv.setPropertyPhysics(MPHC.LABEL_PHYSICSMF, "d", "d", "1");
    }

    /**
     * Method builds a node for the external current density
     *
     * @param srv            - input SigmaServer instance
     * @param nodeLabel      - physics label
     * @param selectionLabel - selection (to which external current density is applied) label
     * @param J_xyz          - input labels of the external current densities
     */
    public static void buildExternalJ(SigmaServer srv, String nodeLabel, String selectionLabel, String[] J_xyz) {
        String nodeLocalLabel = nodeLabel + "_extJ";
        MagneticFieldsPhysicsAPI.addNodeExternalCurrentDensity(srv, nodeLocalLabel, J_xyz);
        MagneticFieldsPhysicsAPI.assignToSelection(srv, nodeLocalLabel, selectionLabel);
    }

    /**
     * Method builds a node with the Ampere Law for the equivalent magnetization
     *
     * @param srv            - input SigmaServer instance
     * @param nodeLabel      - physics label
     * @param selectionLabel - selection (to which Ampere's Law is applied) label
     * @param M_xyz          - input labels of the equivalent magnetization
     */
    public static void buildAmpereLawEqMag(SigmaServer srv, String nodeLabel, String selectionLabel, String[] M_xyz) {
        String nodeLocalLabel = nodeLabel + "_AmpLawEqMag";
        MagneticFieldsPhysicsAPI.addNodeAmperLawEqMag(srv, nodeLocalLabel, M_xyz);
        MagneticFieldsPhysicsAPI.assignToSelection(srv, nodeLocalLabel, selectionLabel);
    }

    /**
     * Method builds a node with the Ampere Law for a linear material
     *
     * @param srv            - input SigmaServer instance
     * @param nodeLabel      - physics label
     * @param selectionLabel - selection (to which Ampere's Law is applied) label
     */
    public static void buildAmpereLawLinearMaterial(SigmaServer srv, String nodeLabel, String selectionLabel) {
        String nodeLocalLabel = nodeLabel + "_AmpLawLin";
        MagneticFieldsPhysicsAPI.addNodeAmpereLawLinear(srv, nodeLocalLabel);
        MagneticFieldsPhysicsAPI.assignToSelection(srv, nodeLocalLabel, selectionLabel);
    }

    /**
     * Method bulds a node with the Ampere Law for a ferromagnetic material with a BH curve
     *
     * @param srv            - input SigmaServer instance
     * @param nodeLabel      - physics label
     * @param selectionLabel - selection (to which Ampere's Law is applied) label
     */
    public static void buildAmpereLawHBcurve(SigmaServer srv, String nodeLabel, String selectionLabel) {
        String nodeLocalLabel = nodeLabel + "_AmpLawBH";
        MagneticFieldsPhysicsAPI.addNodeAmpereLawHBCurve(srv, nodeLocalLabel);
        MagneticFieldsPhysicsAPI.assignToSelection(srv, nodeLocalLabel, selectionLabel);
    }

    /**
     * Method builds a node with a magnetic potential
     *
     * @param srv            - input SigmaServer instance
     * @param nodeLabel      - physics label
     * @param selectionLabel - selection (to which the magnetic potential is applied) label
     */
    public static void buildMagneticPotential(SigmaServer srv, String nodeLabel, String selectionLabel) {
        String nodeLocalLabel = nodeLabel + "_MagPot";
        MagneticFieldsPhysicsAPI.addNodeMagneticPotential(srv, nodeLocalLabel);
        MagneticFieldsPhysicsAPI.assignToSelection(srv, nodeLocalLabel, selectionLabel);
    }

    /**
     * Method builds a node with a perfect magnetic conductor
     *
     * @param srv            - input SigmaServer instance
     * @param nodeLabel      - physics label
     * @param selectionLabel - selection (to which the perfect magnetic conductor is applied) label
     */
    public static void buildPerfectMagneticConductor(SigmaServer srv, String nodeLabel, String selectionLabel) {
        String nodeLocalLabel = nodeLabel + "_MagCond";
        MagneticFieldsPhysicsAPI.addPerfectMagneticConductor(srv, nodeLocalLabel);
        MagneticFieldsPhysicsAPI.assignToSelection(srv, nodeLocalLabel, selectionLabel);
    }

    /**
     * Method builds a node for a coil with null eddy current sum
     *
     * @param srv            - input SigmaServer instance
     * @param nodeLabel      - physics label
     * @param selectionLabel - selection (to which the null current sum is applied) label
     */
    public static void buildCoilNullEddyCurrentsSum(SigmaServer srv, String nodeLabel, String selectionLabel) {
        String nodeLocalLabel = nodeLabel + "_Coil";
        MagneticFieldsPhysicsAPI.addCoilNullEddyCurrentsSum(srv, nodeLocalLabel);
        MagneticFieldsPhysicsAPI.assignToSelection(srv, nodeLocalLabel, selectionLabel);
    }

    /**
     * Method builds a node fore an inifite element domain
     *
     * @param srv            - input SigmaServer instance
     * @param nodeLabel      - physics label
     * @param selectionLabel - selection (to which the infinite element domain is applied) label
     */
    public static void buildInfiniteElementDomain(SigmaServer srv, String nodeLabel, String selectionLabel) {
        String nodeLocalLabel = nodeLabel + "_Coil";
        MagneticFieldsPhysicsAPI.addInfiniteElement(srv, nodeLocalLabel);
        MagneticFieldsPhysicsAPI.assignInfinteElementToSelection(srv, nodeLocalLabel, selectionLabel);
    }

    private static void addNodeExternalCurrentDensity(SigmaServer srv, String nodeLabel, String[] J_xyz) {
        srv.createLabelPhysics(MPHC.LABEL_PHYSICSMF, nodeLabel, "ExternalCurrentDensity", 2);
        srv.setLabelFeaturePhysics(MPHC.LABEL_PHYSICSMF, nodeLabel, nodeLabel);
        srv.setFeaturePhysics(MPHC.LABEL_PHYSICSMF, nodeLabel, "Je", new String[]{J_xyz[0], J_xyz[1], J_xyz[2]});
    }

    private static void addNodeAmperLawEqMag(SigmaServer srv, String nodeLabel, String[] M_xyz) {
        srv.createLabelPhysics(MPHC.LABEL_PHYSICSMF, nodeLabel, "AmperesLaw", 2);
        srv.setLabelFeaturePhysics(MPHC.LABEL_PHYSICSMF, nodeLabel, nodeLabel);
        srv.setFeaturePhysics(MPHC.LABEL_PHYSICSMF, nodeLabel, "ConstitutiveRelationH", "Magnetization");
        srv.setFeaturePhysics(MPHC.LABEL_PHYSICSMF, nodeLabel, "materialType", "solid");
        srv.setFeaturePhysics(MPHC.LABEL_PHYSICSMF, nodeLabel, "M", new String[]{M_xyz[0], M_xyz[1], M_xyz[2]});
    }

    private static void addNodeAmpereLawLinear(SigmaServer srv, String nodeLabel) {
        srv.createLabelPhysics(MPHC.LABEL_PHYSICSMF, nodeLabel, "AmperesLaw", 2);
        srv.setLabelFeaturePhysics(MPHC.LABEL_PHYSICSMF, nodeLabel, nodeLabel);
        srv.setFeaturePhysics(MPHC.LABEL_PHYSICSMF, nodeLabel, "ConstitutiveRelationH", "RelativePermeability");
    }

    private static void addNodeAmpereLawHBCurve(SigmaServer srv, String nodeLabel) {
        srv.createLabelPhysics(MPHC.LABEL_PHYSICSMF, nodeLabel, "AmperesLaw", 2);
        srv.setLabelFeaturePhysics(MPHC.LABEL_PHYSICSMF, nodeLabel, nodeLabel);
        srv.setFeaturePhysics(MPHC.LABEL_PHYSICSMF, nodeLabel, "ConstitutiveRelationH", "HBCurve");
    }

    private static void addNodeMagneticPotential(SigmaServer srv, String nodeLabel) {
        srv.createLabelPhysics(MPHC.LABEL_PHYSICSMF, nodeLabel, "MagneticPotential", 1);
        srv.setLabelFeaturePhysics(MPHC.LABEL_PHYSICSMF, nodeLabel, nodeLabel);
    }

    private static void addPerfectMagneticConductor(SigmaServer srv, String nodeLabel) {
        srv.createLabelPhysics(MPHC.LABEL_PHYSICSMF, nodeLabel, "PerfectMagneticConductor", 1);
        srv.setLabelFeaturePhysics(MPHC.LABEL_PHYSICSMF, nodeLabel, nodeLabel);
    }

    private static void addCoilNullEddyCurrentsSum(SigmaServer srv, String nodeLabel) {
        srv.createLabelPhysics(MPHC.LABEL_PHYSICSMF, nodeLabel, "Coil", 2);
        srv.setLabelFeaturePhysics(MPHC.LABEL_PHYSICSMF, nodeLabel, nodeLabel);
        srv.setFeaturePhysics(MPHC.LABEL_PHYSICSMF, nodeLabel, "coilGroup", true);
        srv.setFeaturePhysics(MPHC.LABEL_PHYSICSMF, nodeLabel, "ICoil", "0[A]");
    }

    private static void addInfiniteElement(SigmaServer srv, String nodeLabel) {
        srv.createCoordSystem(nodeLabel, MPHC.LABEL_GEOM, "InfiniteElement");
        srv.setLabelCoordSystem(nodeLabel, nodeLabel);
        srv.setParamCoordSystem(nodeLabel, "ScalingType", "Cylindrical");
    }

    private static void assignToSelection(SigmaServer srv, String nodeLabel, String selectionLabel) {
        if (selectionLabel != null) {
            srv.setNamedSelectionFeaturePhysics(MPHC.LABEL_PHYSICSMF, nodeLabel, selectionLabel);
        }
    }

    private static void assignInfinteElementToSelection(SigmaServer srv, String nodeLabel, String selectionLabel) {
        srv.setNamedSelectionCoordSystem(nodeLabel, selectionLabel);
    }
}
