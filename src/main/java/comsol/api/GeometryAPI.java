package comsol.api;

import comsol.constants.MPHC;
import model.geometry.basic.Line;
import model.geometry.basic.Point;
import model.geometry.basic.Polygon;
import server.SigmaServer;

/**
 * Class contains a set of static API functions to deal with geometry in COMSOL
 */
public class GeometryAPI {

    private GeometryAPI() {
    }
    public static final int COMPONENT_DIMENSION_1D = 1;  // Index dimension of 1D component
    public static final int COMPONENT_DIMENSION_2D = 2;  // Index dimension of 2D component
    public static final int COMPONENT_DIMENSION_3D = 3;  // Index dimension of 3D component


    /**
     * Method creates a geometry node in a model
     *
     * @param srv - input SigmaServer instance
     */
    /*
    public static void createGeometryNode(SigmaServer srv) {
        srv.createGeom(MPHC.LABEL_GEOM, COMPONENT_DIMENSION_2D);
        srv.setGeomLabel(MPHC.LABEL_GEOM, MPHC.LABEL_GEOM);

        double repairTol = Double.parseDouble(MPHC.PARAM_REPAIRTOL_REL);
        srv.setGeomRepairTol(MPHC.LABEL_GEOM, repairTol);
    }
    */
    public static void createGeometryNode(SigmaServer srv, String geometryLabel, int dimensionality) {
        srv.createGeom(geometryLabel, dimensionality);
        srv.setGeomLabel(geometryLabel, geometryLabel);

        double repairTol = Double.parseDouble(MPHC.PARAM_REPAIRTOL_REL);
        srv.setGeomRepairTol(geometryLabel, repairTol);
    }


    /**
     * Method creates an union selection object for a given input domains
     *
     * @param srv    - input SigmaServer instance
     * @param label  - union selection labels
     * @param inputs - input domains from which the selection is made
     */
    public static void createUnionSelectionObject(SigmaServer srv, String label, String[] inputs) {
        String localFeatureLabel = "UnionSelection";
        srv.createLabelGeometry(MPHC.LABEL_GEOM, label, localFeatureLabel);
        srv.setLabelFeatureGeometry(MPHC.LABEL_GEOM, label, label);
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "entitydim", "-1");
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "input", inputs);
    }

    /**
     * Method creates an union selection domain for a given input domains
     *
     * @param srv    - input SigmaServer instance
     * @param label  - union selection labels
     * @param inputs - input domains from which the selection is made
     */
    public static void createUnionSelectionBoundary(SigmaServer srv, String geomLabel, String label, String[] inputs) {
        String localFeatureLabel = "UnionSelection";
        srv.createLabelGeometry(geomLabel, label, localFeatureLabel);
        srv.setLabelFeatureGeometry(geomLabel, label, label);
        srv.setParamFeatureGeometry(geomLabel, label, "entitydim", "0");
        srv.setParamFeatureGeometry(geomLabel, label, "input", inputs);
    }

    /**
     * Method creates an union selection domain for a given input domains
     *
     * @param srv    - input SigmaServer instance
     * @param label  - union selection labels
     * @param inputs - input domains from which the selection is made
     */
    public static void createUnionSelectionDomain(SigmaServer srv, String geomLabel, String label, String[] inputs) {
        String localFeatureLabel = "UnionSelection";
        srv.createLabelGeometry(geomLabel, label, localFeatureLabel);
        srv.setLabelFeatureGeometry(geomLabel, label, label);
        srv.setParamFeatureGeometry(geomLabel, label, "entitydim", "1");
        srv.setParamFeatureGeometry(geomLabel, label, "input", inputs);
    }

    public static void createUnionSelectionDomain2(SigmaServer srv, String geomLabel, String label, String[][] inputs) {
        String localFeatureLabel = "UnionSelection";
        srv.createLabelGeometry(geomLabel, label, localFeatureLabel);
        srv.setLabelFeatureGeometry(geomLabel, label, label);
        srv.setParamFeatureGeometry(geomLabel, label, "entitydim", "1");
        srv.setParamFeatureGeometry(geomLabel, label, "input", inputs);
    }


    /**
     * Method creates a cumulative selection for a geometry
     *
     * @param srv   - input SigmaServer instance
     * @param label - cumulative selection label
     */
    public static void createCumulativeSelection(SigmaServer srv, String label) {
        String localFeatureLabel = "CumulativeSelection";
        srv.createSelectionGeometry(MPHC.LABEL_GEOM, label, localFeatureLabel);
        srv.setLabelSelectionGeometry(MPHC.LABEL_GEOM, label, label);
    }

    /**
     * Method contributes an existing geometry to a new selection:
     * @param srv            - input SigmaServer instance
     * @param geomLabel      - label for geometry node
     * @param selectionLabel - label for the selection to be created
     * @param featureLabel   - label of the geometry to be contributed to a selection
     */
    public static void contributeGeometrytoSelection(SigmaServer srv, String geomLabel, String selectionLabel, String featureLabel) {
        srv.setParamFeatureGeometry(geomLabel, featureLabel, "selresult", true);
        srv.setParamFeatureGeometry(geomLabel, featureLabel, "selresultshow", "bnd");
        srv.createSelectionGeometry(geomLabel, selectionLabel, "CumulativeSelection");
        srv.setLabelSelectionGeometry(geomLabel, selectionLabel, selectionLabel);
        srv.setParamFeatureGeometry(geomLabel, featureLabel, "contributeto", selectionLabel);
    }

    /**
     * Method creates convertion to solid of domains
     *
     * @param srv   - input SigmaServer instance
     * @param label - label of the convertion to solid
     */
    public static void createConvertToSolid(SigmaServer srv, String label) {
        String localFeatureLabel = "ConvertToSolid";
        srv.createGeom(MPHC.LABEL_GEOM, label, localFeatureLabel);
        srv.setLabelFeatureGeometry(MPHC.LABEL_GEOM, label, label);
    }

    /**
     * Method selects inputs for a geometry feature
     *
     * @param srv    - input SigmaServer instance
     * @param label  - selection label
     * @param inputs - list of domains to be selected
     */
    public static void selectInput(SigmaServer srv, String label, String[] inputs) {
        String localFeatureLabel = "input";
        srv.setSelectionFeatureGeom(MPHC.LABEL_GEOM, label, localFeatureLabel, inputs);
    }

    /**
     * Method determines to what geometric feature given named selection contributes to
     *
     * @param srv      - input SigmaServer instance
     * @param label    - geometric feature label
     * @param selLabel - named selection label
     */
    public static void contributeTo(SigmaServer srv, String label, String selLabel) {
        String localFeatureLabel = "contributeto";
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, localFeatureLabel, selLabel);
    }

    /**
     * Method creates a hyperline in a geometry node
     *
     * @param srv       - input SigmaServer instance
     * @param label     - hyperline label
     * @param coordExpr - expression with coordinates
     */
    public static void createHyperLine(SigmaServer srv, String label, String[] coordExpr) {
        String localFeatureLabel = "ParametricCurve";
        srv.createLabelGeometry(MPHC.LABEL_GEOM, label, localFeatureLabel);
        srv.setLabelFeatureGeometry(MPHC.LABEL_GEOM, label, label);
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "parname", "s");
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "parmin", "0");
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "parmax", "1");
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "rtol", MPHC.PARAM_REPAIRTOL_HYPERLINE);
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "maxknots", MPHC.PARAM_KNOTS_IN_HYPERLINE);
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "coord", coordExpr);
    }

    /**
     * Method creates a hyperarea in a geometry node
     *
     * @param srv            - input SigmaServer instance
     * @param hyperAreaLabel - hyper area label
     * @param linesLabels    - labels of lines from which an area is created
     */
    public static void createHyperArea(SigmaServer srv, String hyperAreaLabel, String[] linesLabels) {
        createConvertToSolid(srv, hyperAreaLabel);
        selectInput(srv, hyperAreaLabel, linesLabels);
    }

    /**
     * Method creates a polygon
     *
     * @param srv   - input SigmaServer instance
     * @param label - polygon label
     * @param poly  - input polygon
     */
    // Refactored & generalized to a n-points polygon
    public static void createPolygon(SigmaServer srv, String label, Polygon poly) {
        String localFeatureLabel = "Polygon";
        srv.createLabelGeometry(MPHC.LABEL_GEOM, label, localFeatureLabel);
        srv.setLabelFeatureGeometry(MPHC.LABEL_GEOM, label, label);
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "source", "table");

        Point[] parray = poly.getVertices();
        for (int point_i = 0; point_i < parray.length; point_i++) {
            srv.setIndexFeatureGeometry(MPHC.LABEL_GEOM, label, "table", Double.toString(parray[point_i].getX()), point_i, 0);
            srv.setIndexFeatureGeometry(MPHC.LABEL_GEOM, label, "table", Double.toString(parray[point_i].getY()), point_i, 1);
        }
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "selresult", "on");
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "selresultshow", "all");
    }

    /**
     * Method creates a ball selection to select geometric entities inside a ball of given center and radius
     *
     * @param srv   - input SigmaServer instance
     * @param label - ball selection label
     * @param input - input selections
     * @param r     - ball radius
     * @param posX  - ball center x-coordinate
     * @param posY  - ball center y-coordinate
     */
    public static void createBallSelection(SigmaServer srv, String label, String input, double r, double posX, double posY) {
        String localFeatureLabel = "BallSelection";
        srv.createLabelGeometry(MPHC.LABEL_GEOM, label, localFeatureLabel);
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "r", r);
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "posx", posX);
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "posy", posY);
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "inputent", "selections");
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "input", input);
    }

    /**
     * Method creates a box selection to select geometric entities within a certain box
     *
     * @param srv      - input SigmaServer instance
     * @param label    - box selecion label
     * @param xyBounds - coordinates of box vertices
     */
    public static void createBoxSelection(SigmaServer srv, String label, double[] xyBounds) {
        String localFeatureLabel = "BoxSelection";
        srv.createLabelGeometry(MPHC.LABEL_GEOM, label, localFeatureLabel);
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "entitydim", "1");
        srv.setLabelFeatureGeometry(MPHC.LABEL_GEOM, label, label);
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "xmin", String.valueOf(xyBounds[0]));
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "xmax", String.valueOf(xyBounds[1]));
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "ymin", String.valueOf(xyBounds[2]));
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "ymax", String.valueOf(xyBounds[3]));
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "condition", "inside");
    }

    /**
     * Method creates a union
     *
     * @param srv   - input SigmaServer instance
     * @param label - union label
     */
    public static void createUnion(SigmaServer srv, String label) {
        String localFeatureLabel = "Union";
        srv.createLabelGeometry(MPHC.LABEL_GEOM, label, localFeatureLabel);
        srv.setLabelFeatureGeometry(MPHC.LABEL_GEOM, label, label);
    }

    /**
     * Method keeps input object after a selection/union is made
     *
     * @param srv   - input SigmaServer instance
     * @param label - label for objects to be kept
     */
    public static void keepInputObjects(SigmaServer srv, String label) {
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "keep", "on");
    }

    /**
     * Method runs the geometry builder
     *
     * @param srv       - input SigmaServer instance
     * @param label     - geometry label to be built
     * @param caseLabel - choice of the geometry builder execution
     */
    public static void run(SigmaServer srv, String label, String caseLabel) {
        switch (caseLabel) {
            case "all":
                srv.runLabelGeometry(MPHC.LABEL_GEOM, label);
                break;
            case "pre":
                srv.runPreLabelGeometry(MPHC.LABEL_GEOM, label);
                break;
            default:
                throw new ComsolApiException(String.format("Case label %s not supported", caseLabel));
        }
    }

    /**
     * Method sets a level of geometry (point, line, area)
     *
     * @param srv   - input SigmaServer instance
     * @param label - label of level
     * @param level - input level
     */
    public static void setResultingLevel(SigmaServer srv, String label, String level) {
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "selresult", "on");
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "selresultshow", level);
    }

    /**
     * Method creates a linear Bezier polygon
     *
     * @param srv   - input SigmaServer instance
     * @param label - polygon label
     * @param line  - input line
     */
    public static void createBezierPolygonLinear(SigmaServer srv, String label, Line line) {
        String localFeatureLabel = "BezierPolygon";
        srv.createLabelGeometry(MPHC.LABEL_GEOM, label, localFeatureLabel);
        srv.setLabelFeatureGeometry(MPHC.LABEL_GEOM, label, label);
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "degree", new int[]{1});

        Point[] parray = line.extractEndPoints();
        for (int point_i = 0; point_i < parray.length; point_i++) {
            srv.setIndexFeatureGeometry(MPHC.LABEL_GEOM, label, "p", Double.toString(parray[point_i].getX()), 0, point_i);
            srv.setIndexFeatureGeometry(MPHC.LABEL_GEOM, label, "p", Double.toString(parray[point_i].getY()), 1, point_i);
        }
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "w", new String[]{"1", "1"});
    }

    /**
     * Method creates a selection at all levels
     *
     * @param srv   - input SigmaServer instance
     * @param label - selection label
     */
    public static void createSelectionAllLevels(SigmaServer srv, String label) {
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "createselection", "on");
    }

    /**
     * Method creates a selection at the domain level
     *
     * @param srv   - input SigmaServer instance
     * @param label - selection label
     */
    public static void createSelectionDomain(SigmaServer srv, String label) {
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "createselection", "on");
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "selresultshow", "dom");
    }

    /**
     * Method creates a selection at the boundary level
     *
     * @param srv   - input SigmaServer instance
     * @param label - selection label
     */
    public static void createSelectionBoundary(SigmaServer srv, String label) {
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "createselection", "on");
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "selresultshow", "bnd");
    }

    /**
     * Method creates a selection at the point level
     *
     * @param srv   - input SigmaServer instance
     * @param label - selection label
     */
    public static final void createSelectionPoint(SigmaServer srv, String label) {
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "createselection", "on");
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "selresultshow", "pnt");
    }

    /**
     * Method finalizes geometry by running repair tolerance utility for the model
     *
     * @param srv   - input SigmaServer instance
     * @param label - geometry label
     */
    public static final void finalizeGeometry(SigmaServer srv, String geomLabel,String label) {
        setRepairTolTypeRelative(srv, label);
        srv.runLabelGeometry(geomLabel, label);
    }

    // Considering renaming this to explicitBoundarySelection? (Matthias 2/4/19)
    /**
     * Method establishes an explicit selection for a given set of inputs
     * The type of geometry selection is on boundaries, through value 1:
     * @param srv    - input SigmaServer instance
     * @param label  - explicit selection label
     * @param inputs - domain inputs
     */
    public static final void explicitSelection(SigmaServer srv, String label, String[] inputs) {
        srv.createLabelGeometry(MPHC.LABEL_GEOM, label, "ExplicitSelection");
        srv.setLabelFeatureGeometry(MPHC.LABEL_GEOM, label, label);
        srv.initSelectionFeatureGeometry(MPHC.LABEL_GEOM, label, "selection", 1);

        for (String input : inputs) {
            srv.setParamSelectionFeatureGeom(MPHC.LABEL_GEOM, label, "selection", input, new int[]{1});
        }
    }


    /**
     * Method creates an Explicit Selection node inside the Definitions node in Component 1
     * @param srv       - input SigmaServer instance
     * @param compLabel - Label of the component, to which the Explicit Selections node must be assigned
     * @param nodeName  - Label for the name of the Explicit Selection node
     * @param nodeLabel - Label for Explicit Selections node
     * @param depth     - 0 = domain, 1 = boundary, 2 = point
     * @param input     - Input boundaries for which a selection is wanted to be made
     */
    public static void createExplicitSelectioninDefinitions(SigmaServer srv, String compLabel, String nodeName, String nodeLabel, int depth, String input) {
        srv.createExplicitSelectionNode(compLabel, nodeName, nodeLabel);
        srv.setExplicitSelectionNodetoGeometry(compLabel, nodeName, depth);
        srv.setExplicitSelectionBoundary(compLabel, nodeName, input);
    }

    /**
     * Method establishes an adjecent selection for a given set of inputs
     *
     * @param srv    - input SigmaServer instance
     * @param label  - adjecent selection label
     * @param inputs - domain inputs
     */
    public static final void adjacentSelection(SigmaServer srv, String label, String[] inputs) {
        String localFeatureLabel = "AdjacentSelection";
        srv.createLabelGeometry(MPHC.LABEL_GEOM, label, localFeatureLabel);
        srv.setLabelFeatureGeometry(MPHC.LABEL_GEOM, label, label);
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "interior", true);
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "input", inputs);
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "exterior", false);
    }

    /**
     * Method establishes an difference selection for a given set of inputs
     *
     * @param srv       - input SigmaServer instance
     * @param label     - difference selection label
     * @param inputsAdd - list of inputs to add
     * @param inputsSub - list of inputs to subtract
     */
    public static final void differenceSelection(SigmaServer srv, String label, String[] inputsAdd, String[] inputsSub) {
        String localFeatureLabel = "DifferenceSelection";
        srv.createLabelGeometry(MPHC.LABEL_GEOM, label, localFeatureLabel);
        srv.setLabelFeatureGeometry(MPHC.LABEL_GEOM, label, label);
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "entitydim", "1");
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "add", inputsAdd);
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "subtract", inputsSub);
    }

    /**
     * Method sets an automatic setting for the repair tolerance
     *
     * @param srv   - input SigmaServer instance
     * @param label - feature label
     */
    public static final void setRepairTolTypeAuto(SigmaServer srv, String label) {
        String localFeatureLabel = "repairtoltype";
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, localFeatureLabel, "auto");
    }

    /**
     * Method sets a relative repair tolerance
     *
     * @param srv   - input SigmaServer instance
     * @param label - feature label
     */
    public static final void setRepairTolTypeRelative(SigmaServer srv, String label) {
        String localFeatureLabel = "repairtoltype";
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, localFeatureLabel, "relative");
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "repairtol", MPHC.PARAM_REPAIRTOL_REL);
    }

    /**
     * Method sets an absolute repair tolerance
     *
     * @param srv   - input SigmaServer instance
     * @param label - feature label
     */
    public static final void setRepairTolTypeAbsolute(SigmaServer srv, String label) {
        String localFeatureLabel = "repairtoltype";
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, localFeatureLabel, "absolute");
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM, label, "absrepairtol", MPHC.PARAM_REPAIRTOL_ABS);
    }

    /**
     * Method builds an union
     *
     * @param srv            - input SigmaServer instance
     * @param labelSelection - selection label
     * @param labelInput     - input labels
     */
    public static void buildUnion(SigmaServer srv, String labelSelection, String[] labelInput) {
        GeometryAPI.createUnion(srv, labelSelection);
        GeometryAPI.keepInputObjects(srv, labelSelection);
        GeometryAPI.run(srv, labelSelection, "pre");
        GeometryAPI.selectInput(srv, labelSelection, labelInput);
        GeometryAPI.setResultingLevel(srv, labelSelection, "all");
        GeometryAPI.run(srv, labelSelection, "all");
    }

    public static final void createPointGeometry(SigmaServer srv, String geomLabel, String nodeName, String nodeLabel, String xValue) {
        srv.createLabelGeometry(geomLabel, nodeLabel, "Point");
        srv.setLabelFeatureGeometry(geomLabel, nodeLabel, nodeName);
        srv.setIndexFeatureGeometry(geomLabel, nodeLabel, "p", xValue, 0, 0);
    }

    public static final void createArrayGeometry(SigmaServer srv, String nodeName, String nodeLabel, String selectionLabel, String sizeLabel, String dispLabel) {
        srv.createLabelGeometry(MPHC.LABEL_GEOM_QH, nodeLabel, "Array");
        srv.setLabelFeatureGeometry(MPHC.LABEL_GEOM_QH, nodeLabel, nodeName);
        srv.setParamSelectionFeatureGeom(MPHC.LABEL_GEOM_QH, nodeLabel, "input", selectionLabel);
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM_QH, nodeLabel, "size", sizeLabel);
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM_QH, nodeLabel, "displ", dispLabel);
    }

    public static final void createIntervalGeometry(SigmaServer srv, String nodeName, String nodeLabel, String leftLabel, String rightLabel) {
        srv.createLabelGeometry(MPHC.LABEL_GEOM_QH, nodeLabel, "Interval");
        srv.setLabelFeatureGeometry(MPHC.LABEL_GEOM_QH, nodeLabel, nodeName);
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM_QH, nodeLabel, "p1", leftLabel);
        srv.setParamFeatureGeometry(MPHC.LABEL_GEOM_QH, nodeLabel, "p2", rightLabel);
    }
}
