package comsol.physics;

import comsol.api.HeatTransferInSolidsPhysicsAPI;
import comsol.constants.MPHC;
import model.domains.Domain;
import model.domains.database.WedgeDomain;
import server.SigmaServer;

/**
 * Class responsible for building domains related with the heat transfer in solid physics
 */
public class HeatTransfer {

    private HeatTransfer() {
    }

    /**
     * Method builds domains related with the heat transfer in solid physics
     *
     * @param srv    - input SigmaServer instance
     * @param domain - input domains to which a CLIQ unit is assinged
     */
    public static void build(SigmaServer srv, Domain domain) {

        switch (domain.getDomainType()) {
            case AIR:
                break;
            case AIRFARFIELD:
                break;
            case BOUNDARY:
                break;
            case COIL:
                break;
            case COILSPLINE:
                break;
            case HOLE:
                break;
            case INSULATION:
                break;
            case IRON:
                break;
            case QUENCHHEATER:
                break;
            case WEDGE:
                buildWedge(srv, (WedgeDomain) domain);
                break;
        }
    }

    private static void buildWedge(SigmaServer srv, WedgeDomain domain) {
        String selectionLabel;
        WedgeDomain wedDom = domain;
        String q0 = String.format("root.%s.mf.Qh", MPHC.LABEL_COMP);
        selectionLabel = MPHC.LABEL_GEOM + "_" + wedDom.getLabel() + "_dom";
        HeatTransferInSolidsPhysicsAPI.buildHeatSource(srv, wedDom.getLabel(), selectionLabel, q0);
    }
}