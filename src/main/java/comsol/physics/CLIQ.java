package comsol.physics;

import comsol.api.GlobalEquationsPhysicsAPI;
import comsol.coil.CoilMPH;
import comsol.definitions.VariablesPhysicsEngine;
import comsol.coil.WindingMPH;
import comsol.constants.MPHC;
import comsol.definitions.IextDefault;
import comsol.definitions.VariablesCLIQ;
import comsol.definitions.utils.Variable;
import comsol.definitions.utils.VariableGroup;
import model.domains.Domain;
import model.domains.DomainType;
import model.domains.database.CoilDomain;
import server.SigmaServer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class responsible for building CLIQ equations in a model
 */
public class CLIQ {
    /**
     * Method builds a CLIQ equations as a general equation and assing it to a coil domain
     *
     * @param srv     - input SigmaServer instance
     * // @param domains - input domains to which a CLIQ unit is assigned
     */

    public static void buildCLIQ(SigmaServer srv, Domain[] domains) {
        List<String> labelWindingList = createLabelWindingList(domains);                                                                        // Disabled, as the WindingsList (WL) is not needed.

        buildCircuitParams(srv);                                                                                                                // Builds 'Circuitparams'
        buildGE(srv, labelWindingList);
        buildExternalCurrent(srv, labelWindingList);

        for (Domain domain : domains) {
            if (domain.isCoil())
            {
                buildJunctionBoxes(srv, MPHC.LABEL_CLIQ_CURRENT_EXT, MPHC.LABEL_JUNCTIONBOX_NO_CLIQ,    CoilMPH.getselectionNoCliqHTs(), domain.getLabel());   // Builds 'JunctionBox'
                buildJunctionBoxes(srv, MPHC.LABEL_CLIQ_CURRENT_WITH_CLIQ, MPHC.LABEL_JUNCTIONBOX_CLIQ, CoilMPH.getselectionCliqHTs(),   domain.getLabel());
            }
        }
                                                                                                                                                // - WL is used for evaluating each winding - which we dont want to anymore!
//        buildCircuitParams(srv);                                                                                                                // Builds 'Circuitparams'
//        buildGE(srv, labelWindingList);                                                                                                         // Builds Global Equations (kvl etc.)
//        buildJunctionBoxes(srv, MPHC.LABEL_CLIQ_CURRENT_EXT,        MPHC.LABEL_JUNCTIONBOX_NO_CLIQ, CoilMPH.getselectionNoCliqWindingsHTs());   // Builds 'JunctionBox'
//        buildJunctionBoxes(srv, MPHC.LABEL_CLIQ_CURRENT_WITH_CLIQ,  MPHC.LABEL_JUNCTIONBOX_CLIQ,    CoilMPH.getselectionCliqWindingsHTs());     // Builds 'JunctionBox_cliq'
//        buildExternalCurrent(srv, labelWindingList);

        //buildGE(srv, labelWindingList);                                                                                                       // Disables the former Global Equations
        //buildJunctionBoxCLIQ(srv, labelWindingList);                                                                                          // Disables 'JunctionBox_cliq'
        //buildJunctionBoxNoCLIQ(srv, labelWindingList);                                                                                        // Disables 'JunctionBox'
    }

    private static void buildCircuitParams(SigmaServer srv) {
        VariableGroup varGroup = VariablesCLIQ.defineVariables();
        varGroup.buildVariableGroup(srv, MPHC.LABEL_COMP, MPHC.LABEL_GEOM, "", 2);
    }

    private static List<String> createLabelWindingList(Domain[] domains) {
        List<String> labelWindingList = new ArrayList<>();
        for (Domain dom : domains) {
            if (dom.getDomainType() == DomainType.COIL) {
                CoilMPH coil = CoilMPH.ofCoilDomain((CoilDomain) dom);
                String[] labelWindings = coil.getWindingPropertyString(WindingMPH::getLabel);
                Collections.addAll(labelWindingList, labelWindings);
            }
        }
        return labelWindingList;
    }

    private static void buildGE(SigmaServer srv, List<String> labelWindingList) {
        String variableName = String.format("%s", MPHC.LABEL_CLIQ_VOLTAGE);
        String equation = String.format("%s*%st-%s", MPHC.LABEL_CLIQ_CAPASITOR, MPHC.LABEL_CLIQ_VOLTAGE, MPHC.LABEL_CLIQ_CURRENT);
        String initialValue = String.format("%s", MPHC.LABEL_CLIQ_VOLTAGE_INITIAL);
        GlobalEquationsPhysicsAPI.buildCLIQEquations(srv, variableName, equation, initialValue, 0);
        GlobalEquationsPhysicsAPI.buildUnits(srv, MPHC.LABEL_PHYSICSGE_ODE_CLIQ, "A", "V");

        //StringBuilder eqKVL_1 = new StringBuilder();
        //StringBuilder eqKVL_2 = new StringBuilder();

        /*
         * These lines generate the voltage over every winding. These are not needed in the restructured model:
         */
       // double halfValue = (double) labelWindingList.size() / 2;
       // for (int i = 0; i < labelWindingList.size(); i++) {
       //     String labelWindingVoltage = "V_" + labelWindingList.get(i);
       //     eqKVL_1.append(String.format("%s+", labelWindingVoltage));

       //     if (i >= halfValue) {
       //         eqKVL_2.append(String.format("%s+", labelWindingVoltage));
       //     }
       // }

        /*
        eqKVL_1.append(String.format("%s*%s+%s*%st",
                MPHC.LABEL_CLIQ_RCROW,
                MPHC.LABEL_CLIQ_CURRENT_EXT,
                MPHC.LABEL_CLIQ_LCIR,
                MPHC.LABEL_CLIQ_CURRENT_EXT));
        eqKVL_2.append(String.format("%s+%s*%s+%s*%st",
                MPHC.LABEL_CLIQ_VOLTAGE,
                MPHC.LABEL_CLIQ_RESISTANCE,
                MPHC.LABEL_CLIQ_CURRENT,
                MPHC.LABEL_CLIQ_INDUCTANCE,
                MPHC.LABEL_CLIQ_CURRENT));
        */

        //eqKVL_1.append(String.format(%s*%s+%s*%st",
        String eqKVL_1 = String.format("%s+%s+%s*%s+%s*%st",        // Voltages across top and bottom half turns are added for 'I'
                MPHC.LABEL_VCOILS_NO_CLIQ,
                MPHC.LABEL_VCOILS_CLIQ,
                MPHC.LABEL_CLIQ_RCROW,
                MPHC.LABEL_CLIQ_CURRENT_EXT,
                MPHC.LABEL_CLIQ_LCIR,
                MPHC.LABEL_CLIQ_CURRENT_EXT);

        String withCLIQeqKVL_2 = String.format("%s+%s+%s*%s+%s*%st",
                MPHC.LABEL_VCOILS_CLIQ,
                MPHC.LABEL_CLIQ_VOLTAGE,
                MPHC.LABEL_CLIQ_RESISTANCE,
                MPHC.LABEL_CLIQ_CURRENT,
                MPHC.LABEL_CLIQ_INDUCTANCE,
                MPHC.LABEL_CLIQ_CURRENT);

        String withoutCLIQeqKVL_2 = String.format("%s*%st",
                MPHC.LABEL_CLIQ_INDUCTANCE,
                MPHC.LABEL_CLIQ_CURRENT);


        String eqKVL_2 = String.format("if(%s>0, %s, %s)",        // Voltages across top and bottom half turns are added for 'I_cliq'
                MPHC.LABEL_CLIQ_SWITCH,
                withCLIQeqKVL_2,
                withoutCLIQeqKVL_2);

        variableName = String.format("%s", MPHC.LABEL_CLIQ_CURRENT_EXT);
        equation = eqKVL_1;
        initialValue = String.format("%s", MPHC.LABEL_CLIQ_CURRENT_EXT_INITIAL);
        GlobalEquationsPhysicsAPI.buildKVLEquations(srv, variableName, equation, initialValue, 0);

        variableName = String.format("%s", MPHC.LABEL_CLIQ_CURRENT);
        equation = eqKVL_2;
        initialValue = String.format("%s", MPHC.LABEL_CLIQ_CURRENT_INITIAL);
        GlobalEquationsPhysicsAPI.buildKVLEquations(srv, variableName, equation, initialValue, 1);
        GlobalEquationsPhysicsAPI.buildUnits(srv, MPHC.LABEL_PHYSICSGE_ODE_KVL, "V", "A");
    }

    /**
     * This method defines and builds a JunctionBox variable group
     * @param srv                   - Input SigmaServer instance
     * @param value                 - Value for JunctionBox instance
     * @param labelJunctionBox      - Label for name for JunctionBox instance
     * @param selectionJunctionBox  - Assign selection to JunctionBox
     */
    private static void buildJunctionBoxes(SigmaServer srv, String value, String labelJunctionBox, String selectionJunctionBox, String coilLabel) {
        List<Variable> varList = new ArrayList<>(); // Creates an array
        String labelGroup = String.format("%s_%s", coilLabel, labelJunctionBox);                                                        // Creates label of 'Jz_versor_HT_minus'
        String label = String.format("%s", MPHC.LABEL_HT_I_SOURCE);                                                                 // Creates variable 'Jz_versor_HT' inside 'Jz_versor_HT_minus'
        varList.add(new Variable(label, value));

        VariableGroup varGroup = new VariableGroup(labelGroup, varList);                                                    // Creates variable group for assigning variable group to domain
        String selectionLabel = String.format("%s_%s_dom", MPHC.LABEL_GEOM, selectionJunctionBox);                              // Label for domain, to which the variable group should apply
        varGroup.buildVariableGroup(srv, MPHC.LABEL_COMP, MPHC.LABEL_GEOM, selectionLabel, 2);                                                                   // Variable group is assigned to said domain
    }

    /**
     * This method builds the external current:
     * @param srv               - Input SigmaServer instance
     * @param labelWindingList  - Evaluation for every winding in Winding List
     */
    private static void buildExternalCurrent(SigmaServer srv, List<String> labelWindingList) {
        ArrayList<Variable> varList = new ArrayList<>();
        for (String LabelWinding : labelWindingList) {
            String label = String.format("%s_%s", MPHC.MARKER_I_WINDING, LabelWinding);
            String value = String.format("%s_%s_EXT(t)", MPHC.MARKER_I_WINDING, LabelWinding);
            varList.add(new Variable(label, value));
            IextDefault.buildNode(srv, String.format("%s_%s_EXT", MPHC.MARKER_I_WINDING, LabelWinding));
            srv.setActiveFunction(String.format("%s_%s_EXT", MPHC.MARKER_I_WINDING, LabelWinding), false); // this is here
        }
    }

// --------------------------------------------------------------------------------------------------------------------
// 'JunctionBox_cliq' is not compiled into COMSOL, as it is not needed in the restructured model:
// ---------------------------------------------------------------------------------------------------------------------
/*
    private static void buildJunctionBoxCLIQ(SigmaServer srv, List<String> labelWindingList) {
        ArrayList<Variable> varList = new ArrayList<>();
        //String labelGroup = MPHC.MARKER_JUNCTIONBOX + "_" + MPHC.MARKER_CLIQ;

        double halfValue = (double) labelWindingList.size() / 2;

        for (int i = 0; i < labelWindingList.size(); i++) {
            String labelWinding = String.format("%s_%s", MPHC.MARKER_I_WINDING, labelWindingList.get(i));

            String value;
            if (i >= halfValue) {
                value = String.format("%s+%s", MPHC.LABEL_CLIQ_CURRENT_EXT, MPHC.LABEL_CLIQ_CURRENT);
            } else {
                value = String.format("%s", MPHC.LABEL_CLIQ_CURRENT_EXT);
            }

            varList.add(new Variable(labelWinding, value));
        }

        //VariableGroup varGroup = new VariableGroup(labelGroup, varList);
        //varGroup.buildVariableGroup(srv, "");
    }
// ---------------------------------------------------------------------------------------------------------------------
// 'JunctionBox' is not compiled into COMSOL, as it is not needed in the restructured model:
// ---------------------------------------------------------------------------------------------------------------------
    private static void buildJunctionBoxNoCLIQ(SigmaServer srv, List<String> labelWindingList) {
        ArrayList<Variable> varList = new ArrayList<>();
        String labelGroup   = String.format("%s", MPHC.LABEL_JUNCTIONBOX_1);                                            // Creates label of 'JunctionBox_1'
            String label1    = String.format("%s", MPHC.LABEL_HT_I_SOURCE);                                             // Creates variable 'Is_HT' inside 'JunctionBox_1'
            String value1    = String.format("%s", MPHC.LABEL_CLIQ_CURRENT_EXT);                                        // Creates expression 'I' for 'Is_HT'
        varList.add(new Variable(label1, value1));

        for (String LabelWinding : labelWindingList) {
         //   String label = String.format("%s_%s", MPHC.MARKER_I_WINDING, LabelWinding);                               // These lines disable 'Iw_...' inside 'JunctionBox_1'
         //   String value = String.format("%s_%s_EXT(t)", MPHC.MARKER_I_WINDING, LabelWinding);                        // These lines disable 'Iw_...' inside 'JunctionBox_1'
         //   varList.add(new Variable(label, value));                                                                  // These lines disable 'Iw_...' inside 'JunctionBox_1'
            IextDefault.buildNode(srv, String.format("%s_%s_EXT", MPHC.MARKER_I_WINDING, LabelWinding));                // This line builds 'Iw_...' pr. winding, outside JunctionBox
        }

        VariableGroup varGroup = new VariableGroup(labelGroup, varList);
        varGroup.buildVariableGroup(srv, "");
        //srv.setActiveVariable(labelGroup, true);                                                                      // Redundant, as it is active by default
    }
*/

// ---------------------------------------------------------------------------------------------------------------------
// TRASHED CODE:   TRASHED CODE:   TRASHED CODE:   TRASHED CODE:   TRASHED CODE:   TRASHED CODE:   TRASHED CODE:
// ---------------------------------------------------------------------------------------------------------------------
/**
* Method defines and builds the variable group 'JunctionBox_1'
*/
/*
        buildJzVersor(srv, "1", MPHC.LABEL_JZ_VERSOR_HT_PLUS, selectionJzPos);                                          // Builds               'Jz_versor_HT_plus'
        buildJzVersor(srv, "-1", MPHC.LABEL_JZ_VERSOR_HT_MINUS, selectionJzNeg);                                        // Builds               'Jz_versor_HT_minus'

    private void buildJzVersor(SigmaServer srv, String value, String labelJzVersorHt, String selectionJz) {
        List<Variable> varList = new ArrayList<>();                                                                     // Creates an array
        String labelGroup = String.format("%s", labelJzVersorHt);                                                       // Creates label of 'Jz_versor_HT_minus'
        String label = String.format("%s", MPHC.LABEL_JZ_VERSOR_HT);                                                    // Creates variable 'Jz_versor_HT' inside 'Jz_versor_HT_minus'
        varList.add(new Variable(label, value));

        VariableGroup varGroup = new VariableGroup(labelGroup, varList);                                                // Creates variable group for assigning variable group to domain
        String selectionLabel = String.format("%s_%s_dom", MPHC.LABEL_GEOM, selectionJz);                               // Label for domain, to which the variable group should apply
        varGroup.buildVariableGroup(srv, selectionLabel);                                                               // Variable group is assigned to said domain
    }
 */

/*
    private static void buildJunctionBoxWithCLIQ(SigmaServer srv) {
        ArrayList<Variable> varList = new ArrayList<>();                                                                // Creates an array
        String labelGroup   = String.format("%s", MPHC.LABEL_JUNCTIONBOX_1);                                            // Creates label of 'JunctionBox_1'
            String label    = String.format("%s", MPHC.LABEL_HT_I_SOURCE);                                              // Creates variable 'Is_HT' inside 'JunctionBox_1'
            String value    = String.format("%s", MPHC.LABEL_CLIQ_CURRENT_EXT);                                         // Creates expression 'I' for 'Is_HT'
        varList.add(new Variable(label, value));                                                                        // Variable group is added to the arraylist

        VariableGroup varGroup = new VariableGroup(labelGroup, varList);                                                // Label and expression added to array
        varGroup.buildVariableGroup(srv, "");                                                                           // Building the array
        srv.setActiveVariable(labelGroup, true);                                                                        // Normally active state (Default)
    }

    private static void buildJunctionBox_2(SigmaServer srv) {
        ArrayList<Variable> varList = new ArrayList<>();                                                                // Creates an array
        String labelGroup   = String.format("%s", MPHC.LABEL_JUNCTIONBOX_2);                                            // Creates label of 'JunctionBox_1'
        String label    = String.format("%s", MPHC.LABEL_HT_I_SOURCE);                                                  // Creates variable 'Is_HT' inside 'JunctionBox_1'
        String value    = String.format("%s+%s", MPHC.LABEL_CLIQ_CURRENT_EXT, MPHC.LABEL_CLIQ_CURRENT);                 // Creates expression 'I' for 'Is_HT'
        varList.add(new Variable(label, value));                                                                        // Variable group is added to the arraylist

        VariableGroup varGroup = new VariableGroup(labelGroup, varList);                                                // Label and expression added to array
        varGroup.buildVariableGroup(srv, "");                                                                           // Building the array
        srv.setActiveVariable(labelGroup, true);                                                                        // Normally active state (Default)
    }

    */

/*
    private static void buildJunctionBoxNoCLIQ(SigmaServer srv, List<String> labelWindingList) {
        ArrayList<Variable> varList = new ArrayList<>();
        String labelGroup = MPHC.MARKER_JUNCTIONBOX;

        for (String LabelWinding : labelWindingList) {
            //String label = String.format("%s_%s", MPHC.MARKER_I_WINDING, LabelWinding);
            //String value = String.format("%s_%s_EXT(t)", MPHC.MARKER_I_WINDING, LabelWinding);
            //varList.add(new Variable(label, value));
            IextDefault.buildNode(srv, String.format("%s_%s_EXT", MPHC.MARKER_I_WINDING, LabelWinding));
            srv.setActiveVariable(labelGroup, false);
        }

        VariableGroup varGroup = new VariableGroup(labelGroup, varList);
        varGroup.buildVariableGroup(srv, "");

        srv.setActiveVariable(labelGroup, true);
    }
*/

/*
    private static void buildWindingCurrent(SigmaServer srv, List<String> labelWindingList) {
        ArrayList<Variable> varList = new ArrayList<>();
        for (String LabelWinding : labelWindingList) {
            String label = String.format("%s_%s", MPHC.MARKER_I_WINDING, LabelWinding);
            String value = String.format("%s_%s_EXT(t)", MPHC.MARKER_I_WINDING, LabelWinding);
            varList.add(new Variable(label, value));
            IextDefault.buildNode(srv, String.format("%s_%s_EXT", MPHC.MARKER_I_WINDING, LabelWinding));
            srv.setActiveVariable(label, false);
        }
    }
*/
}