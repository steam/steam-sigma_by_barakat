package comsol.physics;

import comsol.api.MagneticFieldsPhysicsAPI;
import comsol.constants.MPHC;
import model.domains.database.BoundaryConditionsDomain;
import model.domains.database.*;
import model.domains.*;
import model.domains.BoundaryConditions;
import server.SigmaServer;

/**
 * Class responsible for building domains related with the magnetic field physics
 */
public class MagneticField {

    /**
     * Method builds domains related with the magnetic field physics
     *
     * @param srv    - input SigmaServer instance
     * @param domain - input domains to which a CLIQ unit is assigned
     */
    public static void build(SigmaServer srv, Domain domain) {

        switch (domain.getDomainType()) {
            case AIR:
                buildAir(srv, (AirDomain) domain);
                break;
            case AIRFARFIELD:
                buildArifFarField(srv, (AirFarFieldDomain) domain);
                break;
            case BOUNDARY:
                buildBoundary(srv, (BoundaryConditionsDomain) domain);
                break;
            case COIL:
                break;
            case COILSPLINE:
                break;
            case HOLE:
                buildHole(srv, (HoleDomain) domain);
                break;
            case INSULATION:
                break;
            case IRON:
                buildIron(srv, (IronDomain) domain);
                break;
            case QUENCHHEATER:
                break;
            case WEDGE:
                buildWedge(srv, (WedgeDomain) domain);
                break;
        }
    }

    private static void buildWedge(SigmaServer srv, WedgeDomain domain) {
        String selectionLabel;
        WedgeDomain wedDom = domain;
        selectionLabel = MPHC.LABEL_GEOM + "_" + wedDom.getLabel() + "_dom";
        MagneticFieldsPhysicsAPI.buildCoilNullEddyCurrentsSum(srv, wedDom.getLabel(), selectionLabel);
    }

    private static void buildIron(SigmaServer srv, IronDomain domain) {
        String selectionLabel;
        IronDomain ironDom = domain;
        selectionLabel = MPHC.LABEL_GEOM + "_" + ironDom.getLabel() + "_dom";
        MagneticFieldsPhysicsAPI.buildAmpereLawHBcurve(srv, ironDom.getLabel(), selectionLabel);
    }

    private static void buildHole(SigmaServer srv, HoleDomain domain) {
        String selectionLabel;
        HoleDomain holeDom = domain;
        selectionLabel = MPHC.LABEL_GEOM + "_" + holeDom.getLabel() + "_dom";
        MagneticFieldsPhysicsAPI.buildAmpereLawLinearMaterial(srv, holeDom.getLabel(), selectionLabel);
    }

    private static void buildBoundary(SigmaServer srv, BoundaryConditionsDomain domain) {
        BoundaryConditionsDomain bcDom = domain;
        switchBC(srv, bcDom.getXBCLabel(), bcDom.getXAxisBC());
        switchBC(srv, bcDom.getYBCLabel(), bcDom.getYAxisBC());
    }

    private static void buildArifFarField(SigmaServer srv, AirFarFieldDomain domain) {
        String selectionLabel;
        AirFarFieldDomain airFarFieldDom = domain;
        selectionLabel = MPHC.LABEL_GEOM + "_" + airFarFieldDom.getLabel() + "_dom";
        MagneticFieldsPhysicsAPI.buildAmpereLawLinearMaterial(srv, airFarFieldDom.getLabel(), selectionLabel);
        MagneticFieldsPhysicsAPI.buildInfiniteElementDomain(srv, airFarFieldDom.getLabel(), selectionLabel);
    }

    private static void buildAir(SigmaServer srv, AirDomain domain) {
        String selectionLabel;
        AirDomain airDom = domain;
        selectionLabel = MPHC.LABEL_GEOM + "_" + airDom.getLabel() + "_dom";
        MagneticFieldsPhysicsAPI.buildAmpereLawLinearMaterial(srv, airDom.getLabel(), selectionLabel);
    }

    private static void switchBC(SigmaServer srv, String bclabel, BoundaryConditions bc) {
        String selectionLabel = MPHC.LABEL_GEOM + "_" + bclabel;
        String physicsLabel = "phys_" + bclabel;

        switch (bc) {
            case noBoundaryCondition:
                break;
            case Dirichlet:
                MagneticFieldsPhysicsAPI.buildMagneticPotential(srv, physicsLabel, selectionLabel);
                break;
            case Neumann:
                MagneticFieldsPhysicsAPI.buildPerfectMagneticConductor(srv, physicsLabel, selectionLabel);
                break;
        }
    }
}
