package comsol.materials;

import comsol.api.MaterialsAPI;
import model.materials.Material;
import model.materials.MaterialException;
import model.materials.properties.*;
import server.SigmaServer;

/**
 * Class responsible for dealing with materials in a COMSOL model
 */
public class Materials {

    private Materials() {
    }

    /**
     * Method builds a material with a given definition and assigns it to thee provided selection
     *
     * @param srv            - input SigmaServer instance
     * @param material       - input material definition
     * @param selectionLabel - label of a selection to which material is assinged
     */
    public static void build(SigmaServer srv, Material material, String selectionLabel){
        String label = material.getLabel() + "_" + selectionLabel;
        buildProperties(srv, material, label);
        MaterialsAPI.assignToSelection(srv, label, selectionLabel);
    }

    private static void buildProperties(SigmaServer srv, Material material, String label) {

        MaterialsAPI.add(srv, label);

        for(Property prop : material.getProperties()){
            switch (prop.getType()) {
                case CP:
                    MaterialsAPI.addCp(srv, label, ((Cp) prop).getValue());
                    break;
                case EPSILONR:
                    MaterialsAPI.addEpsilonr(srv,label,((Epsilonr) prop).getValue());
                    break;
                case SIGMA:
                    MaterialsAPI.addSigma(srv, label, ((Sigma) prop).getValue());
                    break;
                case RHO:
                    MaterialsAPI.addRho(srv, label, ((Rho) prop).getValue());
                    break;
                case MUR:
                    MaterialsAPI.addMur(srv, label, ((Mur) prop).getValue());
                    break;
                case HB:
                    MaterialsAPI.addHB(srv, label, ((HB) prop).getValue());
                    break;
                case K:
                    MaterialsAPI.addK(srv, label, ((K) prop).getValue());
                    break;
                default:
                    throw new MaterialException(String.format("Material type %s not supported!", prop.getType().toString()));
            }
        }
    }
}
