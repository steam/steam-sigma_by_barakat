package comsol.postprocessing;

/**
 * Interface placeholder for future implementation. Present to maintain the link with the COMSOL tree.
 */
public interface GlobalEvaluation {

}
