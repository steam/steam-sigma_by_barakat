package comsol;

import comsol.api.*;
import comsol.constants.MPHC;
import comsol.geometry.DomainGeneral;
import comsol.global.Definitions;
import comsol.global.Parameters;
import comsol.materials.Materials;
import comsol.physics.CLIQ;
import comsol.physics.HeatTransfer;
import comsol.physics.MagneticField;
import config.ConfigSigma;
import model.domains.Domain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import server.SigmaServer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class contains a description of a magnet model to be created in COMSOL
 */
public class MagnetMPHBuilder {
    private static final Logger LOGGER = LogManager.getLogger(MagnetMPHBuilder.class);
    // -----------------------------------------------------------------------------------------------------------------
    // Turn parameters
    // -----------------------------------------------------------------------------------------------------------------
    /**
     * Global index of half-turns used to calculate the voltage to ground.
     */
    public static int indexHtGlobal;
    private final String pathCLib;
    private final String pathOutput;

    private final SigmaServer srv;
    private final String comsolBatchPath;

    /**
     * Constructs a MagnetMPHBuilder object responsible for creation of a magnet in COMSOL
     *
     * @param cfg - sigma config
     * @param srv - input SigmaServer instance
     */
    //ToDo: index HT global is a tragedy, there must be a better way to obtain it.
    public MagnetMPHBuilder(ConfigSigma cfg, SigmaServer srv) {
        this.indexHtGlobal      = 0;
        this.pathCLib           = String.format("%s\\%s", cfg.getExternalCFunLibPath(), MPHC.LABEL_MATLIBRARY_VERSION);
        this.pathOutput         = cfg.getOutputModelPath();
        this.comsolBatchPath    = cfg.getComsolBatchPath();
        this.srv                = srv;
    }

    /**
     * Method prepares a template for a model
     */
    public void prepareModelTemplate() {
        srv.prepareModelTemplate();
    }

    /**
     * Method connects to the SIGMA server
     *
     * @throws IOException thrown in case of I/O problems
     */
    public void connectToServer() throws IOException {
        srv.connect(this.comsolBatchPath);
    }

    /**
     * Method disconnects from the SIGMA server
     */
    public void disconnectFromServer() {
        srv.disconnect();
    }

    /**
     * Method builds a COMSOL model by executing a pipeline of steps reflecting the internal structure of COMSOL.
     *
     * @param domains - input domains to be constructed (coil, copper wedge, iron yoke, air, etc.)
     * @throws IOException - thrown in case of I/O problems with the output file
     */

    public void buildMPH(Domain[] domains) throws IOException {
        long startTime = System.currentTimeMillis();

        LOGGER.info("COMSOL: Initialize model nodes");
        DefinitionsAPI.createComponentNode(srv,MPHC.LABEL_COMP);
        GeometryAPI.createGeometryNode(srv, MPHC.LABEL_GEOM, GeometryAPI.COMPONENT_DIMENSION_2D);
        MagneticFieldsPhysicsAPI.addNode(srv);
        HeatTransferInSolidsPhysicsAPI.addNode(srv, "ht",MPHC.LABEL_GEOM); // changed
        DodePhysicsAPI.addNode(srv);
        GlobalEquationsPhysicsAPI.addNode(srv);

        LOGGER.info("COMSOL: Initialize model global definitions");
        buildGlobalDefinitions(srv);

        LOGGER.info("COMSOL: Initialize model domains");
        buildDomains(srv, domains);

        LOGGER.info("COMSOL: Build CLIQ equations");
        CLIQ.buildCLIQ(srv, domains);                 // Please, don't delete this again!

        LOGGER.info("COMSOL: Build default mesh");
        //MeshAPI.createDefaultMesh(srv);
        MeshAPI.createMappedMeshwithFreeTriangular(srv, domains);

        LOGGER.info("COMSOL: Build model");
        srv.build(pathOutput);

        LOGGER.info("COMSOL: Done!");

        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        LOGGER.info("Elapsed time " + elapsedTime / 1e3 + " [s]");
    }

    /**
     * Method saves a model under provided output path
     *
     * @throws IOException in case of file access problems
     */
    public void save() throws IOException {
        LOGGER.info("COMSOL: Save model");
        srv.save(pathOutput);
    }

    /**
     * Method compiles a magnet model
     */
    /*public void compile() throws IOException {
        LOGGER.info("COMSOL: Compile model");
        srv.compile(pathOutput);
    }*/


    private void buildGlobalDefinitions(SigmaServer srv) {
        Parameters.setDefaultGlobalParameters(srv);
        Definitions.buildCfunLibrary(srv, pathCLib);
    }

    private void buildDomains(SigmaServer srv, Domain[] domains) {
        if (domains != null) {
            for (Domain dom : domains) {
                LOGGER.info(String.format("COMSOL: Build domain %s", dom.getLabel()));
                DomainGeneral.build(srv, dom);
                Materials.build(srv, dom.getMaterial(), String.format("%s_%s_dom", MPHC.LABEL_GEOM, dom.getLabel()));
                MagneticField.build(srv, dom);
                HeatTransfer.build(srv, dom);
            }

            List<String> thSelList = new ArrayList<>();
            for (Domain dom : domains) {
                if (dom.isImplementsTH()) {
                    thSelList.add(dom.getLabel());
                }
            }
            GeometryAPI.createUnionSelectionObject(srv, MPHC.LABEL_PHYSICSTH_SELECTION, thSelList.toArray(new String[thSelList.size()]));
            GeometryAPI.finalizeGeometry(srv, MPHC.LABEL_GEOM, "fin");
            HeatTransferInSolidsPhysicsAPI.setNodeSelection(srv);
        }
    }

    // *****************************************************************************************************************
    // Getters
    // *****************************************************************************************************************

    public String getPathCLib() {
        return pathCLib;
    }

    public String getPathOutput() {
        return pathOutput;
    }

    public SigmaServer getSrv() {
        return srv;
    }


}

//    public void buildMPH(Domain[] domains) throws IOException {
//        long startTime = System.currentTimeMillis();
//
//        LOGGER.info("COMSOL: Initialize model nodes");
//        DefinitionsAPI.createComponentNode(srv, MPHC.LABEL_COMP);
//        GeometryAPI.createGeometryNode(srv);
//        MagneticFieldsPhysicsAPI.addNode(srv);
//        HeatTransferInSolidsPhysicsAPI.addNode(srv);
//        DodePhysicsAPI.addNode(srv);
//        GlobalEquationsPhysicsAPI.addNode(srv);
//
//        LOGGER.info("COMSOL: Initialize model global definitions");
//        buildGlobalDefinitions(srv);
//
//        LOGGER.info("COMSOL: Initialize model domains");
//        buildDomains(srv, domains);
//
//        LOGGER.info("COMSOL: Build CLIQ equations");
//        CLIQ.buildCLIQ(srv, domains);                 // Please don't delete this again!
//
//
//        LOGGER.info("COMSOL: Build default mesh");
//        MeshAPI.createDefaultMesh(srv);
//
//        LOGGER.info("COMSOL: Build model");
//        srv.build(pathOutput);
//
//        LOGGER.info("COMSOL: Done!");
//
//        long stopTime = System.currentTimeMillis();
//        long elapsedTime = stopTime - startTime;
//        LOGGER.info("Elapsed time " + elapsedTime / 1e3 + " [s]");
//    }