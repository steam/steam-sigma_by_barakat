package comsol.geometry;

import comsol.api.GeometryAPI;
import model.domains.database.BoundaryConditionsDomain;
import model.geometry.basic.Area;
import model.geometry.basic.Point;
import server.SigmaServer;

/**
 * Class represents a domain boundary condition
 */
public class DomainBoundaryCondition {

    /**
     * Method builds a domain boundary condition based on the provided definition
     *
     * @param srv - input SigmaServer instance
     * @param bcd - domain boundary condition definition
     */
    public static void build(SigmaServer srv, BoundaryConditionsDomain bcd){
        int xIdx = 0;
        Area aX = (Area) bcd.getElements()[xIdx].getGeometry();
        double[] xBounds = extractBounds(aX.getVertices());
        GeometryAPI.createBoxSelection(srv,bcd.getXBCLabel(),xBounds);

        int yIdx = 1;
        Area aY = (Area) bcd.getElements()[yIdx].getGeometry();
        double[] yBounds = extractBounds(aY.getVertices());
        GeometryAPI.createBoxSelection(srv, bcd.getYBCLabel(),yBounds);
    }


    private static double[] extractBounds(Point[] vertices) {
        double[] xyBounds = {0,0,0,0};
        for (int point_i = 0; point_i < vertices.length; point_i++) {
            xyBounds[0] = Math.min(xyBounds[0], vertices[point_i].getX());
            xyBounds[1] = Math.max(xyBounds[1], vertices[point_i].getX());
            xyBounds[2] = Math.min(xyBounds[2], vertices[point_i].getY());
            xyBounds[3] = Math.max(xyBounds[3], vertices[point_i].getY());
        }
        return xyBounds;
    }
}
