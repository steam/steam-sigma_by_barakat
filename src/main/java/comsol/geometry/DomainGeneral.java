package comsol.geometry;

import comsol.api.GeometryAPI;
import comsol.coil.CoilMPH;
import model.domains.Domain;
import model.domains.database.*;
import model.geometry.basic.HyperLine;
import server.SigmaServer;

/**
 * Class builds a domain in a COMSOL Model
 */
public class DomainGeneral {
    /**
     * Method builds a given type of domain in a magnet model
     *
     * @param srv    - input SigmaServer instance
     * @param domain - input domain
     */
    public static void build(SigmaServer srv, Domain domain) {
        switch (domain.getDomainType()){
            case AIR:
                buildGeometry(srv, domain);
                break;
            case AIRFARFIELD:
                buildGeometry(srv, domain);
                break;
            case BOUNDARY:
                DomainBoundaryCondition.build(srv, (BoundaryConditionsDomain) domain);
                break;
            case COIL:
                CoilMPH.ofCoilDomain((CoilDomain) domain).build(srv);
                break;
            case COILSPLINE:
                break;
            case HOLE:
                buildGeometry(srv, domain);
                break;
            case INSULATION:
                buildGeometry(srv, domain);
                break;
            case IRON:
                buildGeometry(srv, domain);
                break;
            case QUENCHHEATER:
                break;
            case WEDGE:
                buildGeometry(srv, domain);
                break;
        }
    }

    private static void buildGeometry(SigmaServer srv, Domain dom){
        GeometryAPI.createCumulativeSelection(srv, dom.getLabel());

        for (int element_i = 0; element_i < dom.getElements().length; element_i++) {
            String elementLabel = dom.getElements()[element_i].getLabel();
            HyperLine[] elementLines = dom.getElements()[element_i].getGeometry().getHyperLines();

            String[] elementLinesLabels = new String[elementLines.length];
            for (int line_i = 0; line_i < elementLines.length; line_i++) {
                elementLinesLabels[line_i] = elementLabel + "_hl" + (line_i + 1);
                String[] elementLinesCoordExpr = elementLines[line_i].getCoordinatesExpr();
                GeometryAPI.createHyperLine(srv, elementLinesLabels[line_i], elementLinesCoordExpr);
            }
            GeometryAPI.createHyperArea(srv, elementLabel, elementLinesLabels);
            GeometryAPI.contributeTo(srv, elementLabel, dom.getLabel());
        }
    }

}
