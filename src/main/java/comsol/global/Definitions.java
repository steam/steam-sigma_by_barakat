package comsol.global;

import comsol.constants.MPHC;
import comsol.api.GlobalDefinitionsAPI;
import server.SigmaServer;

/**
 * Class responsible for dealing with global definitions in a COMSOL mode
 */
public class Definitions {

    private static final String LABEL_T = "T";
    private static final String LABEL_B = "B";
    private static final String LABEL_I = "I";
    private static final String LABEL_SURFACE = "surf";
    private static final String LABEL_RRR = "RRR";
    private static final String LABEL_RHO = "rho";
    private static final String LABEL_C1 = "C1";
    private static final String LABEL_C2 = "C2";
    private static final String LABEL_T_REF_RRR = "TrefRRR";

    /**
     * Method builds a node of an external C function under global definitions
     *
     * @param srv         - input SigmaServer instance
     * @param cFunLibPath - an absolute path to the C function library
     */
    public static void buildCfunLibrary(SigmaServer srv, String cFunLibPath) {

        //General functions
        buildCfunQuenchState(srv, cFunLibPath);
        buildCfunQHCircuit(srv, cFunLibPath);

        //electrical resistivity
        buildCfunRhoCuCUDI(srv, cFunLibPath);
        buildCfunRhoCuNIST(srv, cFunLibPath);

        //thermal conductivity
        buildCfunKCuNIST(srv, cFunLibPath);
        buildCfunKKapton(srv, cFunLibPath);
        buildCfunKG10(srv, cFunLibPath);
        buildCfunKHe(srv, cFunLibPath);
        buildCfunKSteel(srv, cFunLibPath);

        //heat capacity
        buildCfunCpCu(srv, cFunLibPath);
        buildCfunCpNbTi(srv, cFunLibPath);
        buildCfunCpNb3Sn(srv, cFunLibPath);
        buildCfunCpKapton(srv, cFunLibPath);
        buildCfunCpG10(srv, cFunLibPath);
        buildCfunCpHe(srv, cFunLibPath);
        buildCfunCpSteel(srv, cFunLibPath);

        //critical surfaces
        buildCfunTcsNbTi(srv, cFunLibPath);
        buildCfunIcNbTiGSI(srv, cFunLibPath);
        buildCfunIcNbTiD2(srv, cFunLibPath);
        buildCfunIcNb3SnHiLumi(srv, cFunLibPath);
        buildCfunIcNb3SnFCC(srv, cFunLibPath);
    }

    // *****************************************************************************************************************
    // CFUN General
    // *****************************************************************************************************************
    private static void buildCfunQuenchState(SigmaServer srv, String cFunLibPath) {
        String nodeName = String.format("%s", MPHC.LABEL_CFUN_QUENCHSTATE);

        String[][] functions = {{nodeName, "Var,VarCrit"}};
        String[][] plotArguments = {{"0.9", "1.1"}, {"1", "1"}};
        GlobalDefinitionsAPI.buildNodeExternalFunction(srv, cFunLibPath, nodeName, functions, plotArguments);
    }

    // *****************************************************************************************************************
    // CFUN Kapton
    // *****************************************************************************************************************
    private static void buildCfunCpKapton(SigmaServer srv, String cFunLibPath) {
        String nodeName = String.format("%s", MPHC.LABEL_CFUN_CV_KAPTON);
        String nodeArg = String.format("%s", LABEL_T);

        String[][] functions = {{nodeName, nodeArg}};
        String[][] plotArguments = {{"0", "500"}};
        GlobalDefinitionsAPI.buildNodeExternalFunction(srv, cFunLibPath, nodeName, functions, plotArguments);
    }

    private static void buildCfunKKapton(SigmaServer mph, String cFunLibPath) {
        String nodeName = String.format("%s", MPHC.LABEL_CFUN_K_KAPTON);
        String nodeArg = String.format("%s", LABEL_T);

        String[][] functions = {{nodeName, nodeArg}};
        String[][] plotArguments = {{"0", "500"}};
        GlobalDefinitionsAPI.buildNodeExternalFunction(mph, cFunLibPath, nodeName, functions, plotArguments);
    }

    // *****************************************************************************************************************
    // CFUN G10 Glass Fiber
    // *****************************************************************************************************************
    private static void buildCfunCpG10(SigmaServer srv, String cFunLibPath) {
        String nodeName = String.format("%s", MPHC.LABEL_CFUN_CV_G10);
        String nodeArg = String.format("%s", LABEL_T);

        String[][] functions = {{nodeName, nodeArg}};
        String[][] plotArguments = {{"0", "500"}};
        GlobalDefinitionsAPI.buildNodeExternalFunction(srv, cFunLibPath, nodeName, functions, plotArguments);
    }

    private static void buildCfunKG10(SigmaServer mph, String cFunLibPath) {
        String nodeName = String.format("%s", MPHC.LABEL_CFUN_K_G10);
        String nodeArg = String.format("%s", LABEL_T);

        String[][] functions = {{nodeName, nodeArg}};
        String[][] plotArguments = {{"0", "500"}};
        GlobalDefinitionsAPI.buildNodeExternalFunction(mph, cFunLibPath, nodeName, functions, plotArguments);
    }

    // *****************************************************************************************************************
    // CFUN Copper
    // *****************************************************************************************************************
    private static void buildCfunRhoCuCUDI(SigmaServer srv, String cFunLibPath) {
        String nodeName = String.format("%s", MPHC.LABEL_CFUN_RHO_CU_CUDI);
        String nodeArg = String.format("%s,%s,%s,%s", LABEL_T, LABEL_B, LABEL_RRR, LABEL_T_REF_RRR);

        String[][] functions = {{nodeName, nodeArg}};
        String[][] plotArguments = {{"0", "500"}, {"0", "0"}, {"150", "150"}, {"295", "295"}};
        GlobalDefinitionsAPI.buildNodeExternalFunction(srv, cFunLibPath, nodeName, functions, plotArguments);
    }

    private static void buildCfunRhoCuNIST(SigmaServer srv, String cFunLibPath) {
        String nodeName = String.format("%s", MPHC.LABEL_CFUN_RHO_CU_NIST);
        String nodeArg = String.format("%s,%s,%s,%s", LABEL_T, LABEL_B, LABEL_RRR, LABEL_T_REF_RRR);

        String[][] functions = {{nodeName, nodeArg}};
        String[][] plotArguments = {{"0", "500"}, {"0", "0"}, {"150", "150"}, {"295", "295"}};
        GlobalDefinitionsAPI.buildNodeExternalFunction(srv, cFunLibPath, nodeName, functions, plotArguments);
    }

    private static void buildCfunKCuNIST(SigmaServer srv, String cFunLibPath) {
        String nodeName = String.format("%s", MPHC.LABEL_CFUN_K_CU_NIST);
        String nodeArg = String.format("%s,%s,%s,%s", LABEL_T, LABEL_RRR, LABEL_RHO, LABEL_RHO);

        String[][] functions = {{nodeName, nodeArg}};
        String[][] plotArguments = {{"1", "500"}, {"100", "100"}, {"5e-10", "5e-10"}, {"5e-10", "5e-10"}};
        GlobalDefinitionsAPI.buildNodeExternalFunction(srv, cFunLibPath, nodeName, functions, plotArguments);
    }

    private static void buildCfunCpCu(SigmaServer srv, String cFunLibPath) {
        String nodeName = String.format("%s", MPHC.LABEL_CFUN_CV_CU);
        String nodeArg = String.format("%s", LABEL_T);

        String[][] functions = {{nodeName, nodeArg}};
        String[][] plotArguments = {{"0", "500"}};
        GlobalDefinitionsAPI.buildNodeExternalFunction(srv, cFunLibPath, nodeName, functions, plotArguments);
    }

    // *****************************************************************************************************************
    // CFUN Nb-Ti
    // *****************************************************************************************************************
    private static void buildCfunCpNbTi(SigmaServer srv, String cFunLibPath) {
        String nodeName = String.format("%s", MPHC.LABEL_CFUN_CV_NBTI);
        String nodeArg = String.format("%s,%s,%s,%s,%s", LABEL_T, LABEL_B, LABEL_I, LABEL_C1, LABEL_C2);

        String C1_val = "65821.9";
        String C2_val = "-5042.6";
        String[][] functions = {{nodeName, nodeArg}};
        String[][] plotArguments = {{"0", "500"}, {"0", "0"}, {"0", "0"}, {C1_val, C1_val}, {C2_val, C2_val}};
        GlobalDefinitionsAPI.buildNodeExternalFunction(srv, cFunLibPath, nodeName, functions, plotArguments);
    }

    private static void buildCfunTcsNbTi(SigmaServer srv, String cFunLibPath) {
        String nodeName = String.format("%s", MPHC.LABEL_CFUN_TCS_NBTI);
        String nodeArg = String.format("%s,%s,%s,%s", LABEL_B, LABEL_I, LABEL_C1, LABEL_C2);

        String C1_val = "65821.9";
        String C2_val = "-5042.6";
        String[][] functions = {{nodeName, nodeArg}};
        String[][] plotArguments = {{"0", "15"}, {"0", "0"}, {C1_val, C1_val}, {C2_val, C2_val}};
        GlobalDefinitionsAPI.buildNodeExternalFunction(srv, cFunLibPath, nodeName, functions, plotArguments);
    }

    private static void buildCfunIcNbTiGSI(SigmaServer srv, String cFunLibPath) {
        String nodeName = String.format("%s", MPHC.LABEL_CFUN_ICRIT_NBTI_GSI);
        String nodeArg = String.format("%s,%s,%s", LABEL_T, LABEL_B, LABEL_SURFACE);

        String[][] functions = {{nodeName, nodeArg}};
        String[][] plotArguments = {{"0", "12"}, {"0", "15"}, {"1e-5", "1e-5"}};
        GlobalDefinitionsAPI.buildNodeExternalFunction(srv, cFunLibPath, nodeName, functions, plotArguments);
    }

    private static void buildCfunIcNbTiD2(SigmaServer srv, String cFunLibPath) {
        String nodeName = String.format("%s", MPHC.LABEL_CFUN_ICRIT_NBTI_D2);
        String nodeArg = String.format("%s,%s,%s", LABEL_T, LABEL_B, LABEL_SURFACE);

        String[][] functions = {{nodeName, nodeArg}};
        String[][] plotArguments = {{"0", "12"}, {"0", "15"}, {"1e-5", "1e-5"}};
        GlobalDefinitionsAPI.buildNodeExternalFunction(srv, cFunLibPath, nodeName, functions, plotArguments);
    }

    // *****************************************************************************************************************
    // CFUN Nb3Sn
    // *****************************************************************************************************************
    private static void buildCfunCpNb3Sn(SigmaServer srv, String cFunLibPath) {
        String nodeName = String.format("%s", MPHC.LABEL_CFUN_CV_NB3SN);
        String nodeArg = String.format("%s,%s", LABEL_T, LABEL_B);

        String[][] functions = {{nodeName, nodeArg}};
        String[][] plotArguments = {{"1", "20"}, {"0", "30"}};
        GlobalDefinitionsAPI.buildNodeExternalFunction(srv, cFunLibPath, nodeName, functions, plotArguments);
    }

    private static void buildCfunIcNb3SnHiLumi(SigmaServer srv, String cFunLibPath) {
        String nodeName = String.format("%s", MPHC.LABEL_CFUN_ICRIT_NB3SN_HILUMI);
        String nodeArg = String.format("%s,%s,%s", LABEL_T, LABEL_B, LABEL_SURFACE);

        String[][] functions = {{nodeName, nodeArg}};
        String[][] plotArguments = {{"0", "20"}, {"0", "30"}, {"1e-5", "1e-5"}};
        GlobalDefinitionsAPI.buildNodeExternalFunction(srv, cFunLibPath, nodeName, functions, plotArguments);
    }

    private static void buildCfunIcNb3SnFCC(SigmaServer srv, String cFunLibPath) {
        String nodeName = String.format("%s", MPHC.LABEL_CFUN_ICRIT_NB3SN_FCC);
        String nodeArg = String.format("%s,%s,%s", LABEL_T, LABEL_B, LABEL_SURFACE);

        String[][] functions = {{nodeName, nodeArg}};
        String[][] plotArguments = {{"0", "20"}, {"0", "30"}, {"1e-5", "1e-5"}};
        GlobalDefinitionsAPI.buildNodeExternalFunction(srv, cFunLibPath, nodeName, functions, plotArguments);
    }

    // *****************************************************************************************************************
    // CFUN Stainless Steel
    // *****************************************************************************************************************
    private static void buildCfunCpSteel(SigmaServer srv, String cFunLibPath) {
        String nodeName = String.format("%s", MPHC.LABEL_CFUN_CV_STEEL);
        String nodeArg = String.format("%s", LABEL_T);

        String[][] functions = {{nodeName, nodeArg}};
        String[][] plotArguments = {{"1", "300"}};
        GlobalDefinitionsAPI.buildNodeExternalFunction(srv, cFunLibPath, nodeName, functions, plotArguments);
    }

    private static void buildCfunKSteel(SigmaServer srv, String cFunLibPath) {
        String nodeName = String.format("%s", MPHC.LABEL_CFUN_K_STEEL);
        String nodeArg = String.format("%s", LABEL_T);

        String[][] functions = {{nodeName, nodeArg}};
        String[][] plotArguments = {{"1", "300"}};
        GlobalDefinitionsAPI.buildNodeExternalFunction(srv, cFunLibPath, nodeName, functions, plotArguments);
    }

    // *****************************************************************************************************************
    // CFUN Helium
    // *****************************************************************************************************************
    private static void buildCfunCpHe(SigmaServer srv, String cFunLibPath) {
        String nodeName = String.format("%s", MPHC.LABEL_CFUN_CV_HE);
        String nodeArg = String.format("%s", LABEL_T);

        String[][] functions = {{nodeName, nodeArg}};
        String[][] plotArguments = {{"0", "10"}};
        GlobalDefinitionsAPI.buildNodeExternalFunction(srv, cFunLibPath, nodeName, functions, plotArguments);
    }

    private static void buildCfunKHe(SigmaServer srv, String cFunLibPath) {
        String nodeName = String.format("%s", MPHC.LABEL_CFUN_K_HE);
        String nodeArg = String.format("%s,%s", LABEL_T, MPHC.LABEL_PHYSICSTH_T_EXT);

        String[][] functions = {{nodeName, nodeArg}};
        String[][] plotArguments = {{"1", "300"}, {"1.9", "1.9"}};
        GlobalDefinitionsAPI.buildNodeExternalFunction(srv, cFunLibPath, nodeName, functions, plotArguments);
    }

    // *****************************************************************************************************************
    // CFUN Quench Heaters
    // *****************************************************************************************************************
    private static void buildCfunQHCircuit(SigmaServer srv, String cFunLibPath) {
        String nodeName = String.format("%s", MPHC.LABEL_CFUN_QH);
        String nodeArg = String.format("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s", "t", "t_on", "LABEL_T", "U0", "C", "R_warm", "w_QH", "h_QH", "l_QH", "mode");

        String[][] functions = {{nodeName, nodeArg}};
        String[][] plotArguments = {{"0", "1.5"}, {"1", "1"}, {"1.9", "1.9"}, {"450", "450"}, {"7.05e-3", "7.05e-3"},
                {"0.1", "0.1"}, {"15e-3", "15e-3"}, {"25e-6", "25e-6"}, {"14.1", "14.1"}, {"1", "1"}};
        GlobalDefinitionsAPI.buildNodeExternalFunction(srv, cFunLibPath, nodeName, functions, plotArguments);
    }
}