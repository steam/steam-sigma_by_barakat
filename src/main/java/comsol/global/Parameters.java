package comsol.global;

import comsol.QuenchHeaterMPHBuilder;
import comsol.api.GlobalDefinitionsAPI;
import comsol.constants.MPHC;
import server.SigmaServer;

/**
 * Class responsible for dealing with global parameters in a COMSOL model
 */
public class Parameters {

    /**
     * Method sets default global parameters of a magnet model
     *
     * @param srv - input SigmaServer instance
     */
    public static void setDefaultGlobalParameters(SigmaServer srv) {
        int defaultValue = 0;

        // The following lines of '.setParameter' take the following input parameters:
        // .setParameter(SigmaServer srv, label for parameter name, value for parameter, string description of parameter)

        //Flags
        //GlobalDefinitionsAPI.setParameter(srv, MPHC.LABEL_FLAG_ISCC_CROSSOVER, String.valueOf(defaultValue),"");
        //GlobalDefinitionsAPI.setParameter(srv, MPHC.LABEL_FLAG_ISCC_ADJW, String.valueOf(defaultValue),"");
        //GlobalDefinitionsAPI.setParameter(srv, MPHC.LABEL_FLAG_ISCC_ADJN, String.valueOf(defaultValue),"");
        GlobalDefinitionsAPI.setParameter(srv, MPHC.LABEL_FLAG_IFCC, String.valueOf(defaultValue), "");
        GlobalDefinitionsAPI.setParameter(srv, MPHC.LABEL_FLAG_MPERS, String.valueOf(defaultValue), "");

        GlobalDefinitionsAPI.setParameter(srv, MPHC.LABEL_FLAG_QUENCH_ALL, String.valueOf(defaultValue), "");
        GlobalDefinitionsAPI.setParameter(srv, MPHC.LABEL_FLAG_QUENCH_OFF, String.valueOf(defaultValue), "");
        GlobalDefinitionsAPI.setParameter(srv, MPHC.LABEL_PARAM_QUENCH_TIME, String.valueOf(defaultValue).concat(MPHC.UNIT_TIME), "");
        GlobalDefinitionsAPI.setParameter(srv, MPHC.LABEL_MAGNETIC_LENGTH, String.valueOf(1).concat(MPHC.UNIT_LENGTH), "");
    }

    /**
     * Method sets global parameters provided as arrays of (name, value) pair
     *
     * @param srv            - input SigmaServer instance
     * @param parameterName  - an array of input names
     * @param parameterValue - an array of input values
     */
    public static void setGlobalParameters(SigmaServer srv, String[] parameterName, String[] parameterValue, String parameterDescription) {
        for (int i = 0; i < parameterName.length; i++) {
            GlobalDefinitionsAPI.setParameter(srv, parameterName[i], parameterValue[i], parameterDescription);
        }
    }
}
