package comsol.constants;

/**
 * Class with all constants of an MPH COMSOL model
 */
public class MPHC {

    private MPHC() {
    }

    // -----------------------------------------------------------------------------------------------------------------
    // global COMSOL
    // -----------------------------------------------------------------------------------------------------------------
    public static final String LABEL_COMP = "MagnetComp";
    public static final String LABEL_COMP_QH = "QHComp";
    public static final String LABEL_GEOM = "geom1";
    public static final String LABEL_GEOM_QH = "geom2";
    public static final String LABEL_VIEW = "view1";
    public static final String LABEL_EPS = "eps";

    // -----------------------------------------------------------------------------------------------------------------
    // Mesh labels COMSOL
    // -----------------------------------------------------------------------------------------------------------------
    public static final String LABEL_MESH = "mesh1";
    public static final String LABEL_MAPPED_MESH = "map1";
    public static final String LABEL_TRIANGULAR_MESH = "ftri1";
    public static final String PARAM_N_MESH_ELEMS_WIDE = "4";
    public static final String PARAM_N_MESH_ELEMS_NARROW = "2";

    // -----------------------------------------------------------------------------------------------------------------
    // global markers
    // -----------------------------------------------------------------------------------------------------------------
    public static final String MARKER_FLAG = "FLAG";
    public static final String MARKER_CFUN = "CFUN";
    public static final String MARKER_JUNCTIONBOX = "JunctionBox";
    public static final String MARKER_PHYSICSENGINE = "PE";
    public static final String MARKER_CLIQ = "cliq";
    public static final String MARKER_PARAM = "PARAM";
    public static final String MARKER_CIRCUITPARAMS = "CircuitParams";

    public static final String MARKER_CABLE = "C";
    public static final String MARKER_POLE = "P";
    public static final Object MARKER_WINDING = "W";
    public static final Object MARKER_BLOCK = "B";
    public static final Object MARKER_HALFTURN = "HT";
    public static final String MARKER_WIDEBEZIER = "WB";
    public static final String MARKER_NARROWBEZIER = "NB";
    public static final String MARKER_SELECTION = "SEL";

    public static final String MARKER_I_WINDING = "Iw";

    public static final String MARKER_POLARITY_POSITIVE = "_Positive";                                                  // Added by Barakat
    public static final String MARKER_POLARITY_NEGATIVE = "_Negative";                                                  // Added by Barakat

    // -----------------------------------------------------------------------------------------------------------------
    // units
    // -----------------------------------------------------------------------------------------------------------------
    public static final String UNIT_TIME = "[s]";
    public static final String UNIT_LENGTH = "[m]";

    // -----------------------------------------------------------------------------------------------------------------
    // external C-functions
    // -----------------------------------------------------------------------------------------------------------------
    public static final String LABEL_MATLIBRARY_VERSION = "V0.1";
    //electrical resistivity
    public static final String LABEL_CFUN_RHO_CU_CUDI = MARKER_CFUN + "_rhoCuCUDI";
    public static final String LABEL_CFUN_RHO_CU_NIST = MARKER_CFUN + "_rhoCuNIST";
    //thermal diffusivity
    public static final String LABEL_CFUN_K_CU_NIST = MARKER_CFUN + "_kCuNIST";
    public static final String LABEL_CFUN_K_KAPTON = MARKER_CFUN + "_kKapton";
    public static final String LABEL_CFUN_K_G10 = MARKER_CFUN + "_kG10";
    public static final String LABEL_CFUN_K_HE = MARKER_CFUN + "_kHe";
    public static final String LABEL_CFUN_K_STEEL = MARKER_CFUN + "_kSteel";
    //thermal capacitance
    public static final String LABEL_CFUN_CV_CU = MARKER_CFUN + "_CvCu";
    public static final String LABEL_CFUN_CV_NBTI = MARKER_CFUN + "_CvNbTi";
    public static final String LABEL_CFUN_CV_NB3SN = MARKER_CFUN + "_CvNb3SnNIST";
    public static final String LABEL_CFUN_CV_KAPTON = MARKER_CFUN + "_CvKapton";
    public static final String LABEL_CFUN_CV_G10 = MARKER_CFUN + "_CvG10";
    public static final String LABEL_CFUN_CV_HE = MARKER_CFUN + "_CvHe";
    public static final String LABEL_CFUN_CV_STEEL = MARKER_CFUN + "_CvSteel";
    //critical surface fits
    public static final String LABEL_CFUN_TCS_NBTI = MARKER_CFUN + "_TcsNbTi";
    public static final String LABEL_CFUN_ICRIT_NB3SN_HILUMI = MARKER_CFUN + "_IcNb3SnHiLumi";
    public static final String LABEL_CFUN_ICRIT_NB3SN_FCC = MARKER_CFUN + "_IcNb3SnFCC";
    public static final String LABEL_CFUN_ICRIT_NBTI_GSI = MARKER_CFUN + "_IcNbTiGSI";
    public static final String LABEL_CFUN_ICRIT_NBTI_D2 = MARKER_CFUN + "_IcNbTiD2";
    //other functions
    public static final String LABEL_CFUN_TAUIFCC = MARKER_CFUN + "_tauIFCC";
    public static final String LABEL_CFUN_QUENCHSTATE = MARKER_CFUN + "_quenchState";
    public static final String LABEL_CFUN_QH = MARKER_CFUN + "_QHCircuit";

    // -----------------------------------------------------------------------------------
    // global parameters
    // -----------------------------------------------------------------------------------
    //eqMagnetization variables
    public static final String LABEL_FLAG_ISCC_CROSSOVER = MARKER_FLAG + "_iscc_crossover";
    //public static final String LABEL_FLAG_ISCC_ADJW   = MARKER_FLAG + "_iscc_adjw";                                   // Removed by Barakat: Not needed for restructured model
    //public static final String LABEL_FLAG_ISCC_ADJN   = MARKER_FLAG + "_iscc_adjn";                                   // Removed by Barakat: Not needed for restructured model
    public static final String LABEL_FLAG_IFCC = MARKER_FLAG + "_ifcc";
    public static final String LABEL_FLAG_MPERS = MARKER_FLAG + "_M_pers";
    //quench variables
    public static final String LABEL_FLAG_QUENCH_ALL = MARKER_FLAG + "_quench_all";
    public static final String LABEL_FLAG_QUENCH_OFF = MARKER_FLAG + "_quench_off";
    public static final String LABEL_PARAM_QUENCH_TIME = MARKER_PARAM + "_time_quench";
    public static final String LABEL_MAGNETIC_LENGTH = "magLength";

    // -----------------------------------------------------------------------------------------------------------------
    // Geometry parameters
    // -----------------------------------------------------------------------------------------------------------------
    public static final String PARAM_REPAIRTOL_REL = "5.0e-5";
    //public static final String PARAM_REPAIRTOL_REL     = "12e-5";                                                     // set for D2 magnet
    public static final String PARAM_REPAIRTOL_ABS = "5.0e-5";
    public static final String PARAM_REPAIRTOL_HYPERLINE = "1.0e-12";
    public static final String PARAM_KNOTS_IN_HYPERLINE = "2000";

    // -----------------------------------------------------------------------------------------------------------------
    // materials coupling parameters
    // -----------------------------------------------------------------------------------------------------------------
    public static final String LABEL_SURFACE_AVERAGE = "avgSurf";
    public static final String LABEL_SURFACE_INTEGRATION = "intSurf";
    public static final String LABEL_SURFACE_MINIMUM = "minSurf";
    public static final String LABEL_LINEWBEZIER_AVERAGE = "avgLineW";
    public static final String LABEL_LINENBEZIER_AVERAGE = "avgLineN";

    // -----------------------------------------------------------------------------------------------------------------
    // Cable parameters
    // -----------------------------------------------------------------------------------------------------------------
    //Insulation
    public static final String LABEL_CABLE_THICHNESS_INSULATION_NARROWEDGE = "w_insulNarrow_" + MARKER_CABLE;
    public static final String LABEL_CABLE_THICHNESS_INSULATION_WIDEEDGE = "w_insulWide_" + MARKER_CABLE;
    //Filament
    public static final String LABEL_CABLE_DIAMETER_FILAMENT = "d_fil_" + MARKER_CABLE;
    //Strand
    public static final String LABEL_CABLE_DIAMETER_STRAND = "d_strand_" + MARKER_CABLE;
    public static final String LABEL_CABLE_FRACTION_CU = "frac_cu_" + MARKER_CABLE;
    public static final String LABEL_CABLE_FRACTION_SC = "frac_sc_" + MARKER_CABLE;
    public static final String LABEL_CABLE_FRACTION_HE = "frac_he_" + MARKER_CABLE;
    public static final String LABEL_CABLE_FILLFACTOR_INNERVOIDS = "frac_fill_inVoids_" + MARKER_CABLE;
    public static final String LABEL_CABLE_FILLFACTOR_OUTERVOIDS = "frac_fill_outVoids_" + MARKER_CABLE;
    public static final String LABEL_CABLE_RRR = "RRR_" + MARKER_CABLE;
    public static final String LABEL_CABLE_T_REF_RRR = "Tup_RRR_" + MARKER_CABLE;
    //Transient
    public static final String LABEL_CABLE_RC = "Rc_" + MARKER_CABLE;
    public static final String LABEL_CABLE_RA = "Ra_" + MARKER_CABLE;
    public static final String LABEL_CABLE_TWISTPITCH_FILAMENT = "l_tpFil_" + MARKER_CABLE;
    public static final String LABEL_CABLE_FRHO_EFF = "fRhoEff_" + MARKER_CABLE;
    //Cable
    public static final String LABEL_CABLE_WIDTH_BARE = "w_bare_" + MARKER_CABLE;
    public static final String LABEL_CABLE_HEIGHT_INNER_BARE = "h_inBare_" + MARKER_CABLE;
    public static final String LABEL_CABLE_HEIGHT_OUTER_BARE = "h_outBare_" + MARKER_CABLE;
    public static final String LABEL_CABLE_HEIGHT_AVERAGE_BARE = "h_avgBare_" + MARKER_CABLE;
    public static final String LABEL_CABLE_WIDTH_CORE = "w_core_" + MARKER_CABLE;
    public static final String LABEL_CABLE_HEIGHT_CORE = "h_core_" + MARKER_CABLE;
    public static final String LABEL_CABLE_NUMBER_STRANDS = "num_strands_" + MARKER_CABLE;
    public static final String LABEL_CABLE_NUMBER_LAYERSOFSTRANDS = "num_layersOfStrands_" + MARKER_CABLE;
    public static final String LABEL_CABLE_NUMBER_STRANDSPERLAYER = "num_strandsPerLayer_" + MARKER_CABLE;
    public static final String LABEL_CABLE_SURFACE_BARE = "surf_bare_" + MARKER_CABLE;
    public static final String LABEL_CABLE_SURFACE_CONDUCTOR = "surf_cond_" + MARKER_CABLE;
    public static final String LABEL_CABLE_SURFACE_CORE = "surf_core_" + MARKER_CABLE;
    public static final String LABEL_CABLE_SURFACE_INSULATION = "surf_ins_" + MARKER_CABLE;
    public static final String LABEL_CABLE_SURFACE_VOIDS = "surf_voids_" + MARKER_CABLE;
    public static final String LABEL_CABLE_SURFACE_INNERVOIDS = "surf_inVoids_" + MARKER_CABLE;
    public static final String LABEL_CABLE_SURFACE_OUTERVOIDS = "surf_outVoids_" + MARKER_CABLE;
    public static final String LABEL_CABLE_TWISTPITCH_STRAND = "l_tpStrand_" + MARKER_CABLE;
    public static final String LABEL_CABLE_THETA_TWISTPITCH_STRAND = "theta_lTpStrand_" + MARKER_CABLE;
    public static final String LABEL_CABLE_C1 = "C1_" + MARKER_CABLE;
    public static final String LABEL_CABLE_C2 = "C2_" + MARKER_CABLE;
    public static final String LABEL_CABLE_TAU_ISCC_CROSSOVER = "tau_isccxover_" + MARKER_CABLE;
    public static final String LABEL_CABLE_TAU_ISCC_ADJW = "tau_isccadjw_" + MARKER_CABLE;
    public static final String LABEL_CABLE_TAU_ISCC_ADJN = "tau_isccadjn_" + MARKER_CABLE;
    public static final String LABEL_CABLE_TAU_IFCC = "tau_ifcc_" + MARKER_CABLE;
    public static final String LABEL_CABLE_DEGRADATION = "degradation_" + MARKER_CABLE;
    public static final String LABEL_CABLE_KEYSTONE = "keystone_" + MARKER_CABLE;
    public static final String LABEL_CABLE_RHO_STABILIZER = "rho_" + MARKER_CABLE;
    public static final String LABEL_CABLE_RHO_STABILIZER_ZEROB = "rhoZeroB_" + MARKER_CABLE;
    public static final String LABEL_CABLE_I_CRITICAL = "Ic_" + MARKER_CABLE;

    public static final String LABEL_CABLE_CV_CU_MAT = "cv_cuMat_" + MARKER_CABLE;
    public static final String LABEL_CABLE_CV_SC_MAT = "cv_scMat_" + MARKER_CABLE;
    public static final String LABEL_CABLE_CV_HE_MAT = "cv_heMat_" + MARKER_CABLE;
    public static final String LABEL_CABLE_CV_CORE_MAT = "cv_coreMat_" + MARKER_CABLE;
    public static final String LABEL_CABLE_CV_INSULATION_MAT = "cv_insMat_" + MARKER_CABLE;
    public static final String LABEL_CABLE_CV_INNERVOIDS_MAT = "cv_inVoidsMat_" + MARKER_CABLE;
    public static final String LABEL_CABLE_CV_OUTERVOIDS_MAT = "cv_outVoidsMat_" + MARKER_CABLE;
    public static final String LABEL_CABLE_CV_CONDUCTOR = "Cv_conductor_" + MARKER_CABLE;
    public static final String LABEL_CABLE_CV_CORE = "Cv_core_" + MARKER_CABLE;
    public static final String LABEL_CABLE_CV_VOIDS = "Cv_voids_" + MARKER_CABLE;
    public static final String LABEL_CABLE_CV_INSULATION = "Cv_ins_" + MARKER_CABLE;

    // -----------------------------------------------------------------------------------------------------------------
    // Physics engine parameters
    // -----------------------------------------------------------------------------------------------------------------
    public static final String LABEL_PHYSICSENGINE = "physicsEngine";
    public static final String LABEL_JS = "Js_" + MARKER_PHYSICSENGINE;
    public static final String LABEL_TAU = "tau_" + MARKER_PHYSICSENGINE;
    public static final String LABEL_K_SURFACE = "k_surf_" + MARKER_PHYSICSENGINE;
    //public static final String LABEL_K_TWISTPITCH     = "k_tp_" + MARKER_PHYSICSENGINE;
    public static final String LABEL_MX_IFCC = "M_ifccx_" + MARKER_PHYSICSENGINE;
    public static final String LABEL_MY_IFCC = "M_ifccy_" + MARKER_PHYSICSENGINE;
    //public static final String LABEL_M_ISCC_CROSSOVER = "M_isccxover_" + MARKER_PHYSICSENGINE;                        // Removed by Barakat: Not needed for restructured model
    //public static final String LABEL_M_ISCC_ADJW      = "M_isccadjw_" + MARKER_PHYSICSENGINE;                         // Removed by Barakat: Not needed for restructured model
    //public static final String LABEL_M_ISCC_ADJN      = "M_isccadjn_" + MARKER_PHYSICSENGINE;                         // Removed by Barakat: Not needed for restructured model
    //public static final String LABEL_MX_ISCC          = "M_isccx_" + MARKER_PHYSICSENGINE;                            // Removed by Barakat: Not needed for restructured model
    //public static final String LABEL_MY_ISCC          = "M_isccy_" + MARKER_PHYSICSENGINE;                            // Removed by Barakat: Not needed for restructured model
    public static final String LABEL_MX_PERS = "M_persx_" + MARKER_PHYSICSENGINE;
    public static final String LABEL_MY_PERS = "M_persy_" + MARKER_PHYSICSENGINE;
    public static final String LABEL_MX = "M_eqx_" + MARKER_PHYSICSENGINE;
    public static final String LABEL_MY = "M_eqy_" + MARKER_PHYSICSENGINE;
    public static final String LABEL_PHI = "phi_" + MARKER_PHYSICSENGINE;
    public static final String LABEL_Q_IFCC = "Q_ifcc_" + MARKER_PHYSICSENGINE;

    //public static final String LABEL_Q_ISCC           = "Q_iscc_" + MARKER_PHYSICSENGINE;                             // Removed by Barakat: Not needed for restructured model
    public static final String LABEL_QUENCHSTATE = "quenchState_" + MARKER_PHYSICSENGINE;
    public static final String LABEL_R = "R_" + MARKER_PHYSICSENGINE;
    public static final String LABEL_Q_OHM = "Q_ohm_" + MARKER_PHYSICSENGINE;
    public static final String LABEL_Q_TOT = "Q_tot_" + MARKER_PHYSICSENGINE;
    public static final String LABEL_V_DELTA = "V_delta_" + MARKER_PHYSICSENGINE;
    public static final String LABEL_DAMP_FACTOR_CC = "dampFactorCC_" + MPHC.MARKER_PHYSICSENGINE;
    public static final String LABEL_TAU_CC = "tauCC_" + MARKER_PHYSICSENGINE;

    public static final String LABEL_HT_INDEX = "index_" + MARKER_HALFTURN;
    public static final String LABEL_HT_RATIO = "ratio_" + MARKER_HALFTURN;
    public static final String LABEL_HT_ALPHA = "alpha_" + MARKER_HALFTURN;
    public static final String LABEL_HT_JZ_VERSOR = "Jz_versor_" + MARKER_HALFTURN;
    public static final String LABEL_HT_SURFACE = "surf_" + MARKER_HALFTURN;
    public static final String LABEL_HT_I_SOURCE = "Is_" + MARKER_HALFTURN;
    public static final String LABEL_HT_BAVG_WAXIS = "B_avgw_" + MARKER_HALFTURN;
    public static final String LABEL_HT_BAVG_NAXIS = "B_avgn_" + MARKER_HALFTURN;
    public static final String LABEL_HT_RHO = "rho_" + MARKER_HALFTURN;
    public static final String LABEL_HT_CV = "cv_" + MARKER_HALFTURN;
    public static final String LABEL_HT_I_CRITICAL = "Ic_" + MARKER_HALFTURN;
    //public static final String LABEL_V_GROUND_HALFTURN= "V_gnd_halfturn";                                             // Removed by Barakat: Not needed for restructured model
    //public static final String LABEL_V_DELTA_HALFTURN = "V_delta_halfturn";                                           // Removed by Barakat: Not needed for restructured model

    // -----------------------------------------------------------------------------------------------------------------
    // CLIQ parameters
    // -----------------------------------------------------------------------------------------------------------------
    public static final String LABEL_CLIQ_CURRENT_EXT = "I";
    public static final String LABEL_CLIQ_CURRENT_WITH_CLIQ = MPHC.LABEL_CLIQ_CURRENT_EXT + "+" + MPHC.LABEL_CLIQ_CURRENT;  // Added by Barakat. Easier to read!
    public static final String LABEL_CLIQ_CAPASITOR = "C_" + MPHC.MARKER_CLIQ;
    public static final String LABEL_CLIQ_RESISTANCE = "R_" + MPHC.MARKER_CLIQ;
    public static final String LABEL_CLIQ_VOLTAGE = "V_" + MPHC.MARKER_CLIQ;
    public static final String LABEL_CLIQ_CURRENT = "I_" + MPHC.MARKER_CLIQ;
    public static final String LABEL_CLIQ_INDUCTANCE = "L_" + MPHC.MARKER_CLIQ;
    public static final String LABEL_CLIQ_INITIAL = "_0";
    public static final String LABEL_CLIQ_VOLTAGE_INITIAL = "V_" + MPHC.MARKER_CLIQ + LABEL_CLIQ_INITIAL;
    public static final String LABEL_CLIQ_CURRENT_INITIAL = "I_" + MPHC.MARKER_CLIQ + LABEL_CLIQ_INITIAL;
    public static final String LABEL_CLIQ_CURRENT_EXT_INITIAL = LABEL_CLIQ_CURRENT_EXT + LABEL_CLIQ_INITIAL;
    public static final String LABEL_CLIQ_RCROW = "Rcrow";
    public static final String LABEL_CLIQ_LCIR = "Lcir";
    public static final String LABEL_CLIQ_SYMFACTOR = "symFactor";
    public static final String LABEL_CLIQ_SWITCH = "withCLIQ";

    // -----------------------------------------------------------------------------------------------------------------
    // Physics
    // -----------------------------------------------------------------------------------------------------------------
    //Independent variables
    public static final String LABEL_TIME = "t";

    //Magnetic fields
    public static final String LABEL_PHYSICSMF = "mf";
    public static final String LABEL_MU0 = "mu0_const";
    public static final String LABEL_PI = "pi";
    public static final String LABEL_PHYSICSMF_BX = MPHC.LABEL_PHYSICSMF + ".Bx";
    public static final String LABEL_PHYSICSMF_BY = MPHC.LABEL_PHYSICSMF + ".By";
    public static final String LABEL_PHYSICSMF_B = MPHC.LABEL_PHYSICSMF + ".normB";
    public static final String LABEL_PHYSICSMF_DBXDT = "d(" + MPHC.LABEL_PHYSICSMF + ".Bx" + "," + MPHC.LABEL_TIME + ")";
    public static final String LABEL_PHYSICSMF_DBYDT = "d(" + MPHC.LABEL_PHYSICSMF + ".By" + "," + MPHC.LABEL_TIME + ")";

    //Thermal
    public static final String LABEL_PHYSICSTH = "ht";
    public static final String LABEL_PHYSICSTH_T = "T";
    public static final String LABEL_PHYSICSTH_T_EXT = "Text";
    public static final String LABEL_PHYSICSTH_SELECTION = "thDomSel";
    public static final String VALUE_T_OPERATIONAL = "1.9[K]";

    //Dode
    public static final String LABEL_PHYSICSDODE_DODE = "dode";

    //Global Equations
    public static final String LABEL_PHYSICSGE_GE = "ge";
    public static final String LABEL_PHYSICSGE_ODE_CLIQ = "ODE_cliq";
    public static final String LABEL_PHYSICSGE_ODE_KVL = "ODE_kvl";

    // -----------------------------------------------------------------------------------------------------------------
    // Created by Barakat:
    //      New labels for variable groups, selections and integration operators relevant to Quench Heaters
    //      Location here is temporary, as to keep overview of the new labels:
    // -----------------------------------------------------------------------------------------------------------------

    // Additional coil parameter labels:
    public static final String LABEL_VOLTAGES = "Voltages";                                                             // Label for 'Voltages'
    public static final String LABEL_JZ_VERSOR_HT_PLUS = "Jz_versor_" + MARKER_HALFTURN + MARKER_POLARITY_POSITIVE;     // Label for 'Jz_versor_HT_plus'
    public static final String LABEL_JZ_VERSOR_HT_MINUS = "Jz_versor_" + MARKER_HALFTURN + MARKER_POLARITY_NEGATIVE;    // Label for 'Jz_versor_HT_minus'
    public static final String LABEL_JZ_VERSOR_HT = "Jz_versor_" + MARKER_HALFTURN;                                     // Label for 'Jz_versor_HT'
    public static final String LABEL_PHYSICSTH_JZ_POS_SELECTION = "Jz_Pos_Selection";                                     // Label for selection of domains for 'Jz_versor_HT_Neg'
    public static final String LABEL_PHYSICSTH_JZ_NEG_SELECTION = "Jz_Neg_Selection";                                     // Label for selection of domains for 'Jz_versor_HT_Pos'
    public static final String LABEL_VCOILS_NO_CLIQ = "Vcoils_No" + "_" + MARKER_CLIQ;                                  // Label for 'Vcoils_No_cliq'
    public static final String LABEL_VCOILS_CLIQ = "Vcoils_" + MARKER_CLIQ;                                             // Label for 'Vcoils_cliq'
    public static final String LABEL_INTCOILS_NO_CLIQ = "intCoils_No" + "_" + MARKER_CLIQ;                              // Label for 'intCoils_No_cliq'
    public static final String LABEL_INTCOILS_CLIQ = "intCoils_" + MARKER_CLIQ;                                         // Label for 'intCoils_cliq'
    public static final String LABEL_NO_CLIQ_WINDINGS_HT_SELECTION = "No_cliq_HTs_Selection";                              // Label for selection of half-turns with no cliq
    public static final String LABEL_CLIQ_WINDINGS_HT_SELECTION = "cliq_HTs_Selection";                                   // Label for selection of half-turns with cliq
    public static final String LABEL_JUNCTIONBOX = "JunctionBox";                                                       // Label for 'JunctioBox' variable groups
    public static final String LABEL_JUNCTIONBOX_NO_CLIQ = LABEL_JUNCTIONBOX + "_No_" + MARKER_CLIQ;                    // Label for 'JunctionBox_No_cliq'
    public static final String LABEL_JUNCTIONBOX_CLIQ = LABEL_JUNCTIONBOX + "_" + MARKER_CLIQ;                          // Label for 'JunctionBox_cliq'

    // Quench Heater parameter labels:
    public static final String LABEL_QH = "QH_";
    public static final String LABEL_QH_MESH = "QH_Mesh";
    public static final String LABEL_WIDTH_QH = "w" + LABEL_QH;
    public static final String LABEL_TRIGGER_TIME_QH = "tQHtrigger";
    public static final String LABEL_OPERATIONAL_TEMPERATUR = "Top";
    public static final String LABEL_INSULATION_THICKNESS_QH_TO_COIL = "thQHtoCoilIns";
    public static final String LABEL_INSULATION_THICKNESS_TO_COIL = "thCoilIns";
    public static final String LABEL_INSULATION_THICKNESS_QH_TO_BATH = "thQHtoBathIns";
    public static final String LABEL_INSULATION_THICKNESS_QH_STRIP = "thQHSteelStrip";
    public static final String LABEL_EXPONENTIAL_TIME_CONSTANT_DECAY = "tauQH";
    public static final String LABEL_NUMBER_OF_QH_SUBDIVISIONS = "NumQHDivisions";
    public static final String LABEL_FRACTION_OF_QH_STATION = "_fracHeaterStation";
    public static final String LABEL_INITIAL_QH_CURRENT = "IQH0";
    public static final String LABEL_QH_CURRENT = "IQH";
    public static final String LABEL_QUENCH_HEATER_TEMPERATURE = "TQH";
    public static final String LABEL_TEMPERATURE_STABILIZATION = "Temperature_Stabilization";
    public static final String LABEL_RESISTIVITY_OF_STEEL = "resSteel";
    public static final String LABEL_QUENCH_HEATER_PROPERTY = "Property";
    public static final String LABEL_PHYSICSTH_QH = "ht1";
    public static final String VALUE_QUENCH_HEATER_DISPLACEMENT = "1e-3";
    public static final String LABEL_HEATER_DOMAIN = "_Heater_Domain";

    // Quench Heater parameter values:
    public static final int VALUE_NUMBER_OF_QH_SUBDIVISIONS = 10;
    public static final double VALUE_INSULATION_THICKNESS_QH_TO_COIL = 50e-6;
    public static final double VALUE_INSULATION_THICKNESS_TO_COIL = 145e-6;
    public static final double VALUE_INSULATION_THICKNESS_QH_TO_BATH = 500e-6;
    public static final double VALUE_EXPONENTIAL_TIME_CONSTANT_DECAY = 40e-3;
    public static final double VALUE_INITIAL_QH_CURRENT = 100;
    public static final double VALUE_INSULATION_THICKNESS_QH_STRIP = 25e-6;

    // Quench Heater geometry Strings:
    public static final String LABEL_BATH_TEMPERATURE_POINT = "_Bath_Temperature_Point";
    public static final String LABEL_HEATER_TO_BATH_INS_POINT_INIT = "_Heater_to_Bath_Insulation_Point_Initial";
    public static final String LABEL_HEATER_TO_BATH_INS_LAYER = "_Heater_to_Bath_Insulation_Layer";
    public static final String LABEL_HEATER_TO_COIL_INS_POINT_INIT = "_Heater_to_Coil_Insulation_Point_Initial";
    public static final String LABEL_HEATER_TO_COIL_INS_LAYER = "_Heater_to_Coil_Insulation_Layer";
    public static final String LABEL_COIL_INS_POINT_INIT = "_Coil_Insulation_Point_Initial";
    public static final String LABEL_COIL_INS_LAYER = "_Coil_Insulation_Layer";
    public static final String LABEL_HEATER_TO_BATH_INTERVAL =  "_Heater_to_Bath_Interval";
    public static final String LABEL_STEEL_STRIP_INTERVAL = "_Steel_Strip_Interval";
    public static final String LABEL_HEATER_TO_COIL_INTERVAL = "_Heater_to_Coil_Interval";
    public static final String LABEL_COIL_INSULATION_INTERVAL = "_Coil_insulation_Interval";
    public static final String LABEL_COIL_TEMPERATURE_POINT = "_Coil_Temperature_Point";
    public static final String LABEL_AVERAGE_TEMPERATURE = "_Average_Temperature";

    // Quench Heater geometry selection strings:
    public static final String LABEL_BATH_TEMPERATURE_SELECTION = "_Bath_Temperature_Selection";
    public static final String LABEL_HEATER_TO_BATH_INS_POINT_SELECTION = "_Heater_to_Bath_Insulation_Point_Selection";
    public static final String LABEL_HEATER_TO_BATH_INS_TRANSFORM_SELECTION = "_Heater_to_Bath_Insulation_Selection";
    public static final String LABEL_HEATER_TO_COIL_INS_POINT_SELECTION = "_Heater_to_Coil_Insulation_Point_Selection";
    public static final String LABEL_HEATER_TO_COIL_INS_TRANSFORM_SELECTION = "_Heater_to_Coil_Insulation_Selection";
    public static final String LABEL_COIL_INS_POINT_SELECTION = "_Coil_Insulation_Point_Selection";
    public static final String LABEL_COIL_INS_TRANSFORM_SELECTION = "_Coil_Insulation_Selection";
    public static final String LABEL_HEATER_TO_BATH_INTERVAL_SELECTION = "_Heater_to_Bath_Interval_Selection";
    public static final String LABEL_STEEL_STRIP_INTERVAL_SELECTION = "_Steel_Strip_Interval_Selection";
    public static final String LABEL_HEATER_TO_COIL_INTERVAL_SELECTION = "_Heater_to_Coil_Interval_Selection";
    public static final String LABEL_COIL_INS_INTERVAL_SELECTION = "_Coil_insulation_Interval_Selection";
    public static final String LABEL_COIL_TEMPERATURE_SELECTION = "_Coil_Temperature_Selection";

    // Quench Heater materials domain selection labels:
    public static final String LABEL_KAPTON_DOMAIN_SELECTION = "_Kapton_Selection";

    // Thin layers:
    public static final String LABEL_HEATER_TO_BATH_THIN_LAYER = "Heater_to_Bath_Thin_Layer";
    public static final String LABEL_HEATER_TO_COIL_THIN_LAYER = "Heater_to_Coil_Thin_Layer";
    public static final String LABEL_COIL_INSULATION_THIN_LAYER = "Coil_Insulation_Thin_Layer";
}