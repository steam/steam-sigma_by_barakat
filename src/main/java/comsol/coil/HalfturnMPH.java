package comsol.coil;

import comsol.api.DefinitionsAPI;
import comsol.api.GeometryAPI;
import comsol.constants.MPHC;
import comsol.definitions.VariablesHalfturn;
import comsol.definitions.utils.VariableGroup;
import model.geometry.coil.Halfturn;
import server.SigmaServer;

import java.util.function.Function;

/**
 * Class contains a set of methods for generating a half turn in COMSOL with SIGMA Server
 */
public class HalfturnMPH {
    private final String labelRoot;
    private final Halfturn halfturn;
    private String label;
    private String labelSelection;
    private String labelIndex;
    private String labelWBezier;
    private String labelNBezier;
    private String labelAvgCoupling;
    private String labelMinCoupling;
    private String labelAvgWCoupling;
    private String labelAvgNCoupling;

    /**
     * Constructor of a HalfturnMPH object with a given root label and half-turn definition
     *
     * @param labelRoot - root label
     * @param halfturn  - half-turn definition
     */
    public HalfturnMPH(String labelRoot, Halfturn halfturn) {
        this.labelRoot = labelRoot;
        this.halfturn = halfturn;
    }

    protected void defineLabels(String index) {
        this.label              = String.format("%s%s", labelRoot, index);
        this.labelIndex         = index;
        this.labelSelection     = MPHC.LABEL_GEOM + "_" + this.label + "_dom";
        this.labelAvgCoupling   = this.label + "_" + MPHC.LABEL_SURFACE_AVERAGE;
        this.labelMinCoupling   = this.label + "_" + MPHC.LABEL_SURFACE_MINIMUM;
        this.labelAvgWCoupling  = this.label + "_" + MPHC.LABEL_LINEWBEZIER_AVERAGE;
        this.labelAvgNCoupling  = this.label + "_" + MPHC.LABEL_LINENBEZIER_AVERAGE;
    }

    protected void buildGeometry(SigmaServer srv) {
        // Build HalfturnObj
        GeometryAPI.createPolygon(srv, label, halfturn.getPolygon());
        // Build WBeizier
        this.labelWBezier = String.format("%s_%s", label, MPHC.MARKER_WIDEBEZIER);
        GeometryAPI.createBezierPolygonLinear(srv, this.labelWBezier, halfturn.getWideBezier());
        GeometryAPI.createSelectionAllLevels(srv, this.labelWBezier);
        // Build NBeizier
        this.labelNBezier = String.format("%s_%s", label, MPHC.MARKER_NARROWBEZIER);
        GeometryAPI.createBezierPolygonLinear(srv, this.labelNBezier, halfturn.getNarrowBezier());
        GeometryAPI.createSelectionAllLevels(srv, this.labelNBezier);
    }

// ---------------------------------------------------------------------------------------------------------------------
// Deletion of individual half-turn properties:
// All 'C0_Px_Wx_Bx_HTx' are not compiled into COMSOL, as they're not needed in the restructured model:
// ---------------------------------------------------------------------------------------------------------------------
//    protected void buildDefinitions(SigmaServer srv, String labelWinding) {
//        //VariableGroup turnVarGroup = VariablesHalfturn.defineVariables(label, labelWinding, halfturn, labelIndex);
//        //turnVarGroup.buildVariableGroup(srv, labelSelection);
//    }

// ---------------------------------------------------------------------------------------------------------------------
// Deletion of '...avgSurf', '...minSurf', '...avgLineN', '...avgLineW' and '...intSurf:'
// These are not compiled into COMSOL, as they're not needed in the restructured model:
// ---------------------------------------------------------------------------------------------------------------------
// The disablement of the following lines delete the C0_Px_Wx_Bx (xxx as avg, line,):
//    protected void buildComponentCouplings(SigmaServer srv) {
    //DefinitionsAPI.createComponentCoupling(srv, labelAvgCoupling, DefinitionsAPI.ComponentCouplingType.Average);
    //DefinitionsAPI.assignSelection(srv, labelAvgCoupling, String.format("%s_%s_dom", MPHC.LABEL_GEOM, label));

    //DefinitionsAPI.createComponentCoupling(srv, labelMinCoupling, DefinitionsAPI.ComponentCouplingType.Minimum);
    //DefinitionsAPI.assignSelection(srv, labelMinCoupling, String.format("%s_%s_dom", MPHC.LABEL_GEOM, label));

    //DefinitionsAPI.createComponentCoupling(srv, labelAvgWCoupling, DefinitionsAPI.ComponentCouplingType.Average);
    //DefinitionsAPI.assignSelection(srv, labelAvgWCoupling, String.format("%s_%s_bnd", MPHC.LABEL_GEOM, labelWBezier));
    // These 2 lines delete the half-turns 'avgLineW'

    //DefinitionsAPI.createComponentCoupling(srv, labelAvgNCoupling, DefinitionsAPI.ComponentCouplingType.Average);
    //DefinitionsAPI.assignSelection(srv, labelAvgNCoupling, String.format("%s_%s_bnd", MPHC.LABEL_GEOM, labelNBezier));
    // These 2 lines delete the half-turns 'avgLineN'
//    }

    // *****************************************************************************************************************
    // Lambda functions
    // *****************************************************************************************************************

    protected String getHalfturnPropertyString(Function<HalfturnMPH, String> function) {
        return function.apply(this);
    }

    protected Double getHalfturnPropertyDouble(Function<HalfturnMPH, Double> function) {
        return function.apply(this);
    }

    // *****************************************************************************************************************
    // Getters
    // *****************************************************************************************************************

    public String getLabel() {
        return label;
    }

    /* This function is like getLabel(). However, it only returns the labels for which tauVersor is positive. Otherwise it returns null.
     */
    public String getLabelPosPolarity() {
        Double polarity = halfturn.getTauVersor();

        if (polarity > 0) {
            return label;
        } else {
            return null;
        }
    }

    /* This function is like getLabel(). However, it only returns the labels for which tauVersor is negative. Otherwise it returns null.
     */
    public String getLabelNegPolarity() {
        Double polarity = halfturn.getTauVersor();

        if (polarity < 0) {
            return label;
        } else {
            return null;
        }
    }

    public String getLabelIndex() {
        return labelIndex;
    }

    public String getLabelSelection() {
        return labelSelection;
    }

    public String getLabelWBezier() {
        return labelWBezier;
    }

    public String getLabelNBezier() {
        return labelNBezier;
    }

    public String getLabelAvgCoupling() {
        return labelAvgCoupling;
    }

    public String getLabelMinCoupling() {
        return labelMinCoupling;
    }

    public String getLabelAvgWCoupling() {
        return labelAvgWCoupling;
    }

    public String getLabelAvgNCoupling() {
        return labelAvgNCoupling;
    }

    // *****************************************************************************************************************
    // Setters
    // *****************************************************************************************************************

    public void setLabelIndex(String labelIndex) {
        this.labelIndex = labelIndex;
    }
}