package comsol.coil;

import comsol.constants.MPHC;
import model.geometry.coil.Halfturn;
import model.geometry.coil.HalfturnBlock;
import server.SigmaServer;

import java.util.ArrayList;
import java.util.function.Function;

/**
 * Class contains a set of methods for generating a block of half turns in COMSOL with SIGMA Server
 */
public class HalfturnBlockMPH {
    private final String label;
    private final HalfturnMPH[] halfturns;

    /**
     * Constructor of a HalturnBlockMPH object with a given label and definition of half-turn block
     *
     * @param label         - label of a block of half turns
     * @param halfturnBlock - definition of a half-turn block
     */
    public HalfturnBlockMPH(String label, HalfturnBlock halfturnBlock) {
        this.label = label;

        ArrayList<HalfturnMPH> halfturnMPHList = new ArrayList<>();

        for (int ht_i = 0; ht_i < halfturnBlock.getHalfturns().length; ht_i++) {
            Halfturn halfturn = halfturnBlock.getHalfturns()[ht_i];
            String labelRootHalfturn = String.format("%s_%s", this.label, MPHC.MARKER_HALFTURN);
            halfturnMPHList.add(new HalfturnMPH(labelRootHalfturn, halfturn));
        }
        halfturns = halfturnMPHList.toArray(new HalfturnMPH[halfturnBlock.getHalfturns().length]);
    }

    /**
     * Method builds component couplings for each half-turn
     *
     * @param srv - input SigmaServer instance
     */
//    public void buildComponentCouplings(SigmaServer srv) {
//    for (HalfturnMPH halfturn : halfturns) {
//    halfturn.buildComponentCouplings(srv);
//    }
//}

    // *****************************************************************************************************************
    // Lambda functions
    // *****************************************************************************************************************

    protected String[] getHalfturnPropertyString(Function<HalfturnMPH, String> function) {
        ArrayList<String> labelHalfturnsList = new ArrayList<>();

        for (int ht_i = 0; ht_i < halfturns.length; ht_i++) {
            labelHalfturnsList.add(halfturns[ht_i].getHalfturnPropertyString(function));
        }
        return labelHalfturnsList.toArray(new String[labelHalfturnsList.size()]);
    }

    protected Double[] getHalfturnPropertyDouble(Function<HalfturnMPH, Double> function) {
        ArrayList<Double> labelHalfturnsList = new ArrayList<>();

        for (int ht_i = 0; ht_i < halfturns.length; ht_i++) {
            labelHalfturnsList.add(halfturns[ht_i].getHalfturnPropertyDouble(function));
        }
        return labelHalfturnsList.toArray(new Double[labelHalfturnsList.size()]);
    }

    // *****************************************************************************************************************
    // Getters
    // *****************************************************************************************************************

    public HalfturnMPH[] getHalfTurns() {
        return this.halfturns.clone();
    }

    public String getLabel() {
        return this.label;
    }
}
