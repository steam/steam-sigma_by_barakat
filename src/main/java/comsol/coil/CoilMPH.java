package comsol.coil;

import comsol.api.*;
import comsol.constants.MPHC;
import comsol.definitions.utils.Variable;
import comsol.definitions.utils.VariableGroup;
import comsol.definitions.VariablesPhysicsEngine;
import model.domains.database.CoilDomain;
import model.geometry.coil.Pole;
import server.SigmaServer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Function;

import java.util.List;

/**
 * Class contains a set of methods for generating a coil object in COMSOL with SIGMA Server
 */
public class CoilMPH {

    private final String label;
    private final String labelSelection;
    //private final String labelIntOp;                                                                                  // Removed by Barakat: This is replaced by new integration operators
    private final PoleMPH[] poles;

    // Added by Barakat:
    private final   String selectionJzPos;
    private final   String selectionJzNeg;
    private static  String selectionNoCliqHTs;
    private static  String selectionCliqHTs;

    private CoilMPH(String label, String labelSelection, PoleMPH[] poles) {
        this.label = label;
        this.labelSelection = labelSelection;
        //this.labelIntOp = this.label + "_" +\code MPHC.LABEL_SURFACE_INTEGRATION;                                          // Removed by Barakat: This is replaced by new integration operators
        this.poles = poles.clone();

        // Added by Barakat:
        this.selectionJzPos = String.format("%s_%s", label, MPHC.LABEL_PHYSICSTH_JZ_POS_SELECTION);
        this.selectionJzNeg = String.format("%s_%s", label, MPHC.LABEL_PHYSICSTH_JZ_NEG_SELECTION);
        selectionNoCliqHTs  = String.format("%s_%s", label,MPHC.LABEL_NO_CLIQ_WINDINGS_HT_SELECTION);
        selectionCliqHTs    = String.format("%s_%s", label,MPHC.LABEL_CLIQ_WINDINGS_HT_SELECTION);
    }

    /**
     * Method creates a CoilMPH object from a CoilDomain one
     *
     * @param coilDomain - user input with coil domain definition
     * @return CoilMPH object suitable to be executed with SIGMA Server
     */
    public static CoilMPH ofCoilDomain(CoilDomain coilDomain) {
        ArrayList<PoleMPH> poleMPHList = new ArrayList<>();
        for (int p_i = 0; p_i < coilDomain.getCoil().getPoles().length; p_i++) {
            String labelPole = String.format("%s_%s%d", coilDomain.getLabel(), MPHC.MARKER_POLE, p_i);
            Pole pole = coilDomain.getCoil().getPoles()[p_i];
            poleMPHList.add(new PoleMPH(labelPole, pole));
        }

        String label = coilDomain.getLabel();
        String labelSelection = coilDomain.getLabel();
        PoleMPH[] poles = poleMPHList.toArray(new PoleMPH[coilDomain.getCoil().getPoles().length]);
        return new CoilMPH(label, labelSelection, poles);
    }

    /**
     * Method builds a coil object in COMSOL with SIGMA Server
     *
     * @param srv - input SigmaServer instance
     */
    public void build(SigmaServer srv) {
        buildGeometry(srv);                                                                                             // This line builds magnet geometry
        buildDefinitions(srv);                                                                                          // This line builds all variable groups
        buildPhysics(srv);                                                                                              // This line builds physics (heat transer, magnetic fields etc.)
        buildIntegrationOperatorsCoils(srv);                                                                            // This line builds IntegrationOperation
        //buildComponentCouplings(srv);                                                                                 // This the following are not compiled into COMSOL:
        // '...avgSurf', '...minSurf', '...avgLineN', '...avgLineW' and '...intSurf'
    }

    private void buildGeometry(SigmaServer srv) {
        for (PoleMPH pole : poles) {
            pole.buildGeometry(srv);
            pole.buildSelection(srv);
        }

        // Build Jz_versor plus and minus selection
        // The idea is: Get all the halfturns, then select on the halfturns with a specific polarity, and then create selections of the half-turns with specific polarities
        // The Function here uses functions getLabelPosPolarity() and getLabelPosPolarity() in HalfTurnMPH. These are special functions that return the label in case the
        // polarity is the requested polarity. Otherwise they return null.
        // In the method below, the 'null' labels are removed from the string array, and the string array is then passed to GeometryApi, where a explicit domain selection is
        // created.
        //
        // The following section is perhaps dirty programming, but it does work. Consider implementing a cleaning solution, maybe?. Matthias 2/4/19.

        String[] removedNullPosPolarity = getNotNullHTPolarityLabels(getHalfturnPropertyString(HalfturnMPH::getLabelPosPolarity));  // Gets labels for all HT with positive with no nulls
        GeometryAPI.buildUnion(srv, selectionJzPos, removedNullPosPolarity);

        String[] removedNullNegPolarity = getNotNullHTPolarityLabels(getHalfturnPropertyString(HalfturnMPH::getLabelNegPolarity));  // Gets labels for all HT with negative with no nulls
        GeometryAPI.buildUnion(srv, selectionJzNeg, removedNullNegPolarity);

        //GeometryAPI.explicitDomainSelection(srv, MPHC.LABEL_PHYSICSTH_JZ_POS_SELECTION, posPolaritySelection.toArray(new String[posPolaritySelection.size()]));
        //GeometryAPI.explicitDomainSelection(srv, MPHC.LABEL_PHYSICSTH_JZ_NEG_SELECTION, negPolaritySelection.toArray(new String[negPolaritySelection.size()]));
        //String selectionJzPos = String.format("%s"+"_"+"%s", label, MPHC.LABEL_PHYSICSTH_JZ_POS_SELECTION);
        //GeometryAPI.buildUnion(srv, MPHC.LABEL_PHYSICSTH_JZ_POS_SELECTION, posPolaritySelection.toArray(new String[posPolaritySelection.size()]));

        //String selectionJzPos = String.format("%s_%s", label, MPHC.LABEL_PHYSICSTH_JZ_POS_SELECTION);
        //GeometryAPI.buildUnion(srv, selectionJzPos, removedNullPosPolarity);

        //String selectionJzNeg = String.format("%s_%s", label, MPHC.LABEL_PHYSICSTH_JZ_NEG_SELECTION);
        //GeometryAPI.buildUnion(srv, selectionJzNeg, removedNullNegPolarity);
        // Until here

        // Build Coil Selection
        String[] labelHalfturns = getHalfturnPropertyString(HalfturnMPH::getLabel);
        GeometryAPI.buildUnion(srv, labelSelection, labelHalfturns);
        GeometryAPI.createCumulativeSelection(srv, label + "UNION");
        GeometryAPI.contributeTo(srv, labelSelection, label + "UNION");

        // Build Insulation Selection
        for (PoleMPH pole : poles) {
            pole.buildInsulationSelection(srv);
        }

        // Build WBezier Selection
        String[] labelWBezier = getHalfturnPropertyString(HalfturnMPH::getLabelWBezier);
        String selectionWBeizier = String.format("%s%s%s", label, MPHC.MARKER_SELECTION, MPHC.MARKER_WIDEBEZIER);
        GeometryAPI.explicitSelection(srv, selectionWBeizier, labelWBezier);
        DefinitionsAPI.createHideEntities(srv, selectionWBeizier, String.format("%s_%s", MPHC.LABEL_GEOM, selectionWBeizier));

        // Build NBezier Selection
        String[] labelNBezier = getHalfturnPropertyString(HalfturnMPH::getLabelNBezier);
        String selectionNBeizier = String.format("%s%s%s", label, MPHC.MARKER_SELECTION, MPHC.MARKER_NARROWBEZIER);
        GeometryAPI.explicitSelection(srv, selectionNBeizier, labelNBezier);
        DefinitionsAPI.createHideEntities(srv, selectionNBeizier, String.format("%s_%s", MPHC.LABEL_GEOM, selectionNBeizier));

        // Build TopWindingHTs selection
        // String[] hafTurnsLabels =getHalfturnPropertyString(HalfturnMPH::getLabel);
        String[] labelNoCliqHTs = getHalfHalfturns("NoCliqHalfturns");                               // This gets half turns for top windings
        //String selectionTopWindingsHTs = String.format("%s_%s", label, MPHC.LABEL_TOP_WINDINGS_HT_SELECTION);
        GeometryAPI.buildUnion(srv, selectionNoCliqHTs, labelNoCliqHTs );

        // Build BottomWindingHTs selection
        String[] labelCliqHTs = getHalfHalfturns("CliqHalfturns");                                 // This gets half turns for bottom windings
        //String selectionBotWindingsHTs = String.format("%s_%s", label, MPHC.LABEL_BOT_WINDINGS_HT_SELECTION);
        GeometryAPI.buildUnion(srv, selectionCliqHTs, labelCliqHTs);
    }

    private static String[] getNotNullHTPolarityLabels(String[] halfturnPropertyString) {
        String[] labelHTsNegPolarity = halfturnPropertyString;
        return Arrays.stream(labelHTsNegPolarity)
                .filter(value -> value != null)
                .toArray(size -> new String[size]);
    }

    private void buildDefinitions(SigmaServer srv) {
        buildCable(srv);
        buildPhysEngineVariables(srv);
        buildJzVersor(srv, "1", MPHC.LABEL_JZ_VERSOR_HT_PLUS, selectionJzPos);
        buildJzVersor(srv, "-1", MPHC.LABEL_JZ_VERSOR_HT_MINUS, selectionJzNeg);
        buildVoltages(srv);

        //windingVoltage(srv);
        //buildVdeltaVarTable(srv);
        //buildVgroundVarTable(srv);

//        for (PoleMPH pole : poles) {
//            pole.buildDefinitions(srv);
//        }
    }

    private void buildCable(SigmaServer srv) {
        for (PoleMPH pole : poles) {
            pole.buildCable(srv);
        }
    }

    private void buildPhysEngineVariables(SigmaServer srv) {
        VariableGroup phEnVarGroup = VariablesPhysicsEngine.defineVariables(label);
        String selectionLabel = String.format("%s_%s_dom", MPHC.LABEL_GEOM, label);
        phEnVarGroup.buildVariableGroup(srv,MPHC.LABEL_COMP, MPHC.LABEL_GEOM, selectionLabel, 2);
    }

    private void buildPhysics(SigmaServer srv) {
        MagneticFieldsPhysicsAPI.buildAmpereLawEqMag(srv, label, String.format("%s_%s_dom",
                MPHC.LABEL_GEOM,
                label),
                new String[]{MPHC.LABEL_MX, MPHC.LABEL_MY, "0"});
        MagneticFieldsPhysicsAPI.buildExternalJ(srv, label, String.format("%s_%s_dom",
                MPHC.LABEL_GEOM,
                label),
                new String[]{"0", "0", MPHC.LABEL_JS});
        HeatTransferInSolidsPhysicsAPI.buildHeatSource(srv, label, String.format("%s_%s_dom",
                MPHC.LABEL_GEOM,
                label),
                MPHC.LABEL_Q_TOT);
        DodePhysicsAPI.buildDode(srv, String.format("%s_%s_dom", MPHC.LABEL_GEOM, label));

        for (PoleMPH pole : poles) {
            pole.buildPhysics(srv);
        }
    }

    // *****************************************************************************************************************
    // Lambda functions
    // *****************************************************************************************************************
    private String[] getHalfturnPropertyString(Function<HalfturnMPH, String> function) {
        ArrayList<String> labelHalfturnsList = new ArrayList<>();

        for (int p_i = 0; p_i < poles.length; p_i++) {
            String[] labelLocal = poles[p_i].getHalfturnPropertyString(function);
            for (String label_i : labelLocal) {
                labelHalfturnsList.add(label_i);
            }
        }
        return labelHalfturnsList.toArray(new String[labelHalfturnsList.size()]);
    }

    /**
     * This methods gets the list of all half-turns and splits it into 2 evenly sized sublists
     * @param halfTurnSelection     - Value of switch decides if first or second half of half-turns list
     *      - subparam NoCliqHalfturns - The first half (defining HTs of top windings) of th list is returned
     *      - subparam Cliqhalfturns - The second last half (defining HTs of bottom windings) of th list is returned
     * @return                      - The by-case chosen halved half-turns sublist
     */
    private String[] getHalfHalfturns(String halfTurnSelection){

        ArrayList<String> halfTurns= new ArrayList<>(Arrays.asList(getHalfturnPropertyString(HalfturnMPH::getLabel)));
        int size = halfTurns.size();
        ArrayList<String> selection = new ArrayList<>();

        switch (halfTurnSelection){
            case "NoCliqHalfturns":
                selection = new ArrayList<>(halfTurns.subList(0,(size+1)/2));
                break;
            case "CliqHalfturns":
                selection = new ArrayList<>(halfTurns.subList((size+1)/2, size));
                break;
        }

        return selection.toArray(new String[selection.size()]);
    }

    /**
     * Method returns a property string for a winding
     * @param function - input function to be evaluated per winding
     * @return - property string
     */
    public String[] getWindingPropertyString(Function<WindingMPH, String> function) {
        ArrayList<String> labelWindingList = new ArrayList<>();

        for (int p_i = 0; p_i < poles.length; p_i++) {
            String[] labelLocal = poles[p_i].getWindingPropertyString(function);
            for (String label_i : labelLocal) {
                labelWindingList.add(label_i);
            }
        }
        return labelWindingList.toArray(new String[labelWindingList.size()]);
    }

    // *****************************************************************************************************************
    // Getters
    // *****************************************************************************************************************

    public PoleMPH[] getPoles() {
        return poles.clone();
    }

    /**
     * Method defines and builds the variable group Jz_versor_HT
     * @param srv               - Input SigmaServer instance
     * @param value             - Input value of Jz_versor_HT instance
     * @param labelJzVersorHt   - Label for name of Jz_versor_HT instance
     * @param selectionJz       - Assign selection to Jz_versor_HT instance
     */
    private void buildJzVersor(SigmaServer srv, String value, String labelJzVersorHt, String selectionJz) {
        List<Variable> varList = new ArrayList<>();                                                                     // Creates an array
        String labelGroup = String.format("%s_%s", label, labelJzVersorHt);                                                       // Creates label of 'Jz_versor_HT_minus'
        String label = String.format("%s", MPHC.LABEL_JZ_VERSOR_HT);                                                    // Creates variable 'Jz_versor_HT' inside 'Jz_versor_HT_minus'
        varList.add(new Variable(label, value));

        VariableGroup varGroup = new VariableGroup(labelGroup, varList);                                                // Creates variable group for assigning variable group to domain
        String selectionLabel = String.format("%s_%s_dom", MPHC.LABEL_GEOM, selectionJz);                               // Label for domain, to which the variable group should apply
        varGroup.buildVariableGroup(srv, MPHC.LABEL_COMP, MPHC.LABEL_GEOM, selectionLabel, 2);                                                               // Variable group is assigned to said domain
    }

    /**
     * Methods builds variables inside Voltages variable group
     * @param srv - Input SigmaServer instance
     */
    private void buildVoltages(SigmaServer srv) {
        List<Variable> varList = new ArrayList<>();
        String labelGroup = String.format("%s_%s", label, MPHC.LABEL_VOLTAGES);

        // 'Vcoils_No_cliq' is defined and added:
        varList.add(createVoltageVariable(MPHC.LABEL_VCOILS_NO_CLIQ, label + "_" + MPHC.LABEL_INTCOILS_NO_CLIQ));
        // 'Vcoils_cliq' is defined and added:
        varList.add(createVoltageVariable(MPHC.LABEL_VCOILS_CLIQ, label + "_" + MPHC.LABEL_INTCOILS_CLIQ));
        // The created variables are added to Voltages variable group:
        VariableGroup varGroup = new VariableGroup(labelGroup, varList);
        String selectionLabel = String.format("");
        varGroup.buildVariableGroup(srv, MPHC.LABEL_COMP, MPHC.LABEL_GEOM, selectionLabel, 2);
    }

    /**
     * Method defines variables inside Voltages variable group with: Vcoils_x = IntVoils_x(V_delta_PE/surf_HT)*magLength*symFactor:
     * @param labelVcoils   - Label for the name of Vcoils instance
     * @param labelIntCoils - Label for the name of IntCoils instance (Integration Operator) for given instance of Vcoils
     * @return              - Voltages variable group
     */
    private static Variable createVoltageVariable(String labelVcoils, String labelIntCoils) {
        String valueVcoils = String.format("%s(%s/%s)*%s*%s",
                labelIntCoils,
                MPHC.LABEL_V_DELTA,
                MPHC.LABEL_HT_SURFACE,
                MPHC.LABEL_MAGNETIC_LENGTH,
                MPHC.LABEL_CLIQ_SYMFACTOR);
        return new Variable(labelVcoils, valueVcoils);
    }

    /**
     * This method builds an Integration Operator:
     * @param srv - Input SigmaServer instance
     */
    private void buildIntegrationOperatorsCoils(SigmaServer srv) {
        // Builds and assigns selection for C0_intCoils_Top:
        buildAndAssignOperatorToSelection(srv, MPHC.LABEL_INTCOILS_NO_CLIQ, selectionNoCliqHTs);

        // Builds and assigns selection for C0_intCoils_Bot:
        buildAndAssignOperatorToSelection(srv, MPHC.LABEL_INTCOILS_CLIQ, selectionCliqHTs);
    }

    /**
     * This methods defines an Integration Operator with a name and selection:
     * @param srv                           - Input SigmaServer instance
     * @param labelIntegrationOperators     - Label for name of the Integration Operator
     * @param labelWindingsHtSelection      - Label for name of the selection, that the Integration Operator should be assigned to
     */
    private void buildAndAssignOperatorToSelection(SigmaServer srv, String labelIntegrationOperators, String labelWindingsHtSelection) {
        String labelIntegrationOperator = String.format("%s_%s", label, labelIntegrationOperators);
        DefinitionsAPI.createComponentCoupling(srv, labelIntegrationOperator, labelIntegrationOperator, DefinitionsAPI.ComponentCouplingType.Integration, MPHC.LABEL_GEOM);
        DefinitionsAPI.assignSelection(srv, labelIntegrationOperator, String.format("%s_%s_dom", MPHC.LABEL_GEOM, labelWindingsHtSelection));
    }

    // Defined getters, so it can be assigned to JunctionBox_no_cliq in 'CLIQ.java'
    public static String getselectionNoCliqHTs() {
        return selectionNoCliqHTs;
    }
    // Defined getters, so it can be assigned to JunctionBox_cliq in 'CLIQ.java'
    public static String getselectionCliqHTs() {
        return selectionCliqHTs;
    }

//    private String[] getHalfHalfturns(Function<HalfturnMPH, String> function) {
////        int halfvalueHT = getHalfturnPropertyString.size();
////        return  new String[]
////    }
//
////    private String[][] getHalfHalfturns(){
////
////        ArrayList<String> halfTurns = getHalfturnPropertyString();
////        int size = halfTurns.size();
////
////        ArrayList<String> TopHTs = new ArrayList<String>(halfTurns.subList(0,(size+1)/2));
////        ArrayList<String> BotHTs = new ArrayList<String>(halfTurns.subList((size+1)/2,size));
////
////        String[][] set = new String[2][size];
////
////        set[0] =    TopHTs.toArray(new String[TopHTs.size()]);
////        set[1] =    TopHTs.toArray(new String[TopHTs.size()]);
////
////        return set;
////    }

//    private void buildJzVersorMinus (SigmaServer srv) {
//        ArrayList<Variable> varList = new ArrayList<>();                                                              // Creates an array
//        String labelGroup   = String.format("%s", MPHC.LABEL_JZ_VERSOR_HT_MINUS);                                     // Creates label of 'Jz_versor_HT_minus'
//            String label    = String.format("%s", MPHC.LABEL_JZ_VERSOR_HT);                                           // Creates variable 'Jz_versor_HT' inside 'Jz_versor_HT_minus'
//            String value    = "-1";                                                                                   // Creates expression '-1' for 'Jz_versor_HT'
//        varList.add(new Variable(label, value));                                                                      // Variable is created and added
//
//        VariableGroup varGroup = new VariableGroup(labelGroup, varList);                                              // Creates variable group for assigning variable group to domain
//        String selectionLabel = String.format("%s_%s_dom", MPHC.LABEL_GEOM, selectionJzNeg);                          // Label and expression added to array
//        varGroup.buildVariableGroup(srv, selectionLabel);                                                             // Variable group is assigned to said domain
//    }

   /*
    private void buildintSurf1(SigmaServer srv) {
        String labelintSurf1 = String.format("%s_%s", label, MPHC.LABEL_INTCOILS_1);
        DefinitionsAPI.createComponentCoupling(srv, labelintSurf1, DefinitionsAPI.ComponentCouplingType.Integration);
        DefinitionsAPI.assignSelection(srv, labelIntOp, String.format("%s_%s_dom", MPHC.LABEL_GEOM, label));

        //for (PoleMPH pole : poles) {
        //    pole.buildComponentCouplings(srv);
        //}
    }

    private void buildintSurf2(SigmaServer srv) {
        String labelintSurf2 = String.format("%s_%s", label, MPHC.LABEL_INTCOILS_2);
        DefinitionsAPI.createComponentCoupling(srv, labelintSurf2, DefinitionsAPI.ComponentCouplingType.Integration);
        DefinitionsAPI.assignSelection(srv, labelIntOp, String.format("%s_%s_dom", MPHC.LABEL_GEOM, MPHC.LABEL_PHYSICSTH_JZ_NEG_SELECTION));

        //for (PoleMPH pole : poles) {
        //    pole.buildComponentCouplings(srv);
        //}
    }

//    private void buildComponentCoupling(SigmaServer srv) {
//        buildintSurf1(srv);
//        buildintSurf2(srv);
//    }
*/


/*
    private void buildVdeltaVarTable(SigmaServer srv) {
        ArrayList<Variable> variables = new ArrayList<>();
        String[] indexHalfturns = getHalfturnPropertyString(HalfturnMPH::getLabelIndex);
        String[] labelAvgCouplingHalfturns = getHalfturnPropertyString(HalfturnMPH::getLabelAvgCoupling);

        for (int i = 0; i < indexHalfturns.length; i++) {
            String labelVar = String.format("%s%s", MPHC.LABEL_V_DELTA_HALFTURN, indexHalfturns[i]);
            String valueVar = String.format("%s(%s)", labelAvgCouplingHalfturns[i], MPHC.LABEL_V_DELTA);
            variables.add(new Variable(labelVar, valueVar));
        }
        String labelGroup = String.format("Vdelta_%s", label);
        new VariableGroup(labelGroup, variables).buildVariableGroup(srv, "");
    }

    // THESE LINES DEFINE 'Vground' WHICH IS NOT NEEDED IN THE RESTRUCTURED MODEL:
    private void buildVgroundVarTable(SigmaServer srv) {

        ArrayList<Variable> variables = new ArrayList<>();
        String[] indexHalfturns = getHalfturnPropertyString(HalfturnMPH::getLabelIndex);
        String labelVar;
        String valueVar;

        Integer firstIndexHalfturn = Integer.parseInt(indexHalfturns[0]);

        // first line in table, reference ground
        labelVar = (String.format("%s%s", MPHC.LABEL_V_GROUND_HALFTURN, firstIndexHalfturn));
        valueVar = String.format("%s%s", MPHC.LABEL_V_DELTA_HALFTURN, firstIndexHalfturn);
        variables.add(new Variable(labelVar, valueVar));

        for (int i = firstIndexHalfturn + 1; i < firstIndexHalfturn + indexHalfturns.length; i++) {
            labelVar = (String.format("%s%d", MPHC.LABEL_V_GROUND_HALFTURN, i));
            valueVar = String.format("%s%d+%s%d", MPHC.LABEL_V_GROUND_HALFTURN, i - 1, MPHC.LABEL_V_DELTA_HALFTURN, i);
            variables.add(new Variable(labelVar, valueVar));
        }
        String labelGroup = String.format("Vgnd_%s", label);
        new VariableGroup(labelGroup, variables).buildVariableGroup(srv, "");
    }
*/

//    private void buildComponentCouplings(SigmaServer srv) {
//        DefinitionsAPI.createComponentCoupling(srv, labelIntOp, DefinitionsAPI.ComponentCouplingType.Integration);
//        DefinitionsAPI.assignSelection(srv, labelIntOp, String.format("%s_%s_dom", MPHC.LABEL_GEOM, label));
//
//        for (PoleMPH pole : poles) {
//            pole.buildComponentCouplings(srv);
//        }
//    }

// ---------------------------------------------------------------------------------------------------------------------
// 'WindingVoltage' is not compiled into COMSOL, as it is not needed in the restructured model:
// ---------------------------------------------------------------------------------------------------------------------
/*
    private void windingVoltage(SigmaServer srv) {

        ArrayList<Variable> varList = new ArrayList<>();
        String labelGroup = String.format("WindingVoltage_%s", label);

        String[] labelWindings = getWindingPropertyString(WindingMPH::getLabel);
        for (String labelWinding : labelWindings) {
            String labelWindingVoltage = "V_" + labelWinding;

            String labelIntOpWinding = labelWinding + "_" + MPHC.LABEL_SURFACE_INTEGRATION;
            String value = String.format("%s(%s/%s)*%s*%s",
                    labelIntOpWinding,
                    MPHC.LABEL_V_DELTA,
                    MPHC.LABEL_HT_SURFACE,
                    MPHC.LABEL_MAGNETIC_LENGTH,
                    MPHC.LABEL_CLIQ_SYMFACTOR);
            varList.add(new Variable(labelWindingVoltage, value));
        }

        VariableGroup varGroup = new VariableGroup(labelGroup, varList);
        varGroup.buildVariableGroup(srv, "");
    }
    */
}