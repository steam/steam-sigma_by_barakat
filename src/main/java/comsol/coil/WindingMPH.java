package comsol.coil;


import comsol.MagnetMPHBuilder;
import comsol.api.DefinitionsAPI;
import comsol.api.GeometryAPI;
import comsol.api.HeatTransferInSolidsPhysicsAPI;
import comsol.constants.MPHC;
import comsol.definitions.utils.VariableGroup;
import comsol.definitions.VariablesCable;
import comsol.materials.Materials;
import model.geometry.coil.Cable;
import model.geometry.coil.HalfturnBlock;
import model.geometry.coil.Winding;
import model.materials.Material;
import server.SigmaServer;

import java.util.ArrayList;
import java.util.function.Function;

/**
 * Class contains a set of properties and methods representing a coil winding in COMSOL
 */
public class WindingMPH {
    private final String label;
    private final String labelGeomSel;
    private final String labelGeomSelInsInterTurn;
    private final String labelGeomSelInsBoundary;
    private final String labelDefSelInsInterTurn;
    private final String labelDefSelInsExtBoundary;
    private final String labelDefSelBoundary;
    //private final String labelIntOp;                                                                                  // Removed by Barakat: This is replaced by new integration operators

    private final HalfturnBlockMPH[] halfturnBlocks;
    private final int noOfTurns;
    private final Cable cable;

    /**
     * Constructor of an MPH winding object with a given label and winding description
     *
     * @param label   - winding label
     * @param winding - input winding object
     */
    public WindingMPH(String label, Winding winding) {
        this.label = label;
        this.labelGeomSel = label;
        this.labelDefSelBoundary = String.format("%s_%s_bnd", MPHC.LABEL_GEOM, labelGeomSel);
        this.labelGeomSelInsInterTurn = String.format("%s%s_InterTurn", label, MPHC.MARKER_SELECTION);
        this.labelGeomSelInsBoundary = String.format("%s%s_Boundary", label, MPHC.MARKER_SELECTION);
        this.labelDefSelInsInterTurn = String.format("%s_%s", MPHC.LABEL_GEOM, labelGeomSelInsInterTurn);
        this.labelDefSelInsExtBoundary = String.format("%s_%s", MPHC.LABEL_GEOM, labelGeomSelInsBoundary);
        //this.labelIntOp = String.format("%s_%s", this.label, MPHC.LABEL_SURFACE_INTEGRATION);                         // Removed by Barakat:

        this.cable = winding.getCable();
        this.noOfTurns = winding.getNoOfTurns();

        ArrayList<HalfturnBlockMPH> halfturnBlockMPHList = new ArrayList<>();
        for (int b_i = 0; b_i < winding.getBlocks().length; b_i++) {
            String labelHalfturnBlock = String.format("%s_%s%d", this.label, MPHC.MARKER_BLOCK, b_i);
            HalfturnBlock halfturnBlock = winding.getBlocks()[b_i];
            halfturnBlockMPHList.add(new HalfturnBlockMPH(labelHalfturnBlock, halfturnBlock));
        }
        this.halfturnBlocks = halfturnBlockMPHList.toArray(new HalfturnBlockMPH[winding.getBlocks().length]);

        for (int t_i = 0; t_i < noOfTurns; t_i++) {
            for (int b_i = 0; b_i < halfturnBlocks.length; b_i++) {
                this.halfturnBlocks[b_i].getHalfTurns()[t_i].defineLabels(Integer.toString(MagnetMPHBuilder.indexHtGlobal));
                MagnetMPHBuilder.indexHtGlobal++;
            }
        }
    }

    protected void buildGeometry(SigmaServer srv) {
        for (int t_i = 0; t_i < noOfTurns; t_i++) {
            for (int b_i = 0; b_i < halfturnBlocks.length; b_i++) {
                this.halfturnBlocks[b_i].getHalfTurns()[t_i].buildGeometry(srv);
            }
        }
    }

    protected void buildSelection(SigmaServer srv) {
        GeometryAPI.buildUnion(srv, labelGeomSel, getHalfturnPropertyString(HalfturnMPH::getLabel));
    }

//    protected void buildDefinitions(SigmaServer srv) {
//        for (int t_i = 0; t_i < noOfTurns; t_i++) {
//            for (int b_i = 0; b_i < halfturnBlocks.length; b_i++) {
//                this.halfturnBlocks[b_i].getHalfTurns()[t_i].buildDefinitions(srv, label);
//            }
//        }
//    }

    protected void buildCable(SigmaServer srv) {
        String cableName = String.format("%s_%s", cable.getLabel(), labelGeomSel);
        String selName = String.format("%s_%s_dom", MPHC.LABEL_GEOM, labelGeomSel);
        VariableGroup cableVarGroup = VariablesCable.defineVariablesGroup(cableName, cable);
        cableVarGroup.buildVariableGroup(srv, MPHC.LABEL_COMP, MPHC.LABEL_GEOM, selName, 2);

        Material material = new Material(cable.getInsulationMaterial());
        Materials.build(srv, material, labelDefSelBoundary);
    }

    /**
     * Method builds component couplings for coil elements (half-turns)
     *
     * @param srv - input SigmaServer instance
     */
//    public void buildComponentCouplings(SigmaServer srv) {
//        DefinitionsAPI.createComponentCoupling(srv, labelIntOp, DefinitionsAPI.ComponentCouplingType.Integration);
//        DefinitionsAPI.assignSelection(srv, labelIntOp, String.format("%s_%s_dom", MPHC.LABEL_GEOM, label));
//        for (HalfturnBlockMPH halfturnBlock : halfturnBlocks) {
//            halfturnBlock.buildComponentCouplings(srv);
//        }
//    }

    /**
     * Method builds a coil insulation selection
     *
     * @param srv - input SigmaServer instance
     */
    public void buildInsulationSelection(SigmaServer srv) {
        // Build Inter-turn Selection
        String[] labelHalfturns = getHalfturnPropertyString(HalfturnMPH::getLabel);
        GeometryAPI.adjacentSelection(srv, labelGeomSelInsInterTurn, labelHalfturns);
        // Build Coil Boundaries Selection
        GeometryAPI.differenceSelection(srv, labelGeomSelInsBoundary, new String[]{label}, new String[]{labelGeomSelInsInterTurn});
    }

    protected void buildPhysics(SigmaServer srv) {
        String[] thicknessInterTurn = new String[]{Double.toString(2 * cable.wInsulWide)};
        HeatTransferInSolidsPhysicsAPI.buildThickLayer(srv, label + "_InsInterTurn", labelDefSelInsInterTurn, thicknessInterTurn);
        String[] thicknessExternalBoundary = new String[]{Double.toString(cable.wInsulWide)};
        HeatTransferInSolidsPhysicsAPI.buildThickLayer(srv, label + "_InsExtBound", labelDefSelInsExtBoundary, thicknessExternalBoundary);
    }

    // *****************************************************************************************************************
    // Lambda functions
    // *****************************************************************************************************************
    protected String[] getHalfturnPropertyString(Function<HalfturnMPH, String> function) {
        ArrayList<String> labelHalfturnsList = new ArrayList<>();

        for (int b_i = 0; b_i < halfturnBlocks.length; b_i++) {
            String[] labelLocal = halfturnBlocks[b_i].getHalfturnPropertyString(function);
            for (String label_i : labelLocal) {
                labelHalfturnsList.add(label_i);
            }
        }
        return labelHalfturnsList.toArray(new String[labelHalfturnsList.size()]);
    }

    protected Double[] getHalfturnPropertyDouble(Function<HalfturnMPH, Double> function) {
        ArrayList<Double> labelHalfturnsList = new ArrayList<>();

        for (int b_i = 0; b_i < halfturnBlocks.length; b_i++) {
            Double[] labelLocal = halfturnBlocks[b_i].getHalfturnPropertyDouble(function);
            for (Double label_i : labelLocal) {
                labelHalfturnsList.add(label_i);
            }
        }
        return labelHalfturnsList.toArray(new Double[labelHalfturnsList.size()]);
    }

    protected String getWindingPropertyString(Function<WindingMPH, String> function) {
        return function.apply(this);
    }

    // *****************************************************************************************************************
    // Getters
    // *****************************************************************************************************************
    public String getLabel() {
        return label;
    }
}



