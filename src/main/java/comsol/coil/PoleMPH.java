package comsol.coil;

import comsol.constants.MPHC;
import model.geometry.coil.Pole;
import model.geometry.coil.Winding;
import server.SigmaServer;

import java.util.ArrayList;
import java.util.function.Function;

/**
 * Class contains a set of methods for generating a pole in COMSOL with SIGMA Server
 */
public class PoleMPH {
    private final String label;
    private final WindingMPH[] windings;

    /**
     * Constroctor of a PoleMPH object with a given root label and pole definition
     *
     * @param label - pole label
     * @param pole  - pole definition
     */
    public PoleMPH(String label, Pole pole) {
        this.label = label;
        ArrayList<WindingMPH> windingMPHList = new ArrayList<>();
        for (int w_i = 0; w_i < pole.getWindings().length; w_i++) {
            String labelWinding = String.format("%s_%s%d", this.label, MPHC.MARKER_WINDING, w_i);
            Winding winding = pole.getWindings()[w_i];
            windingMPHList.add(new WindingMPH(labelWinding, winding));
        }
        this.windings = windingMPHList.toArray(new WindingMPH[pole.getWindings().length]);
    }

    protected void buildGeometry(SigmaServer srv) {
        for (WindingMPH winding : windings) {
            winding.buildGeometry(srv);
        }
    }

    protected void buildSelection(SigmaServer srv) {
        for (WindingMPH winding : windings) {
            winding.buildSelection(srv);
        }
    }

    /**
     * Method builds insulation selection for each winding in a pole
     *
     * @param srv - input SigmaServer instance
     */
    public void buildInsulationSelection(SigmaServer srv) {
        for (WindingMPH winding : windings) {
            winding.buildInsulationSelection(srv);
        }
    }

    protected void buildCable(SigmaServer srv) {
        for (WindingMPH winding : windings) {
            winding.buildCable(srv);
        }
    }

//    protected void buildDefinitions(SigmaServer srv) {
//        for (WindingMPH winding : windings) {
//            winding.buildDefinitions(srv);
//        }
//    }

    /**
     * Method builds component couplings for each winding in a pole
     *
     * @param srv - input SigmaServer instance
     */
//    public void buildComponentCouplings(SigmaServer srv) {
//        for (WindingMPH winding : windings) {
//            winding.buildComponentCouplings(srv);
//        }
//    }

    protected void buildPhysics(SigmaServer srv) {
        for (WindingMPH winding : windings) {
            winding.buildPhysics(srv);
        }
    }

    // *****************************************************************************************************************
    // Lambda functions
    // *****************************************************************************************************************

    protected String[] getHalfturnPropertyString(Function<HalfturnMPH, String> function) {
        ArrayList<String> labelHalfturnsList = new ArrayList<>();

        for (int w_i = 0; w_i < windings.length; w_i++) {
            String[] labelLocal = windings[w_i].getHalfturnPropertyString(function);
            for (String label_i : labelLocal) {
                labelHalfturnsList.add(label_i);
            }
        }
        return labelHalfturnsList.toArray(new String[labelHalfturnsList.size()]);
    }

    protected String[] getWindingPropertyString(Function<WindingMPH, String> function) {
        ArrayList<String> labelWindingsList = new ArrayList<>();

        for (int w_i = 0; w_i < windings.length; w_i++) {
            labelWindingsList.add(windings[w_i].getWindingPropertyString(function));
        }
        return labelWindingsList.toArray(new String[labelWindingsList.size()]);
    }

    protected Double[] getHalfturnPropertyDouble(Function<HalfturnMPH, Double> function) {
        ArrayList<Double> labelHalfturnsList = new ArrayList<>();

        for (int w_i = 0; w_i < windings.length; w_i++) {
            Double[] labelLocal = windings[w_i].getHalfturnPropertyDouble(function);
            for (Double label_i : labelLocal) {
                labelHalfturnsList.add(label_i);
            }
        }
        return labelHalfturnsList.toArray(new Double[labelHalfturnsList.size()]);
    }

    // *****************************************************************************************************************
    // Getters
    // *****************************************************************************************************************

    public String getLabel() {
        return label;
    }
}
