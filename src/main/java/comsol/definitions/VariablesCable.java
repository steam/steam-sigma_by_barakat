package comsol.definitions;

import comsol.definitions.utils.VariableGroup;
import comsol.definitions.utils.Variable;
import model.geometry.coil.Cable;
import comsol.constants.MPHC;
import model.materials.database.MatDatabase;

import java.util.ArrayList;

/**
 * Class contains parameters needed to define variables of a cable
 */
public class VariablesCable {

    public static final String WARNING_NOT_SUPPORTED_MATERIAL = "Warning: %s not supported as insulation material!";

    private VariablesCable() {
    }

    /**
     * Method defines a variable group with all variables used to characterize a superconducting cable
     *
     * @param labelInput - label of the variable group
     * @param cable      - input cable parameters
     * @return group of variables to be injected to the magnet component of a model
     */
    public static VariableGroup defineVariablesGroup(String labelInput, Cable cable) {

        ArrayList<Variable> variables = new ArrayList<>();

        String labelLocal;
        String valueLocal;

        labelLocal = MPHC.LABEL_CABLE_THICHNESS_INSULATION_NARROWEDGE;
        valueLocal = defineWidthInsulationEdgeNarrow(cable.wInsulNarrow);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_THICHNESS_INSULATION_WIDEEDGE;
        valueLocal = defineWidthInsulationEdgeWide(cable.wInsulWide);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_DIAMETER_FILAMENT;
        valueLocal = defineDiameterFilament(cable.dFilament);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_DIAMETER_STRAND;
        valueLocal = defineDiameterStrand(cable.dstrand);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_FRACTION_CU;
        valueLocal = defineFractionCopper(cable.fracCu);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_FRACTION_SC;
        valueLocal = defineFractionSuperconductor(cable.fracSc);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_RRR;
        valueLocal = defineRRR(cable.RRR);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_T_REF_RRR;
        valueLocal = defineTupReferenceForRRR(cable.TupRRR);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_RC;
        valueLocal = defineRc(cable.Rc);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_RA;
        valueLocal = defineRa(cable.Ra);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_TWISTPITCH_FILAMENT;
        valueLocal = defineTwistPitchFilament(cable.lTp);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_FRHO_EFF;
        valueLocal = defineFactorRhoEffective(cable.fRhoEff);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_WIDTH_BARE;
        valueLocal = defineWidthBareCable(cable.wBare);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_HEIGHT_INNER_BARE;
        valueLocal = defineInnerHeightBareCable(cable.hInBare);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_HEIGHT_OUTER_BARE;
        valueLocal = defineOuterHeightBareCable(cable.hOutBare);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_HEIGHT_AVERAGE_BARE;
        valueLocal = defineAverageHeightBareCable(cable.hInBare, cable.hOutBare);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_WIDTH_CORE;
        valueLocal = defineWidthCore(cable.wCore);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_HEIGHT_CORE;
        valueLocal = defineHeightCore(cable.hCore);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_NUMBER_STRANDS;
        valueLocal = defineNoOfStrands(cable.noOfStrands);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_NUMBER_STRANDSPERLAYER;
        valueLocal = defineNoOfStrandsPerLayer(cable.noOfStrandsPerLayer);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_NUMBER_LAYERSOFSTRANDS;
        valueLocal = defineNoOfLayersOfStrands(cable.noOfLayers);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_TWISTPITCH_STRAND;
        valueLocal = defineTwistPitchStrand(cable.lTpStrand);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_THETA_TWISTPITCH_STRAND;
        valueLocal = defineThetaTwistPitchStrand(cable.thetaTpStrand);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_C1;
        valueLocal = defineC1NbTi(cable.C1);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_C2;
        valueLocal = defineC2NbTi(cable.C2);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_FRACTION_HE;
        valueLocal = defineFractionHelium(cable.fracHe);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_FILLFACTOR_INNERVOIDS;
        valueLocal = defineFractionFillingInnerVoids(cable.fracFillInnerVoids);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_FILLFACTOR_OUTERVOIDS;
        valueLocal = defineFractionFillingOuterVoids(cable.fractFillOuterVoids);
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_TAU_ISCC_CROSSOVER;
        valueLocal = defineTauISCCxOver();
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_TAU_ISCC_ADJW;
        valueLocal = defineTauISCCadjW();
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_TAU_ISCC_ADJN;
        valueLocal = defineTauISCCadjN();
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_TAU_IFCC;
        valueLocal = defineTauIFCC();
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_SURFACE_BARE;
        valueLocal = defineSurfaceBare();
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_SURFACE_CONDUCTOR;
        valueLocal = defineSurfaceConductor();
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_SURFACE_CORE;
        valueLocal = defineSurfaceCore();
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_SURFACE_INSULATION;
        valueLocal = defineSurfaceInsulation();
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_SURFACE_VOIDS;
        valueLocal = defineSurfaceVoids();
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_SURFACE_INNERVOIDS;
        valueLocal = defineSurfaceInnerVoids();
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_SURFACE_OUTERVOIDS;
        valueLocal = defineSurfaceOuterVoids();
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_RHO_STABILIZER;
        valueLocal = defineRhoCu(cable.getResitivityCopperFit());
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_RHO_STABILIZER_ZEROB;
        valueLocal = defineRhoCuZeroB(cable.getResitivityCopperFit());
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_I_CRITICAL;
        valueLocal = defineIc(cable.getCriticalSurfaceFit());
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_CV_SC_MAT;
        valueLocal = defineCvSuperconductorMat(cable.getCriticalSurfaceFit());
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_CV_CU_MAT;
        valueLocal = defineCvCopperMat(cable.getResitivityCopperFit());
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_CV_HE_MAT;
        valueLocal = defineCvHeliumMat();
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_CV_CORE_MAT;
        valueLocal = defineCvCoreMat(cable.getMaterialCore());
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_CV_INSULATION_MAT;
        valueLocal = defineCvInsulationMat(cable.getInsulationMaterial());
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_CV_INNERVOIDS_MAT;
        valueLocal = defineCvVoidsMat(cable.getMaterialInnerVoids());
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_CV_OUTERVOIDS_MAT;
        valueLocal = defineCvVoidsMat(cable.getMaterialOuterVoids());
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_CV_CONDUCTOR;
        valueLocal = defineCvConductor();
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_CV_CORE;
        valueLocal = defineCvCore();
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_CV_INSULATION;
        valueLocal = defineCvInsulation();
        variables.add(new Variable(labelLocal, valueLocal));

        labelLocal = MPHC.LABEL_CABLE_CV_VOIDS;
        valueLocal = defineCvVoids();
        variables.add(new Variable(labelLocal, valueLocal));

        return new VariableGroup(labelInput, variables);
    }


    // *****************************************************************************************************************
    // Definitions (with TEST unit)
    // *****************************************************************************************************************

    // Geometry
    // *****************************************************************************************************************

    /**
     * Method defines the width of insulation of the narrow edge of a cable
     *
     * @param value - insulation width
     * @return value [unit] String
     */
    public static String defineWidthInsulationEdgeNarrow(double value) {
        return String.format("%s[m]", Double.toString(value));
    }

    /**
     * Method defines the width of insulation of the wide edge of a cable
     *
     * @param value - insulation width
     * @return value [unit] String
     */
    public static String defineWidthInsulationEdgeWide(double value) {
        return String.format("%s[m]", Double.toString(value));
    }

    /**
     * Method defines the diameter of a filament
     *
     * @param value - filament diameter
     * @return value [unit] String
     */
    public static String defineDiameterFilament(double value) {
        return String.format("%s[m]", Double.toString(value));
    }

    /**
     * Method defines the twist pitch of a filament
     *
     * @param value - filament twist pitch
     * @return value [unit] String
     */
    public static String defineTwistPitchFilament(double value) {
        return String.format("%s[m]", Double.toString(value));
    }

    /**
     * Method defines the diameter of a strand
     *
     * @param value - strand diameter
     * @return value [unit] String
     */
    public static String defineDiameterStrand(double value) {
        return String.format("%s[m]", Double.toString(value));
    }

    /**
     * Method defines the twist pitch angle of a strand
     *
     * @param value - strand twist pitch angle
     * @return value [unit] String
     */
    public static String defineTwistPitchStrand(double value) {
        return Double.toString(value) + "[m]";
    }

    /**
     * Method defines the twist pitch angle of a strand
     *
     * @param value - strand twist pitch angle
     * @return value [unit] String
     */
    public static String defineThetaTwistPitchStrand(double value) {
        return String.format("%s[1]", Double.toString(value));
    }

    /**
     * Method defines the width of a bare (uninsulated) cable
     *
     * @param value - bare cable width
     * @return value [unit] String
     */
    public static String defineWidthBareCable(double value) {
        return String.format("%s[m]", Double.toString(value));
    }

    /**
     * Method defines the inner height of a bare cable (considering trapezoidal cable cross-section)
     *
     * @param value - inner height of a bare cable
     * @return value [unit] String
     */
    public static String defineInnerHeightBareCable(double value) {
        return String.format("%s[m]", Double.toString(value));
    }

    /**
     * Method defines the inner height of a bare cable (considering trapezoidal cable cross-section)
     *
     * @param value - inner height of a bare cable
     * @return value [unit] String
     */
    public static String defineOuterHeightBareCable(double value) {
        return String.format("%s[m]", Double.toString(value));
    }

    /**
     * Method defines the average height of a bare cable (considering trapezoidal cable cross-section)
     *
     * @param hIn  - inner height of a bare cable
     * @param hOut - outer height of a bare cable
     * @return value [unit] String
     */
    public static String defineAverageHeightBareCable(double hIn, double hOut) {
        String value = Double.toString((hIn + hOut) / 2);
        return String.format("%s[m]", value);
    }

    /**
     * Method defines the width of the core of a cable
     *
     * @param value - core width
     * @return value [unit] String
     */
    public static String defineWidthCore(double value) {
        return String.format("%s[m]", value);
    }

    /**
     * Method defines the height of the core of a cable
     *
     * @param value - core height
     * @return value [unit] String
     */
    public static String defineHeightCore(double value) {
        return String.format("%s[m]", value);
    }

    /**
     * Method defines the number of strands in cable
     *
     * @param value - number of strands
     * @return value [unit] String
     */
    public static String defineNoOfStrands(double value) {
        return String.format("%s[1]", Double.toString(value));
    }

    /**
     * Method defines the number of strands per layer of a cable
     *
     * @param value - number of strands per layer
     * @return value [unit] String
     */
    public static String defineNoOfStrandsPerLayer(double value) {
        return Double.toString(value) + "[1]";
    }

    /**
     * Method defines the number of layers of strands of a cable
     *
     * @param value - number of layers of strands
     * @return value [unit] String
     */
    public static String defineNoOfLayersOfStrands(double value) {
        return Double.toString(value) + "[1]";
    }

    // Areas
    // *****************************************************************************************************************

    /**
     * Method defines the expression for the area of a bare cable
     *
     * @return mathematical expression (see tests)
     */
    public static String defineSurfaceBare() {
        return String.format("%s*%s", MPHC.LABEL_CABLE_WIDTH_BARE, MPHC.LABEL_CABLE_HEIGHT_AVERAGE_BARE);
    }

    /**
     * Method defines the expression for the area of the conductor in a cable
     *
     * @return mathematical expression (see tests)
     */
    public static String defineSurfaceConductor() {
        return String.format("(1/cos(%s))*%s*pi*(%s/2)^2",
                MPHC.LABEL_CABLE_THETA_TWISTPITCH_STRAND,
                MPHC.LABEL_CABLE_NUMBER_STRANDS,
                MPHC.LABEL_CABLE_DIAMETER_STRAND);
    }

    /**
     * Method defines the expression for the area of the core of a cable
     *
     * @return mathematical expression (see tests)
     */
    public static String defineSurfaceCore() {
        return String.format("%s*%s", MPHC.LABEL_CABLE_WIDTH_CORE, MPHC.LABEL_CABLE_HEIGHT_CORE);
    }

    /**
     * Method defines the expression for the area of voids in a cable
     *
     * @return mathematical expression (see tests)
     */
    public static String defineSurfaceVoids() {
        return String.format("%s-(%s+%s)", MPHC.LABEL_CABLE_SURFACE_BARE, MPHC.LABEL_CABLE_SURFACE_CONDUCTOR, MPHC.LABEL_CABLE_SURFACE_CORE);
    }

    /**
     * Method defines the expression for the area of cable insulation
     *
     * @return mathematical expression (see tests)
     */
    public static String defineSurfaceInsulation() {
        return String.format("(%s+2*%s)*(%s+2*%s)-(%s*%s)",
                MPHC.LABEL_CABLE_WIDTH_BARE,
                MPHC.LABEL_CABLE_THICHNESS_INSULATION_NARROWEDGE,
                MPHC.LABEL_CABLE_HEIGHT_AVERAGE_BARE,
                MPHC.LABEL_CABLE_THICHNESS_INSULATION_WIDEEDGE,
                MPHC.LABEL_CABLE_WIDTH_BARE,
                MPHC.LABEL_CABLE_HEIGHT_AVERAGE_BARE);
    }

    /**
     * Method defines the expression for the area of inner voids in a cable
     *
     * @return mathematical expression (see tests)
     */
    public static String defineSurfaceInnerVoids() {
        String scaleFactor = String.format("(%s-1)*(%s-1)/(%s*%s)",
                MPHC.LABEL_CABLE_NUMBER_STRANDSPERLAYER,
                MPHC.LABEL_CABLE_NUMBER_LAYERSOFSTRANDS,
                MPHC.LABEL_CABLE_NUMBER_STRANDSPERLAYER,
                MPHC.LABEL_CABLE_NUMBER_LAYERSOFSTRANDS);
        return String.format("%s*%s", scaleFactor, MPHC.LABEL_CABLE_SURFACE_VOIDS);
    }

    /**
     * Method defines the expression for the area of outer voids in a cable
     *
     * @return mathematical expression (see tests)
     */
    public static String defineSurfaceOuterVoids() {
        String scaleFactor = String.format("(%s+%s-1)/(%s*%s)",
                MPHC.LABEL_CABLE_NUMBER_STRANDSPERLAYER,
                MPHC.LABEL_CABLE_NUMBER_LAYERSOFSTRANDS,
                MPHC.LABEL_CABLE_NUMBER_STRANDSPERLAYER,
                MPHC.LABEL_CABLE_NUMBER_LAYERSOFSTRANDS);
        return String.format("%s*%s", scaleFactor, MPHC.LABEL_CABLE_SURFACE_VOIDS);
    }


    // Fractions
    // *****************************************************************************************************************

    /**
     * Method defines the fraction of copper in the cable
     *
     * @param value fraction of copper
     * @return value [unit] String
     */
    public static String defineFractionCopper(double value) {
        return String.format("%s[1]", Double.toString(value));
    }

    /**
     * Method defines the fraction of superconductor in the cable
     *
     * @param value fraction of superconductor
     * @return value [unit] String
     */
    public static String defineFractionSuperconductor(double value) {
        return String.format("%s[1]", Double.toString(value));
    }

    /**
     * Method defines the fraction of helium in the cable
     *
     * @param value fraction of helium
     * @return value [unit] String
     */
    public static String defineFractionHelium(double value) {
        return Double.toString(value) + "[1]";
    }

    /**
     * Method defines the fraction of filling in inner voids in the cable
     *
     * @param value fraction of filling in inner voids
     * @return value [unit] String
     */
    public static String defineFractionFillingInnerVoids(double value) {
        return Double.toString(value) + "[1]";
    }

    /**
     * Method defines the fraction of filling in outer voids in the cable
     *
     * @param value fraction of filling in outer voids
     * @return value [unit] String
     */
    public static String defineFractionFillingOuterVoids(double value) {
        return Double.toString(value) + "[1]";
    }

    // Resistance
    // *****************************************************************************************************************

    /**
     * Method defines the Residual Rsistivity Ratio (RRR)
     *
     * @param value - RRR
     * @return value [unit] String
     */
    public static String defineRRR(double value) {
        return String.format("%s[1]", Double.toString(value));
    }

    /**
     * Method defines the upper temperature for the RRR reference
     *
     * @param value - the upper temperature for the RRR reference
     * @return value [unit] String
     */
    public static String defineTupReferenceForRRR(double value) {
        return String.format("%s[K]", Double.toString(value));
    }

    /**
     * Method defines the cross-contact resistance of strands (Rc)
     *
     * @param value - Rc value
     * @return value [unit] String
     */
    public static String defineRc(double value) {
        return String.format("%s[ohm]", Double.toString(value));
    }

    /**
     * Method defines the adjacent-contact resistance of strands (Ra)
     *
     * @param value - Ra value
     * @return value [unit] String
     */
    public static String defineRa(double value) {
        return String.format("%s[ohm]", Double.toString(value));
    }

    /**
     * Method defines the effective resistivity factor
     *
     * @param value - the effective resistivity factor
     * @return value [unit] String
     */
    public static String defineFactorRhoEffective(double value) {
        return String.format("%s[1]", Double.toString(value));
    }

    /**
     * Method defines the C1 parameter for the NbTi critical surfacefit
     *
     * @param value - C1 value
     * @return value [unit] String
     */
    public static String defineC1NbTi(double value) {
        return Double.toString(value) + "[A]";
    }

    /**
     * Method defines the C2 parameter for the NbTi critical surfacefit
     *
     * @param value - C2 value
     * @return value [unit] String
     */
    public static String defineC2NbTi(double value) {
        return Double.toString(value) + "[A/T]";
    }

    // Time constants
    // *****************************************************************************************************************

    /**
     * Method defines the expression for the time constant of the cross-over inter-strand coupling currents
     *
     * @return mathematical expression (see tests)
     */
    public static String defineTauISCCxOver() {
        return String.format("%s/120*%s*%s*(%s-1)*%s/%s*1/%s",
                MPHC.LABEL_MU0,
                MPHC.LABEL_CABLE_TWISTPITCH_STRAND,
                MPHC.LABEL_CABLE_NUMBER_STRANDS,
                MPHC.LABEL_CABLE_NUMBER_STRANDS,
                MPHC.LABEL_CABLE_WIDTH_BARE,
                MPHC.LABEL_CABLE_HEIGHT_AVERAGE_BARE,
                MPHC.LABEL_CABLE_RC);
    }

    /**
     * Method defines the expression for the time constant of the adjacent narrow-cable-edge inter-strand coupling currents
     *
     * @return mathematical expression (see tests)
     */
    public static String defineTauISCCadjN() {
        return String.format("%s/8*%s*%s/%s*1/%s",
                MPHC.LABEL_MU0,
                MPHC.LABEL_CABLE_TWISTPITCH_STRAND,
                MPHC.LABEL_CABLE_HEIGHT_AVERAGE_BARE,
                MPHC.LABEL_CABLE_WIDTH_BARE,
                MPHC.LABEL_CABLE_RA);
    }

    /**
     * Method defines the expression for the time constant of the adjacent wide-cable-edge inter-strand coupling currents
     *
     * @return mathematical expression (see tests)
     */
    public static String defineTauISCCadjW() {
        String cookFactorWilson = "2";
        return String.format("%s*%s/6*%s*%s/%s*1/%s",
                cookFactorWilson,
                MPHC.LABEL_MU0,
                MPHC.LABEL_CABLE_TWISTPITCH_STRAND,
                MPHC.LABEL_CABLE_WIDTH_BARE,
                MPHC.LABEL_CABLE_HEIGHT_AVERAGE_BARE,
                MPHC.LABEL_CABLE_RA);
    }

    /**
     * Method defines the expression for the time constant of the inter-filament coupling currents
     *
     * @return mathematical expression (see tests)
     */
    public static String defineTauIFCC() {
        return String.format("%s*(%s/(2*%s))^2*1/(%s*%s)",
                MPHC.LABEL_MU0,
                MPHC.LABEL_CABLE_TWISTPITCH_FILAMENT,
                MPHC.LABEL_PI,
                MPHC.LABEL_CABLE_RHO_STABILIZER,
                MPHC.LABEL_CABLE_FRHO_EFF);
    }

    // Fits
    // *****************************************************************************************************************

    /**
     * Method defines a fit for the copper resistivity
     *
     * @param chosenFit input fit selection
     * @return mathematical expression (see tests)
     */
    public static String defineRhoCu(Cable.ResitivityCopperFitEnum chosenFit) {
        String fitName;
        switch (chosenFit) {
            case rho_Cu_CUDI:
                fitName = MPHC.LABEL_CFUN_RHO_CU_CUDI;
                return String.format("%s(%s[1/K],%s[1/T],%s,%s[1/K])[ohm*m]",
                        fitName,
                        MPHC.LABEL_PHYSICSTH_T,
                        MPHC.LABEL_PHYSICSMF_B,
                        MPHC.LABEL_CABLE_RRR,
                        MPHC.LABEL_CABLE_T_REF_RRR);
            case rho_Cu_NIST:
                fitName = MPHC.LABEL_CFUN_RHO_CU_NIST;
                return String.format("%s(%s[1/K],%s[1/T],%s,%s[1/K])[ohm*m]",
                        fitName,
                        MPHC.LABEL_PHYSICSTH_T,
                        MPHC.LABEL_PHYSICSMF_B,
                        MPHC.LABEL_CABLE_RRR,
                        MPHC.LABEL_CABLE_T_REF_RRR);
            case rho_Cu_TEST:
                return "1e-8/pi[S/m]";
            default:
                throw new RuntimeException(String.format(WARNING_NOT_SUPPORTED_MATERIAL, chosenFit.toString()));
        }
    }

    /**
     * Method defines a fit for the copper resistivity at zero magnetic flux density
     *
     * @param chosenFit input fit selection
     * @return mathematical expression (see tests)
     */
    public static String defineRhoCuZeroB(Cable.ResitivityCopperFitEnum chosenFit) {
        String fitName;
        switch (chosenFit) {
            case rho_Cu_CUDI:
                fitName = MPHC.LABEL_CFUN_RHO_CU_CUDI;
                return String.format("%s(%s[1/K],0,%s,%s[1/K])[ohm*m]", fitName, MPHC.LABEL_PHYSICSTH_T, MPHC.LABEL_CABLE_RRR, MPHC.LABEL_CABLE_T_REF_RRR);
            case rho_Cu_NIST:
                fitName = MPHC.LABEL_CFUN_RHO_CU_NIST;
                return String.format("%s(%s[1/K],0,%s,%s[1/K])[ohm*m]", fitName, MPHC.LABEL_PHYSICSTH_T, MPHC.LABEL_CABLE_RRR, MPHC.LABEL_CABLE_T_REF_RRR);
            case rho_Cu_TEST:
                return "1/6e9[S/m]";
            default:
                throw new RuntimeException(String.format(WARNING_NOT_SUPPORTED_MATERIAL, chosenFit.toString()));
        }
    }

    /**
     * Method defines a fit for the critical current density
     *
     * @param chosenFit input fit selection
     * @return mathematical expression (see tests)
     */
    public static String defineIc(Cable.CriticalSurfaceFitEnum chosenFit) {
        String fitName;
        switch (chosenFit) {
            case Ic_NbTi_GSI:
                fitName = MPHC.LABEL_CFUN_ICRIT_NBTI_GSI;
                return String.format("%s(%s[1/K],%s[1/T],%s*%s[1/m^2])[A]",
                        fitName,
                        MPHC.LABEL_PHYSICSTH_T,
                        MPHC.LABEL_PHYSICSMF_B,
                        MPHC.LABEL_CABLE_FRACTION_SC,
                        MPHC.LABEL_CABLE_SURFACE_CONDUCTOR);
            case Ic_NbTi_D2:
                fitName = MPHC.LABEL_CFUN_ICRIT_NBTI_D2;
                return String.format("%s(%s[1/K],%s[1/T],%s*%s[1/m^2])[A]",
                        fitName,
                        MPHC.LABEL_PHYSICSTH_T,
                        MPHC.LABEL_PHYSICSMF_B,
                        MPHC.LABEL_CABLE_FRACTION_SC,
                        MPHC.LABEL_CABLE_SURFACE_CONDUCTOR);
            case Ic_Nb3Sn_NIST:
                fitName = MPHC.LABEL_CFUN_ICRIT_NB3SN_HILUMI;
                return String.format("%s(%s[1/K],%s[1/T],%s*%s[1/m^2])[A]",
                        fitName,
                        MPHC.LABEL_PHYSICSTH_T,
                        MPHC.LABEL_PHYSICSMF_B,
                        MPHC.LABEL_CABLE_FRACTION_SC,
                        MPHC.LABEL_CABLE_SURFACE_CONDUCTOR);
            case Ic_Nb3Sn_FCC:
                fitName = MPHC.LABEL_CFUN_ICRIT_NB3SN_FCC;
                return String.format("%s(%s[1/K],%s[1/T],%s*%s[1/m^2])[A]",
                        fitName,
                        MPHC.LABEL_PHYSICSTH_T,
                        MPHC.LABEL_PHYSICSMF_B,
                        MPHC.LABEL_CABLE_FRACTION_SC,
                        MPHC.LABEL_CABLE_SURFACE_CONDUCTOR);
            case Ic_TEST:
                return "50000[A]";
            default:
                throw new RuntimeException(String.format(WARNING_NOT_SUPPORTED_MATERIAL, chosenFit.toString()));
        }
    }

    // Cv Materials
    // ***************************************************************0*************************************************

    //todo: find a more elegant solution
    // Resisitivity fit enum is used also for defining the Cv. In this way, we can discriminate the Cv of Cu_TEST

    /**
     * Method defines a fit for the copper heat capacity
     *
     * @param chosenFit input fit selection
     * @return mathematical expression (see tests)
     */
    public static String defineCvCopperMat(Cable.ResitivityCopperFitEnum chosenFit) {
        switch (chosenFit) {
            case rho_Cu_CUDI:
                return String.format("%s(%s[1/K])[J/(m^3*K)]", MPHC.LABEL_CFUN_CV_CU, MPHC.LABEL_PHYSICSTH_T);
            case rho_Cu_NIST:
                return String.format("%s(%s[1/K])[J/(m^3*K)]", MPHC.LABEL_CFUN_CV_CU, MPHC.LABEL_PHYSICSTH_T);
            case rho_Cu_TEST:
                return "500[J/m^3/K]";
            default:
                throw new RuntimeException(String.format(WARNING_NOT_SUPPORTED_MATERIAL, chosenFit.toString()));
        }
    }

    /**
     * Method defines a fit for the superconductor heat capacity
     *
     * @param chosenFit input fit selection
     * @return mathematical expression (see tests)
     */
    public static String defineCvSuperconductorMat(Cable.CriticalSurfaceFitEnum chosenFit) {
        switch (chosenFit) {
            case Ic_NbTi_GSI:
                return String.format("%s(%s[1/K],%s[1/T],%s[1/A],%s[1/A],%s[T/A])[J/m^3/K]",
                        MPHC.LABEL_CFUN_CV_NBTI,
                        MPHC.LABEL_PHYSICSTH_T,
                        MPHC.LABEL_PHYSICSMF_B,
                        MPHC.LABEL_HT_I_SOURCE,
                        MPHC.LABEL_CABLE_C1,
                        MPHC.LABEL_CABLE_C2);
            case Ic_NbTi_D2:
                return String.format("%s(%s[1/K],%s[1/T],%s[1/A],%s[1/A],%s[T/A])[J/m^3/K]",
                        MPHC.LABEL_CFUN_CV_NBTI,
                        MPHC.LABEL_PHYSICSTH_T,
                        MPHC.LABEL_PHYSICSMF_B,
                        MPHC.LABEL_HT_I_SOURCE,
                        MPHC.LABEL_CABLE_C1,
                        MPHC.LABEL_CABLE_C2);
            case Ic_Nb3Sn_NIST:
                return String.format("%s(%s[1/K],%s[1/T])[J/m^3/K]",
                        MPHC.LABEL_CFUN_CV_NB3SN,
                        MPHC.LABEL_PHYSICSTH_T,
                        MPHC.LABEL_PHYSICSMF_B);
            case Ic_Nb3Sn_FCC:
                return String.format("%s(%s[1/K],%s[1/T])[J/m^3/K]",
                        MPHC.LABEL_CFUN_CV_NB3SN,
                        MPHC.LABEL_PHYSICSTH_T,
                        MPHC.LABEL_PHYSICSMF_B);
            case Ic_TEST:
                return "250[J/m^3/K]";
            default:
                throw new RuntimeException(String.format(WARNING_NOT_SUPPORTED_MATERIAL, chosenFit.toString()));
        }
    }

    /**
     * Method defines a fit for the cable core heat capacity
     *
     * @param mat input fit selection
     * @return mathematical expression (see tests)
     */
    public static String defineCvCoreMat(MatDatabase mat) {
        switch (mat) {
            case MAT_STEEL:
                return String.format("%s(%s[1/K])[J/m^3/K]", MPHC.LABEL_CFUN_CV_STEEL, MPHC.LABEL_PHYSICSTH_T);
            case MAT_KAPTON:
                return String.format("%s(%s[1/K])[J/m^3/K]", MPHC.LABEL_CFUN_CV_KAPTON, MPHC.LABEL_PHYSICSTH_T);
            case MAT_GLASSFIBER:
                return String.format("%s(%s[1/K])[J/m^3/K]", MPHC.LABEL_CFUN_CV_G10, MPHC.LABEL_PHYSICSTH_T);
            case MAT_INSULATION_TEST:
                return "750[J/m^3/K]";
            case MAT_VOID:
                return "0[J/m^3/K]";
            default:
                throw new RuntimeException(String.format(WARNING_NOT_SUPPORTED_MATERIAL, mat.toString()));
        }
    }

    /**
     * Method defines a fit for the cable insulation heat capacity
     *
     * @param mat input fit selection
     * @return mathematical expression (see tests)
     */
    public static String defineCvInsulationMat(MatDatabase mat) {
        String cpName = null;
        switch (mat) {
            case MAT_KAPTON:
                cpName = MPHC.LABEL_CFUN_CV_KAPTON;
                return String.format("%s(%s[1/K])[J/m^3/K]", cpName, MPHC.LABEL_PHYSICSTH_T);
            case MAT_GLASSFIBER:
                cpName = MPHC.LABEL_CFUN_CV_G10;
                return String.format("%s(%s[1/K])[J/m^3/K]", cpName, MPHC.LABEL_PHYSICSTH_T);
            case MAT_INSULATION_TEST:
                return "750[J/m^3/K]";
            case MAT_VOID:
                return "0[J/m^3/K]";
            default:
                throw new RuntimeException(String.format(WARNING_NOT_SUPPORTED_MATERIAL, mat.toString()));
        }
    }

    /**
     * Method defines a fit for the helium heat capacity
     *
     * @return mathematical expression (see tests)
     */
    public static String defineCvHeliumMat() {
        return String.format("%s(%s[1/K])[J/(m^3*K)]", MPHC.LABEL_CFUN_CV_HE, MPHC.LABEL_PHYSICSTH_T);
    }

    /**
     * Method defines a fit for the cable voids heat capacity
     *
     * @param mat - input material
     * @return mathematical expression (see tests)
     */
    public static String defineCvVoidsMat(MatDatabase mat) {
        switch (mat) {
            case MAT_KAPTON:
                return String.format("%s(%s[1/K])[J/m^3/K]", MPHC.LABEL_CFUN_CV_KAPTON, MPHC.LABEL_PHYSICSTH_T);
            case MAT_GLASSFIBER:
                return String.format("%s(%s[1/K])[J/m^3/K]", MPHC.LABEL_CFUN_CV_G10, MPHC.LABEL_PHYSICSTH_T);
            case MAT_INSULATION_TEST:
                return "750";
            case MAT_VOID:
                return "0[J/m^3/K]";
            default:
                throw new RuntimeException(String.format(WARNING_NOT_SUPPORTED_MATERIAL, mat.toString()));
        }
    }

    /**
     * Method defines a fit for the cable insulation heat capacity
     *
     * @param mat input fit selection
     * @return mathematical expression (see tests)
     */
    public static String defineCvQuenchHeaterMat(MatDatabase mat) {
        String cpName = null;
        switch (mat) {
            case MAT_KAPTON:
                cpName = "CFUN_Cvkapton";
                return String.format("%s(%s[1/K])[J/kg/K]", cpName, MPHC.LABEL_QUENCH_HEATER_TEMPERATURE);
            case MAT_G10:
                cpName = MPHC.LABEL_CFUN_CV_G10;
                return String.format("%s(%s[1/K])[J/m^3/K]", cpName, MPHC.LABEL_PHYSICSTH_T);
            case MAT_STEEL:
                return "750[J/m^3/K]";
            default:
                throw new RuntimeException(String.format(WARNING_NOT_SUPPORTED_MATERIAL, mat.toString()));
        }
    }



    // Cv Cable
    // ***************************************************************0*************************************************

    /**
     * Method defines a fit for the cable voids heat capacity
     *
     * @return mathematical expression (see tests)
     */
    public static String defineCvConductor() {
        String cvCuBare = String.format("(%s*%s)", MPHC.LABEL_CABLE_FRACTION_CU, MPHC.LABEL_CABLE_CV_CU_MAT);
        String cvScBare = String.format("(%s*%s)", MPHC.LABEL_CABLE_FRACTION_SC, MPHC.LABEL_CABLE_CV_SC_MAT);
        String cvHeBare = String.format("(%s*%s)", MPHC.LABEL_CABLE_FRACTION_HE, MPHC.LABEL_CABLE_CV_HE_MAT);
        return String.format("(%s+%s+%s)*%s", cvCuBare, cvScBare, cvHeBare, MPHC.LABEL_CABLE_SURFACE_CONDUCTOR);
    }

    /**
     * Method defines the heat capacity of cable core
     *
     * @return mathematical expression (see tests)
     */
    public static String defineCvCore() {
        return String.format("%s*%s", MPHC.LABEL_CABLE_CV_CORE_MAT, MPHC.LABEL_CABLE_SURFACE_CORE);
    }

    /**
     * Method defines heat capacity of the cable insulation
     *
     * @return mathematical expression (see tests)
     */
    public static String defineCvInsulation() {
        return String.format("(%s*%s)", MPHC.LABEL_CABLE_CV_INSULATION_MAT, MPHC.LABEL_CABLE_SURFACE_INSULATION);
    }

    /**
     * Method defines heat capacity of the cable voids
     *
     * @return mathematical expression (see tests)
     */
    public static String defineCvVoids() {
        String cvInVoids = String.format("(%s*%s*%s)",
                MPHC.LABEL_CABLE_FILLFACTOR_INNERVOIDS,
                MPHC.LABEL_CABLE_CV_INNERVOIDS_MAT,
                MPHC.LABEL_CABLE_SURFACE_INNERVOIDS);
        String cvOutVoids = String.format("(%s*%s*%s)",
                MPHC.LABEL_CABLE_FILLFACTOR_OUTERVOIDS,
                MPHC.LABEL_CABLE_CV_OUTERVOIDS_MAT,
                MPHC.LABEL_CABLE_SURFACE_OUTERVOIDS);
        return String.format("(%s+%s)", cvInVoids, cvOutVoids);
    }
}
