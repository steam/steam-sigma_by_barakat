package comsol.definitions;

import comsol.definitions.utils.VariableGroup;
import comsol.definitions.utils.Variable;
import comsol.constants.MPHC;

import java.util.ArrayList;

/**
 * Class contains parameters needed to define the physics engine
 */
public class VariablesPhysicsEngine {

    /**
     * Method defines a variable group with all variables used to characterize a superconducting magnet
     *
     * @param coilLabel - label of the variable group
     * @return group of variables to be injected to the magnet component of a model
     */
    public static VariableGroup defineVariables(String coilLabel) {

        ArrayList<Variable> variables = new ArrayList<>();

        String label;
        String value;

        label = MPHC.LABEL_JS;
        value = defineJs();
        variables.add(new Variable(label, value));

        label = MPHC.LABEL_TAU;
        value = defineTauDensity();
        variables.add(new Variable(label, value));

        label = MPHC.LABEL_K_SURFACE;
        value = defineKsurfScalingFactor();
        variables.add(new Variable(label, value));

        label = MPHC.LABEL_MX_IFCC;
        value = defineMIFCCx();
        variables.add(new Variable(label, value));

        label = MPHC.LABEL_MY_IFCC;
        value = defineMIFCCy();
        variables.add(new Variable(label, value));

        label = MPHC.LABEL_MX_PERS;
        value = defineMPersx();
        variables.add(new Variable(label, value));

        label = MPHC.LABEL_MY_PERS;
        value = defineMPersy();
        variables.add(new Variable(label, value));

        label = MPHC.LABEL_MX;
        value = defineMEqx();
        variables.add(new Variable(label, value));

        label = MPHC.LABEL_MY;
        value = defineMEqy();
        variables.add(new Variable(label, value));

        label = MPHC.LABEL_QUENCHSTATE;
        value = definequenchState();
        variables.add(new Variable(label, value));

        label = MPHC.LABEL_Q_IFCC;
        value = defineQIFCC();
        variables.add(new Variable(label, value));

        label = MPHC.LABEL_Q_OHM;
        value = defineQOhm();
        variables.add(new Variable(label, value));

        label = MPHC.LABEL_Q_TOT;
        value = defineQtot();
        variables.add(new Variable(label, value));

        label = MPHC.LABEL_R;
        value = defineR();
        variables.add(new Variable(label, value));

        label = MPHC.LABEL_PHI;
        value = definePhi();
        variables.add(new Variable(label, value));

        label = MPHC.LABEL_V_DELTA;
        value = defineVdelta();
        variables.add(new Variable(label, value));

        label = MPHC.LABEL_TAU_CC;
        value = defineTauDampFactor();
        variables.add(new Variable(label, value));

        label = MPHC.LABEL_DAMP_FACTOR_CC;
        value = defineDampFactor();
        variables.add(new Variable(label, value));

     //*****************************************************************************************************************
     // Deletion of variables inside PhysicsEngine for restructuring:
     //*****************************************************************************************************************

     // Deletion of 'M_isccadjn_PE':
        //label = MPHC.LABEL_M_ISCC_ADJN;
        //value = defineMISCCadjN();
        //variables.add(new Variable(label, value));

     // Deletion of 'M_isccxover_PE':
        //label = MPHC.LABEL_M_ISCC_CROSSOVER;
        //value = defineMISCCxOver();
        //variables.add(new Variable(label, value));

     // Deletion of 'M_isccadjw_PE':
        //label = MPHC.LABEL_M_ISCC_ADJW;
        //value = defineMISCCadjW();
        //variables.add(new Variable(label, value));

     // Deletion of 'M_isccx_PE':
        //label = MPHC.LABEL_MX_ISCC;
        //value = defineMISCCx();
        //variables.add(new Variable(label, value));

     // Deletion of 'M_isccy_PE':
        //label = MPHC.LABEL_MY_ISCC;
        //value = defineMISCCy();
        //variables.add(new Variable(label, value));

     // Deletion of 'Q_iscc_PE':
        //label = MPHC.LABEL_Q_ISCC;
        //value = defineQISCC();
        //variables.add(new Variable(label, value));

        //label = MPHC.LABEL_K_TWISTPITCH;
        //value = defineKtpScalingFactor();
        //variables.add(new Variable(label, value));

     //*****************************************************************************************************************
     // Definition of additional variables inside PhysicsEngine for restructuring:
     //*****************************************************************************************************************

        // Definition of 'ratio_HT'. Value is assigned by calling the method 'defineRatio':
        label = MPHC.LABEL_HT_RATIO;
        value = defineRatio();
        variables.add(new Variable(label, value));

        // Definition of 'surf_HT'. Value is assigned by calling the method 'defineRatio':
        label = MPHC.LABEL_HT_SURFACE;
        value = defineSurface();
        variables.add(new Variable(label, value));

        // Definition of 'rho_HT'. Value is assigned by calling the method 'defineRhoAvg':
        label = MPHC.LABEL_HT_RHO;
        value = defineRhoAvg();
        variables.add(new Variable(label, value));

        // Definition of 'cv_HT'. Value is assigned by calling the method 'defineCpAvg':
        label = MPHC.LABEL_HT_CV;
        value = defineCvAvg();
        variables.add(new Variable(label, value));

        String variableGroupName = String.format("%s_%s", coilLabel, MPHC.LABEL_PHYSICSENGINE);
        return new VariableGroup(variableGroupName, variables);
    }

    // *****************************************************************************************************************
    // Variable buildDefinition: testing units available
    // *****************************************************************************************************************

    /**
     * Method defines the source current density
     *
     * @return mathematical expression (see tests)
     */
    public static String defineJs() {
        return String.format("%s*%s", MPHC.LABEL_TAU, MPHC.LABEL_HT_I_SOURCE);
    }

    /**
     * Method defines the current winding denisty function (tau)
     *
     * @return mathematical expression (see tests)
     */
    public static String defineTauDensity() {
        return String.format("%s*%s/%s", MPHC.LABEL_HT_JZ_VERSOR, MPHC.LABEL_HT_RATIO, MPHC.LABEL_HT_SURFACE);
    }

    /**
     * Method defines a homogenization factor due to the actual cable geometry composed of strands and FEM model made of polygons
     *
     * @return mathematical expression (see tests)
     */
    public static String defineKsurfScalingFactor() {
        return String.format("%s/%s*%s", MPHC.LABEL_HT_RATIO, MPHC.LABEL_HT_SURFACE, MPHC.LABEL_CABLE_SURFACE_CONDUCTOR);
    }

//    public static String defineKtpScalingFactor() {
////        String value = String.format("cos(%s)/(cos(%s))^2", MPHC.LABEL_CABLE_THETA_TWISTPITCH_STRAND,MPHC.LABEL_CABLE_THETA_TWISTPITCH_ELECTRIC);
//        String value = "1";
//        return value;
//    }

    /**
     * Method defines the expression for the equivalent magnetization of inter-filament coupling currents in the x direction
     *
     * @return mathematical expression (see tests)
     */
    public static String defineMIFCCx() {
        String logicFlag = "if(" + MPHC.LABEL_FLAG_IFCC + ">0";
//        String constFactor = "(-2)/" + MPHC.LABEL_MU0;
        String constFactor = "(-1)/" + MPHC.LABEL_MU0;
        String tau = MPHC.LABEL_CABLE_TAU_IFCC;
        String surfaceScale = MPHC.LABEL_K_SURFACE;
        String dampFactor = MPHC.LABEL_DAMP_FACTOR_CC;
        String condition = String.format("%s*%s*(%s)*%s*%s", constFactor, tau, MPHC.LABEL_PHYSICSMF_DBXDT, surfaceScale, dampFactor);
        return logicFlag + "," + condition + ",0)";
    }

    /**
     * Method defines the expression for the equivalent magnetization of inter-filament coupling currents in the y direction
     *
     * @return mathematical expression (see tests)
     */
    public static String defineMIFCCy() {
        String logicFlag = "if(" + MPHC.LABEL_FLAG_IFCC + ">0";
//        String constFactor = "(-2)/" + MPHC.LABEL_MU0;
        String constFactor = "(-1)/" + MPHC.LABEL_MU0;
        String tau = MPHC.LABEL_CABLE_TAU_IFCC;
        String surfaceScale = MPHC.LABEL_K_SURFACE;
        String dampFactor = MPHC.LABEL_DAMP_FACTOR_CC;
        String condition = String.format("%s*%s*(%s)*%s*%s", constFactor, tau, MPHC.LABEL_PHYSICSMF_DBYDT, surfaceScale, dampFactor);
        return logicFlag + "," + condition + ",0)";
    }

    /**
     * Method defines the expression for the equivalent magnetization of adjacent inter-strand coupling currents in the cable narrow side
     *
     * @return mathematical expression (see tests)
    */
//    public static String defineMISCCadjN() {
//        String logicFlag = "if(" + MPHC.LABEL_FLAG_ISCC_ADJN + ">0";
//        String constFactor = "(-1)/" + MPHC.LABEL_MU0;
//        String surfaceScale = MPHC.LABEL_K_SURFACE;
//        String dampFactor = MPHC.LABEL_DAMP_FACTOR_CC;
//        String condition = String.format("%s*%s*d(%s,t)*%s*%s",
//                constFactor,
//                MPHC.LABEL_CABLE_TAU_ISCC_ADJN,
//                MPHC.LABEL_HT_BAVG_WAXIS,
//                surfaceScale,
//                dampFactor);
//        String condition   = constFactor + "*" + MPHC.LABEL_CABLE_TAU_ISCC_ADJN + "*" + MPHC.dBWdtAvg_Name + "*" + surfaceScale;
//        return logicFlag + "," + condition + ",0)";
//    }

    /**
     * Method defines the expression for the equivalent magnetization of adjacent inter-strand coupling currents in cross-over
     *
     * @return mathematical expression (see tests)
     */
//    public static String defineMISCCxOver() {
//        String logicFlag = "if(" + MPHC.LABEL_FLAG_ISCC_CROSSOVER + ">0";
//        String constFactor = "(-1)/" + MPHC.LABEL_MU0;
//        String surfaceScale = MPHC.LABEL_K_SURFACE;
//        String dampFactor = MPHC.LABEL_DAMP_FACTOR_CC;
//        String condition = String.format("%s*%s*d(%s,t)*%s*%s",
//                constFactor,
//                MPHC.LABEL_CABLE_TAU_ISCC_CROSSOVER,
//                MPHC.LABEL_HT_BAVG_NAXIS,
//                surfaceScale,
//                dampFactor);
//        String condition   = constFactor + "*" + MPHC.LABEL_CABLE_TAU_ISCC_CROSSOVER + "*" + MPHC.dBNdtAvg_Name + "*" + surfaceScale;
//        return logicFlag + "," + condition + ",0)";
//    }

    /**
     * Method defines the expression for the equivalent magnetization of adjacent inter-strand coupling currents in the cable wide side
     *
     * @return mathematical expression (see tests)
     */
//    public static String defineMISCCadjW() {
//        String logicFlag = "if(" + MPHC.LABEL_FLAG_ISCC_ADJW + ">0";
//        String constFactor = "(-1)/" + MPHC.LABEL_MU0;
//        String surfaceScale = MPHC.LABEL_K_SURFACE;
//        String dampFactor = MPHC.LABEL_DAMP_FACTOR_CC;
//        String condition = String.format("%s*%s*d(%s,t)*%s*%s",
//                constFactor,
//                MPHC.LABEL_CABLE_TAU_ISCC_ADJW,
//                MPHC.LABEL_HT_BAVG_NAXIS,
//                surfaceScale,
//                dampFactor);
//        String condition   = constFactor + "*" + MPHC.LABEL_CABLE_TAU_ISCC_ADJW + "*" + MPHC.dBNdtAvg_Name + "*" + surfaceScale;
//        return logicFlag + "," + condition + ",0)";
//    }

    /**
     * Method defines the expression for the equivalent magnetization of inter-strand coupling currents in the x direction
     *
     * @return mathematical expression (see tests)
     */
//    public static String defineMISCCx() {
//        String xOver = MPHC.LABEL_M_ISCC_CROSSOVER + "*(-sin(" + MPHC.LABEL_HT_ALPHA + "))";
//        String adjW = MPHC.LABEL_M_ISCC_ADJW + "*(-sin(" + MPHC.LABEL_HT_ALPHA + "))";
//        String adjN = MPHC.LABEL_M_ISCC_ADJN + "*(+cos(" + MPHC.LABEL_HT_ALPHA + "))";
//        return xOver + "+" + adjW + "+" + adjN;
//    }

    /**
     * Method defines the expression for the equivalent magnetization of inter-strand coupling currents in the y direction
     *
     * @return mathematical expression (see tests)
     */
//    public static String defineMISCCy() {
//        String xOver = MPHC.LABEL_M_ISCC_CROSSOVER + "*(+cos(" + MPHC.LABEL_HT_ALPHA + "))";
//        String adjW = MPHC.LABEL_M_ISCC_ADJW + "*(+cos(" + MPHC.LABEL_HT_ALPHA + "))";
//        String adjN = MPHC.LABEL_M_ISCC_ADJN + "*(+sin(" + MPHC.LABEL_HT_ALPHA + "))";
//        return xOver + "+" + adjW + "+" + adjN;
//    }

    /**
     * Method defines the expression for the equivalent magnetization of persistent currents in the x direction
     *
     * @return mathematical expression (see tests)
     */
    public static String defineMPersx() {
        String M_versor = String.format("(-%s/(%s+%s))", MPHC.LABEL_PHYSICSMF_BX, MPHC.LABEL_PHYSICSMF_B, MPHC.LABEL_EPS);
        String geomFactor = String.format("(%s/2)*(4/3*pi)", MPHC.LABEL_CABLE_DIAMETER_FILAMENT);
        String IcritFactor = String.format("%s/%s", MPHC.LABEL_CABLE_I_CRITICAL, MPHC.LABEL_HT_SURFACE);
        String reductFactor = String.format("(1-(%s/%s)^2)", MPHC.LABEL_HT_I_SOURCE, MPHC.LABEL_CABLE_I_CRITICAL);
        String Mpers = String.format("%s*%s*%s*%s", geomFactor, IcritFactor, reductFactor, M_versor);

        return String.format("if(%s>0,%s,0)", MPHC.LABEL_FLAG_MPERS, Mpers);
    }

    /**
     * Method defines the expression for the equivalent magnetization of persistent currents in the y direction
     *
     * @return mathematical expression (see tests)
     */
    public static String defineMPersy() {
        String M_versor = String.format("(-%s/(%s+%s))", MPHC.LABEL_PHYSICSMF_BY, MPHC.LABEL_PHYSICSMF_B, MPHC.LABEL_EPS);
        String geomFactor = String.format("(%s/2)*(4/3*pi)", MPHC.LABEL_CABLE_DIAMETER_FILAMENT);
        String IcritFactor = String.format("%s/%s", MPHC.LABEL_CABLE_I_CRITICAL, MPHC.LABEL_HT_SURFACE);
        String reductFactor = String.format("(1-(%s/%s)^2)", MPHC.LABEL_HT_I_SOURCE, MPHC.LABEL_CABLE_I_CRITICAL);
        String Mpers = String.format("%s*%s*%s*%s", geomFactor, IcritFactor, reductFactor, M_versor);

        return String.format("if(%s>0,%s,0)", MPHC.LABEL_FLAG_MPERS, Mpers);
    }

    /**
     * Method defines the expression for the equivalent magnetization in the x direction
     *
     * @return mathematical expression (see tests)
     */
    public static String defineMEqx() {
        //String M_x1 = MPHC.LABEL_MX_ISCC;         // M_iscc
        String M_x2 = MPHC.LABEL_MX_IFCC;
        String M_x3 = MPHC.LABEL_MX_PERS;
        //return M_x1 + "+" + M_x2 + "+" + M_x3;
        return M_x2 + "+" + M_x3;
    }

    /**
     * Method defines the expression for the equivalent magnetization in the y direction
     *
     * @return mathematical expression (see tests)
     */
    public static String defineMEqy() {
        //String M_y1 = MPHC.LABEL_MY_ISCC;
        String M_y2 = MPHC.LABEL_MY_IFCC;
        String M_y3 = MPHC.LABEL_MY_PERS;
        //return M_y1 + "+" + M_y2 + "+" + M_y3;
        return M_y2 + "+" + M_y3;
    }

    /**
     * Method defines the expression for the heat generation due to the inter-strand coupling currents
     *
     * @return mathematical expression (see tests)
     */
//    public static String defineQISCC() {
//        String qCrossOver = MPHC.LABEL_M_ISCC_CROSSOVER + "*" + "d(" + MPHC.LABEL_HT_BAVG_NAXIS + ",t)";
//        String qAdjacentWide = MPHC.LABEL_M_ISCC_ADJW + "*" + "d(" + MPHC.LABEL_HT_BAVG_NAXIS + ",t)";
//        String qAdjacentNarrow = MPHC.LABEL_M_ISCC_ADJN + "*" + "d(" + MPHC.LABEL_HT_BAVG_WAXIS + ",t)";
//        return "-(" + qCrossOver + "+" + qAdjacentWide + "+" + qAdjacentNarrow + ")";
//    }

    /**
     * Method defines the expression for the heat generation due to the inter-filament coupling currents
     *
     * @return mathematical expression (see tests)
     */
    public static String defineQIFCC() {
        String xComponent = MPHC.LABEL_MX_IFCC + "*" + MPHC.LABEL_PHYSICSMF_DBXDT;
        String yComponent = MPHC.LABEL_MY_IFCC + "*" + MPHC.LABEL_PHYSICSMF_DBYDT;
        return "-(" + xComponent + "+" + yComponent + ")";
    }

    /**
     * Method defines the expression for the quench on-set
     *
     * @return mathematical expression (see tests)
     */
    public static String definequenchState() {
        //String quenchState = String.format("%s(%s[1/A],%s[1/A])", MPHC.LABEL_CFUN_QUENCHSTATE, MPHC.LABEL_HT_I_SOURCE, MPHC.LABEL_HT_I_CRITICAL);
        String quenchState = String.format("%s(%s[1/A],%s[1/A])", MPHC.LABEL_CFUN_QUENCHSTATE, MPHC.LABEL_HT_I_SOURCE, MPHC.LABEL_CABLE_I_CRITICAL);
        String quenchTimeCondition = String.format("if(t>=%s,1,0)", MPHC.LABEL_PARAM_QUENCH_TIME);
        String quenchAllCondition = String.format("if(%s>0,%s,%s)", MPHC.LABEL_FLAG_QUENCH_ALL, quenchTimeCondition, quenchState);
        return String.format("if(%s>0,0,%s)", MPHC.LABEL_FLAG_QUENCH_OFF, quenchAllCondition);
    }

    /**
     * Method defines the expression for the total heat source
     *
     * @return mathematical expression (see tests)
     */
    public static String defineQtot() {
        //return MPHC.LABEL_Q_OHM + "+" + MPHC.LABEL_Q_IFCC + "+" + MPHC.LABEL_Q_ISCC;
        return MPHC.LABEL_Q_OHM + "+" + MPHC.LABEL_Q_IFCC;
    }

    /**
     * Method defines the expression for the resistance
     *
     * @return mathematical expression (see tests)
     */
    public static String defineR() {
        return String.format("%s/%s*%s^2", MPHC.LABEL_HT_RHO, MPHC.LABEL_HT_SURFACE, MPHC.LABEL_HT_RATIO);
    }

    /**
     * Method defines the expression for the flux linkage
     *
     * @return mathematical expression (see tests)
     */
    public static String definePhi() {
        return "(mf.Az)" + "*" + MPHC.LABEL_TAU + "*" + MPHC.LABEL_HT_SURFACE;
    }

    /**
     * Method defines the expression for the Ohmic losses
     *
     * @return mathematical expression (see tests)
     */
    public static String defineQOhm() {
        return String.format("%s*%s^2", MPHC.LABEL_HT_RHO, MPHC.LABEL_JS);
    }

    /**
     * Method defines the expression for the voltage of a half-turn
     *
     * @return mathematical expression (see tests)
     */
    public static String defineVdelta() {
        return String.format("%s*%s+d(%s,t)", MPHC.LABEL_R, MPHC.LABEL_HT_I_SOURCE, MPHC.LABEL_PHI);
    }

    /**
     * Method defines the value of the dampening factor of the coupling losses
     *
     * @return value [unit] String
     */
    public static String defineTauDampFactor() {
        return "10[ms]";
    }

    /**
     * Method defines the expression for the dampening factor of the coupling losses
     *
     * @return mathematical expression (see tests)
     */
    public static String defineDampFactor() {
        return String.format("exp(-t_star/%s)", MPHC.LABEL_TAU_CC);
    }

    /**
     * Method defines the expression for the half-turn ratio
     * @return mathematical expression (see tests)
     */
    // Method: The value of 'ratio_HT' is defined as 'defineRatio' with a value of 1:
    public static String defineRatio() {
        return String.format("1");
    }

    /**
     * Method defines the expression for the half-turn surface
     * @return mathematical expression (see tests)
     */
    // Method: The value of 'surf_HT' is defined as 'defineSurface':
    public static String defineSurface() {
        return String.format("(%s+2*%s)*(%s+2*%s)",
                MPHC.LABEL_CABLE_WIDTH_BARE,
                MPHC.LABEL_CABLE_THICHNESS_INSULATION_WIDEEDGE,
                MPHC.LABEL_CABLE_HEIGHT_AVERAGE_BARE,
                MPHC.LABEL_CABLE_THICHNESS_INSULATION_NARROWEDGE);
    }

    /**
     * Method defines the expression for the half-turn resistivity
     * @return mathematical expression (see tests)
     */
    // Method: The value of 'rho_HT' is defined as 'defineRhoAvg':
    public static String defineRhoAvg() {
        return String.format("%s*(1/(%s*%s))*%s",
                MPHC.LABEL_QUENCHSTATE,
                MPHC.LABEL_K_SURFACE,
                MPHC.LABEL_CABLE_FRACTION_CU,
                MPHC.LABEL_CABLE_RHO_STABILIZER);
    }

    /**
     * Method defines the expression for the half-turn heat capacity
     * @return mathematical expression (see tests)
     */
    // Method: The value of 'cv_HT' is defined as 'defineCvAvg':
    public static String defineCvAvg() {
        return String.format("(%s+%s+%s+%s)/%s",
            MPHC.LABEL_CABLE_CV_CONDUCTOR,
            MPHC.LABEL_CABLE_CV_VOIDS,
            MPHC.LABEL_CABLE_CV_CORE,
            MPHC.LABEL_CABLE_CV_INSULATION,
            MPHC.LABEL_HT_SURFACE);
    }
}
