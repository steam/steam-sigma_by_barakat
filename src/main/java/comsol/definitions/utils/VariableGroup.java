package comsol.definitions.utils;

import comsol.api.DefinitionsAPI;
import comsol.constants.MPHC;
import server.SigmaServer;

import java.util.ArrayList;
import java.util.List;

/**
 * Class contains parameters and methods of a variable group assigned to a selection in a COMSOL model
 */
public class VariableGroup {

    private final String label;
    private final ArrayList<Variable> varsList;

    /**
     * Constructs a VariableGroup object as a list of Variable objects
     *
     * @param label     - label of the variable group
     * @param variables - list of Variable objects
     */
    public VariableGroup(String label, List<Variable> variables) {
        this.label = label;
        this.varsList = new ArrayList<>(variables);
    }

    /**
     * Method builds a variable group in model component and assigns it to a given selection
     *
     * @param srv            - input SigmaServer instance
     * @param labelComp      - Label of component node
     * @param geomLabel      - Label of geometry node
     * @param selectionLabel - label of a selection to which the variable group is assigned
     * @param entityDim      - Entity dimension number (2 for component 1, 1 for component 2)
     */
    public void buildVariableGroup(SigmaServer srv, String labelComp, String geomLabel, String selectionLabel, int entityDim) {

        DefinitionsAPI.createVariableGroup(srv, label, labelComp);    // parameter added

        for (Variable var : varsList) {
            DefinitionsAPI.addVariable(srv, label, var.getLabel(), var.getValue());
        }

        switch (selectionLabel) {
            case "":
                DefinitionsAPI.assignVariableGroupToGlobal(srv, label);
                break;
            default:
                DefinitionsAPI.assignVariableGroupToSelection(srv, geomLabel, label, selectionLabel, entityDim);
        }
    }

    public String getLabel() {
        return label;
    }

    public List<Variable> getVarsList() {
        return new ArrayList<>(varsList);
    }
}
