package comsol.definitions.utils;

/**
 * Class contains parameters of a variable
 */
public class Variable {

    private final String label;
    private final String value;

    /**
     * Constructs a Variable object as a (label, value) pair
     *
     * @param label - label (key, name) of a variable
     * @param value - variable value
     */
    public Variable(String label, String value) {
        this.label  = label;
        this.value  = value;
    }

    public String getLabel() { return label; }

    public String getValue() { return value; }

}
