package comsol.definitions;

import comsol.api.DefinitionsAPI;
import comsol.constants.MPHC;
import server.SigmaServer;

// WARNING WARNING WARNING
// Current values are still hardcoded

/**
 * Class with a method building a default external current node in the magnet component of a model
 */
public class IextDefault {

    private IextDefault() {}
    /**
     * Method builds a node in the magnet component of a model
     *
     * @param srv       - input SigmaServer instance
     * @param labelNode - label of external current node
     */
    public static final void buildNode(SigmaServer srv, String labelNode) {
        DefinitionsAPI.createInterpolationTable(srv, labelNode, MPHC.LABEL_COMP);
        String xUnit = "s";
        String yUnit = "A";
        String[] x = {"0", "1"};
        String[] y = {"0", "1"};
        DefinitionsAPI.addValToInterpolationTable(srv, labelNode, x, y, xUnit, yUnit);
    }
}
