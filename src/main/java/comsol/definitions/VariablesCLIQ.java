package comsol.definitions;

import comsol.constants.MPHC;
import comsol.definitions.utils.Variable;
import comsol.definitions.utils.VariableGroup;

import java.util.ArrayList;

/**
 * Class contains parameters needed to define a CLIQ equations for a monolithic study
 */
public class VariablesCLIQ {

    /**
     * Method defines variables needed for a single CLIQ unit simulation
     *
     * @return group of variables to be injected to the magnet component of a model
     */
    public static VariableGroup defineVariables() {

        ArrayList<Variable> variables = new ArrayList<>();

        String label;
        String value;

        label = MPHC.LABEL_CLIQ_CAPASITOR;
        value = "50e-3[F]";
        variables.add(new Variable(label, value));

        label = MPHC.LABEL_CLIQ_RESISTANCE;
        value = "20e-3[ohm]";
        variables.add(new Variable(label, value));

        label = MPHC.LABEL_CLIQ_INDUCTANCE;
        value = "1e-6[H]";  // Changed to a value close to 0!
        variables.add(new Variable(label, value));

        label = MPHC.LABEL_CLIQ_VOLTAGE_INITIAL;
        value = "1250[V]";
        variables.add(new Variable(label, value));

        label = MPHC.LABEL_CLIQ_CURRENT_INITIAL;
        value = "0[A]";
        variables.add(new Variable(label, value));

        label = MPHC.LABEL_CLIQ_CURRENT_EXT_INITIAL;
        value = "11.96e3[A]";
        variables.add(new Variable(label, value));

        label = MPHC.LABEL_CLIQ_RCROW;
        value = "1e-4[ohm]";
        variables.add(new Variable(label, value));

        label = MPHC.LABEL_CLIQ_LCIR;
        value = "1e-6[H]";
        variables.add(new Variable(label, value));

        label = MPHC.LABEL_CLIQ_SYMFACTOR;
        value = "1";                                           // Value changed from '2' to '1' for full magnet geometry
        variables.add(new Variable(label, value));

        label =  MPHC.LABEL_CLIQ_SWITCH;
        value = "0";                                                            // Changed to 1 for full magnet geometry
        variables.add(new Variable(label, value));

        String variableGroupName = MPHC.MARKER_CIRCUITPARAMS;
        return new VariableGroup(variableGroupName, variables);
    }

    /**
     * Method defines variables needed for a single CLIQ unit simulation (For coil dependent CLIQ setup)
     *
     * @param coilLabel - label of a coil to which CLIQ is assigned
     * @return group of variables to be injected to the magnet component of a model
     */
    public static VariableGroup defineVariables(String coilLabel) {

        ArrayList<Variable> variables = new ArrayList<>();

        String label;
        String value;


        label = String.format("%s_%s", MPHC.LABEL_CLIQ_CAPASITOR, coilLabel);
        value = "50e-3[F]";
        variables.add(new Variable(label, value));

        label = String.format("%s_%s", MPHC.LABEL_CLIQ_RESISTANCE, coilLabel);
        value = "20e-3[ohm]";
        variables.add(new Variable(label, value));

        label = String.format("%s_%s", MPHC.LABEL_CLIQ_INDUCTANCE, coilLabel);
        value = "0[H]";
        variables.add(new Variable(label, value));

        label = String.format("%s_%s", MPHC.LABEL_CLIQ_VOLTAGE_INITIAL, coilLabel);
        value = "1250[V]";
        variables.add(new Variable(label, value));

        label = String.format("%s_%s", MPHC.LABEL_CLIQ_CURRENT_INITIAL, coilLabel);
        value = "0[A]";
        variables.add(new Variable(label, value));

        label = String.format("%s_%s", MPHC.LABEL_CLIQ_CURRENT_EXT_INITIAL, coilLabel);
        value = "11.96e3[A]";
        variables.add(new Variable(label, value));

        label = String.format("%s_%s", MPHC.LABEL_CLIQ_RCROW, coilLabel);
        value = "1e-4[ohm]";
        variables.add(new Variable(label, value));

        label = String.format("%s_%s", MPHC.LABEL_CLIQ_SYMFACTOR, coilLabel);
        value = "1";                                                            // Changed to 1 for full magnet geometry
        variables.add(new Variable(label, value));

        label = String.format("%s_%s", MPHC.LABEL_CLIQ_SWITCH, coilLabel);
        value = "1";                                                            // Changed to 1 for full magnet geometry
        variables.add(new Variable(label, value));

        String variableGroupName = String.format("%s_%s", MPHC.MARKER_CIRCUITPARAMS, coilLabel);
        return new VariableGroup(variableGroupName, variables);
    }
}