package comsol.definitions;

import comsol.constants.MPHC;
import comsol.definitions.utils.Variable;
import comsol.definitions.utils.VariableGroup;
import model.geometry.coil.Halfturn;

import java.util.ArrayList;

/**
 * Class contains variables of a half-turn
 */
public class VariablesHalfturn {

    private VariablesHalfturn() {
    }

    /**
     * Method defines a variable group for a half-turn
     *
     * @param label        - variable group label
     * @param labelWinding - label corresponding to the winding to which a half-turn belongs
     * @param ht           - half-turn object with parameters
     * @param index        - half-turn index
     * @return variables group to be created in COMSOL
     */
    public static VariableGroup defineVariables(String label, String labelWinding, Halfturn ht, String index) {

        ArrayList<Variable> varsList = new ArrayList<>();
        String labelVar;
        String valueVar;

        labelVar = MPHC.LABEL_HT_INDEX;
        valueVar = defineIndex(index);
        varsList.add(new Variable(labelVar, valueVar));

        labelVar = MPHC.LABEL_HT_RATIO;
        valueVar = defineRatio(ht.getRatio());
        varsList.add(new Variable(labelVar, valueVar));

        labelVar = MPHC.LABEL_HT_ALPHA;
        valueVar = defineAlpha(ht.getAlpha());
        varsList.add(new Variable(labelVar, valueVar));

        labelVar = MPHC.LABEL_HT_JZ_VERSOR;
        valueVar = defineTauDensityVersor(ht.getTauVersor());
        varsList.add(new Variable(labelVar, valueVar));

        labelVar = MPHC.LABEL_HT_SURFACE;
        valueVar = defineSurface(ht.getSurface());
        varsList.add(new Variable(labelVar, valueVar));

        labelVar = MPHC.LABEL_HT_I_SOURCE;
        valueVar = defineCurrent(labelWinding);
        varsList.add(new Variable(labelVar, valueVar));

        labelVar = MPHC.LABEL_HT_BAVG_WAXIS;
        valueVar = defineBAvgWAxis(label);
        varsList.add(new Variable(labelVar, valueVar));

        labelVar = MPHC.LABEL_HT_BAVG_NAXIS;
        valueVar = defineBAvgNAxis(label);
        varsList.add(new Variable(labelVar, valueVar));

        labelVar = MPHC.LABEL_HT_RHO;
        valueVar = defineRhoAvg(label);
        varsList.add(new Variable(labelVar, valueVar));

        labelVar = MPHC.LABEL_HT_CV;
        valueVar = defineCpAvg(label);
        varsList.add(new Variable(labelVar, valueVar));

        labelVar = MPHC.LABEL_HT_I_CRITICAL;
        valueVar = defineIcMin(label);
        varsList.add(new Variable(labelVar, valueVar));

        labelVar = MPHC.LABEL_HT_I_CRITICAL;
        valueVar = defineIcMin(label);
        varsList.add(new Variable(labelVar, valueVar));

        //labelVar = MPHC.LABEL_V_GROUND_HALFTURN;
        //valueVar = defineVgnd(index);
        //varsList.add(new Variable(labelVar, valueVar));

        labelVar = MPHC.LABEL_HT_I_SOURCE;
        valueVar = defineIsHT();
        varsList.add(new Variable(labelVar, valueVar));

        String variableGroupName = String.format("%s", label);
        return new VariableGroup(variableGroupName, varsList);
    }

    // *****************************************************************************************************************
    // Variable buildDefinition: testing units available
    // *****************************************************************************************************************

    /**
     * Method defines a half-turn index
     *
     * @param index - half-turn index
     * @return value [unit] String
     */
    public static String defineIndex(String index) {
        return String.format("%s[1]", index);
    }

    /**
     * Method defines a half-turn ratio
     *
     * @param ratio - half-turn ratio
     * @return value [unit] String
     */
    public static String defineRatio(double ratio) {
        return String.format("%s[1]", ratio);
    }

    /**
     * Method defines the alpha angle of a half-turn
     *
     * @param alpha - half-turn angle
     * @return value [unit] String
     */
    public static String defineAlpha(double alpha) {
        return String.format("%s[rad]", alpha);
    }

    /**
     * Method defines the current density versor (tau)
     *
     * @param tauVersor - current density versor
     * @return value [unit] String
     */
    public static String defineTauDensityVersor(double tauVersor) {
        return String.format("%s[1]", tauVersor);
    }

    /**
     * Method defines the surface of a half-turn
     *
     * @param surface - current density versor
     * @return value [unit] String
     */
    public static String defineSurface(double surface) {
        return String.format("%s[m^2]", surface);
    }

    /**
     * Method defines the current label
     *
     * @param label - current label
     * @return value [unit] String
     */
    public static String defineCurrent(String label) {
        return String.format("%s_%s", MPHC.MARKER_I_WINDING, label);
    }

    /**
     * Method defines the average of the magnetic flux density along the wide axis
     *
     * @param label - operator label
     * @return mathematical expression (see tests)
     */
    public static String defineBAvgWAxis(String label) {
        String comp_x = String.format("%s*cos(%s)", MPHC.LABEL_PHYSICSMF_BX, MPHC.LABEL_HT_ALPHA);
        String comp_y = String.format("%s*sin(%s)", MPHC.LABEL_PHYSICSMF_BY, MPHC.LABEL_HT_ALPHA);
        String comp_sum = String.format("%s+%s", comp_x, comp_y);
        return String.format("%s_%s(%s)", label, MPHC.LABEL_LINENBEZIER_AVERAGE, comp_sum);
    }

    /**
     * Method defines the average of the magnetic flux density along the narrow axis
     *
     * @param label - operator label
     * @return mathematical expression (see tests)
     */
    public static String defineBAvgNAxis(String label) {
        String comp_x = String.format("%s*sin(-%s)", MPHC.LABEL_PHYSICSMF_BX, MPHC.LABEL_HT_ALPHA);
        String comp_y = String.format("%s*cos(%s)", MPHC.LABEL_PHYSICSMF_BY, MPHC.LABEL_HT_ALPHA);
        String comp_sum = String.format("%s+%s", comp_x, comp_y);
        return String.format("%s_%s(%s)", label, MPHC.LABEL_LINEWBEZIER_AVERAGE, comp_sum);
    }

    /**
     * Method defines the average resistivity
     *
     * @param label - operator label
     * @return mathematical expression (see tests)
     */
    public static String defineRhoAvg(String label) {
        String rho_avg = String.format("%s_%s(%s)", label, MPHC.LABEL_SURFACE_AVERAGE, MPHC.LABEL_CABLE_RHO_STABILIZER);
        String rho_avg_scaled = String.format("(1/(%s*%s))*%s*1/(cos(%s)^2)",
                MPHC.LABEL_K_SURFACE,
                MPHC.LABEL_CABLE_FRACTION_CU,
                rho_avg,
                MPHC.LABEL_CABLE_THETA_TWISTPITCH_STRAND);
        return String.format("%s*%s", MPHC.LABEL_QUENCHSTATE, rho_avg_scaled);
    }

    /**
     * Method defines the average heat capacity
     *
     * @param label - operator label
     * @return mathematical expression (see tests)
     */
    public static String defineCpAvg(String label) {
        return String.format("%s_%s((%s+%s+%s+%s)/%s)",
                label,
                MPHC.LABEL_SURFACE_AVERAGE,
                MPHC.LABEL_CABLE_CV_CONDUCTOR,
                MPHC.LABEL_CABLE_CV_VOIDS,
                MPHC.LABEL_CABLE_CV_CORE,
                MPHC.LABEL_CABLE_CV_INSULATION,
                MPHC.LABEL_HT_SURFACE);
    }

    /**
     * Method defines the minimum critical current
     *
     * @param label - operator label
     * @return mathematical expression (see tests)
     */
    public static String defineIcMin(String label) {
        return String.format("%s_%s(%s)", label, MPHC.LABEL_SURFACE_MINIMUM, MPHC.LABEL_CABLE_I_CRITICAL);
    }

    /**
     * Method defines the expression for the voltage to ground
     *
     * @param index - half-turn
     * @return mathematical expression (see tests)
     */
//    public static String defineVgnd(String index) {
//        return String.format("%s%s", MPHC.LABEL_V_GROUND_HALFTURN, index);
//    }

    /**
     * Method defines the expression for the half-turn heat capacity
     * @return mathematical expression (see tests)
     */
    // Method: The value of 'Is_HT' is defined as 'defineIsHT':
    public static String defineIsHT() {
        return String.format("%s", MPHC.LABEL_CLIQ_CURRENT_EXT);
    }
}
