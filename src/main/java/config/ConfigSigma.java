package config;

import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.charset.Charset;

/**
 * Class representing a SIGMA config with a parser method
 */
public class ConfigSigma {
    private static final Logger LOGGER = LogManager.getLogger(ConfigSigma.class);
    private String comsolBatchPath;
    private String externalCFunLibPath;
    private String outputModelPath;
    private String javaJDKPath;

    public ConfigSigma() {
    }


    // *****************************************************************************************************************
    // Constructor
    // *****************************************************************************************************************

    /**
     * Method parses a SIGMA config json file
     *
     * @param absoluteConfigPath - absolute path to a SIGMA config json file
     * @return ConfigSigma object with input parameters of SIGMA
     * @throws IOException - thrown in case of I/O errors (e.g., config path does not exist)
     */
    public static ConfigSigma parseFileJSON(String absoluteConfigPath) throws IOException {
        ConfigSigma cfg = new ConfigSigma();
        BufferedReader br = null;
        try {
            Gson gson = new Gson();
            br = new BufferedReader(new InputStreamReader(new FileInputStream(absoluteConfigPath), Charset.defaultCharset()));
            cfg = gson.fromJson(br, ConfigSigma.class);
            br.close();
        } catch (IOException e) {
            LOGGER.error("Can't parse input json file {}", absoluteConfigPath);
            LOGGER.error(e);
        } finally {
            if (br != null) {
                br.close();
            }
        }
        return cfg;
    }

    // *****************************************************************************************************************
    // Getters
    // *****************************************************************************************************************

    public String getComsolBatchPath() {
        return comsolBatchPath;
    }

    public String getExternalCFunLibPath() {
        return externalCFunLibPath;
    }

    public String getOutputModelPath() {
        return outputModelPath;
    }

    public String getJavaJDKPath() {
        return javaJDKPath;
    }

    // *****************************************************************************************************************
    // Setters
    // *****************************************************************************************************************

    public void setComsolBatchPath(String comsolBatchPath) {
        this.comsolBatchPath = comsolBatchPath;
    }

    public void setExternalCFunLibPath(String externalCFunLibPath) {
        this.externalCFunLibPath = externalCFunLibPath;
    }

    public void setOutputModelPath(String outputModelPath) {
        this.outputModelPath = outputModelPath;
    }

    public void setJavaJDKPath(String javaJDKPath) {
        this.javaJDKPath = javaJDKPath;
    }
}
