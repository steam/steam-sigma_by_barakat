package input.Others.T0;

import input.UtilsUserInput;
import model.domains.Domain;
import model.domains.database.AirDomain;
import model.domains.database.AirFarFieldDomain;
import model.domains.database.CoilDomain;
import model.domains.database.QuenchHeaterDomain;
import model.geometry.Element;
import model.geometry.IronYokeParser;
import model.geometry.basic.*;
import model.geometry.coil.Coil;
import model.geometry.coil.Pole;
import model.geometry.coil.Winding;
import model.materials.database.MatDatabase;

import javax.script.ScriptException;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class represents a benchmark magnet T0
 */
public class Magnet_T0 extends UtilsUserInput {


    private final Domain[] domains;

    public Domain[] getDomains() {
        return domains.clone();
    }

    /**
     * Constructs a magnet model in COMSOL with features defined in the constructor body
     *
     * @throws FileNotFoundException thrown in case an input file is not found
     * @throws ScriptException       thrown in case of errors in parsing an iron yoke file
     */
    public Magnet_T0() throws FileNotFoundException, ScriptException {
        //Should you need a dedicated BH characteristics for your iron yoke, please update the database under src/main/resources
        String bhCurveDatabasePath = Magnet_T0.class.getClassLoader().getResource("bh-curve-database.txt").getFile();
        String ironYokeGeometryPath = Magnet_T0.class.getClassLoader().getResource("IronYokeT0.txt").getFile();

        List<Domain> listOfDomains = new ArrayList<>();
        listOfDomains.add(new AirDomain("airDomain", MatDatabase.MAT_AIR, air()));
        listOfDomains.add(new AirFarFieldDomain("airFarFieldDomain", MatDatabase.MAT_AIR, airFarField()));
        listOfDomains.add(new CoilDomain("C0", MatDatabase.MAT_COIL, coil()));
//        listOfDomains.add(new IronDomain("ironYoke", MatDatabase.MAT_IRON1, parseIronYokeGeometry()));
        IronYokeParser.addListOfIronYokesAndHoles(listOfDomains, ironYokeGeometryPath, bhCurveDatabasePath);

        domains = listOfDomains.toArray(new Domain[listOfDomains.size()]);
    }

    private Coil coil() {

        double insThickness=1e-4;           // Insulation thickness is added by Barakat
        double x0 = 0.01;
        double y0 = 0.005;
        double w = 15e-3+2*insThickness;
        double h = 1.5e-3+2*insThickness;


        Point kp11 = Point.ofCartesian(x0, y0 + w);
        Point kp12 = Point.ofCartesian(x0, y0);
        Point kp13 = Point.ofCartesian(x0 + h, y0);
        Point kp14 = Point.ofCartesian(x0 + h, y0 + w);

        Line ln11 = Line.ofEndPoints(kp11, kp14);
        Line ln12 = Line.ofEndPoints(kp12, kp11);
        Line ln13 = Line.ofEndPoints(kp12, kp13);
        Line ln14 = Line.ofEndPoints(kp13, kp14);

        Area ha11p = Area.ofHyperLines(new HyperLine[]{ln11, ln12, ln13, ln14});
        Area ha12p = ha11p.translate(2 * h, 0);
        Area ha13p = ha11p.translate(4 * h, 0);

        Area ha11n = ha11p.mirrorY();
        Area ha12n = ha12p.mirrorY();
        Area ha13n = ha13p.mirrorY();

        // windings:
        Winding w0 = Winding.ofAreas(new Area[]{ha11p, ha11n}, new int[]{+1, -1}, 2, 1, new Cable_T0());
        Winding w1 = Winding.ofAreas(new Area[]{ha12p, ha12n}, new int[]{+1, -1}, 2, 1, new Cable_T0());
        Winding w2 = Winding.ofAreas(new Area[]{ha13p, ha13n}, new int[]{+1, -1}, 2, 1, new Cable_T0());

        // poles:
        Pole p1 = Pole.ofWindings(new Winding[]{w0, w1, w2});
        Pole p2 = p1.mirrorX().reverseWindingDirection();

        // Coil:
        return Coil.ofPoles(new Pole[]{p1, p2});
    }

    private Element[] air() {
        // POINTS

        double r = 0.5;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r, 0);
        Point kp2 = Point.ofCartesian(0, r);

        // LINES
        Line ln1 = Line.ofEndPoints(kpc, kp1);
        Arc ln2 = Arc.ofEndPointsCenter(kp1, kp2, kpc);
        Line ln3 = Line.ofEndPoints(kp2, kpc);

        // AREAS
        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3});
        Area ar2 = ar1.mirrorY();
        Area ar3 = ar2.mirrorX();
        Area ar4 = ar1.mirrorX();

        // ELEMENTS
        Element el1 = new Element("AIR_El1", ar1);
        Element el2 = new Element("AIR_El2", ar2);
        Element el3 = new Element("AIR_El3", ar3);
        Element el4 = new Element("AIR_El4", ar4);

        return new Element[]{el1, el2, el3, el4};
    }

    private Element[] airFarField() {
        // POINTS

        double r = 0.5;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r, 0);
        Point kp2 = Point.ofCartesian(0, r);

        Point kp1_out = Point.ofCartesian(r * 1.05, 0);
        Point kp4_out = Point.ofCartesian(0, r * 1.05);

        // LINES
        Line ln1 = Line.ofEndPoints(kp1, kp1_out);
        Arc ln2 = Arc.ofEndPointsCenter(kp1_out, kp4_out, kpc);
        Line ln3 = Line.ofEndPoints(kp4_out, kp2);
        Arc ln4 = Arc.ofEndPointsCenter(kp2, kp1, kpc);

        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3, ln4});
        Area ar2 = ar1.mirrorY();
        Area ar3 = ar2.mirrorX();
        Area ar4 = ar1.mirrorX();

        Element el1 = new Element("FAR_El1", ar1);
        Element el2 = new Element("FAR_El2", ar2);
        Element el3 = new Element("FAR_El3", ar3);
        Element el4 = new Element("FAR_El4", ar4);

        return new Element[]{el1, el2, el3, el4};
    }

    private Element[] iron_yoke() {
        double deg2rad = Math.PI / 180;
        double r_int = 0.04;
        double r_ext = 0.06;


        Point kp1 = Point.ofPolar(r_int, 0 * deg2rad);
        Point kp2 = Point.ofPolar(r_ext, 0 * deg2rad);
        Point kp3 = Point.ofPolar(r_ext, 90 * deg2rad);
        Point kp4 = Point.ofPolar(r_int, 90 * deg2rad);

        Point kp0 = Point.ofCartesian(0, 0);

        Arc ln1 = Arc.ofEndPointsCenter(kp1, kp4, kp0);
        Line ln2 = Line.ofEndPoints(kp1, kp2);
        Arc ln3 = Arc.ofEndPointsCenter(kp2, kp3, kp0);
        Line ln4 = Line.ofEndPoints(kp4, kp3);

        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3, ln4});
        Area ar2 = ar1.mirrorY();
        Area ar3 = ar2.mirrorX();
        Area ar4 = ar1.mirrorX();

        Element el1 = new Element("IY_El1", ar1);
        Element el2 = new Element("IY_El2", ar2);
        Element el3 = new Element("IY_El3", ar3);
        Element el4 = new Element("IY_El4", ar4);

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
        return new Element[]{el1, el2, el3, el4};
    }

    private Element[] insulation(String coilName) {
        double x0 = 0.02;
        double y0 = 0.01;
        double w = 15e-3;
        double h = 1.5e-3;
        double step_dx = 3e-3;
        double n = 0;

        n = 3;
        Point kp11 = Point.ofCartesian(step_dx * (n - 1) + x0 + h + h, y0 + w);
        Point kp12 = Point.ofCartesian(step_dx * (n - 1) + x0 + h, y0 + w);
        Point kp13 = Point.ofCartesian(step_dx * (n - 1) + x0 + h, y0);
        Point kp14 = Point.ofCartesian(step_dx * (n - 1) + x0 + h + h, y0);

        n = 4;
        Point kp21 = Point.ofCartesian(step_dx * (n - 1) + x0 + h + h, y0 + w);
        Point kp22 = Point.ofCartesian(step_dx * (n - 1) + x0 + h, y0 + w);
        Point kp23 = Point.ofCartesian(step_dx * (n - 1) + x0 + h, y0);
        Point kp24 = Point.ofCartesian(step_dx * (n - 1) + x0 + h + h, y0);

        Line ln11 = Line.ofEndPoints(kp12, kp11);
        Line ln12 = Line.ofEndPoints(kp12, kp13);
        Line ln13 = Line.ofEndPoints(kp13, kp14);
        Line ln14 = Line.ofEndPoints(kp11, kp14);

        Line ln21 = Line.ofEndPoints(kp22, kp21);
        Line ln22 = Line.ofEndPoints(kp22, kp23);
        Line ln23 = Line.ofEndPoints(kp23, kp24);
        Line ln24 = Line.ofEndPoints(kp21, kp24);

        // Insulation, pole 1
        Area ins11 = Area.ofHyperLines(new HyperLine[]{ln11, ln12, ln13, ln14});
        Area ins12 = Area.ofHyperLines(new HyperLine[]{ln21, ln22, ln23, ln24});

        // Insulation, pole 2
        Area ins21 = ins11.mirrorY();
        Area ins22 = ins12.mirrorY();

        // Insulation, pole 3
        Area ins31 = ins21.mirrorX();
        Area ins32 = ins22.mirrorX();

        // Insulation, pole 4
        Area ins41 = ins11.mirrorX();
        Area ins42 = ins12.mirrorX();

        Element el1_InsCoilQ1 = new Element(coilName + "Q1_El1_Ins", ins11);
        Element el2_InsCoilQ1 = new Element(coilName + "Q1_El2_Ins", ins12);
        Element el1_InsCoilQ2 = new Element(coilName + "Q2_El1_Ins", ins21);
        Element el2_InsCoilQ2 = new Element(coilName + "Q2_El2_Ins", ins22);
        Element el1_InsCoilQ3 = new Element(coilName + "Q3_El1_Ins", ins31);
        Element el2_InsCoilQ3 = new Element(coilName + "Q3_El2_Ins", ins32);
        Element el1_InsCoilQ4 = new Element(coilName + "Q4_El1_Ins", ins41);
        Element el2_InsCoilQ4 = new Element(coilName + "Q4_El2_Ins", ins42);

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
        Element[] quad1 = {el1_InsCoilQ1, el2_InsCoilQ1};
        Element[] quad2 = {el1_InsCoilQ2, el2_InsCoilQ2};
        Element[] quad3 = {el1_InsCoilQ3, el2_InsCoilQ3};
        Element[] quad4 = {el1_InsCoilQ4, el2_InsCoilQ4};

        return quadrantsToBuild(quad1, quad2, quad3, quad4);
    }

    private Element[] wedges(String coilName) {

        double x0 = 0.02;
        double y0 = 0.01;
        double w = 15e-3;
        double h = 1.5e-3;
        double step_dx = 3e-3;
        double n;

        n = 1;
        Point kp11 = Point.ofCartesian(step_dx * (n - 1) + x0 + h + h, y0 + w);
        Point kp12 = Point.ofCartesian(step_dx * (n - 1) + x0 + h, y0 + w);
        Point kp13 = Point.ofCartesian(step_dx * (n - 1) + x0 + h, y0);
        Point kp14 = Point.ofCartesian(step_dx * (n - 1) + x0 + h + h, y0);

        n = 2;
        Point kp21 = Point.ofCartesian(step_dx * (n - 1) + x0 + h + h, y0 + w);
        Point kp22 = Point.ofCartesian(step_dx * (n - 1) + x0 + h, y0 + w);
        Point kp23 = Point.ofCartesian(step_dx * (n - 1) + x0 + h, y0);
        Point kp24 = Point.ofCartesian(step_dx * (n - 1) + x0 + h + h, y0);

        Line ln11 = Line.ofEndPoints(kp12, kp11);
        Line ln12 = Line.ofEndPoints(kp12, kp13);
        Line ln13 = Line.ofEndPoints(kp13, kp14);
        Line ln14 = Line.ofEndPoints(kp11, kp14);

        Line ln21 = Line.ofEndPoints(kp22, kp21);
        Line ln22 = Line.ofEndPoints(kp22, kp23);
        Line ln23 = Line.ofEndPoints(kp23, kp24);
        Line ln24 = Line.ofEndPoints(kp21, kp24);

        // wedges, pole 1
        Area wed11 = Area.ofHyperLines(new HyperLine[]{ln11, ln12, ln13, ln14});
        Area wed12 = Area.ofHyperLines(new HyperLine[]{ln21, ln22, ln23, ln24});

        // wedges, pole 2
        Area wed21 = wed11.mirrorY();
        Area wed22 = wed12.mirrorY();

        // wedges, pole 3
        Area wed31 = wed21.mirrorX();
        Area wed32 = wed22.mirrorX();

        // wedges, pole 4
        Area wed41 = wed11.mirrorX();
        Area wed42 = wed12.mirrorX();

        Element el1_wedgeCoilQ1 = new Element(coilName + "Q1_El1_wedge", wed11);
        Element el2_wedgeCoilQ1 = new Element(coilName + "Q1_El2_wedge", wed12);
        Element el1_wedgeCoilQ2 = new Element(coilName + "Q2_El1_wedge", wed21);
        Element el2_wedgeCoilQ2 = new Element(coilName + "Q2_El2_wedge", wed22);
        Element el1_wedgeCoilQ3 = new Element(coilName + "Q3_El1_wedge", wed31);
        Element el2_wedgeCoilQ3 = new Element(coilName + "Q3_El2_wedge", wed32);
        Element el1_wedgeCoilQ4 = new Element(coilName + "Q4_El1_wedge", wed41);
        Element el2_wedgeCoilQ4 = new Element(coilName + "Q4_El2_wedge", wed42);

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
        Element[] quad1 = {el1_wedgeCoilQ1, el2_wedgeCoilQ1};
        Element[] quad2 = {el1_wedgeCoilQ2, el2_wedgeCoilQ2};
        Element[] quad3 = {el1_wedgeCoilQ3, el2_wedgeCoilQ3};
        Element[] quad4 = {el1_wedgeCoilQ4, el2_wedgeCoilQ4};

        return quadrantsToBuild(quad1, quad2, quad3, quad4);
    }

    private Element[] holes_yoke() {

        // VARIABLES
        double mm = 0.001;

        // radius of holesYoke
        double x1 = 0 * mm;
        double y1 = 190 * mm;
        double r1 = 30 * mm;

        double x2 = 156 * mm;
        double y2 = 109 * mm;
        double r2 = 14 * mm;

        double x3 = 184 * mm;
        double y3 = 49 * mm;
        double r3 = 14 * mm;


        // POINTS
        Point kp2 = Point.ofCartesian(x2, y2);
        Point kp3 = Point.ofCartesian(x3, y3);

        Point kp1_1 = Point.ofCartesian(x1, y1 + r1);
        Point kp1_3 = Point.ofCartesian(x1 + r1, y1);
        Point kp1_2 = Point.ofCartesian(x1, y1 - r1);
        Arc ln1_1 = Arc.ofThreePoints(kp1_1, kp1_3, kp1_2);
        Line ln1_2 = Line.ofEndPoints(kp1_1, kp1_2);

        // LINES
        Circumference ln2 = Circumference.ofCenterRadius(kp2, r2);
        Circumference ln3 = Circumference.ofCenterRadius(kp3, r3);

        // AREAS
        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1_1, ln1_2});
        Area ar2 = Area.ofHyperLines(new HyperLine[]{ln2});
        Area ar3 = Area.ofHyperLines(new HyperLine[]{ln3});

        // MIRRORED AREAS
        Area ar1_2 = ar1.mirrorY();
        Area ar1_3 = ar1_2.mirrorX();
        Area ar1_4 = ar1.mirrorX();

        Area ar2_2 = ar2.mirrorY();
        Area ar2_3 = ar2_2.mirrorX();
        Area ar2_4 = ar2.mirrorX();

        Area ar3_2 = ar3.mirrorY();
        Area ar3_3 = ar3_2.mirrorX();
        Area ar3_4 = ar3.mirrorX();

        // ELEMENTS
        Element ho1 = new Element("IY_HOLE1", ar1);
        Element ho2 = new Element("IY_HOLE2", ar2);
        Element ho3 = new Element("IY_HOLE3", ar3);

        // MIRRORED ELEMENTS
        Element ho1_2 = new Element("IY_HOLE1_2", ar1_2);
        Element ho1_3 = new Element("IY_HOLE1_3", ar1_3);
        Element ho1_4 = new Element("IY_HOLE1_4", ar1_4);

        Element ho2_2 = new Element("IY_HOLE2_2", ar2_2);
        Element ho2_3 = new Element("IY_HOLE2_3", ar2_3);
        Element ho2_4 = new Element("IY_HOLE2_4", ar2_4);

        Element ho3_2 = new Element("IY_HOLE3_2", ar3_2);
        Element ho3_3 = new Element("IY_HOLE3_3", ar3_3);
        Element ho3_4 = new Element("IY_HOLE3_4", ar3_4);

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
        Element[] quad1 = {ho1, ho2, ho3};
        Element[] quad2 = {ho1_2, ho1_3, ho1_4};
        Element[] quad3 = {ho2_2, ho2_3, ho2_4};
        Element[] quad4 = {ho3_2, ho3_3, ho3_4};
        return quadrantsToBuild(quad1, quad2, quad3, quad4);
    }
}
