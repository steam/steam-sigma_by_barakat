package input.Others.T0;

import model.geometry.coil.Cable;
import model.materials.database.MatDatabase;

/**
 * Created by STEAM on 05/09/2016. TALES #492 and roxie_vs10.cadata as references
 */
public class Cable_T0 extends Cable {

    /**
     * Constructs a cable object with parameters assigned in setCableParameters() method
     */
    public Cable_T0() {
        this.label = "cable_T0";

        //Insulation
        this.wInsulNarrow = 100e-6; // [m];
        this.wInsulWide = 100e-6; // [m];
        //Filament
        this.dFilament = 50.0e-6; // [m];
        //Strand
        this.dstrand = 0.75e-3; // [m];
        this.fracCu = 0.6;
        this.fracSc = 0.4;
        this.RRR = 100;
        this.TupRRR = 295; // [K];
        this.Top = 1.9; // [K];
        //Transient
        this.Rc = 100e-6; // [ohm];
        this.Ra = 100e-6; // [ohm];
        this.fRhoEff = 1; // [1]; from TALES
        this.lTp = 20e-3; // [m];
        //Cable insulated
        this.wBare = 15e-3; // [m]; Roxie/Tales sheet 3 width (not sheet 4)
        this.hInBare = 1.5e-3; // [m];
        this.hOutBare = 1.5e-3; // [m];
        this.noOfStrands = 40;
        this.noOfStrandsPerLayer = 20;
        this.noOfLayers = 2;
        this.lTpStrand = 0.1; // [m];
        this.wCore = 0; // [m];
        this.hCore = 0; // [m];
        this.thetaTpStrand = Math.atan2((wBare - dstrand), (lTpStrand / 2));
        this.C1 = 0; // [A];
        this.C2 = 0; // [A/T];
        this.fracHe = 0.0; //[%]
        this.fracFillInnerVoids = 1; //[-]
        this.fractFillOuterVoids = 1; //[-]

        this.criticalSurfaceFit = CriticalSurfaceFitEnum.Ic_Nb3Sn_NIST;
        this.insulationMaterial = MatDatabase.MAT_GLASSFIBER;
        this.materialInnerVoids = MatDatabase.MAT_GLASSFIBER;
        this.materialOuterVoids = MatDatabase.MAT_GLASSFIBER;
        this.materialCore = MatDatabase.MAT_VOID;
        this.resitivityCopperFit = ResitivityCopperFitEnum.rho_Cu_NIST;
    }
}