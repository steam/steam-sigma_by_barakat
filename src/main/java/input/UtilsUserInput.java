package input;

import model.geometry.Element;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.IntStream;

/**
 * Created by STEAM on 09/11/2016.
 */
public class UtilsUserInput {

    private int[] quadrantsToBeConstructed;

    public int[] getQuadrantsToBeConstructed() {
        return quadrantsToBeConstructed.clone();
    }

    public void setQuadrantsToBeConstructed(int[] quadrantsToBeConstructed) {
        this.quadrantsToBeConstructed = quadrantsToBeConstructed.clone();
    }

    public Element[] quadrantsToBuild(Element[] quad1, Element[] quad2, Element[] quad3, Element[] quad4) {

        int initialCapacity = quad1.length + quad2.length + quad3.length + quad4.length;
        Collection<Element> elementsCollection = new ArrayList<>(initialCapacity);

        if (IntStream.of(quadrantsToBeConstructed).anyMatch(x -> x == 1)) {
            for (Element val : quad1) {
                elementsCollection.add(val);
            }
        }
        if (IntStream.of(quadrantsToBeConstructed).anyMatch(x -> x == 2)) {
            for (Element val : quad2) {
                elementsCollection.add(val);
            }
        }
        if (IntStream.of(quadrantsToBeConstructed).anyMatch(x -> x == 3)) {
            for (Element val : quad3) {
                elementsCollection.add(val);
            }
        }
        if (IntStream.of(quadrantsToBeConstructed).anyMatch(x -> x == 4)) {
            for (Element val : quad4) {
                elementsCollection.add(val);
            }
        }

        Element[] elementsToBuild = new Element[elementsCollection.size()];
        elementsCollection.toArray(elementsToBuild);
        return elementsToBuild;
    }
}
