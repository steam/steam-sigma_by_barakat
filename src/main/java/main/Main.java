package main;

import comsol.MagnetMPHBuilder;
import comsol.QuenchHeaterMPHBuilder; // Added
import config.ConfigSigma;
import input.Others.T0.Magnet_T0;
import model.domains.Domain;
import server.SigmaServer;
import server.TxtSigmaServer;

import javax.script.ScriptException;
import java.io.IOException;

/**
 * Main class of the project with a sample generation process for the T0 magnet
 */
public class Main {

    /**
     * Method executes model building process with SIGMA
     * @param args execution arguments (first element of the array is path to SIGMA config)
     * @throws IOException thrown in case of I/O problems of the input model
     * @throws ScriptException thrown in case of errors in parsing an iron yoke file
     */
    public static void main(String[] args) throws IOException, ScriptException {
        // LVL 1 *******************************************************************************
        // Parse config file:
        ConfigSigma cfg = ConfigSigma.parseFileJSON(args[0]);

        // Magnet construction in java:
        Magnet_T0 magnet = new Magnet_T0();

        // LVL 2 ******************************************************************************
        Domain[] domains = magnet.getDomains();

        // LVL 3 ******************************************************************************
        // Magnet construction in COMSOL:
        SigmaServer srv = new TxtSigmaServer(cfg.getJavaJDKPath(), cfg.getComsolBatchPath());
        //SigmaServer srv = new MphSigmaServer(cfg.getOutputModelPath());

        MagnetMPHBuilder builder = new MagnetMPHBuilder(cfg, srv);
            builder.connectToServer();
            builder.prepareModelTemplate();
            builder.buildMPH(domains);
            builder.save();
            builder.disconnectFromServer();

        // LVL 4 ******************************************************************************
        // Quench Heater construction in COMSOL:
        QuenchHeaterMPHBuilder builderQH = new QuenchHeaterMPHBuilder(cfg, srv);    // Added
            builder.connectToServer();                                              // Added
            builderQH.buildQuenchHeaterMPH();                                       // Added - Important!
            builder.save();                                                         // Moved
            builder.disconnectFromServer();                                         // Moved

        // Compilation is done after all the builders are done.
        srv.compile(cfg.getOutputModelPath());
    }
}