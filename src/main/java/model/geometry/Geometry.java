package model.geometry;

import model.geometry.basic.Point;

/**
 * Abstract class for all geometrical objects
 */
public abstract class Geometry {

    /**
     * Method translates a geometric object by a given distance in x an y direction
     *
     * @param dx - displacement in x
     * @param dy - displacement in y
     * @return a copy of a translated geometric object
     */
    public abstract Geometry translate(double dx, double dy);

    /**
     * Method mirrors a geometric object with respect to the horizontal plane
     *
     * @return a copy of a mirrored geometric object
     */
    public abstract Geometry mirrorX();

    /**
     * Method mirrors a geometric object with respect to the vertical plane
     *
     * @return a copy of a mirrored geometric object
     */
    public abstract Geometry mirrorY();

    /**
     * Method rotates a geometric object by a given angle with respect to the x-axis around the center of a coordinate system
     *
     * @param angle angle of rotation
     * @return a copy of a rotated geometric object
     */
    public abstract Geometry rotate(double angle);

    /**
     * Method rotates a geometric object by a given angle with respect to the provided center of rotation
     * @param angle angle of rotation
     * @param kpCenter coordinates of the center of rotation
     * @return a copy of a rotated geometric object
     */
    public abstract Geometry rotate(double angle, Point kpCenter);
}
