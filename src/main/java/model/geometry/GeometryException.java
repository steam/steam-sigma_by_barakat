package model.geometry;

/**
 * Class defines custom exception thrown in case of geometry issues
 */
public class GeometryException extends RuntimeException {
    private final String message;
    /**
     * Constructs GeometryException object with a given message
     * @param message input exception message
     */
    public GeometryException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
