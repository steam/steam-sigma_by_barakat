package model.geometry;

import model.geometry.basic.HyperArea;

/**
 * Class represents an element, which consists of a hyperarea and a label (needed to recognize the hyperarea in COMSOL)
 */
public class Element {

    private final HyperArea geometry;
    private final String label;

    /**
     * Constructs an element object from a label and geometry represented as a hyperarea
     *
     * @param label    - input label
     * @param geometry - input hyperarea representing the geometry
     */
    public Element(String label, HyperArea geometry) {
        this.label = label;
        this.geometry = geometry;
    }

    public HyperArea getGeometry() { return geometry; }

    public String getLabel() { return label; }

}
