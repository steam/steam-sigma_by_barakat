package model.geometry;

/**
 * Enum allows for selection of the input geometry unit
 */
public enum Unit {
    MILLIMETER,
    METER,
}
