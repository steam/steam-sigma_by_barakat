package model.geometry.coil;

import model.geometry.Geometry;
import model.geometry.basic.Area;
import model.geometry.basic.Line;
import model.geometry.basic.Point;
import model.geometry.basic.Polygon;

import java.util.ArrayList;

/**
 * Class representing a block of half-turns in COMSOL
 */
public class HalfturnBlock extends Geometry {
    private final Area block;
    private final Halfturn[] halfturns;

    private HalfturnBlock(Area block, Halfturn[] halfturns){
        this.block = block;
        this.halfturns = halfturns.clone();
    }

    // *****************************************************************************************************************
    // Static constructors
    // *****************************************************************************************************************

    static HalfturnBlock ofHalfturns(Area block, Cable cable, int noOfTurnsNominal, int noOfTurns, int tauVersor) {
        Halfturn[] halfturns = createHalfturns(block, cable, noOfTurnsNominal, noOfTurns, tauVersor);
        return new HalfturnBlock(block,halfturns);
    }

    // *****************************************************************************************************************
    // Getters
    // *****************************************************************************************************************

    public Halfturn[] getHalfturns() {
        return halfturns.clone();
    }

    public Area getBlock() {
        return block;
    }

    // *****************************************************************************************************************
    // Geometry interface
    // *****************************************************************************************************************

    @Override
    public HalfturnBlock translate(double dx, double dy) {
        Halfturn[] halfturnsNew = new Halfturn[halfturns.length];
        for(int ht_i=0; ht_i < halfturns.length; ht_i++) {
            halfturnsNew[ht_i] = halfturns[ht_i].translate(dx,dy);
        }
        return new HalfturnBlock(block.translate(dx,dy),halfturnsNew);
    }

    @Override
    public HalfturnBlock mirrorX() {
        Area blockNew = block.mirrorX();
        Halfturn[] halfturnsNew = new Halfturn[halfturns.length];
        for(int ht_i=0; ht_i < halfturns.length; ht_i++) {
            halfturnsNew[ht_i] = halfturns[ht_i].mirrorX();
        }
        return new HalfturnBlock(blockNew,halfturnsNew);
    }

    @Override
    public HalfturnBlock mirrorY() {
        Area blockNew = block.mirrorY();
        Halfturn[] halfturnsNew = new Halfturn[halfturns.length];
        for(int ht_i=0; ht_i < halfturns.length; ht_i++) {
            halfturnsNew[ht_i] = halfturns[ht_i].mirrorY();
        }
        return new HalfturnBlock(blockNew,halfturnsNew);
    }

    @Override
    public HalfturnBlock rotate(double angle) {
        Area blockNew = block.rotate(angle);
        Halfturn[] halfturnsNew = new Halfturn[halfturns.length];
        for(int ht_i=0; ht_i < halfturns.length; ht_i++) {
            halfturnsNew[ht_i] = halfturns[ht_i].rotate(angle);
        }
        return new HalfturnBlock(blockNew,halfturnsNew);
    }

    @Override
    public HalfturnBlock rotate(double angle, Point kpCenter) {
        Area blockNew = block.rotate(angle, kpCenter);
        Halfturn[] halfturnsNew = new Halfturn[halfturns.length];
        for(int ht_i=0; ht_i < halfturns.length; ht_i++) {
            halfturnsNew[ht_i] = halfturns[ht_i].rotate(angle, kpCenter);
        }
        return new HalfturnBlock(blockNew,halfturnsNew);
    }


    // *****************************************************************************************************************
    // Specific Methods
    // *****************************************************************************************************************

    /**
     * Method reverses the winding direction of half-turns within a block, i.e., the electrical connection order
     *
     * @return a block of half-turns with reverted winding direction
     */
    public HalfturnBlock reverseWindingDirection(){
        Halfturn[] halfturnsNew = new Halfturn[halfturns.length];
        int noOfHalfturns = halfturns.length;
        for (int ht_i = 0; ht_i < noOfHalfturns; ht_i++) {
            halfturnsNew[ht_i] = halfturns[noOfHalfturns-ht_i-1].reverseWindingDirection();
        }
        return new HalfturnBlock(block.reverseLines(),halfturnsNew);
    }

    private static Halfturn[] createHalfturns(Area block, Cable cable, int noOfTurnsNominal, int noOfTurns, int tauVersor) {
        double ratio = (double) noOfTurnsNominal/noOfTurns;
        ArrayList<Halfturn> htList = new ArrayList<>();

            Point[] points1 = block.getHyperLines()[0].extractPoints(noOfTurns +1);
            Point[] points3 = block.getHyperLines()[2].extractPoints(noOfTurns +1);

            for(int turn_i = 0; turn_i < noOfTurns; turn_i++) {

                Line l1 = Line.ofEndPoints(points1[turn_i],   points1[turn_i+1]);
                Line l2 = Line.ofEndPoints(points1[turn_i],   points3[turn_i]);
                Line l3 = Line.ofEndPoints(points3[turn_i],   points3[turn_i+1]);
                Line l4 = Line.ofEndPoints(points1[turn_i+1], points3[turn_i+1]);

                Polygon polygon = Polygon.ofLines(new Line[] {l1,l2,l3,l4});

                htList.add(Halfturn.ofPolygon(cable, polygon, ratio, tauVersor));
            }
        return htList.toArray(new Halfturn[htList.size()]);
    }


}
