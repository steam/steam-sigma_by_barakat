package model.geometry.coil;

import model.geometry.Geometry;
import model.geometry.basic.Area;
import model.geometry.basic.Point;

import java.util.ArrayList;

/**
 * Class representing a winding in COMSOL
 */
public class Winding extends Geometry{

    private final HalfturnBlock[] blocks;
    private final int noOfTurns;
    private final int[] tauVersor;
    private final Cable cable;

    private Winding(HalfturnBlock[] blocks, int noOfTurns, int[] tauVersor, Cable cable) {
        this.blocks = blocks.clone();
        this.noOfTurns = noOfTurns;
        this.tauVersor = tauVersor.clone();
        this.cable = cable;
    }

    // *****************************************************************************************************************
    // Static constructors
    // *****************************************************************************************************************

    /**
     * Method creates a winding from a geometric definition of areas and parameters
     *
     * @param areas            array of areas with geometric representation of a winding
     * @param tauVersor        array of winding density versors (direction of current)
     * @param noOfTurnsNominal number of turns in the magnet design
     * @param noOfTurns        number of turns per winding (less or equal to noOfTurnsNominal; if less, then homogenization is applied)
     * @param cable            input cable parameters
     * @return Winding object with initialized parameters and geometry
     */
    public static Winding ofAreas(Area[] areas, int[] tauVersor, int noOfTurnsNominal, int noOfTurns, Cable cable) {

        ArrayList<HalfturnBlock> blockList = new ArrayList<>();
        for (int area_i = 0; area_i < areas.length; area_i++) {
            blockList.add(HalfturnBlock.ofHalfturns(areas[area_i],cable, noOfTurnsNominal, noOfTurns,tauVersor[area_i]));
        }
        HalfturnBlock[] blocks = blockList.toArray(new HalfturnBlock[blockList.size()]);

        return new Winding(blocks, noOfTurns, tauVersor, cable);
    }

    // *****************************************************************************************************************
    // Getters
    // *****************************************************************************************************************

    public HalfturnBlock[] getBlocks() {
        return blocks.clone();
    }

    public int getNoOfTurns() {return noOfTurns;}

    public int[] getTauVersor() {
        return tauVersor.clone();
    }

    public Cable getCable() {
        return cable;
    }


    // *****************************************************************************************************************
    // Geometry interface
    // *****************************************************************************************************************

    @Override
    public Winding translate(double dx, double dy) {
        HalfturnBlock[] blocksNew = new HalfturnBlock[blocks.length];
        for(int block_i=0; block_i < blocks.length; block_i++) {
            blocksNew[block_i] = blocks[block_i].translate(dx, dy);
        }
        return new Winding(blocksNew, noOfTurns, tauVersor, cable);
    }

    @Override
    public Winding mirrorX() {
        HalfturnBlock[] blocksNew = new HalfturnBlock[blocks.length];
        for(int block_i=0; block_i < blocks.length; block_i++) {
            blocksNew[block_i] = blocks[block_i].mirrorX();
        }
        return new Winding(blocksNew, noOfTurns, tauVersor, cable);
    }

    @Override
    public Winding mirrorY() {
        HalfturnBlock[] blocksNew = new HalfturnBlock[blocks.length];
        for(int block_i=0; block_i < blocks.length; block_i++) {
            blocksNew[block_i] = blocks[block_i].mirrorY();
        }
        return new Winding(blocksNew, noOfTurns, tauVersor, cable);
    }

    @Override
    public Winding rotate(double angle) {
        HalfturnBlock[] blocksNew = new HalfturnBlock[blocks.length];
        for(int block_i=0; block_i < blocks.length; block_i++) {
            blocksNew[block_i] = blocks[block_i].rotate(angle);
        }
        return new Winding(blocksNew, noOfTurns, tauVersor, cable);
    }

    @Override
    public Winding rotate(double angle, Point kpCenter) {
        HalfturnBlock[] blocksNew = new HalfturnBlock[blocks.length];
        for(int block_i=0; block_i < blocks.length; block_i++) {
            blocksNew[block_i] = blocks[block_i].rotate(angle, kpCenter);
        }
        return new Winding(blocksNew, noOfTurns, tauVersor, cable);
    }

    // *****************************************************************************************************************
    // Specific Methods
    // *****************************************************************************************************************

    /**
     * Method reverts the order of windings (electrical ordering)
     *
     * @return object with reverted winding direction
     */
    public Winding reverseWindingDirection() {
        HalfturnBlock[] blocksNew = new HalfturnBlock[blocks.length];
        for(int block_i=0; block_i < blocks.length; block_i++) {
            blocksNew[block_i] = blocks[block_i].reverseWindingDirection();
        }
        return new Winding(blocksNew, noOfTurns, tauVersor, cable);
    }

    /**
     * Method reverts the direction of current flow in a winding
     *
     * @return object with opposite current direction
     */
    public Winding reverseWindingCurrent() {
        int[] tauVersorNew = new int[tauVersor.length];
        for (int tv_i = 0; tv_i < tauVersor.length; tv_i++) {
            tauVersorNew[tv_i]=(-1)*tauVersor[tv_i];
        }
        return new Winding(blocks, noOfTurns, tauVersorNew, cable);
    }
}
