package model.geometry.coil;

import model.geometry.Geometry;
import model.geometry.basic.Line;
import model.geometry.basic.Point;
import model.geometry.basic.Polygon;

import java.util.ArrayList;

/**
 * Class representing a half-turn in COMSOL
 */
public class Halfturn extends Geometry {
    private final Polygon polygon;
    private final Line wideBezier;
    private final Line narrowBezier;
    private final Strand[] strands;
    private final Cable cable;
    private final double tauVersor;
    private final double surface;
    private final double alpha;
    private final double tau;
    private final double ratio;

    private Halfturn(Cable cable, Polygon polygon, double ratio, double tauVersor) {
        this.polygon = polygon;
        this.wideBezier = createlBezier();
        this.narrowBezier = createsBezier();
        this.cable = cable;
        this.alpha = wideBezier.calculateAngularCoefficient();
        this.tauVersor = tauVersor;
        this.surface = polygon.calculateSurface();
        this.tau = calculateTau(polygon, ratio, tauVersor);
        this.ratio = ratio;
        this.strands = createStrands();
    }

    // *****************************************************************************************************************
    // Static constructors
    // *****************************************************************************************************************

    /**
     * Method creates a half-turn from a geometric definition of a polygon and parameters
     *
     * @param cable     - input cable parameters
     * @param polygon   - input polygon representing a turn
     * @param ratio     - ratio of half-turns in case homogenization is applied
     * @param tauVersor - winding density versor (direction of current)
     * @return a HalfTurn object
     */
    public static Halfturn ofPolygon(Cable cable, Polygon polygon, double ratio, double tauVersor){
        return new Halfturn(cable, polygon, ratio, tauVersor);
    }

    // *****************************************************************************************************************
    // Getters
    // *****************************************************************************************************************

    public Polygon getPolygon() {
        return polygon;
    }

    public double getTauVersor() {
        return tauVersor;
    }

    public Line getWideBezier() {
        return wideBezier;
    }

    public Line getNarrowBezier() {
        return narrowBezier;
    }

    public double getSurface() {
        return surface;
    }

    public double getAlpha() {
        return alpha;
    }

    public double getTau() {
        return tau;
    }

    public Strand[] getStrands() {
        return strands.clone();
    }

    public double getRatio() {
        return ratio;
    }

    // *****************************************************************************************************************
    // Geometry interface
    // *****************************************************************************************************************

    @Override
    public Halfturn translate(double dx, double dy) {
        return new Halfturn(cable, polygon.translate(dx, dy), ratio, tauVersor);
    }

    @Override
    public Halfturn mirrorX() {
        return new Halfturn(cable, polygon.mirrorX(), ratio, tauVersor);
    }

    @Override
    public Halfturn mirrorY() {
        return new Halfturn(cable, polygon.mirrorY(), ratio, tauVersor);
    }

    @Override
    public Halfturn rotate(double angle) {
        return new Halfturn(cable, polygon.rotate(angle), ratio, tauVersor);
    }

    @Override
    public Halfturn rotate(double angle, Point kpc) {
        return new Halfturn(cable, polygon.rotate(angle, kpc), ratio, tauVersor);
    }

    // *****************************************************************************************************************
    // Specific Methods
    // *****************************************************************************************************************

    Halfturn reverseWindingDirection(){
        return new Halfturn(cable, polygon.reverseLines(), ratio, tauVersor);
    }


    private Line createlBezier() {
        Point[] points1 = polygon.getHyperLines()[0].extractPoints(3);
        Point[] points3 = polygon.getHyperLines()[2].extractPoints(3);
        return Line.ofEndPoints(points1[1], points3[1]);
    }

    private Line createsBezier() {
        Point[] points2 = polygon.getHyperLines()[1].extractPoints(3);
        Point[] points4 = polygon.getHyperLines()[3].extractPoints(3);
        return Line.ofEndPoints(points2[1], points4[1]);
    }

    private static double calculateTau(Polygon polygon, double ratio, double tauVersor) {
        return  ratio / polygon.calculateSurface() * tauVersor;
    }

    private Strand[] createStrands() {
        int noOfLayers = cable.noOfLayers;
        int noOfStandsPerLayer = cable.noOfStrandsPerLayer;
        double diamStrand = cable.dstrand;

        double xTilt = -diamStrand*noOfStandsPerLayer/2+diamStrand/2;
        double yTilt = 0;
        Point kpTilt = Point.ofCartesian(xTilt,yTilt);
        double angleTilt = Math.atan2((cable.hOutBare-cable.hInBare)/2,cable.wBare);

        double x;
        double y;
        ArrayList<Strand> strandsList = new ArrayList<>();

        for (int l_i = 0; l_i < noOfLayers; l_i++) {
            x = -diamStrand*noOfStandsPerLayer/2+diamStrand/2;
            y = diamStrand/2*(noOfLayers-1-2*l_i);

            Point kp1 = Point.ofCartesian(x,y);
            Point kp2 = Point.ofCartesian((-1)*x,y);

            Line l1 = Line.ofEndPoints(kp1,kp2);
            Point[] centers = l1.extractPoints(noOfStandsPerLayer);

            for (int p_i = 0; p_i < centers.length; p_i++) {
                x = polygon.getBaricenter().getX();
                y = polygon.getBaricenter().getY();

                Point centerMoved =centers[p_i].rotate(angleTilt,kpTilt).rotate(alpha).translate(x,y);
                strandsList.add(Strand.ofPoint(centerMoved));
            }
        }
        return strandsList.toArray(new Strand[strandsList.size()]);
    }

}

