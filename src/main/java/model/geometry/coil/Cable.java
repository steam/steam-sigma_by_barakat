package model.geometry.coil;

import model.materials.database.MatDatabase;

/**
 * Class with input cable parameters
 */
public class Cable {

    /**
     * Label of a cable in COMSOL model
     */
    public String label;

    //Insulation
    /**
     * Width of the narrow edge of cable insulation
     */
    public double wInsulNarrow;

    /**
     * Width of the wide edge of cable insulation
     */
    public double wInsulWide;

    //Filament
    /**
     * Filament diameter
     */
    public double dFilament;

    //Strand
    /**
     * Strand diameter
     */
    public double dstrand;

    /**
     * Fraction of copper
     */
    public double fracCu;

    /**
     * Fraction of superconductor
     */
    public double fracSc;

    /**
     * Residual Resistivity Ratio of Copper
     */
    public double RRR;

    /**
     * Upper temperature for RRR calculation
     */
    public double TupRRR;

    /**
     * Operational temperature (e.g., for the LHC main dipole it is 1.9 K)
     */
    public double Top;

    //Transient
    /**
     * Cross-contact resistance (ISCC)
     */
    public double Rc;

    /**
     * Adjecent resistance (ISCC)
     */
    public double Ra;

    /**
     * Effective resitivity ratio
     */
    public double fRhoEff;

    /**
     * Filament twist pitch
     */
    public double lTp;

    //Cable
    /**
     * Width of a bare cable
     */
    public double wBare;

    /**
     * Height of the innermost part of a bare cable
     */
    public double hInBare;

    /**
     * Height of the outermost part of a bare cable
     */
    public double hOutBare;

    /**
     * Number of strands in a cable
     */
    public int noOfStrands;

    /**
     * Number of strands per layer
     */
    public int noOfStrandsPerLayer;

    /**
     * Number of layers in a cable
     */
    public int noOfLayers;

    /**
     * Twist pitch of a strand
     */
    public double lTpStrand;

    /**
     * Width of a cable's core
     */
    public double wCore;

    /**
     * Height of a cable's core
     */
    public double hCore;

    /**
     * Twist pitch angle of a strand
     */
    public double thetaTpStrand;

    /**
     * C1 parameter for the critical current density of Nb-Ti cables
     */
    public double C1;

    /**
     * C2 parameter for the critical current density of Nb-Ti cables
     */
    public double C2;

    /**
     * Fraction of helium in a cable
     */
    public double fracHe;

    /**
     * Fraction of a filling material in the inner voids
     */
    public double fracFillInnerVoids;

    /**
     * Fraction of a filling material in the outer voids
     */
    public double fractFillOuterVoids;

    /**
     * Choice of copper resistivity fit
     */
    public ResitivityCopperFitEnum resitivityCopperFit;

    /**
     * Choice of critical current fit
     */
    public CriticalSurfaceFitEnum criticalSurfaceFit;

    /**
     * Choice of the insulation material
     */
    public MatDatabase insulationMaterial;

    /**
     * Choice of the material for the inner voids
     */
    public MatDatabase materialInnerVoids;

    /**
     * Choice of the material in the cable core
     */
    public MatDatabase materialCore;

    public Cable setwInsulNarrow(double wInsulNarrow) {
        this.wInsulNarrow = wInsulNarrow;
        return this;
    }

    public Cable setwInsulWide(double wInsulWide) {
        this.wInsulWide = wInsulWide;
        return this;
    }

    public Cable setdFilament(double dFilament) {
        this.dFilament = dFilament;
        return this;
    }

    public Cable setDstrand(double dstrand) {
        this.dstrand = dstrand;
        return this;
    }

    public Cable setFracCu(double fracCu) {
        this.fracCu = fracCu;
        return this;
    }

    public Cable setFracSc(double fracSc) {
        this.fracSc = fracSc;
        return this;
    }

    public Cable setTupRRR(double tupRRR) {
        TupRRR = tupRRR;
        return this;
    }

    public Cable setTop(double top) {
        Top = top;
        return this;
    }

    public Cable setRc(double rc) {
        Rc = rc;
        return this;
    }

    public Cable setRa(double ra) {
        Ra = ra;
        return this;
    }

    public Cable setfRhoEff(double fRhoEff) {
        this.fRhoEff = fRhoEff;
        return this;
    }

    public Cable setlTp(double lTp) {
        this.lTp = lTp;
        return this;
    }

    public Cable setwBare(double wBare) {
        this.wBare = wBare;
        return this;
    }

    public Cable sethInBare(double hInBare) {
        this.hInBare = hInBare;
        return this;
    }

    public Cable sethOutBare(double hOutBare) {
        this.hOutBare = hOutBare;
        return this;
    }

    public Cable setNoOfStrands(int noOfStrands) {
        this.noOfStrands = noOfStrands;
        return this;
    }

    public Cable setNoOfStrandsPerLayer(int noOfStrandsPerLayer) {
        this.noOfStrandsPerLayer = noOfStrandsPerLayer;
        return this;
    }

    public Cable setNoOfLayers(int noOfLayers) {
        this.noOfLayers = noOfLayers;
        return this;
    }

    public Cable setlTpStrand(double lTpStrand) {
        this.lTpStrand = lTpStrand;
        return this;
    }

    public Cable setwCore(double wCore) {
        this.wCore = wCore;
        return this;
    }

    public Cable sethCore(double hCore) {
        this.hCore = hCore;
        return this;
    }

    public Cable setThetaTpStrand(double thetaTpStrand) {
        this.thetaTpStrand = thetaTpStrand;
        return this;
    }

    public Cable setC1(double c1) {
        C1 = c1;
        return this;
    }

    public Cable setC2(double c2) {
        C2 = c2;
        return this;
    }

    public Cable setFracHe(double fracHe) {
        this.fracHe = fracHe;
        return this;
    }

    public Cable setFracFillInnerVoids(double fracFillInnerVoids) {
        this.fracFillInnerVoids = fracFillInnerVoids;
        return this;
    }

    public Cable setFractFillOuterVoids(double fractFillOuterVoids) {
        this.fractFillOuterVoids = fractFillOuterVoids;
        return this;
    }

    public Cable setResitivityCopperFit(ResitivityCopperFitEnum resitivityCopperFit) {
        this.resitivityCopperFit = resitivityCopperFit;
        return this;
    }

    public Cable setCriticalSurfaceFit(CriticalSurfaceFitEnum criticalSurfaceFit) {
        this.criticalSurfaceFit = criticalSurfaceFit;
        return this;
    }

    public Cable setInsulationMaterial(MatDatabase insulationMaterial) {
        this.insulationMaterial = insulationMaterial;
        return this;
    }

    public Cable setMaterialInnerVoids(MatDatabase materialInnerVoids) {
        this.materialInnerVoids = materialInnerVoids;
        return this;
    }

    public Cable setMaterialCore(MatDatabase materialCore) {
        this.materialCore = materialCore;
        return this;
    }

    public Cable setMaterialOuterVoids(MatDatabase materialOuterVoids) {
        this.materialOuterVoids = materialOuterVoids;
        return this;
    }

    /**
     * Choice of the material for the outer voids
     */
    public MatDatabase materialOuterVoids;

    /**
     * Choice of the resistivity copper fit
     */
    public enum ResitivityCopperFitEnum {
        rho_Cu_CUDI,
        rho_Cu_NIST,
        rho_Cu_TEST
    }

    /**
     * Choice of the critical surface fit
     */
    public enum CriticalSurfaceFitEnum {
        Ic_NbTi_GSI,
        Ic_NbTi_D2,
        Ic_Nb3Sn_NIST,
        Ic_Nb3Sn_FCC,
        Ic_TEST
    }

    public String getLabel() {
        return label;
    }

    public ResitivityCopperFitEnum getResitivityCopperFit() {
        return resitivityCopperFit;
    }

    public CriticalSurfaceFitEnum getCriticalSurfaceFit() {
        return criticalSurfaceFit;
    }

    public MatDatabase getInsulationMaterial() {
        return insulationMaterial;
    }

    public MatDatabase getMaterialInnerVoids() {
        return materialInnerVoids;
    }

    public MatDatabase getMaterialOuterVoids() {
        return materialOuterVoids;
    }

    public MatDatabase getMaterialCore() {
        return materialCore;
    }

    public Cable setLabel(String label) {
        this.label = label;
        return this;
    }

    public Cable setRRR(double RRR) {
        this.RRR = RRR;
        return this;
    }
}
