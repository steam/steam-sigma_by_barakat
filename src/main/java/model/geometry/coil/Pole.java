package model.geometry.coil;

import model.geometry.Geometry;
import model.geometry.basic.Point;

/**
 * Class representing a pole in COMSOL
 */
public class Pole extends Geometry {
    private final Winding[] windings;

    private Pole(Winding[] windings) {
        this.windings = windings.clone();
    }

    // *****************************************************************************************************************
    // Static constructors
    // *****************************************************************************************************************

    /**
     * Method creates a pole from the input windings definition
     *
     * @param windings - input array of windings
     * @return a pole made of windings
     */
    public static Pole ofWindings(Winding[] windings){
        return new Pole(windings);
    }

    // *****************************************************************************************************************
    // Getters
    // *****************************************************************************************************************

    public Winding[] getWindings() {
        return windings.clone();
    }

    // *****************************************************************************************************************
    // Geometry interface
    // *****************************************************************************************************************

    @Override
    public Pole translate(double dx, double dy) {
        Winding[] windingsNew = new Winding[windings.length];
        for(int winding_i=0; winding_i < windings.length; winding_i++) {
            windingsNew[winding_i] = windings[winding_i].translate(dx, dy);
        }
        return new Pole(windingsNew);
    }

    @Override
    public Pole mirrorX() {
        Winding[] windingsNew = new Winding[windings.length];
        for(int winding_i=0; winding_i < windings.length; winding_i++) {
            windingsNew[winding_i] = windings[winding_i].mirrorX();
        }
        return new Pole(windingsNew);
    }

    @Override
    public Pole mirrorY() {
        Winding[] windingsNew = new Winding[windings.length];
        for(int winding_i=0; winding_i < windings.length; winding_i++) {
            windingsNew[winding_i] = windings[winding_i].mirrorY();
        }
        return new Pole(windingsNew);
    }

    @Override
    public Pole rotate(double angle) {
        Winding[] windingsNew = new Winding[windings.length];
        for(int winding_i=0; winding_i < windings.length; winding_i++) {
            windingsNew[winding_i] = windingsNew[winding_i].rotate(angle);
        }
        return new Pole(windingsNew);
    }

    @Override
    public Pole rotate(double angle, Point kpCenter) {
        Winding[] windingsNew = new Winding[windings.length];
        for(int winding_i=0; winding_i < windings.length; winding_i++) {
            windingsNew[winding_i] = windingsNew[winding_i].rotate(angle, kpCenter);
        }
        return new Pole(windingsNew);
    }

    // *****************************************************************************************************************
    // Specific Methods
    // *****************************************************************************************************************

    /**
     * Method reverses the winding direction of half-turns within a pole, i.e., the electrical connection order
     *
     * @return a pole with reverted winding direction
     */
    public Pole reverseWindingDirection() {
        Winding[] windNew = new Winding[windings.length];
        for(int wind_i=0; wind_i < windings.length; wind_i++) {
            windNew[wind_i] = windings[wind_i].reverseWindingDirection();
        }
        return new Pole(windNew);
    }

}
