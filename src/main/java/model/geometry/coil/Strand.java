package model.geometry.coil;

import model.geometry.Geometry;
import model.geometry.basic.Point;


/**
 * Class representing a strand in COMSOL
 */
public class Strand extends Geometry {
    private final Point center;

    private Strand(Point center) {
        this.center = center;
    }

    // *****************************************************************************************************************
    // Static constructors
    // *****************************************************************************************************************

    static Strand ofPoint(Point position) {
        return new Strand(position);
    }

    // *****************************************************************************************************************
    // Getters
    // *****************************************************************************************************************

    public Point getCenter() {
        return center;
    }

    // *****************************************************************************************************************
    // Geometry interface
    // *****************************************************************************************************************

    @Override
    public Strand translate(double dx, double dy) {
        return new Strand(center.translate(dx, dy));
    }

    @Override
    public Strand mirrorX() {
        return new Strand(center.mirrorX());
    }

    @Override
    public Strand mirrorY() {
        return new Strand(center.mirrorY());
    }

    @Override
    public Strand rotate(double angle) {
        return new Strand(center.rotate(angle));
    }

    @Override
    public Strand rotate(double angle, Point kpCenter) {
        return new Strand(center.rotate(angle, kpCenter));
    }

    // *****************************************************************************************************************
    // Specific Methods
    // *****************************************************************************************************************


}
