package model.geometry.coil;

import model.geometry.Geometry;
import model.geometry.basic.Point;

/**
 * Created by STEAM on 09/11/2017.
 */
public class Coil extends Geometry{
    private final Pole[] poles;

    private Coil(Pole[] poles) {
        this.poles = poles.clone();
    }

    // *****************************************************************************************************************
    // Static constructors
    // *****************************************************************************************************************

    /**
     * Method creates a coil from poles
     *
     * @param poles - input array of poles
     * @return coil object
     */
    public static Coil ofPoles(Pole[] poles) {
        return new Coil(poles);
    }

    // *****************************************************************************************************************
    // Getters
    // *****************************************************************************************************************

    public Pole[] getPoles() {
        return poles.clone();
    }

    // *****************************************************************************************************************
    // Geometry interface
    // *****************************************************************************************************************

    @Override
    public Coil translate(double dx, double dy) {
        Pole[] polesNew = new Pole[poles.length];
        for(int pole_i=0; pole_i < poles.length; pole_i++) {
            polesNew[pole_i] = poles[pole_i].translate(dx, dy);
        }
        return new Coil(polesNew);
    }

    @Override
    public Coil mirrorX() {
        Pole[] polesNew = new Pole[poles.length];
        for(int pole_i=0; pole_i < poles.length; pole_i++) {
            polesNew[pole_i] = poles[pole_i].mirrorX();
        }
        return new Coil(polesNew);
    }

    @Override
    public Coil mirrorY() {
        Pole[] polesNew = new Pole[poles.length];
        for(int pole_i=0; pole_i < poles.length; pole_i++) {
            polesNew[pole_i] = poles[pole_i].mirrorY();
        }
        return new Coil(polesNew);
    }

    @Override
    public Coil rotate(double angle) {
        Pole[] polesNew = new Pole[poles.length];
        for(int pole_i=0; pole_i < poles.length; pole_i++) {
            polesNew[pole_i] = poles[pole_i].rotate(angle);
        }
        return new Coil(polesNew);
    }

    @Override
    public Coil rotate(double angle, Point kpCenter) {
        Pole[] polesNew = new Pole[poles.length];
        for(int pole_i=0; pole_i < poles.length; pole_i++) {
            polesNew[pole_i] = poles[pole_i].rotate(angle,kpCenter);
        }
        return new Coil(polesNew);
    }

    // *****************************************************************************************************************
    // Specific Methods
    // *****************************************************************************************************************
}
