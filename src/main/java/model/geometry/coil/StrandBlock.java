package model.geometry.coil;


import model.geometry.Geometry;
import model.geometry.basic.Point;

/**
 * Class representing a block of strands in COMSOL
 */
public class StrandBlock extends Geometry {
    private final Strand[] strands;

    private StrandBlock(Strand[] strands) {
        this.strands = strands.clone();
    }

    // *****************************************************************************************************************
    // Static constructors
    // *****************************************************************************************************************

    /**
     * Static method creating a StrandBlock object from an array of strand positions
     * @param position array of strand positions in a block
     * @return immutable StrandBlock object
     */
    public static StrandBlock ofStrand(Strand[] position) {
        return new StrandBlock(position);
    }

    // *****************************************************************************************************************
    // Getters
    // *****************************************************************************************************************

    public Strand[] getStrands() {
        return strands.clone();
    }

    // *****************************************************************************************************************
    // Geometry interface
    // *****************************************************************************************************************

    @Override
    public StrandBlock translate(double dx, double dy) {
        Strand[] strandNew = new Strand[strands.length];
        for(int strand_i=0; strand_i < strands.length; strand_i++) {
            strandNew[strand_i] = strands[strand_i].translate(dx, dy);
        }
        return new StrandBlock(strandNew);
    }

    @Override
    public StrandBlock mirrorX() {
        Strand[] strandNew = new Strand[strands.length];
        for(int strand_i=0; strand_i < strands.length; strand_i++) {
            strandNew[strand_i] = strands[strand_i].mirrorX();
        }
        return new StrandBlock(strandNew);
    }

    @Override
    public StrandBlock mirrorY() {
        Strand[] strandNew = new Strand[strands.length];
        for(int strand_i=0; strand_i < strands.length; strand_i++) {
            strandNew[strand_i] = strands[strand_i].mirrorY();
        }
        return new StrandBlock(strandNew);
    }

    @Override
    public StrandBlock rotate(double angle) {
        Strand[] strandNew = new Strand[strands.length];
        for(int strand_i=0; strand_i < strands.length; strand_i++) {
            strandNew[strand_i] = strands[strand_i].rotate(angle);
        }
        return new StrandBlock(strandNew);
    }

    @Override
    public StrandBlock rotate(double angle, Point kpCenter) {
        Strand[] strandNew = new Strand[strands.length];
        for(int strand_i=0; strand_i < strands.length; strand_i++) {
            strandNew[strand_i] = strands[strand_i].rotate(angle,kpCenter);
        }
        return new StrandBlock(strandNew);
    }


    // *****************************************************************************************************************
    // Specific Methods
    // *****************************************************************************************************************
}
