package model.geometry;

import model.geometry.basic.HyperArea;


/**
 * Extended class of Element class referring to the iron yoke element
 */
public class IronYokeElement extends Element{
    /**
     * Constructs an iron yoke element with a given label and geometry definition
     *
     * @param label    iron yoke label
     * @param geometry iron yoke geometry definition
     */
    public IronYokeElement(String label, HyperArea geometry) {
        super(label, geometry);
    }
}
