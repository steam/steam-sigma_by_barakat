package model.geometry;

import model.domains.Domain;
import model.domains.database.AirDomain;
import model.domains.database.IronDomain;
import model.geometry.basic.*;
import model.materials.database.MatDatabase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utilities.IronYokeParserUtils;
import utils.TextFile;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class with methods parsing the iron yoke geometry text file
 */
public class IronYokeParser {

    private static final Logger LOGGER = LogManager.getLogger(IronYokeParser.class);

    private static final int NB_BLOCK_IRON_YOKE = 4;
    private static final int NB_BLOCK_INFINITE = 99999;
    private static final String VARIABLES_KEYWORD = "--VARIABLES";
    private static final String SPACE_KEYWORD = "(\\w+)";
    private static final String JAVASCRIPT = "JavaScript";
    private static final String NODES_KEYWORD = "--NODES";
    private static final String LINES_KEYWORD = "--LINES";
    private static final String AREAS_KEYWORD = "--AREAS";
    private static final String HOLES_KEYWORD = "--HOLES";
    private static final int NB_OF_ELLIPTICARC_PARAMETERS = 4;

    /**
     * Method adds a list of IronHokes and Holes into one
     *
     * @param listOfDomains            list of input domains
     * @param ironYokeGeometryFilePath an absolute path to an iron yoke geometry file
     * @param bhCurveDatabasePath      an absolut path to a BH characteristics file
     * @throws FileNotFoundException thrown in case of any of the files is missing
     * @throws ScriptException       thrown in case of mathematical script execution errors
     */
    public static void addListOfIronYokesAndHoles(List<Domain> listOfDomains,
                                                  String ironYokeGeometryFilePath,
                                                  String bhCurveDatabasePath) throws FileNotFoundException, ScriptException {
        //Add iron yoke domains
        Map<String, List<Element>> bhLabelToIYElement = IronYokeParser.parseIronYokeGeometry(ironYokeGeometryFilePath,
                Unit.METER);
        for (Map.Entry<String, List<Element>> entry : bhLabelToIYElement.entrySet()) {
            List<Element> elements = entry.getValue();
            StringBuilder sb = new StringBuilder("IY");
            for (Element e : elements) {
                sb.append(e.getLabel());
            }
            String bhLabel = entry.getKey();
            Element[] arrayOfElements = elements.toArray(new Element[elements.size()]);
            if (arrayOfElements[0] instanceof IronYokeElement) {
                IronDomain id = new IronDomain(sb.toString(), bhCurveDatabasePath, bhLabel, arrayOfElements);
                listOfDomains.add(id);
            } else if (arrayOfElements[0] instanceof HoleInIronYokeElement) {
                listOfDomains.add(new AirDomain(sb.toString(), MatDatabase.MAT_AIR, arrayOfElements));
            }
        }


    }

    /**
     * Method parses an iron yoke geometry file with a given unit
     *
     * @param ironYokeGeometryPath an absolute path to an iron yoke geometry file
     * @param unit                 unit of the geometry (either meter or millimeter)
     * @return a map from a label of a geometric entity to a corresponding list of elements
     * @throws FileNotFoundException thrown in case of any of the files is missing
     * @throws ScriptException       thrown in case of mathematical script execution errors
     */
    public static Map<String, List<Element>> parseIronYokeGeometry(String ironYokeGeometryPath, Unit unit) throws FileNotFoundException, ScriptException {
        List<String> ironGeometryText = TextFile.read(ironYokeGeometryPath); //read text file
        int currentIndex = skipComments(ironGeometryText);
        int nbOfBlockParsed = 0;

        //Create maps for Variables, Points, Lines and Areas
        Map<String, Double> variableMap = new HashMap<>();
        Map<String, Point> pointMap = new HashMap<>();
        Map<String, HyperLine> hyperLineMap = new HashMap<>();
        Map<String, Area> areaMap = new HashMap<>();
        Map<String, String> bhLabelMap = new HashMap<>();
        Map<String, List<Element>> ironMap = new HashMap<>();
        Map<String, Area> holeMap = new HashMap<>();

        //Browse the text file
        while (nbOfBlockParsed <= NB_BLOCK_IRON_YOKE) {
            String blockName = ironGeometryText.get(currentIndex); //Get the name of the current block
            currentIndex++;
            boolean isEmptyLine = false;
            while (!isEmptyLine) { //while the current block contains lines
                String line = ironGeometryText.get(currentIndex);
                Matcher lineVariable = Pattern.compile(SPACE_KEYWORD).matcher(line);
                boolean wasVariable = lineVariable.find();
                if (wasVariable) {
                    String[] dividedLine = line.split("=");
                    switch (blockName) {
                        case VARIABLES_KEYWORD:
                            parseVariable(variableMap, dividedLine);
                            break;
                        case NODES_KEYWORD:
                            parsePoint(variableMap, pointMap, dividedLine, unit);
                            break;
                        case LINES_KEYWORD:
                            parseHyperLine(pointMap, variableMap, hyperLineMap, dividedLine, unit);
                            break;
                        case AREAS_KEYWORD:
                            parseArea(hyperLineMap, areaMap, dividedLine, bhLabelMap);
                            break;
                        case HOLES_KEYWORD:
                            parseHole(variableMap, pointMap, hyperLineMap, areaMap, holeMap, bhLabelMap, line, unit);
                            break;
                        default:
                            LOGGER.error("{}, is not supported", blockName);
                    }
                    currentIndex++;
                } else {
                    while (!wasVariable && nbOfBlockParsed <= NB_BLOCK_IRON_YOKE) {
                        isEmptyLine = true;
                        currentIndex++;
                        if (currentIndex < ironGeometryText.size()) {
                            line = ironGeometryText.get(currentIndex);
                            lineVariable = Pattern.compile(SPACE_KEYWORD).matcher(line);
                            wasVariable = lineVariable.find();
                        } else {
                            nbOfBlockParsed = NB_BLOCK_INFINITE;
                        }
                    }
                }
            }
            nbOfBlockParsed++;
        }

        Set<String> setString = areaMap.keySet();
        Iterator<String> iterator = setString.iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            String bhLabel = bhLabelMap.get(key);
            // If is iron
            if (holeMap.containsKey(key)) {
                Element holeElement = new HoleInIronYokeElement(key, areaMap.get(key));
                if (ironMap.containsKey(bhLabel)) {
                    ironMap.get(bhLabel).add(holeElement);
                } else {
                    List<Element> elementList = new ArrayList<>();
                    elementList.add(holeElement);
                    ironMap.put(bhLabel, elementList);
                }
            } else {
                Element ironElement = new IronYokeElement(key, areaMap.get(key));
                if (ironMap.containsKey(bhLabel)) {
                    ironMap.get(bhLabel).add(ironElement);
                } else {
                    List<Element> elementList = new ArrayList<>();
                    elementList.add(ironElement);
                    ironMap.put(bhLabel, elementList);
                }
            }
        }

        return ironMap;
    }

    /**
     * Method displays an iron yoke
     *
     * @param ironMap Map of irons
     */
    public static void display(Map<String, List<Element>> ironMap) {
        Set<String> setString2 = ironMap.keySet();
        Iterator<String> iterator2 = setString2.iterator();
        LOGGER.info("   ");
        LOGGER.info(".................. Iron Map ..................");
        while (iterator2.hasNext()) {
            String key2 = iterator2.next();
            LOGGER.info("BH label : " + key2);
            LOGGER.info("Size of elementList : " + ironMap.get(key2).size());
            for (int i = 0; i < ironMap.get(key2).size(); i++) {
                LOGGER.info(ironMap.get(key2).get(i).getLabel());
            }
            LOGGER.info("   ");
        }

        LOGGER.info("    ");
    }

    /**
     * Method skips the comments in the beginning of the file
     *
     * @param ironGeometryText list of string containing lines of text file
     * @return index of the first line of variables
     */
    public static int skipComments(List<String> ironGeometryText) {
        int currentIndex = 0;
        boolean variableFound = false;
        while (!variableFound) {
            String currentLine = ironGeometryText.get(currentIndex);
            Matcher variableLine = Pattern.compile("(--VARIABLES)").matcher(currentLine);
            if (variableLine.find()) {
                variableFound = true;
            }
            currentIndex++;
        }
        currentIndex = currentIndex - 1;
        return currentIndex;
    }

    /**
     * Method parses variable line and create Map<String, Double>
     *
     * @param variableMap Map of variables to store
     * @param dividedLine Line to parse
     * @return Map of variables, the keys are the names of variables and values are the numerical values of the variables
     * @throws ScriptException   thrown in case of errors in parsing an iron yoke file
     * @throws GeometryException thrown in case an input file contains a dv variable used for optimization of an iron yoke
     */
    public static Map<String, Double> parseVariable(Map<String, Double> variableMap, String[] dividedLine) throws ScriptException, GeometryException {
        String valueString = dividedLine[1].substring(dividedLine[1].indexOf(""), dividedLine[1].indexOf(';'));
        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName(JAVASCRIPT);
        Matcher dv = Pattern.compile("(dv)(\\s+)").matcher(dividedLine[0]);
        boolean wasDv = dv.find();
        if (wasDv) {
            throw new GeometryException("This variable : "
                    + dividedLine[0] +
                    ", contains dv keyword which is not supported by the parser. Please replace the variable value with the final value after optimisation.");
        } else {
            String variableKey = getName(dividedLine);
            double variableValue = IronYokeParserUtils.mathConversion(valueString, variableMap);
            variableMap.put(variableKey, variableValue);
        }

        return variableMap;
    }


    /**
     * Method parses Points
     *
     * @param variableMap Map<String,Double> of variables to be used as parameters
     * @param pointMap    Map of points to create
     * @param unit        unit of the geometry (either meter or millimeter)
     * @param dividedLine line to parse
     * @return Map<String, Point> a map from a point label to a point object
     * @throws ScriptException thrown in case of errors in parsing an iron yoke file
     */
    public static Map<String, Point> parsePoint(Map<String, Double> variableMap,
                                                Map<String, Point> pointMap,
                                                String[] dividedLine,
                                                Unit unit) throws ScriptException {
        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName(JAVASCRIPT);
        String pointKey = getName(dividedLine);
        String pointParameters = dividedLine[1].substring(dividedLine[1].indexOf('[') + 1, dividedLine[1].indexOf(']'));
        Matcher cartesianOrPolar = Pattern.compile("(,)").matcher(dividedLine[1]);
        boolean wasCartesian = cartesianOrPolar.find();
        if (wasCartesian) {
            List<String> parametersList = new ArrayList<>(Arrays.asList(pointParameters.split(",")));
            String parameter1 = IronYokeParserUtils.replaceByNumericValue(parametersList.get(0).replaceAll("\\s", ""), variableMap);
            String parameter2 = IronYokeParserUtils.replaceByNumericValue(parametersList.get(1).replaceAll("\\s", ""), variableMap);
            Double x = new BigDecimal(engine.eval(IronYokeParserUtils.checkMathExpression(parameter1)).toString()).doubleValue();
            Double y = new BigDecimal(engine.eval(IronYokeParserUtils.checkMathExpression(parameter2)).toString()).doubleValue();
            x = IronYokeParserUtils.conversionOfTwoNumbers(x, y, unit)[0];
            y = IronYokeParserUtils.conversionOfTwoNumbers(x, y, unit)[1];
            Point cartesianPoint = Point.ofCartesian(x, y);
            pointMap.put(pointKey, cartesianPoint);
        } else {
            List<String> parametersList = new ArrayList<>(Arrays.asList(pointParameters.split("@")));
            String parameter1 = IronYokeParserUtils.replaceByNumericValue(parametersList.get(0).replaceAll("\\s", ""), variableMap);
            String parameter2 = IronYokeParserUtils.replaceByNumericValue(parametersList.get(1).replaceAll("\\s", ""), variableMap);
            Double r = new BigDecimal(engine.eval(IronYokeParserUtils.checkMathExpression(parameter1)).toString()).doubleValue();
            r = IronYokeParserUtils.conversionOfOneNumber(r, unit);
            Double phi = new BigDecimal(engine.eval(IronYokeParserUtils.checkMathExpression(parameter2)).toString()).doubleValue();
            Point polarPoint = Point.ofPolar(r, phi);
            pointMap.put(pointKey, polarPoint);
        }
        return pointMap;
    }

    /**
     * Method parses Hyperlines
     *
     * @param pointMap     Map of points  Map<String,Point>
     * @param variableMap  Map of variables  Map<String,Double>
     * @param hyperLineMap Map of HyperLines  Map<String,HyperLine>
     * @param dividedLine  Line to parse
     * @param unit         unit of the input geometry, either m or mm
     * @return Map<String       ,               HyperLine> a map from a hyperline label to a hyperline object
     * @throws ScriptException thrown in case of errors in parsing an iron yoke file
     */
    public static Map<String, HyperLine> parseHyperLine(Map<String, Point> pointMap,
                                                        Map<String, Double> variableMap,
                                                        Map<String, HyperLine> hyperLineMap,
                                                        String[] dividedLine,
                                                        Unit unit) throws ScriptException {
        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName(JAVASCRIPT);
        String hyperLineKey = getName(dividedLine);
        Matcher arcMatcher = Pattern.compile("(Arc)").matcher(dividedLine[1]);
        boolean wasArc = arcMatcher.find();
        String hyperLinesParameters = dividedLine[1].substring(dividedLine[1].indexOf('(') + 1, dividedLine[1].indexOf(')'));
        List<String> parametersList = new ArrayList<>(Arrays.asList(hyperLinesParameters.split(",")));
        if (wasArc) {
            Matcher ellipticArcMatcher = Pattern.compile("(Elliptic)").matcher(hyperLinesParameters);
            boolean wasEllipticArc = ellipticArcMatcher.find();
            if (wasEllipticArc) {
                String[] ellipticArcParameters = new String[NB_OF_ELLIPTICARC_PARAMETERS];
                ellipticArcParameters[0] = parametersList.get(0).replaceAll("\\s", "");
                ellipticArcParameters[1] = parametersList.get(1).replaceAll("\\s", "");
                Point point1 = pointMap.get(ellipticArcParameters[0]);
                Point point2 = pointMap.get(ellipticArcParameters[1]);
                String aString = IronYokeParserUtils.replaceByNumericValue(parametersList.get(3).replaceAll("\\s", ""), variableMap);
                Double a = new BigDecimal(engine.eval(aString).toString()).doubleValue();
                String bString = IronYokeParserUtils.replaceByNumericValue(parametersList.get(4).replaceAll("\\s", ""), variableMap);
                Double b = new BigDecimal(engine.eval(bString).toString()).doubleValue();
                a = IronYokeParserUtils.conversionOfTwoNumbers(a, b, unit)[0];
                b = IronYokeParserUtils.conversionOfTwoNumbers(a, b, unit)[1];
                if (parametersList.size() == 5) {
                    EllipticArc ellipticArc = EllipticArc.ofEndPointsAxes(point1, point2, a, b);
                    hyperLineMap.put(hyperLineKey, ellipticArc);
                } else {
                    //Ask Marco
                }
            } else {
                Point point1 = pointMap.get(parametersList.get(0).replaceAll("\\s", ""));
                Point point2 = pointMap.get(parametersList.get(1).replaceAll("\\s", ""));
                if (pointMap.containsKey(parametersList.get(3).replaceAll("\\s", ""))) {
                    Point point3 = pointMap.get(parametersList.get(3).replaceAll("\\s", ""));
                    Arc arc = Arc.ofEndPointsCenter(point1, point2, point3);
                    hyperLineMap.put(hyperLineKey, arc);
                } else {
                    String parameter3 = IronYokeParserUtils.replaceByNumericValue(parametersList.get(3).replaceAll("\\s", ""), variableMap);
                    Double angle = new BigDecimal(engine.eval(parameter3).toString()).doubleValue();
                    angle = IronYokeParserUtils.conversionOfOneNumber(angle, unit);
                    Arc arc = Arc.ofEndPointsRadius(point1, point2, angle);
                    hyperLineMap.put(hyperLineKey, arc);
                }
            }
        }
        Matcher lineMatcher = Pattern.compile("(Line)").matcher(hyperLinesParameters);
        boolean wasLine = lineMatcher.find();
        if (wasLine) {
            Point point1 = pointMap.get(parametersList.get(0).replaceAll("\\s", ""));
            Point point2 = pointMap.get(parametersList.get(1).replaceAll("\\s", ""));
            Line line = Line.ofEndPoints(point1, point2);
            hyperLineMap.put(hyperLineKey, line);
        }

        Matcher barMatcher = Pattern.compile("(Bar)").matcher(hyperLinesParameters);
        boolean wasBar = barMatcher.find();
        if (wasBar) {
            Point point1 = pointMap.get(parametersList.get(0).replaceAll("\\s", ""));
            Point point2 = pointMap.get(parametersList.get(1).replaceAll("\\s", ""));
            String parameter3 = IronYokeParserUtils.replaceByNumericValue(parametersList.get(3).replaceAll("\\s", ""), variableMap);
            Double heigh = new BigDecimal(engine.eval(parameter3).toString()).doubleValue();
            heigh = IronYokeParserUtils.conversionOfOneNumber(heigh, unit);
            Line[] bar = RoxieGeometryInferface.bar(point1, point2, heigh);
            hyperLineMap.put(hyperLineKey, bar[0]);
            hyperLineMap.put(hyperLineKey, bar[1]);
            hyperLineMap.put(hyperLineKey, bar[2]);
        }

        Matcher circumferenceMatcher = Pattern.compile("(Circle)").matcher(hyperLinesParameters);
        boolean wasCircumference = circumferenceMatcher.find();
        if (wasCircumference) {
            if (pointMap.containsKey(parametersList.get(1))) {
                Point point1 = pointMap.get(parametersList.get(0).replaceAll("\\s", ""));
                Point point2 = pointMap.get(parametersList.get(1).replaceAll("\\s", ""));
                Circumference circumference = Circumference.ofDiameterEndPoints(point1, point2);
                hyperLineMap.put(hyperLineKey, circumference);
            } else {
                Point point1 = pointMap.get(parametersList.get(0).replaceAll("\\s", ""));
                String parameter2 = IronYokeParserUtils.replaceByNumericValue(parametersList.get(1).replaceAll("\\s", ""), variableMap);
                Double radius = new BigDecimal(engine.eval(parameter2).toString()).doubleValue();
                radius = IronYokeParserUtils.conversionOfOneNumber(radius, unit);
                Circumference circumference = Circumference.ofCenterRadius(point1, radius);
                hyperLineMap.put(hyperLineKey, circumference);
            }

        }
        return hyperLineMap;
    }

    /**
     * Method parses an area representing an iron yoke
     *
     * @param hyperLineMap - Map of HyperLines  Map<String,HyperLine>
     * @param areaMap      - Map of Areas
     * @param dividedLine  - Line to parse
     * @return Map<String       ,               Area>, the keys are the label of areas and the values are the Areas
     */
    private static Map<String, Area> parseArea(Map<String, HyperLine> hyperLineMap,
                                               Map<String, Area> areaMap,
                                               String[] dividedLine,
                                               Map<String, String> bhLabelMap) {
        String areaKey = getName(dividedLine);
        String parameters = dividedLine[1].substring(dividedLine[1].indexOf('(') + 1, dividedLine[1].indexOf(')'));
        List<String> parametersList = new ArrayList<>(Arrays.asList(parameters.split(",")));
        String[] areaParameters = new String[parametersList.size()];
        HyperLine[] areaHyperLines = new HyperLine[parametersList.size() - 1];
        for (int i = 0; i < parametersList.size() - 1; i++) {
            areaParameters[i] = parametersList.get(i);
            areaHyperLines[i] = hyperLineMap.get(areaParameters[i]);
        }
        Area area = Area.ofHyperLines(areaHyperLines);
        bhLabelMap.put(areaKey, parametersList.get(parametersList.size() - 1).replaceAll("\\s", ""));
        areaMap.put(areaKey, area);
        return areaMap;
    }

    /**
     * Method parses an area representing a hole in an iron yoke
     *
     * @param variableMap Map of variables  Map<String,Double>
     * @param pointMap    Map of points  Map<String,Point>
     * @param line        Map of HyperLines  Map<String,HyperLine>
     * @param unit        String containing unit of variables given by the user
     * @throws ScriptException thrown in case of errors in parsing an iron yoke file
     */
    private static Map<String, Area> parseHole(Map<String, Double> variableMap,
                                               Map<String, Point> pointMap,
                                               Map<String, HyperLine> hyperLineMap,
                                               Map<String, Area> areaMap,
                                               Map<String, Area> holeMap,
                                               Map<String, String> bhLabelMap,
                                               String line,
                                               Unit unit) throws ScriptException {
        Matcher hyperLineMatcher = Pattern.compile("(Circle)").matcher(line);
        boolean wasHyperLine = hyperLineMatcher.find();
        if (wasHyperLine) {
            String[] dividedLine = line.split("=");
            parseHyperLine(pointMap, variableMap, hyperLineMap, dividedLine, unit);
        }

        Matcher areaMatcher = Pattern.compile("(HyperArea)").matcher(line);
        boolean wasArea = areaMatcher.find();
        if (wasArea) {
            String[] dividedLine = line.split("=");
            parseArea(hyperLineMap, areaMap, dividedLine, bhLabelMap);
        }

        Matcher holeMatcher = Pattern.compile("(HyperHoleOf)").matcher(line);
        boolean wasHole = holeMatcher.find();
        if (wasHole) {
            String holeParameters = line.substring(line.indexOf('(') + 1, line.indexOf(')'));
            List<String> holeParametersList = new ArrayList<>(Arrays.asList(holeParameters.split(",")));
            String holeLabel = holeParametersList.get(0);
            areaMap.put(holeLabel, areaMap.get(holeLabel));
            holeMap.put(holeLabel, areaMap.get(holeLabel));
        }

        return areaMap;
    }

    /**
     * Method extracts the key from the first part of the line
     *
     * @param dividedLine line to parse divided in fonction of "="
     * @return String containing only the key (without spaces)
     */
    private static String getName(String[] dividedLine) {
        String[] name = dividedLine[0].split("\\s+");
        for (int i = 0; i < name.length; i++) {
            name[i] = name[i].replaceAll("[^\\w]", "");
        }

        return name[0];
    }
}
