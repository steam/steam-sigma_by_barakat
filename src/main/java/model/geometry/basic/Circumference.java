package model.geometry.basic;

/**
 * Class of geometrical definition of a circumference
 * The default constructor is Circumference.ofCenterRadius(kpc, r)
 * For more details please check the geometry documentation.
 */
public class Circumference extends HyperLine {
    private final Point kpc;
    private final double r;

    private Circumference(Point kpc, double r) {
        this.kpc = kpc;
        this.r = r;
    }

    // Static constructors

    /**
     * Method constructs a circumference with a given center and radius
     *
     * @param kpc - the center of a circumference
     * @param r   - the radius of a circumference
     * @return Circumference object
     */
    public static Circumference ofCenterRadius(Point kpc, double r) {
        return new Circumference(kpc, r);
    }

    /**
     * Method constructs a circumference from two end points of its diameter
     *
     * @param p1 - first point of a diameter
     * @param p2 - second point of a diameter
     * @return Circumference object
     */
    public static Circumference ofDiameterEndPoints(Point p1, Point p2) {
        double xc = ( p1.getX() + p2.getX() ) / 2;
        double yc = ( p1.getY() + p2.getY() ) / 2;
        Point kpc = Point.ofCartesian(xc,yc);
        double r = Math.sqrt( Math.pow(p2.getX()-p1.getX(),2) + Math.pow(p2.getY()-p1.getY(),2) ) / 2.0;
        return new Circumference(kpc, r);
    }

    // Getters

    public Point getCenter() {
        return kpc;
    }

    public double getRadius() {
        return r;
    }

    // Methods imposed by the abstract class geometry

    @ Override
    public Circumference translate(double dx, double dy) {
        return new Circumference(kpc.translate(dx, dy), r);
    }

    @ Override
    public Circumference mirrorX() {
        return new Circumference(kpc.mirrorX(), r);
    }

    @ Override
    public Circumference mirrorY() {
        return new Circumference(kpc.mirrorY(), r);
    }

    @ Override
    public Circumference rotate(double angle) {
        return new Circumference(kpc.rotate(angle), r);
    }

    @ Override
    public Circumference rotate(double angle, Point kpc) {
        return new Circumference(kpc.rotate(angle, kpc), r);
    }

    // Methods imposed by the abstract class HyperLine

    @ Override
    public Point[] extractPoints(int noOfPoints) {
        Point[] parray = new Point[noOfPoints];
        for(int i=0; i<noOfPoints; i++) {
            double angle = -Math.PI + (2*Math.PI) * i / noOfPoints;
            Point kp = Point.ofPolar(r, angle);
            parray[i] = kp.translate(kpc.getX(), kpc.getY());
        }
        return parray;
    }

    @ Override
    public Circumference reverse() {
        return this;
    }

    @ Override
    public String[] getCoordinatesExpr() {
        String xExpr = kpc.getX() + "+" + r + "*cos(s*2*pi)";
        String yExpr = kpc.getY() + "+" + r + "*sin(s*2*pi)";
        return new String[] {xExpr, yExpr};
    }
}