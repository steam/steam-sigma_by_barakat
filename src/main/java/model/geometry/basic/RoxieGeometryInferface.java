package model.geometry.basic;

/**
 * Class with a set of methods corresponding to special geometric entities present in ROXIE
 */
public class RoxieGeometryInferface {

    private RoxieGeometryInferface() {
    }
    /**
     * Method creates a ROXIE bar figure from two points and a height
     *
     * @param kp1 - first point
     * @param kp2 - secnd point
     * @param h   - height
     * @return array of lines representing a bar figure
     */
    public static Line[] bar(Point kp1, Point kp2, double h) {

        double x1 = kp1.getX();
        double y1 = kp1.getY();
        double x2 = kp2.getX();
        double y2 = kp2.getY();

        double phi;
        if (x1 == x2) {
            phi = Math.PI / 2;
        } else {
            phi = Math.atan2((y2 - y1), (x2 - x1));
        }

        phi = phi - Math.PI/2;

        double x3 = x1 - h*Math.cos(phi);
        double y3 = y1 - h*Math.sin(phi);
        double x4 = x2 - h*Math.cos(phi);
        double y4 = y2 - h*Math.sin(phi);

        Point kp3 = Point.ofCartesian(x3, y3);
        Point kp4 = Point.ofCartesian(x4, y4);

        Line l1 = Line.ofEndPoints( kp1, kp3 );
        Line l2 = Line.ofEndPoints( kp3, kp4 );
        Line l3 = Line.ofEndPoints( kp4, kp2 );

        return new Line[] {l1, l2, l3};
    }

    /**
     * Method creates a ROXIE corner in set of lines
     *
     * @param kp1 - first point
     * @param kp2 - second point
     * @return array of lines representing a ROXIE cornerIn
     */
    public static Line[] cornerIn(Point kp1, Point kp2) {
        Point kp3 = Point.ofCartesian(kp1.getX(), kp2.getY());

        Line l1 = Line.ofEndPoints(kp1, kp3);
        Line l2 = Line.ofEndPoints(kp3, kp2);

        return new Line[] {l1, l2};
    }

    /**
     * Method constructs a set of lines representing a ROXIE notch
     *
     * @param kp1   - first point
     * @param kp2   - second point
     * @param alpha - alpha angle
     * @param beta  - beta angle
     * @return an array of lines representing a ROXIE notch
     */
    public static Line[] notch(Point kp1, Point kp2, double alpha, double beta) {

        double x1 = kp1.getX();
        double y1 = kp1.getY();
        double x2 = kp2.getX();
        double y2 = kp2.getY();

        double m1 = Math.tan(alpha);
        double m2 = Math.tan(beta);

        double x3 = (m1*x1-m2*x2-y1+y2)/(m1-m2);
        double y3 = m1*(x3-x1)+y1;

        Point kp3 = Point.ofCartesian(x3, y3);

        Line l1 = Line.ofEndPoints(kp1, kp3);
        Line l2 = Line.ofEndPoints(kp3, kp2);

        return new Line[] {l1, l2};
    }
}
