package model.geometry.basic;

import model.geometry.Geometry;

/**
 * Abstract class of geometrical lines
 * Subclasses are Line (straight line), Arc (circumference sector) and Circumference (full circumference)
 * For more details please check the geometry documentation.
 */
public abstract class HyperLine extends Geometry {

    // Methods imposed by the abstract class geometry

    @ Override
    public abstract HyperLine translate(double dx, double dy);

    @ Override
    public abstract HyperLine mirrorX();

    @ Override
    public abstract HyperLine mirrorY();

    @ Override
    public abstract HyperLine rotate(double angle);

    @ Override
    public abstract HyperLine rotate(double angle, Point kpCenter);

    // Methods specific to this abstract class

    /**
     * Method extracts a given number of points from a hyperline
     *
     * @param noOfPoints - number of points to be extacted
     * @return an array of extracted points
     */
    public abstract Point[] extractPoints(int noOfPoints);

    /**
     * Method reverses the direction of a line
     *
     * @return a reverted hyperline object
     */
    public abstract HyperLine reverse();

    /**
     * Method extracts expression with coordinates for the line
     *
     * @return array of expressions with coordinates
     */
    public abstract String[] getCoordinatesExpr();

    /**
     * Method extracts end points of a line
     *
     * @return an array of end points of a line
     */
    public Point[] extractEndPoints() {
        return extractPoints(2);
    }

}
