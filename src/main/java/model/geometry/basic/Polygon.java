package model.geometry.basic;

/**
 * Class of geometrical definition of a polygon
 * The default constructor is Polygon.ofLines(lines)
 * For more details please check the geometry documentation.
 */
public class Polygon extends HyperArea {

    private final Line[] lines;

    private Polygon(Line[] lines) {
        this.lines = lines.clone();
    }

    // Static constructors

    /**
     * Method constructs a polygon from an array of lines
     *
     * @param lines - array of lines
     * @return Polygon object
     */
    public static Polygon ofLines(Line[] lines) {
        return new Polygon(lines);
    }

    // Methods imposed by the abstract class geometry

    @ Override
    public Polygon translate(double dx, double dy) {
        Line[] linesNew = new Line[lines.length];
        for(int i = 0; i< lines.length; i++){
            linesNew[i] = lines[i].translate(dx, dy);
        }
        return new Polygon(linesNew);
    }

    @ Override
    public Polygon mirrorX() {
        Line[] linesNew = new Line[lines.length];
        for(int i = 0; i< lines.length; i++){
            linesNew[i] = lines[i].mirrorX();
        }
        return new Polygon(linesNew);
    }

    @ Override
    public Polygon mirrorY() {
        Line[] linesNew = new Line[lines.length];
        for(int i = 0; i< lines.length; i++){
            linesNew[i] = lines[i].mirrorY();
        }
        return new Polygon(linesNew);
    }

    @ Override
    public Polygon rotate(double angle) {
        Line[] linesNew = new Line[lines.length];
        for(int i = 0; i< lines.length; i++){
            linesNew[i] = lines[i].rotate(angle);
        }
        return new Polygon(linesNew);
    }

    @ Override
    public Polygon rotate(double angle, Point kpc) {
        Line[] linesNew = new Line[lines.length];
        for(int i = 0; i< lines.length; i++){
            linesNew[i] = lines[i].rotate(angle, kpc);
        }
        return new Polygon(linesNew);
    }

    // Methods imposed by the abstract class HyperArea

    @ Override
    public Line[] getHyperLines() {
        return lines.clone();
    }

    @ Override
    public Polygon reverseLines() {
        Line[] linesNew = new Line[lines.length];
        for(int i = 0; i< lines.length; i++){
            linesNew[i] = lines[i].reverse();
        }
        return new Polygon(linesNew);
    }

    // Example of use for extract vertices:
    //    kp2   ln1   kp1
    //     X----->-----X
    //     |           |
    //     |           |
    // ln2 V           V ln4
    //     |           |
    //     |           |
    //     X----->-----X
    //    kp3   ln3   kp4

    @ Override
    public Point[] getVertices() {
        int nPoints = lines.length;
        Point[] parray = new Point[nPoints];

        //
        for (int i = 0; i < nPoints - 1; i++) {
            Line l_i = lines[i];
            Line l_j = lines[i + 1];
            parray[i] = l_i.extractCommonPointWith(l_j);
        }

        Line l_i = lines[nPoints - 1];
        Line l_j = lines[0];
        parray[nPoints - 1] = l_i.extractCommonPointWith(l_j);

        return parray;
    }
    
    // Polygon specific methods

    public double calculateSurface() {
        Point[] parray = getVertices();
        return calculatePolyArea(parray);
    }

    // http://www.seas.upenn.edu/~sys502/extra_materials/Polygon%20Area%20and%20Centroid.pdf
    private static double calculatePolyArea(Point[] parray) {
        int j;
        int n = parray.length;
        double area = 0;

        for (int i = 0; i < n; i++) {
            j = (i + 1) % n;
            area += parray[i].getX() * parray[j].getY();
            area -= parray[j].getX() * parray[i].getY();
        }
        area /= 2.0;
        return Math.abs(area);
    }

    @ Override
    public Point getBaricenter() {
        Point[] vertices = getVertices();
        int noOfVertices = vertices.length;
        double xBaricenter = 0;
        double yBaricenter = 0;
        for (int vi = 0; vi < noOfVertices; vi++) {
            xBaricenter += vertices[vi].getX() / noOfVertices;
            yBaricenter += vertices[vi].getY() / noOfVertices;
        }
        return Point.ofCartesian(xBaricenter,yBaricenter);
        }
    }

