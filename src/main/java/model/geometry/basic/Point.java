package model.geometry.basic;

import model.geometry.Geometry;

/**
 * Class of geometrical definition of a point
 * A point can be built through Point.ofCartesian(x,y) or Point.ofPolar(r,phi)
 * For more details please check the geometry documentation.
 */
public class Point extends Geometry {

    private final double x;
    private final double y;

    private Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    // Static constructors

    /**
     * Method creates a point from two cartesian coordinates
     *
     * @param x x coordinate
     * @param y y coordinate
     * @return new Point object
     */
    public static Point ofCartesian(double x, double y) {
        return new Point(x, y);
    }

    /**
     * Method creates a point from polar coordinates
     *
     * @param r   - radius
     * @param phi - angle
     * @return new Point object
     */
    public static Point ofPolar(double r, double phi) {
        double x = r * Math.cos(phi);
        double y = r * Math.sin(phi);
        return new Point(x, y);
    }

    // Getters

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getR() {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    }

    public double getPhi() {
        return Math.atan2(y, x); // atan2 is a 4 quadrants atan function;
    }

    // Methods imposed by the abstract class geometry

    @Override
    public Point translate(double dx, double dy) {
        return new Point(this.x + dx, this.y + dy);
    }

    @Override
    public Point mirrorX() {
        return new Point(this.x, -this.y);
    }

    @Override
    public Point mirrorY() {
        return new Point(-this.x, this.y);
    }

    @Override
    public Point rotate(double angle) {
        return Point.ofPolar(this.getR(), this.getPhi() + angle);
    }

    @Override
    public Point rotate(double angle, Point kpCenter) {
        Point pLocal = this.translate(-kpCenter.x, -kpCenter.y);
        Point pNewLocal = Point.ofPolar(pLocal.getR(), pLocal.getPhi() + angle);
        return pNewLocal.translate(kpCenter.x, kpCenter.y);
    }

    // Point specific methods

    /**
     * Method checks if two points are eaual (
     *
     * @param kp - point to be compared with
     * @return true if points are equal, false otherwise
     */
    public boolean isEqualToPoint(Point kp) {
        return (this.x == kp.x && this.y == kp.y);
    }

}
