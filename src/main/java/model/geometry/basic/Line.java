package model.geometry.basic;

import model.geometry.GeometryException;

/**
 * Class of geometrical straight line segment
 * The optional input of Line() is by default an array of its two endpoints
 * For more details please check the geometry documentation.
 */
public class Line extends HyperLine {

    private final Point kp1;
    private final Point kp2;

    private Line(Point kp1, Point kp2) {
        this.kp1 = kp1;
        this.kp2 = kp2;
    }

    // Static constructors

    /**
     * Method creates a straight line from two end points
     *
     * @param kp1 - start point
     * @param kp2 - end point
     * @return Line object
     */
    public static Line ofEndPoints(Point kp1, Point kp2) {
        return new Line(kp1, kp2);
    }

    // Getters

    public Point getKp1() {
        return kp1;
    }

    public Point getKp2() {
        return kp2;
    }

    // Methods imposed by the abstract class geometry

    @ Override
    public Line translate(double dx, double dy) {
        return new Line(kp1.translate(dx, dy), kp2.translate(dx, dy));
    }

    @ Override
    public Line mirrorX() {
        return new Line(kp1.mirrorX(), kp2.mirrorX());
    }

    @ Override
    public Line mirrorY() {
        return new Line(kp1.mirrorY(), kp2.mirrorY());
    }

    @ Override
    public Line rotate(double angle) {
        return new Line(kp1.rotate(angle), kp2.rotate(angle));
    }

    @ Override
    public Line rotate(double angle, Point kpCenter) {
        return new Line(kp1.rotate(angle, kpCenter), kp2.rotate(angle, kpCenter));
    }
    
    // Methods imposed by the abstract class HyperLine

    @Override
    public Point[] extractPoints(int noOfPoints) {
        Point[] parray = new Point[noOfPoints];
        for(int i=0; i<noOfPoints; i++) {
            double xi = kp1.getX() + ( kp2.getX()-kp1.getX() ) * i / (noOfPoints-1);
            double yi = kp1.getY() + ( kp2.getY()-kp1.getY() ) * i / (noOfPoints-1);
            parray[i] = Point.ofCartesian(xi, yi);
        }
        return parray;
    }

    @ Override
    public Line reverse() {
        return new Line(kp2, kp1);
    }

    @Override
    public String[] getCoordinatesExpr() {
        String xExpr = kp1.getX() + "+(" + kp2.getX() + "-" + kp1.getX() + ")*s";
        String yExpr = kp1.getY() + "+(" + kp2.getY() + "-" + kp1.getY() + ")*s";
        return new String[] {xExpr, yExpr};
    }

    // Line specific methods

    /**
     * Method calculates the angular coefficient of the line
     *
     * @return angular coefficient in radians
     */
    public double calculateAngularCoefficient(){
        double dx = kp2.getX()-kp1.getX();
        double dy = kp2.getY()-kp1.getY();
        return Math.atan2(dy, dx);
    }

    /**
     * Method calculates the versors associated with the line
     *
     * @return versor of the line
     */
    public Point calculateVersor() {
        double dx = kp2.getX()-kp1.getX();
        double dy = kp2.getY()-kp1.getY();
        double normOfVec = Math.sqrt(Math.pow(dx,2)+Math.pow(dy,2));

        double dxNorm = dx / normOfVec;
        double dyNorm = dy / normOfVec;

        return Point.ofCartesian(dxNorm,dyNorm);
    }

    /**
     * Method calculated the length of the line
     *
     * @return the length of the line
     */
    public double calculateLength() {
        double dx = kp2.getX()-kp1.getX();
        double dy = kp2.getY()-kp1.getY();
        return Math.sqrt(Math.pow(dx,2)+Math.pow(dy,2));
    }

    /**
     * Method compares the line with an input line and extracts common points
     *
     * @param l1 - input line with which common points are extracted
     * @return point common with the provided line
     */
    public Point extractCommonPointWith(Line l1){
        if (kp1.isEqualToPoint(l1.kp1)){
            return kp1;
        }
        else if (kp1.isEqualToPoint(l1.kp2)){
            return kp1;
        }
         else if (kp2.isEqualToPoint(l1.kp1)){
            return kp2;
        }
        else if (kp2.isEqualToPoint(l1.kp2)){
            return kp2;
        }
        else{
            throw new GeometryException("Lines have no points in common!");
        }
    }
}
