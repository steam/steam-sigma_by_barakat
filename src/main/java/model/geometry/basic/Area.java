package model.geometry.basic;

import model.geometry.GeometryException;

/**
 * Class of geometrical definition of a closed surface bounded by HyperLines
 * The default constructor is Area.ofHyperLines(hyperLines)
 * For more details please check the geometry documentation.
 */
public class Area extends HyperArea {

    private final HyperLine[] hyperLines;

    private Area(HyperLine[] hyperLines) {this.hyperLines = hyperLines.clone();
    }

    // Static constructors

    /**
     * Method constructs an area from a set of connected hyperlines
     *
     * @param hyperLines - input array of connected hyperlines
     * @return Area object
     */
    public static Area ofHyperLines(HyperLine[] hyperLines) {
        return new Area(hyperLines);
    }

    // Methods imposed by the abstract class geometry

    @ Override
    public Area translate(double dx, double dy) {
        HyperLine[] hyperLinesNew = new HyperLine[hyperLines.length];
        for(int i = 0; i< hyperLines.length; i++){
            hyperLinesNew[i] = hyperLines[i].translate(dx, dy);
        }
        return new Area(hyperLinesNew);
    }

    @ Override
    public Area mirrorX() {
        HyperLine[] hyperLinesNew = new HyperLine[hyperLines.length];
        for(int i = 0; i< hyperLines.length; i++){
            hyperLinesNew[i] = hyperLines[i].mirrorX();
        }
        return new Area(hyperLinesNew);
    }

    @ Override
    public Area mirrorY() {
        HyperLine[] hyperLinesNew = new HyperLine[hyperLines.length];
        for(int i = 0; i< hyperLines.length; i++){
            hyperLinesNew[i] = hyperLines[i].mirrorY();
        }
        return new Area(hyperLinesNew);
    }

    @ Override
    public Area rotate(double angle) {
        HyperLine[] hyperLinesNew = new HyperLine[hyperLines.length];
        for(int i = 0; i< hyperLines.length; i++){
            hyperLinesNew[i] = hyperLines[i].rotate(angle);
        }
        return new Area(hyperLinesNew);
    }

    @ Override
    public Area rotate(double angle, Point kpc) {
        HyperLine[] hyperLinesNew = new HyperLine[hyperLines.length];
        for(int i = 0; i< hyperLines.length; i++){
            hyperLinesNew[i] = hyperLines[i].rotate(angle, kpc);
        }
        return new Area(hyperLinesNew);
    }

    // Methods imposed by the abstract class HyperArea

    @ Override
    public HyperLine[] getHyperLines() {
        return hyperLines.clone();
    }

    @ Override
    public Area reverseLines() {
        HyperLine[] hyperLinesNew = new HyperLine[hyperLines.length];
        for(int i = 0; i< hyperLines.length; i++){
            hyperLinesNew[i] = hyperLines[i].reverse();
        }
        return new Area(hyperLinesNew);
    }

    // Example of use for extract vertices:
    //    kp2   ln1   kp1
    //     X----->-----X
    //     |           |
    //     |           |
    // ln2 V           V ln4
    //     |           |
    //     |           |
    //     X----->-----X
    //    kp3   ln3   kp4
    @ Override
    public Point[] getVertices(){
        int nPoints = hyperLines.length;
        Point[] parray = new Point[nPoints];
        parray[0] = hyperLines[0].extractPoints(2)[1];


        for (int i = 0; i < nPoints-1; i++) {
            Point kp1 = hyperLines[i].extractPoints(2)[0];
            Point kp2 = hyperLines[i].extractPoints(2)[1];
            if ( parray[i].isEqualToPoint(kp1) ) {
                parray[i+1] = kp2;
            } else if ( parray[i].isEqualToPoint(kp2) ) {
                parray[i+1] = kp1;
            }
            else{
                throw new GeometryException("IF-ELSE not handled");
            }
        }
        return parray;
    }

    @Override
    public double calculateSurface() {
        throw new GeometryException("calculateSurface of a generic area is not supported");
    }

    @ Override
    public Point getBaricenter() {
        Point[] vertices = getVertices();
        int noOfVertices = vertices.length;
        double xBaricenter = 0;
        double yBaricenter = 0;
        for (int vi = 0; vi < noOfVertices; vi++) {
            xBaricenter += vertices[vi].getX() / noOfVertices;
            yBaricenter += vertices[vi].getY() / noOfVertices;
        }
        return Point.ofCartesian(xBaricenter,yBaricenter);
    }
}
