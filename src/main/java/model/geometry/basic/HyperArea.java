package model.geometry.basic;

import model.geometry.Geometry;
import model.geometry.GeometryException;

/**
 * Abstract class of geometrical closed surfaces
 * Subclasses are Area and Polygon
 * For more details please check the buildCoilObj documentation.
 */
public abstract class HyperArea extends Geometry {

    // Methods imposed by the abstract class buildCoilObj

    @Override
    public abstract HyperArea translate(double dx, double dy);

    @Override
    public abstract HyperArea mirrorX();

    @Override
    public abstract HyperArea mirrorY();

    @Override
    public abstract HyperArea rotate(double angle);

    @Override
    public abstract HyperArea rotate(double angle, Point kpCenter);

    // Methods specific to this abstract class

    /**
     * Method reverts the order of lines in a hyperarea
     *
     * @return new HyperArea object
     */
    public abstract HyperArea reverseLines();

    /**
     * Calculates the surface of a hyperarea
     *
     * @return surface of a hyperarea
     * @throws GeometryException in case of problems with calculation
     */
    public abstract double calculateSurface() throws GeometryException;

    public abstract HyperLine[] getHyperLines();

    public abstract Point[] getVertices();

    public abstract Point getBaricenter();
}
