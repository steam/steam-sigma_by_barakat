package model.geometry.basic;

/**
 * Class of geometrical definition of an arc of circumference
 * The default constructor is Arc.ofPointCenterAngle(kp1, kpc, dTheta)
 * For more details please check the geometry documentation.
 */
public class Arc extends HyperLine {

    private final Point kpc;
    private final Point kp1;
    private final double dTheta;

    private Arc(Point kp1, Point kpc, double dTheta) {
        this.kpc = kpc;
        this.kp1 = kp1;
        this.dTheta = dTheta;
    }

    // Static constructors

    /**
     * Method creates an arc from a point, a center, and an angle
     *
     * @param kp1    starting point
     * @param kpc    center point
     * @param dTheta angle
     * @return Arc object
     */
    public static Arc ofPointCenterAngle(Point kp1, Point kpc, double dTheta) {
        return new Arc(kp1, kpc, dTheta);
    }

    /**
     * Method creates an arc from a start, end, and center points
     *
     * @param kp1 start point
     * @param kp2 end point
     * @param kpc center point
     * @return Arc object
     */
    public static Arc ofEndPointsCenter(Point kp1, Point kp2, Point kpc) {
        double dTheta = calculatedTheta(kp1.translate(-kpc.getX(), -kpc.getY()), kp2.translate(-kpc.getX(), -kpc.getY()));
        return new Arc(kp1, kpc, dTheta);
    }

    /**
     * Method creates an arc from two points and a radius
     *
     * @param kp1 starting point
     * @param kp2 final point
     * @param r   radius
     * @return Arc object
     */
    public static Arc ofEndPointsRadius(Point kp1, Point kp2, double r) {

        double x1 = kp1.getX();
        double y1 = kp1.getY();
        double x2 = kp2.getX();
        double y2 = kp2.getY();

        double x12 = Math.pow(x1, 2);
        double y12 = Math.pow(y1, 2);
        double x22 = Math.pow(x2, 2);
        double y22 = Math.pow(y2, 2);

        double xc;
        double yc;
        double rAbs = Math.abs(r);
        if (r > 0) {
            xc = (y2 * Math.pow((-(x12 - 2 * x1 * x2 + x22 + y12 - 2 * y1 * y2 + y22) * (x12 - 2 * x1 * x2 + x22 + y12 - 2 * y1 * y2 + y22 - 4 * Math.pow(rAbs, 2))), 0.5) - y1 * Math.pow((-(x12 - 2 * x1 * x2 + x22 + y12 - 2 * y1 * y2 + y22) * (x12 - 2 * x1 * x2 + x22 + y12 - 2 * y1 * y2 + y22 - 4 * Math.pow(rAbs, 2))), 0.5) - x1 * x22 - x12 * x2 + x1 * y12 + x1 * y22 + x2 * y12 + x2 * y22 + Math.pow(x1, 3) + Math.pow(x2, 3) - 2 * x1 * y1 * y2 - 2 * x2 * y1 * y2) / (2 * (x12 - 2 * x1 * x2 + x22 + y12 - 2 * y1 * y2 + y22));
            yc = (x1 * Math.pow((-(x12 - 2 * x1 * x2 + x22 + y12 - 2 * y1 * y2 + y22) * (x12 - 2 * x1 * x2 + x22 + y12 - 2 * y1 * y2 + y22 - 4 * Math.pow(rAbs, 2))), 0.5) - x2 * Math.pow((-(x12 - 2 * x1 * x2 + x22 + y12 - 2 * y1 * y2 + y22) * (x12 - 2 * x1 * x2 + x22 + y12 - 2 * y1 * y2 + y22 - 4 * Math.pow(rAbs, 2))), 0.5) + x12 * y1 + x12 * y2 + x22 * y1 + x22 * y2 - y1 * y22 - y12 * y2 + Math.pow(y1, 3) + Math.pow(y2, 3) - 2 * x1 * x2 * y1 - 2 * x1 * x2 * y2) / (2 * (x12 - 2 * x1 * x2 + x22 + y12 - 2 * y1 * y2 + y22));
        } else {
            xc = (y1 * Math.pow((-(x12 - 2 * x1 * x2 + x22 + y12 - 2 * y1 * y2 + y22) * (x12 - 2 * x1 * x2 + x22 + y12 - 2 * y1 * y2 + y22 - 4 * Math.pow(rAbs, 2))), 0.5) - y2 * Math.pow((-(x12 - 2 * x1 * x2 + x22 + y12 - 2 * y1 * y2 + y22) * (x12 - 2 * x1 * x2 + x22 + y12 - 2 * y1 * y2 + y22 - 4 * Math.pow(rAbs, 2))), 0.5) - x1 * x22 - x12 * x2 + x1 * y12 + x1 * y22 + x2 * y12 + x2 * y22 + Math.pow(x1, 3) + Math.pow(x2, 3) - 2 * x1 * y1 * y2 - 2 * x2 * y1 * y2) / (2 * (x12 - 2 * x1 * x2 + x22 + y12 - 2 * y1 * y2 + y22));
            yc = (x2 * Math.pow((-(x12 - 2 * x1 * x2 + x22 + y12 - 2 * y1 * y2 + y22) * (x12 - 2 * x1 * x2 + x22 + y12 - 2 * y1 * y2 + y22 - 4 * Math.pow(rAbs, 2))), 0.5) - x1 * Math.pow((-(x12 - 2 * x1 * x2 + x22 + y12 - 2 * y1 * y2 + y22) * (x12 - 2 * x1 * x2 + x22 + y12 - 2 * y1 * y2 + y22 - 4 * Math.pow(rAbs, 2))), 0.5) + x12 * y1 + x12 * y2 + x22 * y1 + x22 * y2 - y1 * y22 - y12 * y2 + Math.pow(y1, 3) + Math.pow(y2, 3) - 2 * x1 * x2 * y1 - 2 * x1 * x2 * y2) / (2 * (x12 - 2 * x1 * x2 + x22 + y12 - 2 * y1 * y2 + y22));
        }
        Point kpc = Point.ofCartesian(xc, yc);

        double dTheta = calculatedTheta(kp1.translate(-xc, -yc), kp2.translate(-xc, -yc));
        return new Arc(kp1, kpc, dTheta);
    }

    /**
     * Method constructs an arc from three points
     *
     * @param kp1 first point
     * @param kp2 second point
     * @param kp3 third point
     * @return Arc object
     */
    public static Arc ofThreePoints(Point kp1, Point kp2, Point kp3) {

        double x1 = kp1.getX();
        double y1 = kp1.getY();
        double x2 = kp2.getX();
        double y2 = kp2.getY();
        double x3 = kp3.getX();
        double y3 = kp3.getY();

        double a = ((Math.pow(x2, 2) + Math.pow(y2, 2)) * (y1 - y3)) / (x1 * y2 - x2 * y1 - x1 * y3 + x3 * y1 + x2 * y3 - x3 * y2) - ((Math.pow(x1, 2) + Math.pow(y1, 2)) * (y2 - y3)) / (x1 * y2 - x2 * y1 - x1 * y3 + x3 * y1 + x2 * y3 - x3 * y2) - ((Math.pow(x3, 2) + Math.pow(y3, 2)) * (y1 - y2)) / (x1 * y2 - x2 * y1 - x1 * y3 + x3 * y1 + x2 * y3 - x3 * y2);
        double b = ((Math.pow(x1, 2) + Math.pow(y1, 2)) * (x2 - x3)) / (x1 * y2 - x2 * y1 - x1 * y3 + x3 * y1 + x2 * y3 - x3 * y2) - ((Math.pow(x2, 2) + Math.pow(y2, 2)) * (x1 - x3)) / (x1 * y2 - x2 * y1 - x1 * y3 + x3 * y1 + x2 * y3 - x3 * y2) + ((Math.pow(x3, 2) + Math.pow(y3, 2)) * (x1 - x2)) / (x1 * y2 - x2 * y1 - x1 * y3 + x3 * y1 + x2 * y3 - x3 * y2);
        double xc = -a / 2;
        double yc = -b / 2;
        Point kpc = Point.ofCartesian(-a / 2, -b / 2);

        double dTheta = calculatedTheta(kp1.translate(-xc, -yc), kp2.translate(-xc, -yc), kp3.translate(-xc, -yc));
        return new Arc(kp1, kpc, dTheta);
    }

    // Getters

    public Point getKpc() {
        return kpc;
    }

    public Point getKp1() {
        return kp1;
    }

    public double getDTheta() {
        return dTheta;
    }

    // Methods imposed by the abstract class geometry

    @Override
    public Arc translate(double dx, double dy) {
        Point kpcNew = kpc.translate(dx, dy);
        Point kp1New = kp1.translate(dx, dy);
        return new Arc(kp1New, kpcNew, dTheta);
    }

    @Override
    public Arc mirrorX() {
        Point kpcNew = kpc.mirrorX();
        Point kp1New = kp1.mirrorX();
        return new Arc(kp1New, kpcNew, -dTheta);
    }

    @Override
    public Arc mirrorY() {
        Point kpcNew = kpc.mirrorY();
        Point kp1New = kp1.mirrorY();
        return new Arc(kp1New, kpcNew, -dTheta);
    }

    @Override
    public Arc rotate(double angle) {
        Point kpcNew = kpc.rotate(angle);
        Point kp1New = kp1.rotate(angle);
        return new Arc(kp1New, kpcNew, dTheta);
    }

    @Override
    public Arc rotate(double angle, Point kpCenter) {
        Point kpcNew = kpc.rotate(angle, kpCenter);
        Point kp1New = kp1.rotate(angle, kpCenter);
        return new Arc(kp1New, kpcNew, dTheta);
    }

    // Methods imposed by the abstract class HyperLine

    @Override
    public Point[] extractPoints(int noOfPoints) {
        Point kp1Local = kp1.translate(-kpc.getX(), -kpc.getY());

        double alphaStep = dTheta / (noOfPoints - 1);
        Point[] parray = new Point[noOfPoints];

        double angle_i;
        double r = kp1Local.getR();
        for (int i = 0; i < noOfPoints; i++) {
            angle_i = kp1Local.getPhi() + i * alphaStep;
            Point kpiLocal = Point.ofPolar(r, angle_i);
            parray[i] = kpiLocal.translate(kpc.getX(), kpc.getY());
        }
        return parray;
    }

    @Override
    public Arc reverse() {
        Point kp1Local = kp1.translate(-this.kpc.getX(), -this.kpc.getY());
        Point kp1NewLocal = Point.ofPolar(kp1Local.getR(), kp1Local.getPhi() + dTheta);
        Point kp1New = kp1NewLocal.translate(this.kpc.getX(), this.kpc.getY());

        return new Arc(kp1New, kpc, -dTheta);
    }

    @Override
    public String[] getCoordinatesExpr() {
        Point kp1Local = kp1.translate(-kpc.getX(), -kpc.getY());

        String xExpr = kpc.getX() + "+" + kp1Local.getR() + "*cos(" + kp1Local.getPhi() + "+(" + dTheta + ")*s" + ")";
        String yExpr = kpc.getY() + "+" + kp1Local.getR() + "*sin(" + kp1Local.getPhi() + "+(" + dTheta + ")*s" + ")";

        return new String[]{xExpr, yExpr};
    }

    // Arc specific methods

    private static double calculatedTheta(Point kp1, Point kp2) {
        double dTheta = kp2.getPhi() - kp1.getPhi();
        while (Math.abs(dTheta) > Math.PI) {
            dTheta = dTheta - 2 * Math.PI * Math.signum(dTheta);
        }
        return dTheta;
    }

    private static double calculatedTheta(Point kp1, Point kp2, Point kp3) {
        double dTheta12 = kp2.getPhi() - kp1.getPhi();
        while (Math.abs(dTheta12) > Math.PI) {
            dTheta12 = dTheta12 - 2 * Math.PI * Math.signum(dTheta12);
        }
        double dTheta23 = kp3.getPhi() - kp2.getPhi();
        while (Math.abs(dTheta23) > Math.PI) {
            dTheta23 = dTheta23 - 2 * Math.PI * Math.signum(dTheta23);
        }
        return dTheta12 + dTheta23;
    }
}
