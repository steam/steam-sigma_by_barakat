package model.geometry.basic;

/**
 * Class of geometrical definition of an arc of ellipse
 * The default constructor is EllipticArc.ofPointCenterAngleAxes(kp1, kpc, dTheta, a, b)
 * For more details please check the geometry documentation.
 */
public class EllipticArc extends HyperLine {

    private final Point kpc;
    private final Point kp1;
    private final double dTheta;
    private final double a;
    private final double b;

    private EllipticArc(Point kp1, Point kpc, double dTheta, double a, double b) {
        this.kp1 = kp1;
        this.kpc = kpc;
        this.dTheta = dTheta;
        this.a = a;
        this.b = b;
    }

    // Static constructors

    /**
     * Method constructs an elliptic arc with given point, center, axes, and angle
     *
     * @param kp1    - angle starting point
     * @param kpc    - center
     * @param dTheta - arc angle
     * @param a      - semi-major axis
     * @param b      - semi-minor axis
     * @return EllipticArc object
     */
    public static EllipticArc ofPointCenterAngleAxes(Point kp1, Point kpc, double dTheta, double a, double b) {
        return new EllipticArc(kp1, kpc, dTheta, a, b);
    }

    /**
     * Method constructs an elliptic arve with given two points and axes
     *
     * @param kp1 - start point
     * @param kp2 - end point
     * @param a   - semi-major axis
     * @param b   - semi-minor axis
     * @return EllipticArc object
     */
    public static EllipticArc ofEndPointsAxes(Point kp1, Point kp2, double a, double b) {

        double x1 = kp1.getX();
        double y1 = kp1.getY();
        double x2 = kp2.getX();
        double y2 = kp2.getY();

        double x12 = Math.pow(x1,2);
        double y12 = Math.pow(y1,2);
        double x22 = Math.pow(x2,2);
        double y22 = Math.pow(y2,2);
        double a2 = Math.pow(a,2);
        double b2 = Math.pow(b,2);

        double xc = x1/2 + x2/2 - (a*Math.pow( (-(- 4*a2*b2 + a2*y12 - 2*a2*y1*y2 + a2*y22 + b2*x12 - 2*b2*x1*x2 + b2*x22)/(a2*y12 - 2*a2*y1*y2 + a2*y22 + b2*x12 - 2*b2*x1*x2 + b2*x22)), 0.5 )*(y1 - y2))/(2*b) ;
        double yc = y1/2 + y2/2 + (b*Math.pow( (-(- 4*a2*b2 + a2*y12 - 2*a2*y1*y2 + a2*y22 + b2*x12 - 2*b2*x1*x2 + b2*x22)/(a2*y12 - 2*a2*y1*y2 + a2*y22 + b2*x12 - 2*b2*x1*x2 + b2*x22)), 0.5 )*(x1 - x2))/(2*a) ;
        Point kpc = Point.ofCartesian(xc, yc);

        double dTheta = calculatedTheta(kp1.translate(-xc, -yc), kp2.translate(-xc, -yc));

        return new EllipticArc(kp1, kpc, dTheta, a, b);
    }

    // Getters

    public Point getKpc() {
        return kpc;
    }

    public Point getKp1() {
        return kp1;
    }

    public double getDTheta() {
        return dTheta;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }


    // Methods imposed by the abstract class geometry

    @ Override
    public EllipticArc translate(double dx, double dy) {
        Point kpcNew = kpc.translate(dx, dy);
        Point kp1New = kp1.translate(dx, dy);
        return new EllipticArc(kp1New, kpcNew, dTheta, a, b);
    }

    @ Override
    public EllipticArc mirrorX() {
        Point kpcNew = kpc.mirrorX();
        Point kp1New = kp1.mirrorX();
        return new EllipticArc(kp1New, kpcNew, -dTheta, a, b);
    }

    @ Override
    public EllipticArc mirrorY() {
        Point kpcNew = kpc.mirrorY();
        Point kp1New = kp1.mirrorY();
        return new EllipticArc(kp1New, kpcNew, -dTheta, a, b);
    }

    @ Override
    public EllipticArc rotate(double angle) {
        Point kpcNew = kpc.rotate(angle);
        Point kp1New = kp1.rotate(angle);
        return new EllipticArc(kp1New, kpcNew, dTheta, a, b);
    }

    @ Override
    public EllipticArc rotate(double angle, Point kpCenter) {
        Point kpcNew = kpc.rotate(angle, kpCenter);
        Point kp1New = kp1.rotate(angle, kpCenter);
        return new EllipticArc(kp1New, kpcNew, dTheta, a, b);
    }

    // Methods imposed by the abstract class HyperLine

    @ Override
    public Point[] extractPoints(int noOfPoints) {
        Point kp1Local = kp1.translate(-kpc.getX(), -kpc.getY());

        double alphaStep = dTheta / (noOfPoints-1);
        Point[] parray = new Point[noOfPoints];

        double angle_i;
        double r_i;
        for(int i=0; i<noOfPoints; i++) {
            angle_i = kp1Local.getPhi() + i * alphaStep;
            r_i = (a*b) / (Math.sqrt( Math.pow(b*Math.cos(angle_i), 2) + Math.pow(a*Math.sin(angle_i), 2) ));
            Point kpiLocal = Point.ofPolar(r_i, angle_i);
            parray[i] = kpiLocal.translate(kpc.getX(), kpc.getY());
        }
        return parray;
    }

    @ Override
    public EllipticArc reverse() {
        Point kp1Local = kp1.translate(-kpc.getX(), -kpc.getY());

        double angleNew = kp1Local.getPhi() + dTheta;
        double rNew = (a*b) / (Math.sqrt( Math.pow(b*Math.cos(angleNew), 2) + Math.pow(a*Math.sin(angleNew), 2) ));

        Point kp1NewLocal = Point.ofPolar(rNew, angleNew);
        Point kp1New = kp1NewLocal.translate(kpc.getX(), kpc.getY());

        return new EllipticArc(kp1New, kpc, -dTheta, a, b);
    }

    @ Override
    public String[] getCoordinatesExpr() {
        Point kp1Local = kp1.translate(-kpc.getX(), -kpc.getY());

        String angleExpr = kp1Local.getPhi() + "+" + dTheta + "*s";
        String rExpr = (a*b) + "/(sqrt((" + b + "*cos(" + angleExpr + "))^2 + (" + a + "*sin(" + angleExpr + "))^2 ))";

        String xExpr = kpc.getX() + "+" + rExpr + "*cos(" + angleExpr + ")";
        String yExpr = kpc.getY() + "+" + rExpr + "*sin(" + angleExpr + ")";

        return new String[]{xExpr, yExpr};
    }

    // EllipticArc specific methods

    private static double calculatedTheta(Point kp1, Point kp2) {
        double dTheta = kp2.getPhi() - kp1.getPhi();
        while ( Math.abs(dTheta)>Math.PI ) {
            dTheta = dTheta - 2*Math.PI*Math.signum(dTheta);
        }
        return dTheta;
    }

}
