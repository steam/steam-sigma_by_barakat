package model.geometry;

import model.geometry.basic.HyperArea;

/**
 * Extended class of Element class referring to holes in the iron yoke element
 */
public class HoleInIronYokeElement extends Element{
    /**
     * Constructs a HoleInIronYokeElement with a given label and geometry
     *
     * @param label    - label to be used in COMSOL
     * @param geometry - hyperarea representing the iron yoke geometry
     */
    public HoleInIronYokeElement(String label, HyperArea geometry) {
        super(label, geometry);
    }
}
