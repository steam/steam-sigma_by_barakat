package model.domains;

import model.geometry.Element;
import model.materials.Material;
import model.materials.database.MatDatabase;

/**
 * Abstract class of a domain containing general methods and properties of a domain in COMSOL
 */
public class Domain {
    private String label;
    private Element[] elements;
    private Material material;
    private DomainType domainType;
    private boolean implementsTH;

    /**
     * Method initializes a domain
     *
     * @param label - domain label
     * @param mat   - material library
     */
    public void domainInit(String label, MatDatabase mat) {
        this.label = label;
        this.material = new Material(mat);
        this.implementsTH = false;
    }

    /**
     * Private constructor for the Domain class
     *
     * @param builder - Domain builder
     */
    public Domain(Builder builder) {
        this.label = builder.label;
        this.elements = builder.elements;
        this.material = builder.material;
        this.domainType = builder.domainType;
        this.implementsTH = builder.implementsTH;
    }

    /**
     * Builder class of Domain
     */
    public static class Builder {
        private String label;
        private Element[] elements;
        private Material material;
        private DomainType domainType;
        private boolean implementsTH;

        /**
         * Method sets the label of a Builder object
         *
         * @param label string containing a label of the Domain class
         * @return a Builder object with set label
         */
        public Builder label(String label) {
            this.label = label;
            return this;
        }

        /**
         * Method sets the the elements array of a Builder object
         *
         * @param elements array of geometric entities describing for the Domain class
         * @return a Builder object with set elements
         */
        public Builder elements(Element[] elements) {
            this.elements = elements.clone();
            return this;
        }

        /**
         * Method sets the material type for a Builder object
         *
         * @param material double containing the scaling of the BHiron
         * @return a Builder object with set material
         */
        public Builder material(Material material) {
            this.material = material;
            return this;
        }
        /**
         * Method sets the domain type of a Builder object
         *
         * @param domainType double table containing the BH values
         * @return a Builder object with domainType
         */

        public Builder domainType(DomainType domainType) {
            this.domainType = domainType;
            return this;
        }

        /**
         * Method sets the flag denoting implementation of heat transfer physics type for a Domain object
         *
         * @param implementsTH flag for implementation of heat transfer physics (true - implemented, false - not implemented)
         * @return a Builder object with flag
         */
        public Builder implementsTH(boolean implementsTH) {
            this.implementsTH = implementsTH;
            return this;
        }

        /**
         * Method builds a Domain object
         *
         * @return a Domain object
         */
        public Builder build() {
            return this;
        }
    }
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Element[] getElements() {
        return elements.clone();
    }

    public void setElements(Element[] elements) {
        this.elements = elements.clone();
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public DomainType getDomainType() {
        return domainType;
    }

    public void setDomainType(DomainType domainType) {
        this.domainType = domainType;
    }

    public boolean isImplementsTH() {
        return implementsTH;
    }

    public boolean isCoil() {
        return (getDomainType() == DomainType.COIL);
    }
    public void setImplementsTH(boolean implementsTH) {
        this.implementsTH = implementsTH;
    }
}

