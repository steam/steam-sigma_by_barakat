package model.domains;

/**
 * Class representing the Boundary Conditions in COMSOL
 */
public enum BoundaryConditions {
    Dirichlet,
    Neumann,
    noBoundaryCondition
}
