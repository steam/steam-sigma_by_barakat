package model.domains.database;

import model.domains.Domain;
import model.domains.DomainType;
import model.geometry.coil.Coil;
import model.materials.Material;
import model.materials.database.MatDatabase;

/**
 * Class representing the Coil implementing the Domain abstract class in COMSOL
 */
public class CoilDomain extends Domain {

    private final Coil coil;

    /**
     * Constructs a CoilDomain object with a given label, material, parameters and geometry
     *
     * @param label - coil label
     * @param mat   - definition of the coil material
     * @param coil  - coil definition composed of poles, blocks of turns, and turns
     */
    public CoilDomain(String label, MatDatabase mat, Coil coil) {
        super(new Domain.Builder()
                .label(label)
                .material(new Material(mat))
                .domainType(DomainType.COIL)
                .implementsTH(true)
                .build());
        this.coil = coil;
    }

    public Coil getCoil() {
        return coil;
    }

}
