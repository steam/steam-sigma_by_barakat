package model.domains.database;

import model.domains.*;
import model.geometry.Element;
import model.materials.Material;
import model.materials.database.MatDatabase;


/**
 * Class representing the Boundary Conditions implementing the Domain abstract class in COMSOL
 */
public class BoundaryConditionsDomain extends Domain {

    private final BoundaryConditions xAxisBC;
    private final BoundaryConditions yAxisBC;
    private final String xBCLabel;
    private final String yBCLabel;

    /**
     * Constructor of a BoundaryConditionsDomain element representing a boundary condition
     *
     * @param label    - label of the boundary condition
     * @param mat      - material type
     * @param xAxisBC  - horizontal axis of a boundary condition
     * @param yAxisBC  - vertical axis of a boundary condition
     * @param elements - geometric elements representing a boundary condition
     */
    public BoundaryConditionsDomain(String label, MatDatabase mat, BoundaryConditions xAxisBC, BoundaryConditions yAxisBC, Element[] elements) {
        super(new Domain.Builder()
                .label(label)
                .elements(elements)
                .material(new Material(mat))
                .domainType(DomainType.BOUNDARY)
                .implementsTH(false)
                .build());
        this.xAxisBC = xAxisBC;
        this.yAxisBC = yAxisBC;
        this.xBCLabel = "x_" + xAxisBC.toString();
        this.yBCLabel = "y_" + xAxisBC.toString();
    }

    public BoundaryConditions getXAxisBC() {
        return xAxisBC;
    }

    public BoundaryConditions getYAxisBC() {
        return yAxisBC;
    }

    public String getXBCLabel() {
        return xBCLabel;
    }

    public String getYBCLabel() {
        return yBCLabel;
    }
}

