package model.domains.database;

import model.domains.Domain;
import model.domains.DomainType;
import model.geometry.Element;
import model.materials.Material;
import model.materials.database.MatDatabase;

/**
 * Class representing the Insulation Domains implementing the Domain abstract class in COMSOL
 */
public class InsulationDomain extends Domain {

    /**
     * Constructs an InsulationDomain object with a given label, material, and elements
     *
     * @param label    - insulation domain label
     * @param mat      - insulation domain material
     * @param elements - insulation domain elements
     */
    public InsulationDomain(String label, MatDatabase mat, Element[] elements) {
        super(new Domain.Builder()
                .label(label)
                .elements(elements)
                .material(new Material(mat))
                .domainType(DomainType.INSULATION)
                .implementsTH(true)
                .build());
    }
}
