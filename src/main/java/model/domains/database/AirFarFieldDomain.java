package model.domains.database;

import model.domains.Domain;
import model.domains.DomainType;
import model.geometry.Element;
import model.materials.Material;
import model.materials.database.MatDatabase;

/**
 * Class representing the Air Far Field Domain (to close the magnetic field lines in a finite space) in COMSOL
 */
public class AirFarFieldDomain extends Domain {

    /**
     * Constructor of an AirFarFieldDomain object with given label, material, and elements
     *
     * @param label    - AirFarFieldDomain label
     * @param mat      - material definition
     * @param elements - geometric elements
     */
    public AirFarFieldDomain(String label, MatDatabase mat, Element[] elements) {
        super(new Domain.Builder()
                .label(label)
                .elements(elements)
                .material(new Material(mat))
                .domainType(DomainType.AIRFARFIELD)
                .implementsTH(false)
                .build());
    }
}