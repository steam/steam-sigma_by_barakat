package model.domains.database;

import model.domains.Domain;
import model.domains.DomainType;
import model.geometry.Element;
import model.materials.Material;
import model.materials.database.MatDatabase;

/**
 * Class representing the Quench Heaters Domains implementing the Domain abstract class in COMSOL
 */
public class QuenchHeaterDomain extends Domain {
    /**
     * Constructs a QuenchHeaterDomain object with a given label, materials, and elements
     *
     * @param label    - label in COMSOL
     * @param mat      - input material property
     * @param elements - input geometric representation
     */
    public QuenchHeaterDomain(String label, MatDatabase mat, Element[] elements) {
        super(new Domain.Builder()
                .label(label)
                .elements(elements)
                .material(new Material(mat))
                .domainType(DomainType.QUENCHHEATER)
                .implementsTH(false)
                .build());
    }
}

