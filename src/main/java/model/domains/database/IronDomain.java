package model.domains.database;

import model.domains.Domain;
import model.domains.DomainType;
import model.geometry.Element;
import model.materials.Material;
import model.materials.database.MatDatabase;

import java.io.FileNotFoundException;

/**
 * Class representing the Iron Yoke Domains implementing the Domain abstract class in COMSOL
 */
public class IronDomain extends Domain {

    /**
     * Constructs an IronDomain object with a hardcoded BH characteristics
     *
     * @param label    - label of an IronDomain object in the COMSOL model
     * @param mat      - material to be implemented with a hard-coded BH characteristics
     * @param elements - geometric representation of the iron yoke
     */
    public IronDomain(String label, MatDatabase mat, Element[] elements) {
        super(new Domain.Builder()
                .label(label)
                .elements(elements)
                .material(new Material(mat))
                .domainType(DomainType.IRON)
                .implementsTH(false)
                .build());
    }

    /**
     * Constructs an IronDomain object with a BH characteristics from a text database
     *
     * @param label            - label of an IronDomain object in the COMSOL model
     * @param ironDatabasePath - an absolute path to the IronYoke database
     * @param ironLabel        - label of a BH characteristics to be used in model
     * @param elements         - geometric representation of the iron yoke
     * @throws FileNotFoundException thrown in case of the iron yoke library does not exist
     */
    public IronDomain(String label, String ironDatabasePath, String ironLabel, Element[] elements) throws FileNotFoundException {
        super(new Domain.Builder()
                .label(label)
                .elements(elements)
                .material(new Material(ironDatabasePath, ironLabel))
                .domainType(DomainType.IRON)
                .implementsTH(false)
                .build());
    }
}