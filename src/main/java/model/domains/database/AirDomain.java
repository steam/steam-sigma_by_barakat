package model.domains.database;

import model.domains.Domain;
import model.domains.DomainType;
import model.geometry.Element;
import model.materials.Material;
import model.materials.database.MatDatabase;

/**
 * Class representing the Air Domain in COMSOL
 */
public class AirDomain extends Domain {

    /**
     * Constructor of an AirDomain object with given label, material, and elements
     *
     * @param label    - AirDomain label
     * @param mat      - material definition
     * @param elements - geometric elements
     */
    public AirDomain(String label, MatDatabase mat, Element[] elements) {
        super(new Domain.Builder()
                .label(label)
                .elements(elements)
                .material(new Material(mat))
                .domainType(DomainType.AIR)
                .implementsTH(false)
                .build());
    }
}
