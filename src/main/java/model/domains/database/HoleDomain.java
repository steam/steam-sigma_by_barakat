package model.domains.database;

import model.domains.Domain;
import model.domains.DomainType;
import model.geometry.Element;
import model.materials.Material;
import model.materials.database.MatDatabase;

/**
 * Class representing the Hole Domains implementing the Domain abstract class in COMSOL
 */
public class HoleDomain extends Domain {

    /**
     * Constructs a HoleDomain for an iron yoke
     *
     * @param label    - hole domain label
     * @param mat      - hole material
     * @param elements - geometrical description of constituing elements
     */
    public HoleDomain(String label, MatDatabase mat, Element[] elements) {
        super(new Domain.Builder()
                .label(label)
                .elements(elements)
                .material(new Material(mat))
                .domainType(DomainType.HOLE)
                .implementsTH(false)
                .build());
    }
}
