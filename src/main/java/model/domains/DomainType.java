package model.domains;

/**
 * Enum representing available types of domains
 */
public enum DomainType {
    AIR,
    AIRFARFIELD,
    BOUNDARY,
    COIL,
    COILSPLINE,
    HOLE,
    INSULATION,
    IRON,
    QUENCHHEATER,
    WEDGE
}
