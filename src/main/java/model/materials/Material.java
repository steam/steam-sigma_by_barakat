package model.materials;

import model.materials.database.DataFormat;
import model.materials.database.DatabaseMockUp;
import model.materials.database.MatDatabase;
import model.materials.properties.*;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class representing a material in COMSOL
 */
public class Material {
    private final String label;
    private final List<Property> properties = new ArrayList<>();

    /**
     * Constructs a material object based on a given material type
     *
     * @param mat input material type
     */
    public Material(MatDatabase mat) {
        this.label = mat.toString();
        DataFormat data = DatabaseMockUp.fetchMatData(mat);
        addDefinedProperties(data);
    }

    /**
     * Constructs an iron yoke material object based on iron database path and label
     * @param ironDatabasePath path to the iron yoke BH characteristics database
     * @param ironLabel        label of a BH characteristics to be queried from the database
     * @throws FileNotFoundException in case the iron yoke database file is not found
     */
    public Material(String ironDatabasePath, String ironLabel) throws FileNotFoundException {
        this.label = ironLabel;
        DataFormat data = DatabaseMockUp.fetchIronData(ironDatabasePath, ironLabel);
        addDefinedProperties(data);
    }

    private void addDefinedProperties(DataFormat data) {
        if (data.getCp() != null) {
            properties.add(new Cp(data.getCp()));
        }
        if (data.getEpsilonr() != null) {
            properties.add(new Epsilonr(data.getEpsilonr()));
        }
        if (data.getSigma() != null) {
            properties.add(new Sigma(data.getSigma()));
        }
        if (data.getRho() != null) {
            properties.add(new Rho(data.getRho()));
        }
        if (data.getMur() != null) {
            properties.add(new Mur(data.getMur()));
        }
        if (data.getHB() != null) {
            properties.add(new HB(data.getHB()));
        }
        if (data.getK() != null) {
            properties.add(new K(data.getK()));
        }
    }

    public List<Property> getProperties() {
        return new ArrayList<>(properties);
    }

    public String getLabel() {
        return label;
    }
}
