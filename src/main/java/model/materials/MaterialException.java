package model.materials;

/**
 * Class defines custom exception thrown in case of material issues
 */
public class MaterialException extends RuntimeException {
    private final String message;
    /**
     * Constructs MaterialException object with a given message
     * @param message input exception message
     */
    public MaterialException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}