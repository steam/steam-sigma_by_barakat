package model.materials.properties;

/**
 * Class representing the HB characteristics of a material
 */
public class HB extends Property{
    private final String[][] value;

    /**
     * Constructs an object with HB characteristic values
     * @param value input values
     */
    public HB(String[][] value) {
        super(Type.HB);
        this.value = value.clone();
    }

    public String[][] getValue() {
        return value.clone();
    }
}

