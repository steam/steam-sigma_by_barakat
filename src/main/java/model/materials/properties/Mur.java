package model.materials.properties;

/**
 * Class representing the relative permeability of a material
 */
public class Mur extends Property{
    private final String value;

    /**
     * Constructs an object with relative permeability value
     * @param value input value
     */
    public Mur(String value) {
        super(Type.MUR);
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

