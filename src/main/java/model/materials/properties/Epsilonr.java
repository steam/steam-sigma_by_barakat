package model.materials.properties;

/**
 * Class representing the relative permittivity of a material
 */
public class Epsilonr extends Property{
    private final String value;

    /**
     * Constructs an object with relative permittivity value
     * @param value input value
     */
    public Epsilonr(String value) {
        super(Type.EPSILONR);
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

