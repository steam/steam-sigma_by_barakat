package model.materials.properties;

/**
 * Class representing the thermal conductivity of a material
 */
public class K extends Property{
    private final String value;

    /**
     * Constructs an object with thermal conductivity value
     * @param value input values
     */
    public K(String value) {
        super(Type.K);
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

