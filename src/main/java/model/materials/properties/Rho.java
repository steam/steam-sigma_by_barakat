package model.materials.properties;

/**
 * Class representing the resistivity of a material
 */
public class Rho extends Property{
    private final String value;

    /**
     * Constructs an object with a given resistivity value
     * @param value input values
     */
    public Rho(String value) {
        super(Type.RHO);
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

