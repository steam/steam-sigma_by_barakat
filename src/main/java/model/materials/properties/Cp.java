package model.materials.properties;

/**
 * Class representing the heat capacity of a material
 */
public class Cp extends Property{
    private final String value;

    /**
     * Constructs an object with heat capacity value
     * @param value input value
     */
    public Cp(String value) {
        super(Type.CP);
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

