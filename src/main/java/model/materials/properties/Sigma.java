package model.materials.properties;

/**
 * Class representing the conductivity of a material
 */
public class Sigma extends Property{
    private final String[] value;

    /**
     * Constructs an object with a given conductivity value
     * @param value input values
     */
    public Sigma(String[] value) {
        super(Type.SIGMA);
        this.value = value.clone();
    }

    public String[] getValue() {
        return value.clone();
    }
}

