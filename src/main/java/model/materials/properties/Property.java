package model.materials.properties;

/**
 * Created by STEAM on 31/10/2017.
 */
public abstract class Property {
    private final Type type;

    /**
     * Constructor for an abstract class with input type
     * @param type input type to be set
     */
    public Property(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    /**
     * Enum representing supported choice of material properties in SIGMA models
     */
    public enum Type {
        CP,
        EPSILONR,
        SIGMA,
        RHO,
        MUR,
        BH,
        HB,
        K;
    }
}

