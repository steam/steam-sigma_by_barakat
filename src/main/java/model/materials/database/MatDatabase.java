package model.materials.database;

/**
 * Enum with the list of supported materials
 */
public enum MatDatabase {
    MAT_AIR,
    MAT_COIL,
    MAT_COPPER,
    MAT_KAPTON,
    MAT_GLASSFIBER,
    MAT_INSULATION_TEST,
    MAT_STEEL,
    MAT_IRON1,
    MAT_IRON2,
    MAT_VOID,
    MAT_NULL,
    MAT_COIL_TEST,
    MAT_G10 // Material added for Quench Heater
}