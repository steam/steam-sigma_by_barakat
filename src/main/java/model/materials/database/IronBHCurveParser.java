package model.materials.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parser of a database with iron BH curves
 */
public class IronBHCurveParser {

    private static final int NB_COLUMN = 2;

    protected static String[][] getBHCurve(IronBHCurve bhIron) {
        String bhTabString[][] = new String[bhIron.getBhTable().length][NB_COLUMN]; //Create table for BH values
        for (int i = 0; i < bhIron.getBhTable().length; i++) {
            for (int j = 0; j < NB_COLUMN; j++) {
                bhTabString[i][j] = Double.toString(bhIron.getBhTable()[i][j]);
            }
        }
        return bhTabString;
    }


    /**
     * Methods parsing a text file containing bhIron
     *
     * @param listOfLines list of string containing lines of text file
     * @return Map containing BHiron parsed
     */
    protected static Map<String, IronBHCurve> parseBHCurve(List<String> listOfLines) {
        Map<String, IronBHCurve> ironBHCurveMap = new HashMap<>();
        int bhIronNameIndex = 0;
        ArrayList<IronBHCurve> bhCurveIronList = new ArrayList<>(); //List of BHCurveIron
                    /* BHiron1
                    44 0.98
                    0       0
                    0.00712 7.941831506
                    0.0167 15.88366301
                    0.02925 23.82549452
                    0.04522 31.76732602 */
        while (bhIronNameIndex + 1 < listOfLines.size()) {

            //Name------------------------------------------------------------------------------------------------------
            String name = listOfLines.get(bhIronNameIndex);
            name = name.replaceAll("\\W", "");

            //Number of lines and scaling-------------------------------------------------------------------------------
            String nbLineScalingLine = listOfLines.get(bhIronNameIndex + 1);
            double[] nbLinesScaling = IronBHCurveParser.readNbLineScaling(nbLineScalingLine);
            int nbLine = (int) nbLinesScaling[0];
            double scaling = nbLinesScaling[1];
            //BH curve--------------------------------------------------------------------------------------------------
            List<String> bhIronSublist = listOfLines.subList(bhIronNameIndex + 2, nbLine + bhIronNameIndex + 2);
            double[][] bh = IronBHCurveParser.readBH(bhIronSublist, scaling);
            bhIronNameIndex = nbLine + bhIronNameIndex + 2;
            //Discard additional lines
            bhIronNameIndex = discardAdditionalLines(bhIronNameIndex, listOfLines);
            //Create BHiron object
            IronBHCurve bhIron = new IronBHCurve.Builder()
                    .name(name)
                    .nbLine(nbLine)
                    .scaling(scaling)
                    .bhTable(bh)
                    .build();
            bhCurveIronList.add(bhIron);
            ironBHCurveMap.put(name, bhIron);
        }
        return ironBHCurveMap;
    }

    /**
     * Method parses BH values
     *
     * @param bhIronSublist list of string containing the lines with the BH curve values
     * @return index of next BHiron name
     */
    private static double[][] readBH(List<String> bhIronSublist, double scaling) {
        double bhTab[][] = new double[bhIronSublist.size()][NB_COLUMN]; //Create table for BH values

        for (int i = 0; i < bhIronSublist.size() - 1; i++) {
            bhTab[i] = IronBHCurveParser.parseLine(bhIronSublist.get(i));
            double hValueScaled = bhTab[i][1] * scaling;
            bhTab[i][1] = hValueScaled;
        }
        //Reading last line
        int lastLineIndex = bhIronSublist.size() - 1;
        double[] output = IronBHCurveParser.parseLastLine(bhIronSublist.get(lastLineIndex));
        bhTab[lastLineIndex][0] = output[0];
        bhTab[lastLineIndex][1] = output[1];

        return bhTab;
    }


    /**
     * Method parses number of lines and scaling from a string containing line
     *
     * @param line string containing the line with number of lines and scaling
     * @return ArrayList containing the number of lines as Integer and the scaling as double
     */
    protected static double[] readNbLineScaling(String line) {
        return IronBHCurveParser.parseLine(line);
    }

    /**
     * Method increments the index as long as it finds additional rows
     *
     * @param bhIronNameIndex index of last line of each BHiron sublist
     * @param listOfLines     list of string containing lines of text file
     * @return integer presenting the index of the next BHiron
     */

    private static int discardAdditionalLines(int bhIronNameIndex, List<String> listOfLines) {
        boolean isExtraLine = true;
        int newBhIronNameIndex = bhIronNameIndex;
        while (bhIronNameIndex + 1 < listOfLines.size() && isExtraLine) {
            isExtraLine = checkAdditionalLines(listOfLines.get(newBhIronNameIndex));
            if (isExtraLine) {
                newBhIronNameIndex++;
            }
        }
        return newBhIronNameIndex;
    }


    /**
     * Method checks if the line in parameter is additional or not
     *
     * @param line string containing the line to be checked
     * @return boolean equals true if the line is additional
     */
    private static boolean checkAdditionalLines(String line) {
        boolean isExtraLine = false;
        Matcher lineValue = Pattern.compile("(\\d+.\\d+)(\\s+)").matcher(line);
        boolean wasBHValue = lineValue.find();
        if (wasBHValue) {
            isExtraLine = true;
        }
        return isExtraLine;
    }


    /**
     * Method Parses last line with specified separator into list of Doubles
     *
     * @param line string containing the line to be split
     * @return double table containing BH values
     */
    private static double[] parseLastLine(String line) {
        Pattern pattern = Pattern.compile("(\\s+)");
        String[] numberAsString = pattern.split(line);
        double[] numbers = new double[2];
        numbers[0] = Double.valueOf(numberAsString[0]);
        numbers[1] = Double.valueOf(numberAsString[1]);
        //The remaining elements are a comment

        return numbers;
    }

    /**
     * Method Parses line with specified separator into list of Doubles
     *
     * @param line string containing the line to be split
     * @return double table containing BH values
     */
    protected static double[] parseLine(String line) throws RuntimeException {
        Pattern pattern = Pattern.compile("(\\s+)");
        String[] numberAsString = pattern.split(line);
        double[] numbers = new double[numberAsString.length];
        if (numberAsString.length != 2) {
            String errorMessage = String.format("Line %s does not contain two numbers as expected", line);
            throw new RuntimeException(errorMessage);
        }
        numbers[0] = Double.valueOf(numberAsString[0]);
        numbers[1] = Double.valueOf(numberAsString[1]);

        return numbers;
    }
}
