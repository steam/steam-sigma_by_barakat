package model.materials.database;

import comsol.constants.MPHC;
import model.materials.MaterialException;
import utils.TextFile;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;


/**
 * Class with a mock-up of the database with material properties
 */
public class DatabaseMockUp {

    public static final String FUNCTION_OF_T = "(T[1/K])";

    /**
     * Method fetches material property definition for a given material type
     * @param mat input material type
     * @return data format with material properties characterizing a given material
     * @throws MaterialException thrown in case a material is not supported
     */
    public static DataFormat fetchMatData(MatDatabase mat) throws MaterialException {
        switch (mat) {
            case MAT_AIR:
                return fetchMatAir();
            case MAT_COIL:
                return fetchMatCoil();
            case MAT_COPPER:
                return fetchMatCopper();
            case MAT_KAPTON:
                return fetchMatKapton();
            case MAT_GLASSFIBER:
                return fetchMatGlassFiber();
            case MAT_STEEL:
                return fetchMatSteel();
            case MAT_IRON1:
                return fetchMatIron1();
            case MAT_IRON2:
                return fetchMatIron2();
            case MAT_VOID:
                return fetchMatVoid();
            case MAT_NULL:
                return fetchMatNull();
            case MAT_INSULATION_TEST:
                return fetchMatGlassFiberTest();
            case MAT_COIL_TEST:
                return fetchMatCoilTest();
            default:
                throw new MaterialException(String.format("Material %s not supported", mat.toString()));
        }
    }

    /**
     * Method fetches a set of material properties characterizing an iron element with a given characteristics
     * @param ironDatabasePath an absolute path to the IronYoke database
     * @param ironLabel        label of a BH characteristics to be used in model
     * @return data format with material properties characterizing an iron yoke
     * @throws FileNotFoundException thrown in case the database file is not found
     */
    public static DataFormat fetchIronData(String ironDatabasePath, String ironLabel) throws FileNotFoundException {
        return fetchMatIron(ironDatabasePath, ironLabel);
    }

    private static DataFormat fetchMatIron(String ironDatabasePath, String ironLabel) throws FileNotFoundException {
        String cp = null;
        String epsilonr = "1";
        String[] sigma = new String[]{"1.12e7[S/m]", "1.12e7[S/m]", "0[S/m]"};
        String rho = "1";
        String mur = null;
        String[][] hbCurve = getBhCurveFromIronDatabase(ironDatabasePath, ironLabel);
        String k = null;
        return new DataFormat(cp, epsilonr, sigma, rho, mur, hbCurve, k);
    }

    protected static String[][] getBhCurveFromIronDatabase(String ironDatabasePath, String ironLabel) throws FileNotFoundException {
        List<String> listOfLines = TextFile.read(ironDatabasePath);
        Map<String, IronBHCurve> ironBHCurveMap = IronBHCurveParser.parseBHCurve(listOfLines);

        if (ironBHCurveMap.containsKey(ironLabel)) {
            IronBHCurve bhIron = ironBHCurveMap.get(ironLabel);
            return IronBHCurveParser.getBHCurve(bhIron);
        } else {
            throw new Error(String.format("Iron label : %s, does not exist in the Database stored in %s!", ironLabel, ironDatabasePath));
        }
    }

    private static final DataFormat fetchMatAir() {
        String cp = "0[J/(kg*K)]";
        String epsilonr = "1";
        String[] sigma = new String[]{"0[S/m]"};
        String rho = "1";
        String mur = "1";
        String[][] hbCurve = null;
        String k = "0[W/(m*K)]";
        return new DataFormat(cp, epsilonr, sigma, rho, mur, hbCurve, k);
    }

    private static final DataFormat fetchMatCoil() {
        String cp = String.format("%s/(1[kg/m^3])", MPHC.LABEL_HT_CV);
        String epsilonr = "1";
        String[] sigma = new String[]{"0[S/m]"};
        String rho = "1";
        String mur = "1";
        String[][] hbCurve = null;

        // k definition
        String scalingFactor = String.format("(%s/%s)", MPHC.LABEL_CABLE_FRACTION_CU, MPHC.LABEL_K_SURFACE);
        String k = String.format("%s*%s(T[1/K],%s,%s[1/(ohm*m)],%s[1/(ohm*m)])[W/(m*K)]",
                scalingFactor,
                MPHC.LABEL_CFUN_K_CU_NIST,
                MPHC.LABEL_CABLE_RRR,
                MPHC.LABEL_CABLE_RHO_STABILIZER_ZEROB,
                MPHC.LABEL_CABLE_RHO_STABILIZER);
        return new DataFormat(cp, epsilonr, sigma, rho, mur, hbCurve, k);
    }

    private static DataFormat fetchMatCopper() {
        String rho_el = String.format("%s(%s[1/K],%s[1/T],%s,%s)",
                MPHC.LABEL_CFUN_RHO_CU_CUDI,
                MPHC.LABEL_PHYSICSTH_T,
                MPHC.LABEL_PHYSICSMF_B,
                1,
                300);
        String cp = MPHC.LABEL_CFUN_CV_CU + FUNCTION_OF_T;
        String epsilonr = "1";
        String[] sigma = new String[]{"1/" + rho_el + "[ohm*m]"};
        String rho = "1";
        String mur = "1";
        String[][] hbCurve = null;
        //String k = MPHC.LABEL_CFUN_K_CU_NIST + "(T[1/K]," + rho_el + ")";
        String k = "2.44*10^(-8) * T / "+rho_el+"[m*K^2/W]";

        return new DataFormat(cp, epsilonr, sigma, rho, mur, hbCurve, k);
    }

    private static DataFormat fetchMatKapton() {
        String cp = MPHC.LABEL_CFUN_CV_KAPTON + "(T[1/K])";
        String epsilonr = "1";
        String[] sigma = new String[]{"0[S/m]"};
        String rho = "1";
        String mur = "1";
        String[][] hbCurve = null;
        String k = MPHC.LABEL_CFUN_K_KAPTON + "(T[1/K])";
        return new DataFormat(cp, epsilonr, sigma, rho, mur, hbCurve, k);
    }

    private static DataFormat fetchMatGlassFiber() {
        String cp = MPHC.LABEL_CFUN_CV_G10 + "(T[1/K])";
        String epsilonr = "1";
        String[] sigma = new String[]{"0[S/m]"};
        String rho = "1";
        String mur = "1";
        String[][] hbCurve = null;
        String k = MPHC.LABEL_CFUN_K_G10 + "(T[1/K])";
        return new DataFormat(cp, epsilonr, sigma, rho, mur, hbCurve, k);
    }

    private static DataFormat fetchMatSteel() {
        String cp = "475[J/(kg*K)]";
        String epsilonr = "1";
        String[] sigma = new String[]{"1.12e7[S/m]", "1.12e7[S/m]", "0[S/m]"};
        String rho = "7850[kg/m^3]";
        String mur = "1.02";
        String[][] hbCurve = null;
        String k = "44.5[W/(m*K)]";
        return new DataFormat(cp, epsilonr, sigma, rho, mur, hbCurve, k);
    }

    private static DataFormat fetchMatIron1() {
        String cp = null;
        String epsilonr = "1";
        String[] sigma = new String[]{"1.12e7[S/m]", "1.12e7[S/m]", "0[S/m]"};
        String rho = "1";
        String mur = null;
        String[][] hbCurve = IronDatabaseROXIE.HBCurveIron1;
        String k = null;
        return new DataFormat(cp, epsilonr, sigma, rho, mur, hbCurve, k);
    }

    private static DataFormat fetchMatIron2() {
        String cp = null;
        String epsilonr = "1";
        String[] sigma = new String[]{"1.12e7[S/m]", "1.12e7[S/m]", "0[S/m]"};
        String rho = "1";
        String mur = null;
        String[][] hbCurve = IronDatabaseROXIE.HBCurveIron2;
        String k = null;
        return new DataFormat(cp, epsilonr, sigma, rho, mur, hbCurve, k);
    }

    private static DataFormat fetchMatVoid() {
        String cp = "0[J/(kg*K)]";
        String epsilonr = "1";
        String[] sigma = new String[]{"0[S/m]"};
        String rho = "1";
        String mur = "1";
        String[][] hbCurve = null;
        String k = "0[W/(m*K)]";
        return new DataFormat(cp, epsilonr, sigma, rho, mur, hbCurve, k);
    }

    private static DataFormat fetchMatNull() {
        String cp = null;
        String epsilonr = null;
        String[] sigma = null;
        String rho = null;
        String mur = null;
        String[][] hbCurve = null;
        String k = null;
        return new DataFormat(cp, epsilonr, sigma, rho, mur, hbCurve, k);
    }

    private static DataFormat fetchMatGlassFiberTest() {
        String cp = "750[J/(kg*K)]";
        String epsilonr = "1";
        String[] sigma = new String[]{"0[S/m]"};
        String rho = "1";
        String mur = "1";
        String[][] hbCurve = null;
        String k = "0.01[W/(m*K)]";
        return new DataFormat(cp, epsilonr, sigma, rho, mur, hbCurve, k);
    }

    private static DataFormat fetchMatCoilTest() {
        String cp = String.format("%s/(1[kg/m^3])", MPHC.LABEL_HT_CV);
        String epsilonr = "1";
        String[] sigma = new String[]{"0[S/m]"};
        String rho = "1";
        String mur = "1";
        String[][] hbCurve = null;
        String k = "300[W/(m*K)]";
        return new DataFormat(cp, epsilonr, sigma, rho, mur, hbCurve, k);
    }
}
