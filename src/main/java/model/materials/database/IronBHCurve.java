package model.materials.database;

import java.util.ArrayList;
import java.util.List;

/**
 * Class representing an iron BH curve
 */
public class IronBHCurve {
    private final String name;
    private final double scaling;
    private final int nbLines;
    private final double[][] bhTable;

    /**
     * Getters
     */
    String getName() {
        return name;
    }

    double getScaling() {
        return scaling;
    }

    int getNbLines() {
        return nbLines;
    }

    double[][] getBhTable() {
        return this.bhTable.clone();
    }


    /**
     * Builder class of IronBHCurve
     */
    static class Builder {
        private String name;
        private double scaling;
        private int nbLines;
        private double[][] bhTable;

        /**
         * Method sets the name of the BHiron
         *
         * @param name string containing the name of the BHiron
         * @return Builder object with set variable
         */
        Builder name(String name) {
            this.name = name;
            return this;
        }

        /**
         * Method sets the number of lines of the BHiron
         *
         * @param nbLines integer containing the name of the BHiron
         * @return Builder object with set variable
         */
        Builder nbLine(int nbLines) {
            this.nbLines = nbLines;
            return this;
        }

        /**
         * Method sets the scaling of the BHiron
         *
         * @param scaling double containing the scaling of the BHiron
         * @return Builder object with set variable
         */
        Builder scaling(double scaling) {
            this.scaling = scaling;
            return this;
        }
        /**
         * Method sets the BH table of the BHiron
         *
         * @param bhTable double table containing the BH values
         * @return Builder object with set variable
         */

        Builder bhTable(double[][] bhTable) {
            this.bhTable = bhTable.clone();
            return this;
        }


        /**
         * Method builds an IronBHCurve object
         *
         * @return an IronBHCurve object
         */
        IronBHCurve build() {
            return new IronBHCurve(this);
        }
    }

    private IronBHCurve(Builder builder) {
        this.name = builder.name;
        this.nbLines = builder.nbLines;
        this.scaling = builder.scaling;
        this.bhTable = builder.bhTable.clone();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.name).append('\n');
        sb.append(String.format("%d\t%f%n", this.nbLines, this.scaling));
        for (double [] line: this.bhTable) {
            sb.append(String.format("%f\t%f%n", line[0], line[1]));
        }
        return sb.toString();
    }

    /**
     * Method converts a line of a BH characteristics into a string
     * @return List of BHiron objects
     */
    List<String> toListString() {
        ArrayList<String> outputList = new ArrayList<>();

        outputList.add(this.name);
        outputList.add(String.format("%d\t%f", this.nbLines, this.scaling));
        for (double [] line: this.bhTable) {
            outputList.add(String.format("%f\t%f", line[0], line[1]));
        }
        return outputList;
    }
}
