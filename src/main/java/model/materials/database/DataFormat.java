package model.materials.database;

/**
 * Class represents a data format for a material with given set of properties
 */
public class DataFormat {
    private final String cp;
    private final String epsilonr;
    private final String[] sigma;
    private final String rho;
    private final String mur;
    private final String[][] HB;
    private final String k;

    /**
     * Method constructs a material with given set of properties
     * @param cp heat capacity value
     * @param epsilonr relative permittivity value
     * @param sigma electrical conductivity values
     * @param rho electrical resistivity value
     * @param mur relative permeability value
     * @param HB HB characteristic values
     * @param k thermal conductivity value
     */
    public DataFormat(String cp, String epsilonr, String[] sigma, String rho, String mur, String[][] HB, String k) {
        this.cp = cp;
        this.epsilonr = epsilonr;
        this.sigma = sigma.clone();
        this.rho = rho;
        this.mur = mur;

        if (HB == null) {
            this.HB = null;
        } else {
            this.HB = HB.clone();
        }

        this.k = k;
    }

    public String getCp() {
        return this.cp;
    }

    public String getEpsilonr() {
        return this.epsilonr;
    }

    public String[] getSigma() {
        return this.sigma.clone();
    }

    public String getRho() {
        return this.rho;
    }

    public String getMur() {
        return this.mur;
    }

    public String[][] getHB() {
        if (HB == null) {
            return null;
        } else {
            return this.HB.clone();
        }
    }

    public String getK() {
        return this.k;
    }
}

