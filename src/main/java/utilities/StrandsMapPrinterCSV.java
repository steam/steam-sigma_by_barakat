package utilities;

import model.geometry.coil.Coil;
import model.geometry.coil.Strand;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

/**
 * Class writes a strands map to a csv file
 */
public class StrandsMapPrinterCSV {
    private static final Logger LOGGER = LogManager.getLogger(StrandsMapPrinterCSV.class);
    private final Coil coil;
    private final int coilCounter;
    private final String fieldmapFilePath;

    private StrandsMapPrinterCSV(Coil coil, int coilCounter, String fieldmapFilePath) throws FileNotFoundException {
        this.coil = coil;
        this.coilCounter = coilCounter;
        this.fieldmapFilePath = fieldmapFilePath;
        writeToFile();
    }

    /**
     * Method prints a strands map to a csv file
     * @param coil input coil object with coil geometry
     * @param coilCounter coil counter in case of multiple coils
     * @param fieldmapFilePath output path to a csv file with a strands map
     * @return object with strand map
     * @throws FileNotFoundException thrown in case output file does not exist
     */
    public static StrandsMapPrinterCSV fromCoil(Coil coil, int coilCounter, String fieldmapFilePath) throws FileNotFoundException {
        return new StrandsMapPrinterCSV(coil, coilCounter, fieldmapFilePath);
    }

    private void writeToFile() throws FileNotFoundException {
        File outputFile = new File(fieldmapFilePath);
        PrintWriter pw = null;
        StringBuilder sb = new StringBuilder();

        // if first coil_OLD, initialize the file, otherwise append the new text
        if (coilCounter == 0) {
            pw = new PrintWriter(outputFile);
            sb.append(composeHeader());

        } else {
            try {
                pw = new PrintWriter(new FileWriter(outputFile, true));
            } catch (IOException e) {
                LOGGER.error(e);
                throw new RuntimeException();
            }
        }

        for (int p_i = 0; p_i < coil.getPoles().length; p_i++) {
            for (int w_i = 0; w_i < coil.getPoles()[p_i].getWindings().length; w_i++) {
                for (int b_i = 0; b_i < coil.getPoles()[p_i].getWindings()[w_i].getBlocks().length; b_i++) {
                    for (int ht_i = 0; ht_i < coil.getPoles()[p_i].getWindings()[w_i].getBlocks()[b_i].getHalfturns().length; ht_i++) {
                        Strand[] strands = coil.getPoles()[p_i].getWindings()[w_i].getBlocks()[b_i].getHalfturns()[ht_i].getStrands();
                        for (int s_i = 0; s_i < strands.length; s_i++) {

                            double x_i = strands[s_i].getCenter().getX();
                            double y_i = strands[s_i].getCenter().getY();
                            String line_i = String.format("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s%n", coilCounter, p_i, w_i, b_i, ht_i, ht_i, x_i, y_i);
                            sb.append(line_i);
                        }
                    }
                }
            }
        }

        pw.write(sb.toString());
        pw.close();
        LOGGER.info("Writing done!");
    }

    private static String composeHeader() {
        String coilNo = "COIL";
        String poleNo = "POLE";
        String windingNo = "WINDING";
        String blockNo = "BLOCK";
        String halfturnNo = "HALFTURN";
        String strandNo = "STRAND";
        String xPos = "X[m]";
        String yPos = "Y[m]";
        String Bx = "Bx[T]";
        String By = "By[T]";
        String Bnorm = "Bnorm[T]";
        return String.format("%s\t %s\t %s\t %s\t %s\t %s\t %s\t %s\t %s\t %s\t %s %n",
                coilNo, poleNo, windingNo, blockNo, halfturnNo, strandNo, xPos, yPos, Bx, By, Bnorm);
    }
}

