package utilities;

import model.domains.Domain;
import model.domains.database.CoilDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;

/**
 * Class performs extraction of a strand map of magnetic field
 */
public class StrandsMapExtractor {
    private static final Logger LOGGER = LogManager.getLogger(StrandsMapExtractor.class);
    /**
     * Method builds a field map
     * @param fieldmapFilePath output field map path
     * @param domains input domains for which strands map is extracted
     */
    public static void build(String fieldmapFilePath, Domain[] domains) {
        //For each domain
        int coilCounter = 0;
        for (Domain d : domains) {
            //For each coil
            if (d.getClass() == CoilDomain.class) {
                CoilDomain cd = (CoilDomain) d;
                try {
                    StrandsMapPrinterCSV.fromCoil(cd.getCoil(), coilCounter, fieldmapFilePath);
                    coilCounter++;
                } catch (FileNotFoundException e) {
                    LOGGER.error(e);
                }
            }
        }
    }
}
