package utilities;

import model.geometry.Unit;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.math.BigDecimal;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class contains utilities for iron yoke parser
 */
public class IronYokeParserUtils {

    private static final String JAVASCRIPT = "JavaScript";
    private static final int CONVERSION_RATE = 1000;
    private static final String SPACE_KEYWORD = "(\\w+)";

    /**
     * Method performs path conversion of input expression
     *
     * @param valueString the parameters of the variable
     * @param variableMap Map of variables  Map<String,Double>
     * @return the result of the mathematical expressions
     * @throws ScriptException thrown in case of script execution problems
     */
    public static double mathConversion(String valueString, Map<String, Double> variableMap) throws ScriptException {
        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName(JAVASCRIPT);
        valueString = replaceByNumericValue(valueString, variableMap);
        Matcher pi = Pattern.compile("Pi").matcher(valueString);
        boolean wasPi = pi.find();
        Matcher cos = Pattern.compile("Cos").matcher(valueString);
        boolean wasCos = cos.find();
        Matcher sin = Pattern.compile("Sin").matcher(valueString);
        boolean wasSin = sin.find();
        Matcher asin = Pattern.compile("Asin").matcher(valueString);
        boolean wasAsin = asin.find();
        Matcher sqrt = Pattern.compile("Sqrt").matcher(valueString);
        boolean wasSqrt = sqrt.find();
        Matcher atan = Pattern.compile("Atan").matcher(valueString);
        boolean wasAtan = atan.find();
        Matcher tan = Pattern.compile("Tan").matcher(valueString);
        boolean wasTan = tan.find();
        double variableValue;
        if (wasPi) {
            valueString = valueString.replace("Pi", "Math.PI");
            variableValue = new BigDecimal(engine.eval(valueString).toString()).doubleValue();
        } else if (wasCos) {
            valueString = valueString.replace("Cos", "Math.cos");
            variableValue = new BigDecimal(engine.eval(valueString).toString()).doubleValue();
        } else if (wasSin) {
            valueString = valueString.replace("Sin", "Math.sin");
            variableValue = new BigDecimal(engine.eval(valueString).toString()).doubleValue();
        } else if (wasAsin) {
            valueString = valueString.replace("Asin", "Math.asin");
            variableValue = new BigDecimal(engine.eval(valueString).toString()).doubleValue();
        } else if (wasSqrt) {
            valueString = valueString.replace("Sqrt", "Math.sqrt");
            variableValue = new BigDecimal(engine.eval(valueString).toString()).doubleValue();
        } else if (wasAtan) {
            valueString = valueString.replace("Atan", "Math.atan");
            variableValue = new BigDecimal(engine.eval(valueString).toString()).doubleValue();
        } else if (wasTan) {
            valueString = valueString.replace("Tan", "Math.tan");
            variableValue = new BigDecimal(engine.eval(valueString).toString()).doubleValue();
        } else {
            variableValue = new BigDecimal(engine.eval(valueString).toString()).doubleValue();
        }

        return variableValue;
    }


    /**
     * Method performs a conversion of two numbers for a given unit
     *
     * @param firstNumber   First number to convert
     * @param secondNumber   Second number to convert
     * @param unit The unit of numbers to convert
     * @return Numbers converted
     */
    public static double[] conversionOfTwoNumbers(double firstNumber, double secondNumber, Unit unit) {
        double[] valuesTab = new double[2];
        if (unit.equals(Unit.MILLIMETER)) {
            firstNumber = firstNumber / CONVERSION_RATE;
            secondNumber = secondNumber / CONVERSION_RATE;
        }
        valuesTab[0] = firstNumber;
        valuesTab[1] = secondNumber;
        return valuesTab;
    }

    /**
     * Method performs a conversion of a single number
     *
     * @param number    number to convert
     * @param unit the unit of number to convert
     * @return number converted
     */
    public static double conversionOfOneNumber(double number, Unit unit) {
        if (unit.equals(Unit.MILLIMETER)) {
            number = number / CONVERSION_RATE;
        }
        return number;
    }


    /**
     * Method replaces a string by a numeric value
     *
     * @param stringToReplace String to be replaced with it numeric value
     * @param variableMap     Map of variables  Map<String,Double>
     * @return String containing the parameter as a mathematical operation with numeric value
     */
    public static String replaceByNumericValue(String stringToReplace, Map<String, Double> variableMap) {
        String[] result = stringToReplace.split("[-(+*/)]");
        for (String aResult : result) {
            aResult = aResult.replaceAll("\\W", "");
            if (variableMap.containsKey(aResult)) {
                Double variableDouble = variableMap.get(aResult);
                String variableString = Double.toString(variableDouble);
                stringToReplace = stringToReplace.replace(aResult, variableString);
            }
        }

        return stringToReplace;
    }

    /**
     * Method performs a check of a mathematical expression
     *
     * @param stringToCheck String to check
     * @return String replaced
     */
    public static String checkMathExpression(String stringToCheck) {
        Matcher lineAtan = Pattern.compile(SPACE_KEYWORD).matcher(stringToCheck);
        boolean wasAtan = lineAtan.find();
        if (wasAtan) {
            stringToCheck = stringToCheck.replace("Atan", "Math.atan");
        }

        Matcher pi = Pattern.compile("pi").matcher(stringToCheck);
        boolean wasPi = pi.find();
        if (wasPi) {
            stringToCheck = stringToCheck.replace("pi", "Math.PI");
        }

        Matcher cos = Pattern.compile("Cos").matcher(stringToCheck);
        boolean wasCos = cos.find();
        if (wasCos) {
            stringToCheck = stringToCheck.replace("Cos", "Math.cos");
        }

        Matcher sin = Pattern.compile("Sin").matcher(stringToCheck);
        boolean wasSin = sin.find();
        if (wasSin) {
            stringToCheck = stringToCheck.replace("Sin", "Math.sin");
        }
        return stringToCheck;
    }
}
